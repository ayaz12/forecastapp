package markematics.active17213.Helper;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;


import com.markematics.Active17213.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import markematics.active17213.DataAccess.DAO.LocationDAO;

/**
 * Created by Haris on 11/8/2016.
 */

public class Receiver extends BroadcastReceiver implements LocationListener {

    private static final int MY_NOTIFICATION_ID = 1;
    NotificationManager notificationManager;
    Notification myNotification;
    private LocationManager locationMangaer = null;
    private LocationListener locationListener = null;
    private static final String TAG = "Debug";
    private Boolean flag = false;
    Context context;
    Double lat = 0.0;
    Double lng = 0.0;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
      //  Toast.makeText(context, "Alarm received!", Toast.LENGTH_LONG).show();
        locationMangaer = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        //	editLocation.setText("Please!! move your device to see the changes in coordinates."+"\nWait..");

        //  pb.setVisibility(View.VISIBLE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 19);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.SECOND, 0);

        Calendar  currentTime = Calendar.getInstance();

        if(currentTime.getTimeInMillis() > calendar.getTimeInMillis())
        {
            Toast.makeText(context,""+currentTime.getTime(),Toast.LENGTH_SHORT).show();
            stopAlarm();
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if ( !locationMangaer.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            showDialog();
        }
        else {
            locationMangaer.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 5, this);
        }
        // Intent myIntent = new Intent(context, ActivityHome.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(
//                context,
//                0,
//                myIntent,
//                Intent.FLAG_ACTIVITY_NEW_TASK);


        //  LocationService locationService = new LocationService(context);
        // locationService.getLocation();
    }

    @Override
    public void onLocationChanged(Location location) {
      //  Toast.makeText(context, location.getLongitude() +" "+ location.getLatitude()+"", Toast.LENGTH_LONG).show();


        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
       // Toast.makeText(context,""+currentTime.getTime(),Toast.LENGTH_SHORT).show();
        locationMangaer.removeUpdates(this);
        lng = location.getLongitude();
        lat = location.getLatitude();
        LocationDAO locationDAO = new LocationDAO(context);
        KeyValueDB keyValueDB = new KeyValueDB(context.getSharedPreferences(context.getResources().getString(R.string.keyValueDb), Context.MODE_PRIVATE));
        String Uid = keyValueDB.getValue("hrId","");
        String userName = keyValueDB.getValue("name","");
        String city = keyValueDB.getValue("city","");
        if (!Uid.isEmpty())
        {
            markematics.active17213.Model.Location loc = new markematics.active17213.Model.Location();
            loc.setHrId(Uid);
            loc.setLng(lng);
            loc.setLat(lat);
            loc.setDate(getTimeDate());
            loc.setUserName(userName);
            loc.setCity(city);
                locationDAO.add(loc);
//            if (!ViewController.rId.isEmpty())
//            {
//                Toast.makeText(context,"Shop Id "+ViewController.rId,Toast.LENGTH_SHORT).show();
//                loc.setrId(Integer.parseInt(ViewController.rId));
//            }
//            locationDAO.add(loc);
        }

//        myNotification = new NotificationCompat.Builder(context)
//                .setContentTitle("Locations")
//                .setContentText("Lat: " + location.getLatitude() + " Lng: " + location.getLongitude())
//                .setTicker("Location Change!")
//                .setWhen(System.currentTimeMillis())
//
//                .setDefaults(Notification.DEFAULT_SOUND)
//                .setAutoCancel(true)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .build();
//
//        notificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(MY_NOTIFICATION_ID, myNotification);

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

//        backgroundAsync.renderList();
//        backgroundAsync.sync();

    private void stopAlarm()
    {
        AlarmManager alarmManager=(AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, Receiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }
private String getTimeDate()
{
    Calendar c = Calendar.getInstance();
    System.out.println("Current time => " + c.getTime());

    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
    String formattedDate = df.format(c.getTime());
    return formattedDate;
}
    public void showDialog() {


        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting DialogHelp Title
        alertDialog.setTitle("Admin says:");

        // Setting DialogHelp Message
        alertDialog
                .setMessage("Please turn on your GPS");

        // on pressing cancel button
        alertDialog.setNegativeButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });

        // Showing Alert Message
        alertDialog.show();


    }
}




