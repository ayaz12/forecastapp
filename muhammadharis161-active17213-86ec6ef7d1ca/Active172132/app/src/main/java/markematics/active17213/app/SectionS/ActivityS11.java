package markematics.active17213.app.SectionS;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityIntro;

public class ActivityS11 extends AppCompatActivity {

    TextView textViewS11;
    EditText editTextS11;
    Typeface tf;
    Button btnNext;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s11);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S11");
        toolbar.setTitleTextColor(Color.WHITE);
        textViewS11 = (TextView) findViewById(R.id.tViewS11);
        editTextS11 = (EditText) findViewById(R.id.editTextS11);
        btnNext = (Button) findViewById(R.id.btnS11);

        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();
    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void add() {
        active.setQuestionS11(editTextS11.getText().toString());
    }

    private boolean validationSuccess() {

        if (editTextS11.getText().toString().isEmpty()) {

            editTextS11.setError("Please enter Reason");
            return false;
        }
        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS11.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                            if (validationSuccess()) {
                                add();
                                Utility.showExitDialog(ActivityS11.this, "کیا آپ انٹرویو ختم کرنا چاہتی ہیں", active);

                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                            active = (Active) getIntent().getSerializableExtra("active");
                            tf = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            textViewS11.setTypeface(tf);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
