package markematics.active17213.Helper;

import android.content.Context;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import markematics.active17213.DataAccess.DAO.ActiveDAO;
import markematics.active17213.Model.Active;

/**
 * Created by Muhammad Haris on 10/11/2017.
 */

public class CRUD {

    Context context;
    public CRUD(Context context)
    {
        this.context = context;
    }

    public void add(Active active)
    {
        active.setDateTime(getDateTime());
        active.setActiveJSON(new Gson().toJson(active));
        ActiveDAO activeDAO = new ActiveDAO(context);
        activeDAO.add(active);
    }

    private String getDateTime()
    {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
}
