package markematics.active17213.app.SectionR;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.app.SectionG.ActivityG1;


public class ActivityR1 extends AppCompatActivity {

    TextView tViewR1;
    EditText editTextR1;
    Button btnNext;
    String  er1;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r1);

        tViewR1 = (TextView) findViewById(R.id.tViewR1);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question R1");
        toolbar.setTitleTextColor(Color.WHITE);

        editTextR1 = (EditText) findViewById(R.id.editTextR1);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");


        active = (Active) getIntent().getSerializableExtra("active");

        btnNext = (Button) findViewById(R.id.btnNext);

        tViewR1.setTypeface(tf);
         editTextR1.setTypeface(tf);



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
    }

    private void next() {
        initialize();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n", Toast.LENGTH_SHORT).show();
        } else {
            add();
            proceed();

        }


    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (er1.isEmpty()) {
            editTextR1.setError("درج کریں");
            valid = false;
        }

        return valid;
    }

    private void proceed() {

            Type listTypeB4 = new TypeToken<ArrayList<QuestionB4>>() {
            }.getType();
            List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listTypeB4);
            boolean isCanbebeB4 = false;
            for (int i = 0; i< questionB4s.size(); i++)
            {
                if (questionB4s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                {
                    isCanbebeB4 = true;
                }
            }
            if (isCanbebeB4)
            {
                boolean isCanbebeB5 = false;
                Type listTypeB5 = new TypeToken<ArrayList<QuestionB4>>() {
                }.getType();
                List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listTypeB5);

                for (int i = 0; i< questionB4s.size(); i++)
                {
                    if (questionB5s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                    {
                        isCanbebeB5 = true;

                    }
                }
                if (isCanbebeB5)
                {
                    Intent nextPage = new Intent(ActivityR1.this, ActivityG1.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);
                    return;
                }
                else
                {
                    Intent nextPage = new Intent(ActivityR1.this, ActivityR2.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);
                    return;
                }
            }
            else
            {
                Intent nextPage = new Intent(ActivityR1.this, ActivityG1.class);
                nextPage.putExtra("active", active);
                startActivity(nextPage);
                return;
            }

    }

    private void initialize() {

        er1 = editTextR1.getText().toString().trim();

    }

    private void add(){

        active.setQuestionR1(editTextR1.getText().toString());

    }
}