package markematics.active17213.app.SectionF;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.lassana.recorder.AudioRecorder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionC.ActivityC1;
import markematics.active17213.app.SectionG.ActivityG1;
import markematics.active17213.app.SectionR.ActivityR1;
import markematics.active17213.app.SectionR.ActivityR2;


public class ActivityF6 extends AppCompatActivity {
    TextView tViewf6 ,tViewF6ins;
    LinearLayout f6_layout;
    Button btnNext;

    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB1> questionF6s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f6);


        tViewf6 = (TextView) findViewById(R.id.tViewF6);
        tViewF6ins = (TextView)findViewById(R.id.tViewF6Ins);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question F6");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

       f6_layout = (LinearLayout) findViewById(R.id.f6_layout);
//        F4blayout = (LinearLayout) findViewById(R.id.F4b_layout);
//        F5layout = (LinearLayout) findViewById(R.id.F5_layout);
//        F6layout = (LinearLayout) findViewById(R.id.F6_layout);

        btnNext = (Button) findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewf6.setTypeface(tf);
        tViewF6ins.setTypeface(tf);

        randerF6Layout();


        btnNext.setOnClickListener(btnNext_OnClickListener);

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate())
            {
                Intent nextPage = new Intent(ActivityF6.this, ActivityF7.class);
                nextPage.putExtra("active", active);
                startActivity(nextPage);



            }
        }
    };

    private boolean validate() {

        if (questionF6s.size() == 0) {
            showDialog("Please select at least one option 6");
            return false;
        }
        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityF6.this, ActivityR1.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);

    }



    private void randerF6Layout() {
        String[] arrayF6 = getResources().getStringArray(R.array.arrayf4);

        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionF5s = new Gson().fromJson(active.getQuestionF5(), listType);


        List<Brand> brands = new ArrayList<>();
        for (int i =0;i<questionF5s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionF5s.get(i).getCode());
            brand.setName(questionF5s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionF5s.get(i).getOpenEnded():questionF5s.get(i).getBrand());
            brands.add(brand);
        }


        for (int i = 0; i <brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);


            label.setTypeface(tf);
            label.setText(brands.get(i).getName());
            label.setTypeface(tf);
            label.setTag(i + 1);
            chkBox1.setTag(v);

            chkBox1.setOnCheckedChangeListener(checkBox1_OnCheckedChangeListener);
               if (brands.get(i).getName().equalsIgnoreCase("Others")) {
                   editText.setVisibility(View.VISIBLE);
            }
            f6_layout.addView(v);
            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
    }

    private CompoundButton.OnCheckedChangeListener checkBox1_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            if (chkBox1.isChecked()) {

                for (int i = 0; i < f6_layout.getChildCount(); i++) {
                    View childView =f6_layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < f6_layout.getChildCount(); i++) {
                    View childView = f6_layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }

            }

        }
    };
    private void add() {

        questionF6s = new ArrayList<>();
        for (int i = 0; i < f6_layout.getChildCount(); i++) {
            View v = f6_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);

            if (chkBox1.isChecked()) {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int) label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionF6s.add(questionB1);
            }
            active.setQuestionF6(new Gson().toJson(questionF6s));

        }
    }    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }


}
