package markematics.active17213.app.SectionS;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.lassana.recorder.AudioRecorder;
import com.markematics.Active17213.R;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityIntro;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionA.ActivityA1;

public class ActivityS14S15 extends Activity {

    EditText editTextS15;
    TextView textViewS14, textViewS15,lblChildName;
    Spinner spinnerS14;
    Button buttonS14;
    Active active;
    Toolbar toolbar;
    Typeface tf;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s14_s15);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S14 S15 ");
        toolbar.setTitleTextColor(Color.WHITE);
        textViewS14 = (TextView) findViewById(R.id.tViewS14);
        lblChildName = (TextView)findViewById(R.id.lblChildName);
        spinnerS14 = (Spinner) findViewById(R.id.spinner_s14);




        textViewS15 = (TextView) findViewById(R.id.tViewS15);
        editTextS15 = (EditText) findViewById(R.id.editTextS15);

        buttonS14 = (Button) findViewById(R.id.btnS14);
        buttonS14.setTextColor(Color.parseColor("#FFFFFF"));
        buttonS14.setOnClickListener(btnNext_OnClickListener);

        new AsyncHandler(REQUEST_RENDER).execute();
        spinnerS14.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //I.E. if in the height spinner CM is selected I would like to hide the second height edittext field.
                // I'm not sure if this is meant to be "height1" or "height"
                if (position == 1) {
                    editTextS15.setVisibility(View.INVISIBLE);
                    textViewS15.setVisibility(View.INVISIBLE);
                    buttonS14.setText("NEXT");
                    buttonS14.setTextColor(Color.parseColor("#FFFFFF"));

                } else {
                    editTextS15.setVisibility(View.VISIBLE);
                    textViewS15.setVisibility(View.VISIBLE);
                    buttonS14.setText("Finish");
                    buttonS14.setTextColor(Color.parseColor("#FFFFFF"));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        new AsyncHandler(REQUEST_ADD).execute();
        }
    };


    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityS14S15.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityS14S15.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    Intent intent = new Intent(ActivityS14S15.this, ActivityA1.class);
                    intent.putExtra("active", active);

                    startActivity(intent);
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private void add()
    {
        active.setQuestionS15(editTextS15.getText().toString());
        active.setQuestionS14(""+spinnerS14.getSelectedItemPosition());
    }
    private boolean validationSuccess() {

        if (editTextS15.getVisibility() == View.VISIBLE) {
            if (editTextS15.getText().toString().isEmpty()) {

                editTextS15.setError("Please enter Reason");
                return false;
            }
        }
        if (spinnerS14.getSelectedItemPosition() == 0)
        {
            showDialog("Please select at least one option");
            return false;
        }
        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS14S15.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                            if (validationSuccess())
                            {
                                add();
                                if (spinnerS14.getSelectedItemPosition() == 1)
                                {
                                    StopRecording();

                                }else
                                {
                                    Utility.showExitDialog(ActivityS14S15.this, "کیا آپ انٹرویو ختم کرنا چاہتی ہیں", active );
//                    Intent i = new Intent(ActivityS14S15.this, ActivityIntro.class);
//                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(i);
//                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                }
                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            ArrayAdapter<String> spinnerS14ArrayAdapter = new ArrayAdapter<String>(ActivityS14S15.this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayS14));
                            spinnerS14.setAdapter(spinnerS14ArrayAdapter);
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            active = (Active) getIntent().getSerializableExtra("active");
                            lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
                            lblChildName.setTypeface(tf);

                            textViewS14.setTypeface(tf);
                            textViewS15.setTypeface(tf);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }

}
