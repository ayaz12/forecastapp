package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/9/2017.
 */

public class QuestionB14 {
    private int brandCode;
    private String brandName;
    private int responseCode;

    public int getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(int brandCode) {
        this.brandCode = brandCode;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
