package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionE6;
import markematics.active17213.Model.QuestionE7;
import markematics.active17213.app.SectionF.ActivityF1F2;

/**
 * Created by ZEB Rana on 16-Oct-17.
 */

public class ActivityE7 extends AppCompatActivity {

    TextView tViewE7;
    LinearLayout e7layout;
    Button btnNext;
    Active active;
    Typeface tf;
    Toolbar toolbar;
    List<QuestionE7> questionE7s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e7);

        tViewE7 = (TextView) findViewById(R.id.tViewE7);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question E7");
        toolbar.setTitleTextColor(Color.WHITE);

        e7layout = (LinearLayout) findViewById(R.id.E7_layout);

        btnNext = (Button) findViewById(R.id.btnNext);



        active = (Active) getIntent().getSerializableExtra("active");

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewE7.setTypeface(tf);
        randerE7Layout();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
    }

    private void next() {
        {
            add();
            if (validate()) {
                Intent intent = new Intent(ActivityE7.this, ActivityE8.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    }

    private void randerE7Layout() {
        String[] arrayE7 = getResources().getStringArray(R.array.arraye7);
        for (int i = 0; i < arrayE7.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
          EditText editText = (EditText)v.findViewById(R.id.editText);

            label.setTypeface(tf);
            label.setText(arrayE7[i]);
            label.setTag(i+1);
            if (label.getText().toString().equalsIgnoreCase("کوئی رشتہ دار")||label.getText().toString().equalsIgnoreCase("دیگر"))
            {
                editText.setVisibility(View.VISIBLE);
            }
            e7layout.addView(v);
        }
    }

    private void add()

    {
        questionE7s = new ArrayList<>();
        for (int i=0;i<e7layout.getChildCount();i++)
        {
            View v = e7layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            if (chkBox.isChecked())
            {
                QuestionE7 questionE7 = new QuestionE7();
                questionE7.setLabel(label.getText().toString());
                questionE7.setCode((int)label.getTag());
                questionE7.setOpenEnded(editText.getText().toString());
                questionE7s.add(questionE7);
            }

        }

        active.setQuestionE7(new Gson().toJson(questionE7s));
    }

    private boolean validate()
    {
        for (int i=0;i<questionE7s.size();i++)
        {
            if (questionE7s.get(i).getLabel().equalsIgnoreCase("دیگر")||questionE7s.get(i).getLabel().equalsIgnoreCase("کوئی رشتہ دار"))
            {
                if (questionE7s.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter");
                    return false;
                }
            }
        }
        if (questionE7s.size() == 0)
        {
            showDialog("Please Select At least one option");
            return false;
        }
        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
