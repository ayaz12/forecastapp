package markematics.active17213.app.SectionR;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

import markematics.active17213.Model.Active;
import markematics.active17213.app.SectionG.ActivityG1;


public class ActivityR2 extends AppCompatActivity {

    TextView  tViewR2;
    EditText  editTextR2;
    Button btnNext;
    String er2;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r2);

        tViewR2 = (TextView) findViewById(R.id.tViewR2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question R2");
        toolbar.setTitleTextColor(Color.WHITE);

         editTextR2 = (EditText) findViewById(R.id.editTextR2);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        active = (Active) getIntent().getSerializableExtra("active");
        btnNext = (Button) findViewById(R.id.btnNext);
        tViewR2.setTypeface(tf);
        editTextR2.setTypeface(tf);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
    }

    private void next() {
        initialize();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n", Toast.LENGTH_SHORT).show();
        } else {
            add();
            proceed();

        }


    }

    private boolean validationSuccess() {

        boolean valid = true;

        if (er2.isEmpty()) {
            editTextR2.setError("درج کریں");
            valid = false;
        }

        return valid;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityR2.this, ActivityG1.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void initialize() {


        er2 = editTextR2.getText().toString().trim();
    }

    private void add(){


        active.setQuestionR2(editTextR2.getText().toString());
    }
}