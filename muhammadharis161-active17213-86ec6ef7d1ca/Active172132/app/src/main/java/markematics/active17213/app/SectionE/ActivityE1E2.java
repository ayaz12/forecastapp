package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.github.lassana.recorder.AudioRecorderBuilder;
import com.google.gson.Gson;
import com.markematics.Active17213.R;


import java.io.File;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionE3;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionC.ActivityC1;

/**
 * Created by ZEB Rana on 16-Oct-17.
 */

public class ActivityE1E2 extends AppCompatActivity {

    TextView tViewE1, tViewE2;
    Spinner spinnere2, spinnere1;
    Typeface tf;
    Button btnNext;
    Toolbar toolbar;
    Active active;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e1_e2);

        tViewE1 = (TextView) findViewById(R.id.tViewE1);
        tViewE2 = (TextView) findViewById(R.id.tViewE2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question E1 E2");
        toolbar.setTitleTextColor(Color.WHITE);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        spinnere1 = (Spinner) findViewById(R.id.spinnere1);
        spinnere2 = (Spinner) findViewById(R.id.spinnere2);


        btnNext = (Button) findViewById(R.id.btnNext);

        tViewE1.setTypeface(tf);
        tViewE2.setTypeface(tf);
        active = (Active) getIntent().getSerializableExtra("active");

        ArrayAdapter<String> spinnersE1ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayE1));
        spinnere1.setAdapter(spinnersE1ArrayAdapter);

        ArrayAdapter<String> spinnersE2ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayE2));
        spinnere2.setAdapter(spinnersE2ArrayAdapter);



        btnNext.setOnClickListener(btnNext_OnClickListener);
        StartRecording();
    }

    private void StartRecording()
    {
                if (ViewController.mAudioRecorder == null)
        {
            String filePath = active.gethR_ID()+"_"+active.getCity()+"_SecE_"+System.currentTimeMillis();
            active.setAudioPathSecE(filePath);
            recordAudio(filePath);

        }
    }
    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate()) {
                Intent intent = new Intent(ActivityE1E2.this, ActivityE3.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    };


    private void add() {
        active.setQuestionE1("" + spinnere1.getSelectedItemPosition());
        active.setQuestionE2("" + spinnere2.getSelectedItemPosition());
    }

    private boolean validate() {
        if (spinnere1.getSelectedItemPosition() == 0) {
            showDialog("Please Select E1");
            return false;
        }
        if (spinnere1.getSelectedItemPosition() == 0) {
            showDialog("Please Select E2");
            return false;
        }

        return true;
    }
    private void recordAudio(String fileName) {


        final RecorderApplication application = RecorderApplication.getApplication(ActivityE1E2.this);
        ViewController.mAudioRecorder = application.getRecorder();
        if (ViewController.mAudioRecorder == null
                || ViewController.mAudioRecorder.getStatus() == AudioRecorder.Status.STATUS_UNKNOWN) {
            ViewController.mAudioRecorder = AudioRecorderBuilder.with(application)
                    .fileName(createDirectoryAndSaveFile(fileName))
                    .config(AudioRecorder.MediaRecorderConfig.DEFAULT)
                    .loggable()
                    .build();
            application.setRecorder(ViewController.mAudioRecorder);
        }
        start();
    }
    private void message(String message) {
        Toast.makeText(ActivityE1E2.this,message,Toast.LENGTH_SHORT).show();
    }
    private void start()
    {
        ViewController.mAudioRecorder .start(new AudioRecorder.OnStartListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onException(Exception e) {

                //   invalidateViews();
                message(getString(R.string.error_audio_recorder, e));
            }
        });
    }
    private String createDirectoryAndSaveFile( String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.ActiveQuant/Audio/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.ActiveQuant/Audio/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.ActiveQuant/Audio/"), fileName+ ".mp3" );
        if (file.exists()) {
            file.delete();
        }

        return file.getPath();
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
