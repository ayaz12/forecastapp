package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/12/2017.
 */

public class User {
    String hrId;
    String name;
    String city;

    public String getHrId() {
        return hrId;
    }

    public void setHrId(String hrId) {
        this.hrId = hrId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
