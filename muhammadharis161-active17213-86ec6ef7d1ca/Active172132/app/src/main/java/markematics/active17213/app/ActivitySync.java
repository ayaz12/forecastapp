package markematics.active17213.app;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.markematics.Active17213.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import markematics.active17213.DataAccess.DAO.ActiveDAO;
import markematics.active17213.DataAccess.DAO.LocationDAO;
import markematics.active17213.DataAccess.HttpHandler;
import markematics.active17213.Helper.MultipartUtility;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Location;

public class ActivitySync extends AppCompatActivity{

    TextView textViewsync,textViewtotalquestioner,textViewtotalfiles;
    Button btnSync;
    Toolbar toolbar;
    List<Active> activeList;
    String REQUEST_ACTIVE = "volumePrice";
    String REQUEST_LOCATION= "location";
    String REQUEST_AUDIO= "AUDIO";
    List<File> fileList;
    List<Location> locations;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Sync");
        toolbar.setTitleTextColor(Color.WHITE);

        textViewsync = (TextView)findViewById(R.id.textViewSync);
        textViewtotalquestioner = (TextView)findViewById(R.id.tViewTotQuest);
        textViewtotalfiles = (TextView)findViewById(R.id.tViewTotFile);
        btnSync =(Button)findViewById(R.id.btnSync);
        btnSync.setOnClickListener(btnSync_OnClickListener);

        getAllActive();
        getFiles();
        getLocations();
        setLabels();
    }
    private View.OnClickListener btnSync_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        syncActive();
        }
    };
    private  String setLabels()
    {
        ActiveDAO activeDAO = new ActiveDAO(ActivitySync.this);

        String aciveCount = "Total Questionnaire :"+ activeDAO.getAll().size();




        textViewtotalquestioner.setText(aciveCount);

        textViewtotalfiles.setText("Total Files:" +fileList.size());


        return aciveCount;


    }
    private void getAllActive() {
        ActiveDAO entryExitDAO = new ActiveDAO(ActivitySync.this);
        activeList = entryExitDAO.getAll();


    }
    private void getLocations()
    {
        LocationDAO locationDAO = new LocationDAO(ActivitySync.this);
        locations = locationDAO.getLocations();
    }
    private void getFiles()
    {
        fileList = new ArrayList<>();
        String path = Environment.getExternalStorageDirectory().toString()+"/.ActiveQuant/Audio/";
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        Log.d("Files", "Size: "+ files.length);
        for (int i = 0; i < files.length; i++)
        {
            Log.d("Files", "FileName:" + files[i].getName());
            if (files[i].getName().contains(".mp3")) {
                fileList.add(files[i]);
            }
        }
    }
    private boolean rename(File from, File to) {
        return from.getParentFile().exists() && from.exists() && from.renameTo(to);
    }
    private void syncActive() {

        if (activeList.size() == 0) {
//            getFiles();
//            syncAudioFiles();
            syncLocations();
        }
        else {
            if (isNetworkAvailable()) {


                String jsonObject = getActiveJSON(activeList.get(0));
                String url = "http://94.75.217.75/Ontex/ontex.svc/syncActiveQuant";//getString(R.string.site_url) + getString(R.string.api_post);
                JSONObject parameters = new JSONObject();
                try {
                    parameters.put("data", jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("Url", url);
                Log.i("Parameters", parameters.toString());
                new AsyncHandler(REQUEST_ACTIVE).execute(url, parameters);
            } else {
                showDilog("Internet connection not found");
            }
        }
    }

    private void syncLocations() {

        if (locations.size() == 0) {

//            showDilog("No record found");
//            return;
            //  syncInvoices();
            // syncAudios();
            syncAudioFiles();
        }
        else {
            if (isNetworkAvailable()) {


                String jsonObject = getLocationJsonObject(locations.get(0));
                String url = "http://94.75.217.75/Ontex/ontex.svc/syncTrackRecord";//getString(R.string.site_url) + getString(R.string.api_post);
                JSONObject parameters = new JSONObject();
                try {
                    parameters.put("data", jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("Url", url);
                Log.i("Parameters", parameters.toString());
                new AsyncHandler(REQUEST_LOCATION).execute(url, parameters);
            } else {
                showDilog("Internet connection not found");
            }
        }
    }
    private void syncAudioFiles() {

        if (fileList.size() == 0) {

            showDilog("All data Successfully Synced");
            return;
            //syncLocations();
        }
        else {
            if (isNetworkAvailable()) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date lastModDate = new Date(fileList.get(0).lastModified());

                String formattedDate = df.format(lastModDate);
                String name[] = fileList.get(0).getName().replace(".", "_").split("_");
                String fileName = fileList.get(0).getName().replace(".mp3", "");

                new AsyncHandler(REQUEST_AUDIO).execute("http://94.75.217.75/Ontex/ontex.svc/UploadFile?path=" +fileName);
                Log.i("Url","http://94.75.217.75/Ontex/ontex.svc//UploadFile?path=" + fileName);
            } else {
                showDilog("Internet connection not found");
            }
        }
    }

    private void showDilog(String msg) {
        new AlertDialog.Builder(ActivitySync.this)
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public String getActiveJSON(Active active) {

        Gson gson = new Gson();
        String json = gson.toJson(active);
        return json.toString();

    }
    public String getLocationJsonObject(Location location) {

        Gson gson = new Gson();
        String json = gson.toJson(location);
        return json.toString();

    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivitySync.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


                if (request.equalsIgnoreCase(REQUEST_ACTIVE)) {
                    JsonParser parser = new JsonParser();
                    String retVal = parser.parse(result).getAsString();
                    JSONObject json = new JSONObject(retVal);
                    Toast.makeText(ActivitySync.this, json.getString("msg"), Toast.LENGTH_SHORT).show();
                    String val = json.getString("val");

                    // tvCount.setText("add count: " + itemCount );
                    if (activeList.size() > 0) {
                        ActiveDAO activeDAO = new ActiveDAO(ActivitySync.this);
                        activeDAO.delete(activeList.get(0).getId());
                        ;
                        //  File currentFile = new File("/sdcard/GTEntryExit/Audio/"+entryExitList.get(0).getPath()+".mp3");
//                        File currentFile = new File(new File("/sdcard/GTEntryExit/Audio/"), entryExitList.get(0).getPath()+ ".mp3" );
//                        File newFile = new File(new File("/sdcard/GTEntryExit/Audio/"), val+ ".mp3" );
//
//                        if(rename(currentFile, newFile)){
//                            //Success
//                            Log.i("TAG", "Success");
//                        } else {
//                            //Fail
//                            Log.i("TAG", "Fail");
//                        }
                        activeList.remove(0);
                        String volume = "Total Questionnaire :"+ activeList.size();
                        textViewtotalquestioner.setText(volume);
                        syncActive();
                    }
                }

                else if (request.equalsIgnoreCase(REQUEST_LOCATION)) {
                    JsonParser parser = new JsonParser();
                    String retVal = parser.parse(result).getAsString();
                    JSONObject json = new JSONObject(retVal);
                    Toast.makeText(ActivitySync.this, json.getString("msg"), Toast.LENGTH_SHORT).show();

                    // tvCount.setText("add count: " + itemCount );
                    if (locations.size() > 0) {
                        LocationDAO locationDAO = new LocationDAO(ActivitySync.this);
                        locationDAO.delete(locations.get(0));
                        locations.remove(0);

                        syncLocations();
                    }
                }
                else if(request.equalsIgnoreCase(REQUEST_AUDIO))
                {
                    if (fileList.size() > 0) {
                        fileList.get(0);
                        try {
                            // delete(fileList.get(0));
                            fileList.get(0).delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        fileList.remove(0);
                        String volume = "Total Files :"+ fileList.size();
                        textViewtotalfiles.setText(volume);
                        syncAudioFiles();


                    }
                }
            }
            catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }


        @SuppressWarnings("unchecked")
        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;
            String url = (String) params[0];
            if (request.equalsIgnoreCase(REQUEST_AUDIO)) {
                URL url1 = null;
                try {
                    url1 = new URL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                try {
                    final MultipartUtility http = new MultipartUtility(url1);
                    http.addFilePart("someFile", fileList.get(0));

                    final byte[] bytes = http.finish();
                    OutputStream os = null;
                    try {
                        os = new FileOutputStream(fileList.get(0));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    os.write(bytes);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            else {
                JSONObject jsonObject = (JSONObject) params[1];
                responseObject = new HttpHandler(url, jsonObject, ActivitySync.this).PostServer();

            }
            return responseObject;

        }
    }

}
