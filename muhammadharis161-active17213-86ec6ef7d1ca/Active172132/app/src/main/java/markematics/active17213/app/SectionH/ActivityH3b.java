package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH3a;
import markematics.active17213.Model.QuestionH3b;

public class ActivityH3b extends AppCompatActivity {

    TextView tViewH3b;
    Button btnNext;
    Typeface tf;
    LinearLayout H3blayout;
    Toolbar toolbar;
    Active active;
    List<QuestionH3b> questionH3bH;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h3b);

        tViewH3b = (TextView) findViewById(R.id.tViewH3b);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H3b");
        toolbar.setTitleTextColor(Color.WHITE);



        active = (Active) getIntent().getSerializableExtra("active");

        H3blayout = (LinearLayout) findViewById(R.id.H3b_layout);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH3b.setTypeface(tf);
        randerH3bLayout();
        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {
        add();
        if (validationSuccess()) {

            proceed();
        } else {



        }


    }

    private boolean validationSuccess() {

        boolean valid = true;
            if (questionH3bH.size() == 0)
            {
                showDialog("please select at least one option H3b");
                return false;
            }

        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH3b.this, ActivityH4.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }


    private void randerH3bLayout() {
        String[] arrayH3b = getResources().getStringArray(R.array.arrayH3b);
        for (int i = 0; i < arrayH3b.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH3b[i]);
            label.setTag(i+1);
            H3blayout.addView(v);
        }
    }

    private void add() {


        questionH3bH = new ArrayList<>();
        for (int j = 0; j < H3blayout.getChildCount(); j++) {
            View view = H3blayout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            QuestionH3b questionH3b = new QuestionH3b();

            if (checkBox.isChecked()) {
                questionH3b.setCode((int) labeled.getTag());
                questionH3b.setStatement(labeled.getText().toString());
                questionH3bH.add(questionH3b);
            }
        }


        active.setQuestionH3b(new Gson().toJson(questionH3bH));
    }


    }
