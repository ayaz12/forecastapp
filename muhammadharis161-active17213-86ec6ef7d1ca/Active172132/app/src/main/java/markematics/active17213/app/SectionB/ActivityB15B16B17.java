package markematics.active17213.app.SectionB;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB15B16B17 extends AppCompatActivity {
    TextView tViewB15,tViewB16,tViewB17;
    EditText editTextB16,editTextB17;
    Button btnB15;
    String emptyedittextb17,brandname;
    LinearLayout b15layout;
    View view;
    Spinner B15;
    Typeface tf;
    Toolbar toolbar;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question D15 D16 17");
        toolbar.setTitleTextColor(Color.WHITE);
        setContentView(R.layout.activity_b15_b16_17);
        tViewB15 = (TextView)findViewById(R.id.tViewB15);
        tViewB16 = (TextView)findViewById(R.id.tViewB16);
        tViewB17 = (TextView)findViewById(R.id.tViewB17);
        editTextB16 = (EditText)findViewById(R.id.editTextB16);
        editTextB17 = (EditText)findViewById(R.id.editTextB17);
        btnB15 = (Button)findViewById(R.id.btnB15);
        b15layout =(LinearLayout)findViewById(R.id.B15Layout);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewB15.setTypeface(tf);
        tViewB16.setTypeface(tf);
        tViewB17.setTypeface(tf);
        editTextB16.setTypeface(tf);
        editTextB17.setTypeface(tf);
        btnB15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
randerB15Layout();
    }

    private void next() {
        initialize();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n",Toast.LENGTH_SHORT).show();
        } else {
            proceed();
        }
    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (emptyedittextb17.isEmpty()) {
            editTextB17.setError(" برانڈ");
            valid = false;

        }
        if (brandname.isEmpty()) {
            editTextB16.setError("درج کریں");
            valid = false;

        }

        return valid;
    }

    private void initialize() {
     emptyedittextb17 = (editTextB17).getText().toString().trim();
        brandname = (editTextB16).getText().toString().trim();

    }

    private void proceed() {
        {Intent nextPage = new Intent(ActivityB15B16B17.this, ActivityB18Old.class);
            startActivity(nextPage);

            gotoNextPage(view);
        }


    }

    private void gotoNextPage(View view) {
    }

    private void randerB15Layout() {
        String[] arrayB15 = getResources().getStringArray(R.array.arrayB15);
        for (int i = 0; i < arrayB15.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setText(arrayB15[i]);
            label.setTypeface(tf);
            b15layout.addView(v);
        }
    }}