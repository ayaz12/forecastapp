package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.github.lassana.recorder.AudioRecorder;
import com.markematics.Active17213.R;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionB.ActivityB1;
import markematics.active17213.app.SectionB.ActivityB1B2;


public class ActivityA11 extends AppCompatActivity {
    TextView  tViewA11aTape, tViewA11bTape, tViewA11aPant, tViewA11bPant,lblChildName;
    Typeface tf;
    CheckBox checkBoxTape, checkBoxPant;
    Button btnNext;
    EditText editTextA11Tape,editTextA11Pant;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a11a_a11b);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A11");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewA11aTape=(TextView)findViewById(R.id.tViewA11aTape);
        tViewA11bTape=(TextView)findViewById(R.id.tViewA11bTape);
        tViewA11aPant=(TextView)findViewById(R.id.tViewA11aPant);
        tViewA11bPant=(TextView)findViewById(R.id.tviewA11bPant);
        lblChildName = (TextView)findViewById(R.id.lblChildName);

        editTextA11Tape = (EditText) findViewById(R.id.editTextA11Tape);
        editTextA11Pant = (EditText) findViewById(R.id.editTextA11Pant);

        checkBoxTape = (CheckBox) findViewById(R.id.checkboxTape);
        checkBoxPant = (CheckBox) findViewById(R.id.checkboxPant);


        btnNext =(Button)findViewById(R.id.btnNext);


        btnNext.setOnClickListener(btnNext_OnClickListener);
        checkBoxTape.setOnCheckedChangeListener(onCheckedTape_OnCheckedChangeListener);
        checkBoxPant.setOnCheckedChangeListener(onCheckedPant_OnCheckedChangeListener);

        new AsyncHandler(REQUEST_RENDER).execute();

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityA11.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityA11.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    Intent intent = new Intent(ActivityA11.this, ActivityB1.class);
                    intent.putExtra("active", active);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private CompoundButton.OnCheckedChangeListener onCheckedTape_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b)
            {
                editTextA11Tape.setEnabled(true);
            }else
            {
                editTextA11Tape.setEnabled(false);
            }
        }
    };
    private CompoundButton.OnCheckedChangeListener onCheckedPant_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b)
            {
                editTextA11Pant.setEnabled(true);
            }else
            {
                editTextA11Pant.setEnabled(false);
            }
        }
    };
    private boolean validationSuccess() {

//        if (!checkBoxTape.isChecked())
//        {
//            showDialog("Please select A11a");
//            return false;
//        }
        if (!(checkBoxTape.isChecked()) && !(checkBoxPant.isChecked()))
        {
            showDialog("Please select at least one check box");
            return false;
        }

        if (checkBoxTape.isChecked())
        {
            if (editTextA11Tape.getText().toString().isEmpty())
            {
                editTextA11Tape.setError("درج کریں");
                return false;
            }
        }
//        if (!checkBoxPant.isChecked())
//        {
//            showDialog("Please select A11b");
//            return false;
//        }

        if (checkBoxPant.isChecked())
        {
            if (editTextA11Pant.getText().toString().isEmpty())
            {
                editTextA11Pant.setError("درج کریں");
                return false;
            }
        }




        return true;
    }


    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

        private void add()
    {
        active.setTape(checkBoxTape.isChecked()?1:0);
        active.setPant(checkBoxPant.isChecked()?2:0);
        active.setQuestionA11a(editTextA11Tape.getText().toString());
        active.setQuestionA11b(editTextA11Pant.getText().toString());

    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA11.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validationSuccess())
                            {
                                add();
                                if (!checkBoxPant.isChecked()) {
                                    Intent intent = new Intent(ActivityA11.this, ActivityA12A13.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }
                                else
                                {

                                    StopRecording();

                                }
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            active = (Active) getIntent().getSerializableExtra("active");
                            tf=Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

                            lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
                            lblChildName.setTypeface(tf);
                            tViewA11aTape.setTypeface(tf);
                            tViewA11bTape.setTypeface(tf);
                            tViewA11aPant.setTypeface(tf);
                            tViewA11bPant.setTypeface(tf);
                            checkBoxTape.setTypeface(tf);
                            checkBoxPant.setTypeface(tf);
                            editTextA11Pant.setEnabled(false);
                            editTextA11Tape.setEnabled(false);

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}