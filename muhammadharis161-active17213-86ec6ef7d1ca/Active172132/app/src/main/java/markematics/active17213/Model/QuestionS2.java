package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/3/2017.
 */

public class QuestionS2 {
    private String statement;
    private int code;


    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
