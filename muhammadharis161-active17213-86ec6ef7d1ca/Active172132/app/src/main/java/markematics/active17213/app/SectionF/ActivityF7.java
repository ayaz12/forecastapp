package markematics.active17213.app.SectionF;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.lassana.recorder.AudioRecorder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionG.ActivityG1;
import markematics.active17213.app.SectionR.ActivityR1;
import markematics.active17213.app.SectionR.ActivityR2;


public class ActivityF7 extends AppCompatActivity {
    TextView tViewf7,lblBrandName;
    EditText editTextF7;
    Button btnNext;
    Typeface tf;
    String f7;
    Toolbar toolbar;
    Active active;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f7);


        tViewf7 = (TextView) findViewById(R.id.tViewF7);
        lblBrandName = (TextView)findViewById(R.id.lblBrandName);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question F7");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        editTextF7 = (EditText) findViewById(R.id.editTextF7);

        btnNext = (Button) findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewf7.setTypeface(tf);
        editTextF7.setTypeface(tf);
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionF6s = new Gson().fromJson(active.getQuestionF6(), listType);
        lblBrandName.setText(questionF6s.get(0).getBrand()+" برانڈ کا نام ");
        lblBrandName.setTypeface(tf);



        btnNext.setOnClickListener(btnNext_OnClickListener);

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate())
            {
                GoToNext();


            }
        }
    };

    private void GoToNext() {
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
        List<Brand> brands = new ArrayList<>();
        for (int i =0;i<questionB1s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB2s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB3s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
            brands.add(brand);
        }
        boolean isCanbebe = false;
        for (int i = 0;i<brands.size();i++)
        {
            if (brands.get(i).getName().equalsIgnoreCase("Canbebe"))
            {
                isCanbebe = true;
            }
        }
        if (isCanbebe)
        {
            Type listTypeB4 = new TypeToken<ArrayList<QuestionB4>>() {
            }.getType();
            List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listTypeB4);
            boolean isCanbebeB4 = false;
            for (int i = 0; i< questionB4s.size(); i++)
            {
                if (questionB4s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                {
                    isCanbebeB4 = true;
                }
            }
            if (isCanbebeB4)
            {
                boolean isCanbebeB5 = false;
                Type listTypeB5 = new TypeToken<ArrayList<QuestionB4>>() {
                }.getType();
                List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listTypeB5);

                for (int i = 0; i< questionB5s.size(); i++)
                {
                    if (questionB5s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                    {
                        isCanbebeB5 = true;

                    }
                }
                if (isCanbebeB5)
                {
                    Intent nextPage = new Intent(ActivityF7.this, ActivityG1.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);
                    return;
                }
                else
                {
                    Intent nextPage = new Intent(ActivityF7.this, ActivityR2.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);
                    return;
                }
            }
            else
            {
                Intent nextPage = new Intent(ActivityF7.this, ActivityR1.class);
                nextPage.putExtra("active", active);
                startActivity(nextPage);
                return;
            }
        }
        else
        {
            Intent nextPage = new Intent(ActivityF7.this, ActivityG1.class);
            nextPage.putExtra("active", active);
            startActivity(nextPage);
            return;
        }
    }

    private boolean validate() {

        if (editTextF7.getText().toString().isEmpty()) {
            editTextF7.setError("درج کریں");
            return false;
        }
        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityF7.this, ActivityR1.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);

    }

    private void initialize() {
        f7 = editTextF7.getText().toString().trim();

    }
    private void add() {

        active.setQuestionF7(editTextF7.getText().toString());
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

}
