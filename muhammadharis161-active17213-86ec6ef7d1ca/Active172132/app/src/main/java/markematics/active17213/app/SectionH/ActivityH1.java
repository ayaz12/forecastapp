package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.github.lassana.recorder.AudioRecorderBuilder;
import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH3a;
import markematics.active17213.Model.QuestionH3b;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionB.ActivityB8;
import markematics.active17213.app.SectionB.ActivityB9;
import markematics.active17213.app.SectionE.ActivityE1E2;

public class ActivityH1 extends AppCompatActivity {

    TextView tViewH1;
    Button btnNext;
    Spinner spinnerH1;
    Typeface tf;
    Toolbar toolbar;
    Active active;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h1);

        tViewH1 = (TextView) findViewById(R.id.tViewH1);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H1");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");
//
        spinnerH1 = (Spinner) findViewById(R.id.spinnerH1);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH1.setTypeface(tf);
        ArrayAdapter<String> spinnerE8ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH1.setAdapter(spinnerE8ArrayAdapter);
        btnNext = (Button) findViewById(R.id.btnNext);
        StartRecording();
        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (spinnerH1.getSelectedItemPosition()!=0)

                {
                    next();
                }
                else {
                    showDialog("Please Select Atleast One Option");
                }

            }
        });

    }

    private void next() {
        add();
        if (spinnerH1.getSelectedItemPosition() == 1) {
            Intent intent = new Intent(ActivityH1.this, ActivityH2.class);
            intent.putExtra("active", active);
            startActivity(intent);


        } else {
            Intent intent = new Intent(ActivityH1.this, ActivityH4.class);
            intent.putExtra("active", active);
            startActivity(intent);

        }
    }

    private void StartRecording()
    {
        if (ViewController.mAudioRecorder == null)
        {
            String filePath = active.gethR_ID()+"_"+active.getCity()+"_SecHN_"+System.currentTimeMillis();
            active.setAudioPathSecH(filePath);
            recordAudio(filePath);

        }
    }
    private void recordAudio(String fileName) {


        final RecorderApplication application = RecorderApplication.getApplication(ActivityH1.this);
        ViewController.mAudioRecorder = application.getRecorder();
        if (ViewController.mAudioRecorder == null
                || ViewController.mAudioRecorder.getStatus() == AudioRecorder.Status.STATUS_UNKNOWN) {
            ViewController.mAudioRecorder = AudioRecorderBuilder.with(application)
                    .fileName(createDirectoryAndSaveFile(fileName))
                    .config(AudioRecorder.MediaRecorderConfig.DEFAULT)
                    .loggable()
                    .build();
            application.setRecorder(ViewController.mAudioRecorder);
        }
        start();
    }
    private void message(String message) {
        Toast.makeText(ActivityH1.this,message,Toast.LENGTH_SHORT).show();
    }
    private void start()
    {
        ViewController.mAudioRecorder .start(new AudioRecorder.OnStartListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onException(Exception e) {

                //   invalidateViews();
                message(getString(R.string.error_audio_recorder, e));
            }
        });
    }
    private String createDirectoryAndSaveFile( String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.ActiveQuant/Audio/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.ActiveQuant/Audio/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.ActiveQuant/Audio/"), fileName+ ".mp3" );
        if (file.exists()) {
            file.delete();
        }

        return file.getPath();
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private void add() {

        active.setQuestionH1("" + spinnerH1.getSelectedItemPosition());
    }}