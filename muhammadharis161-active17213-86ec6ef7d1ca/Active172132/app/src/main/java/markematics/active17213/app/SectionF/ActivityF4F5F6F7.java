package markematics.active17213.app.SectionF;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.app.SectionG.ActivityG1;
import markematics.active17213.app.SectionR.ActivityR1;
import markematics.active17213.app.SectionR.ActivityR2;


public class ActivityF4F5F6F7 extends AppCompatActivity {
    TextView tViewf4a, tViewf4b, tViewf5, tViewf6, tViewf7;
    LinearLayout F4a_4b_f5_f6layout;
    EditText editTextF7;
    Button btnNext;
    String f7;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB1> questionF4as;
    List<QuestionB1> questionF4bs;
    List<QuestionB1> questionF5s;
    List<QuestionB1> questionF6s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f4_f5_f6_f7);


        tViewf4a = (TextView) findViewById(R.id.tViewF4a);
         tViewf5 = (TextView) findViewById(R.id.tViewF5);
        tViewf6 = (TextView) findViewById(R.id.tViewF6);
        tViewf7 = (TextView) findViewById(R.id.tViewF7);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question F4 F5 F6 F7");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        F4a_4b_f5_f6layout = (LinearLayout) findViewById(R.id.f4a_f4b_f5_layout);
//        F4blayout = (LinearLayout) findViewById(R.id.F4b_layout);
//        F5layout = (LinearLayout) findViewById(R.id.F5_layout);
//        F6layout = (LinearLayout) findViewById(R.id.F6_layout);

        editTextF7 = (EditText) findViewById(R.id.editTextF7);

        btnNext = (Button) findViewById(R.id.btnNext);

        tViewf4a.setTypeface(tf);
      //  tViewf4b.setTypeface(tf);
        tViewf5.setTypeface(tf);
        tViewf6.setTypeface(tf);
        tViewf7.setTypeface(tf);

        editTextF7.setTypeface(tf);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        randerF4aLayout();


      btnNext.setOnClickListener(btnNext_OnClickListener);

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate())
            {
                Type listType = new TypeToken<ArrayList<QuestionB1>>() {
                }.getType();
                List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
                List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
                List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
                List<Brand> brands = new ArrayList<>();
                for (int i =0;i<questionB1s.size();i++)
                {
                    Brand brand = new Brand();
                    brand.setCode(questionB1s.get(i).getCode());
                    brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
                            questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
                    brands.add(brand);
                }
                for (int i =0;i<questionB2s.size();i++)
                {
                    Brand brand = new Brand();
                    brand.setCode(questionB2s.get(i).getCode());
                    brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
                            questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
                    brands.add(brand);
                }
                for (int i =0;i<questionB3s.size();i++)
                {
                    Brand brand = new Brand();
                    brand.setCode(questionB3s.get(i).getCode());
                    brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
                            questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
                    brands.add(brand);
                }
                boolean isCanbebe = false;
                for (int i = 0;i<brands.size();i++)
                {
                    if (brands.get(i).getName().equalsIgnoreCase("Canbebe"))
                    {
                        isCanbebe = true;
                    }
                }
                if (isCanbebe)
                {
                    Type listTypeB4 = new TypeToken<ArrayList<QuestionB4>>() {
                    }.getType();
                    List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listTypeB4);
                    boolean isCanbebeB4 = false;
                    for (int i = 0; i< questionB4s.size(); i++)
                    {
                        if (questionB4s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                        {
                          isCanbebeB4 = true;
                        }
                    }
                    if (isCanbebeB4)
                    {
                        boolean isCanbebeB5 = false;
                        Type listTypeB5 = new TypeToken<ArrayList<QuestionB4>>() {
                        }.getType();
                        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listTypeB5);

                        for (int i = 0; i< questionB5s.size(); i++)
                        {
                            if (questionB5s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                            {
                                isCanbebeB5 = true;

                            }
                        }
                        if (isCanbebeB5)
                        {
                            Intent nextPage = new Intent(ActivityF4F5F6F7.this, ActivityG1.class);
                            nextPage.putExtra("active", active);
                            startActivity(nextPage);
                            return;
                        }
                        else
                        {
                            Intent nextPage = new Intent(ActivityF4F5F6F7.this, ActivityR2.class);
                            nextPage.putExtra("active", active);
                            startActivity(nextPage);
                            return;
                        }
                    }
                    else
                    {
                        Intent nextPage = new Intent(ActivityF4F5F6F7.this, ActivityR1.class);
                        nextPage.putExtra("active", active);
                        startActivity(nextPage);
                        return;
                    }
                }
                else
                {
                    Intent nextPage = new Intent(ActivityF4F5F6F7.this, ActivityG1.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);
                    return;
                }


            }
        }
    };

    private boolean validate() {

        if (questionF4as.size() == 0) {
            showDialog("Please select at least one option 4a");
            return false;
        }
        if (questionF4bs.size() == 0) {
            showDialog("Please select at least one option 4bs");
            return false;
        }
        if (questionF5s.size() == 0) {
            showDialog("Please select at least one option 5");
            return false;
        }
        if (questionF6s.size() == 0) {
            showDialog("Please select at least one option 6");
            return false;
        }
        if (editTextF7.getText().toString().isEmpty()) {
            editTextF7.setError("درج کریں");
            return false;
        }
        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityF4F5F6F7.this, ActivityR1.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);

    }

    private void initialize() {
        f7 = editTextF7.getText().toString().trim();

    }


    private void randerF4aLayout() {
        String[] arrayF4a = getResources().getStringArray(R.array.arrayf4);

        for (int i = 0; i < arrayF4a.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.f4a_f4b_f5_f6_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            label.setTypeface(tf);
            label.setText(arrayF4a[i]);
            label.setTypeface(tf);
            label.setTag(i + 1);
            chkBox1.setTag(v);
            chkBox2.setTag(v);
            chkBox3.setTag(v);
            chkBox4.setTag(v);
            chkBox1.setOnCheckedChangeListener(checkBox1_OnCheckedChangeListener);
            chkBox2.setOnCheckedChangeListener(checkBox2_OnCheckedChangeListener);
            chkBox3.setOnCheckedChangeListener(checkBox3_OnCheckedChangeListener);
            chkBox4.setOnCheckedChangeListener(checkBox4_OnCheckedChangeListener);
            if (arrayF4a[i].equalsIgnoreCase("Others")) {
                editText.setVisibility(View.VISIBLE);
            }
            F4a_4b_f5_f6layout.addView(v);
            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
    }

    private CompoundButton.OnCheckedChangeListener checkBox1_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            if (chkBox1.isChecked()) {
                chkBox2.setEnabled(false);

                for (int i = 0; i < F4a_4b_f5_f6layout.getChildCount(); i++) {
                    View childView = F4a_4b_f5_f6layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < F4a_4b_f5_f6layout.getChildCount(); i++) {
                    View childView = F4a_4b_f5_f6layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }
                chkBox2.setEnabled(true);

            }

        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox2_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            if (chkBox2.isChecked()) {
                chkBox1.setEnabled(false);
           //     chkBox3.setEnabled(false);
            } else {
                chkBox1.setEnabled(true);
            //    chkBox3.setEnabled(true);
            }

        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox3_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            if (chkBox3.isChecked()) {
                chkBox1.setEnabled(false);
                chkBox2.setEnabled(false);

            } else {
                chkBox1.setEnabled(true);
                chkBox2.setEnabled(true);
            }

        }
    };

    private CompoundButton.OnCheckedChangeListener checkBox4_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);

            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            if (chkBox4.isChecked()) {

                for (int i = 0; i < F4a_4b_f5_f6layout.getChildCount(); i++) {
                    View childView = F4a_4b_f5_f6layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox4);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < F4a_4b_f5_f6layout.getChildCount(); i++) {
                    View childView = F4a_4b_f5_f6layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox4);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }

            }

        }
    };
    private void add() {

        questionF4as = new ArrayList<>();
        questionF4bs = new ArrayList<>();
        questionF5s = new ArrayList<>();
        questionF6s = new ArrayList<>();
        for (int i = 0; i < F4a_4b_f5_f6layout.getChildCount(); i++) {
            View v = F4a_4b_f5_f6layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            if (chkBox1.isChecked())
            {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionF4as.add(questionB1);
            }
            if (chkBox2.isChecked())
            {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionF4bs.add(questionB1);
            }
            if (chkBox3.isChecked())
            {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionF5s.add(questionB1);
            }
            if (chkBox4.isChecked())
            {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionF6s.add(questionB1);
            }
        }

        active.setQuestionF4a(new Gson().toJson(questionF4as));
        active.setQuestionF4b(new Gson().toJson(questionF4as));
        active.setQuestionF5(new Gson().toJson(questionF5s));
        active.setQuestionF6(new Gson().toJson(questionF6s));

        active.setQuestionF7(editTextF7.getText().toString());
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
