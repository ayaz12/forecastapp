package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionE3;
import markematics.active17213.Model.QuestionS2;


public class ActivityE1E2E3 extends AppCompatActivity {

    TextView tViewE1, tViewE2, tViewE3;
    Spinner spinnere2, spinnere1;
    LinearLayout e3layout;
    Typeface tf;
    Button btnNext;
    Toolbar toolbar;
    Active active;
    List<QuestionE3> questionE3s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e1_e2_e3);

        tViewE1 = (TextView) findViewById(R.id.tViewE1);
        tViewE2 = (TextView) findViewById(R.id.tViewE2);
        tViewE3 = (TextView) findViewById(R.id.tViewE3);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question E1 E2 E3");
        toolbar.setTitleTextColor(Color.WHITE);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        spinnere1 = (Spinner) findViewById(R.id.spinnere1);
        spinnere2 = (Spinner) findViewById(R.id.spinnere2);

        e3layout = (LinearLayout) findViewById(R.id.e3_layout);

        btnNext = (Button) findViewById(R.id.btnNext);

        tViewE1.setTypeface(tf);
        tViewE2.setTypeface(tf);
        tViewE3.setTypeface(tf);
        active = (Active) getIntent().getSerializableExtra("active");

        ArrayAdapter<String> spinnersE1ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayE1));
        spinnere1.setAdapter(spinnersE1ArrayAdapter);

        ArrayAdapter<String> spinnersE2ArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, getResources().getStringArray(R.array.arrayE2));
        spinnere2.setAdapter(spinnersE2ArrayAdapter);

        randerE3Layout();

        btnNext.setOnClickListener(btnNext_OnClickListener);
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate())
            {
                Intent intent = new Intent(ActivityE1E2E3.this, ActivityE4E5.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    };

    private void randerE3Layout() {
        String[] arrayE3 = getResources().getStringArray(R.array.arrayE3);
        for (int i = 0; i < arrayE3.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            label.setTypeface(tf);
            label.setText(arrayE3[i]);
            label.setTag(i+1);
            if (label.getText().toString().equalsIgnoreCase("کوئی رشتہ دار (وضاحت )"))
            {
                editText.setVisibility(View.VISIBLE);
            }
            e3layout.addView(v);
        }
    }

    private void add() {
        active.setQuestionE1("" + spinnere1.getSelectedItemPosition());
        active.setQuestionE2("" + spinnere2.getSelectedItemPosition());

        questionE3s = new ArrayList<>();
        for (int i = 0; i < e3layout.getChildCount(); i++) {
            View v = e3layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            QuestionE3 questionE3 = new QuestionE3();
            if (chkBox.isChecked())
            {
                questionE3.setLabel(label.getText().toString());
                questionE3.setCode((int)label.getTag());
                questionE3s.add(questionE3);
            }

        }

        active.setQuestionE3(new Gson().toJson(questionE3s));
    }
    private boolean validate()
    {
        if (spinnere1.getSelectedItemPosition() == 0)
        {
            showDialog("Please Select E1");
            return false;
        }
        if (spinnere1.getSelectedItemPosition() == 0)
        {
            showDialog("Please Select E2");
            return false;
        }
        if (questionE3s.size() == 0)
        {
            showDialog("Please Select At least one option");
            return false;
        }



        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}