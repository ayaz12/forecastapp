package markematics.active17213.app.SectionS;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityIntro;
import markematics.active17213.app.SectionA.ActivityA1;
import markematics.active17213.app.SectionA.ActivityA2a;

import static java.security.AccessController.getContext;

public class ActivityS1 extends Activity {

    Spinner spinnerS1;
    TextView tViewS1;
    Typeface tf;
    Button btnNext;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s1);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S1");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewS1 = (TextView) findViewById(R.id.tViewS1);
        spinnerS1 = (Spinner) findViewById(R.id.spinS1);
        btnNext = (Button) findViewById(R.id.btnCityS1);

        btnNext.setOnClickListener(btnNext_OnClickListener);

        new AsyncHandler(REQUEST_RENDER).execute();
        spinnerS1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 3) {
                    active.setQuestionS1("" + position);
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                    btnNext.setText("Finish");
                } else {
                    active.setQuestionS1("" + position);
                    btnNext.setText("Next");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private boolean validate() {
        if (spinnerS1.getSelectedItemPosition() == 0) {
            showDialog("Please Select At least one option");
            return false;
        }


        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS1.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate()) {
                                if (btnNext.getText().toString().equalsIgnoreCase("Finish"))

                                {
                                    //save data local database
                                } else {
                                    if (spinnerS1.getSelectedItemPosition() == 2) {
                                        Intent intent = new Intent(ActivityS1.this, ActivityIntro.class);
                                        intent.putExtra("active", active);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(ActivityS1.this, ActivityInfo.class);
                                        intent.putExtra("active", active);
                                        startActivity(intent);
                                    }
                                }
                            }

                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                            active = (Active) getIntent().getSerializableExtra("active");
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewS1.setTypeface(tf);
                            ArrayAdapter<String> spinnerS1ArrayAdapter = new ArrayAdapter<String>(ActivityS1.this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayS1));
                            spinnerS1.setAdapter(spinnerS1ArrayAdapter);

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
