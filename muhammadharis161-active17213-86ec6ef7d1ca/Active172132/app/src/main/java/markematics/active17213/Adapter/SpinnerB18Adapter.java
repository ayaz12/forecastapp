package markematics.active17213.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.markematics.Active17213.R;

import java.util.List;

import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionS12;

/**
 * Created by Muhammad Haris on 10/4/2017.
 */

public class SpinnerB18Adapter extends ArrayAdapter<Brand> {
    LayoutInflater flater;

    public SpinnerB18Adapter(Activity context, int resouceId, int textviewId, List<Brand> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Brand rowItem = getItem(position);

        View rowview = flater.inflate(R.layout.spinner_item,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.txt);
        txtTitle.setText(rowItem.getName() );



        return rowview;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

            convertView = flater.inflate(R.layout.spinner_item,parent, false);

        Brand rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txt);
        txtTitle.setText(rowItem.getName());

        return convertView;
    }
}
