package markematics.active17213.DataAccess;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class HttpHandler {
	
	InputStream inputStream = null;

	String requestURL = null;
	Context context;
	HashMap<String,String> requestParameter = null;
	String attachmentName = "bitmap";
	String attachmentFileName = "bitmap.bmp";
	String crlf = "\r\n";
	String twoHyphens = "--";
	String boundary =  "*****";

	JSONArray jsonArray;
	JSONObject jsonObject;



	public HttpHandler(String url, Context context)
	{
		this.requestURL = url;
		this.context = context;
	}
	public HttpHandler(String url, HashMap<String,String> parameters, Context context)
	{
		this.requestURL = url;
		requestParameter = parameters;
		this.context = context;
	}



	public HttpHandler(String url, JSONArray parameters, Context context)
	{
		this.requestURL = url;
		jsonArray = parameters;
		this.context = context;
	}
	public HttpHandler(String url, JSONObject parameters, Context context)
	{
		this.requestURL = url;
		jsonObject = parameters;
		this.context = context;
	}
	// This method use for getting any type data from server
	public String getResponse()
	{


		HttpURLConnection urlConnection;
		StringBuilder response= new StringBuilder();
		try {
                /* forming th java.net.URL object */

			URL Url = new URL(requestURL);

		urlConnection = (HttpURLConnection) Url.openConnection();

                /* for Get request */
		urlConnection.setRequestMethod("GET");

		BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

		String line;
		while ((line = r.readLine()) != null)
			response.append(line);
	}
		catch (Exception e)
		{
			e.getMessage().toString();
		}

		return  response.toString();
	}
	public String PostServer()
	{
		URL url;
		String response = "";
		try {
			url = new URL(requestURL);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(15000);
			conn.setConnectTimeout(15000);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			conn.setDoInput(true);
			conn.setDoOutput(true);


			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(os, "UTF-8"));
			if (jsonObject.length()!=0)
			{
				writer.write(jsonObject.toString());
				writer.flush();
				writer.close();

			}
			os.close();
			int responseCode=conn.getResponseCode();

			if (responseCode == HttpsURLConnection.HTTP_OK) {
				String line;
				BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
				while ((line=br.readLine()) != null) {
					response+=line;
				}
			}
			else {
				response="";

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}
	public String PostMultipartToServer()
	{
		URL url;
		String response = "";
		try {
			url = new URL(requestURL);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(15000);
			conn.setConnectTimeout(15000);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Cache-Control", "no-cache");
			conn.setRequestProperty(
					"Content-Type", "multipart/form-data;boundary=" + this.boundary);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			DataOutputStream request = new DataOutputStream(
					conn.getOutputStream());

			request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
			request.writeBytes("Content-Disposition: form-data; name=\"" +
					this.attachmentName + "\";filename=\"" +
					this.attachmentFileName + "\"" + this.crlf);
			request.writeBytes(this.crlf);



			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(os, "UTF-8"));
			if (jsonObject.length()!=0)
			{
				writer.write(jsonObject.toString());
				writer.flush();
				writer.close();

			}
			os.close();
			int responseCode=conn.getResponseCode();

			if (responseCode == HttpsURLConnection.HTTP_OK) {
				String line;
				BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
				while ((line=br.readLine()) != null) {
					response+=line;
				}
			}
			else {
				response="";

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}
    public String PutServer()
    {
        HttpURLConnection urlConnection;
        StringBuilder response= new StringBuilder();
        try {
          /* forming th java.net.URL object */
            URL url = new URL(requestURL);
            urlConnection = (HttpURLConnection) url.openConnection();

                /* for Get request */
            urlConnection.setRequestMethod("PUT");
            BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;
            while ((line = r.readLine()) != null)
                response.append(line);
        }
        catch (Exception e)
        {
            e.getMessage().toString();
        }

        return  response.toString();
    }

	public String deleteResponse()
	{


		HttpURLConnection urlConnection;
		StringBuilder response= new StringBuilder();
		try {
                /* forming th java.net.URL object */
			URL url = new URL(requestURL);
			urlConnection = (HttpURLConnection) url.openConnection();

                /* for Get request */
			urlConnection.setRequestMethod("DELETE");
			BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			String line;
			while ((line = r.readLine()) != null)
				response.append(line);
		}
		catch (Exception e)
		{
			e.getMessage().toString();
		}

		return  response.toString();
	}

	private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for(Map.Entry<String, String> entry : params.entrySet()){
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
		}

		return result.toString();
	}

	public String isInternetAvailable(Context context)
	{
		String error = "";
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean isInternetAvailable = false;
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						isInternetAvailable = true;
						break;

					}
		}

		
		
		if (isInternetAvailable)
		{
		//	errorCode.setHasError(false);
		}
			else
			{
				//errorCode.setHasError(true);
			//	errorCode.setErrorMessage(context.getString(R.string.msg_internet));
			}
			
	//	error = errorCode.getErrorMessage();
		return error;
	}

}
