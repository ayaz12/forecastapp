package markematics.active17213.Model;

/**
 * Created by mas on 10/9/2017.
 */

public class QuestionC1 {

    private int code;
    private String statement;
    private Brand[] brands;

    public Brand[] getBrands() {
        return brands;
    }

    public void setBrands(Brand[] brands) {
        this.brands = brands;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }


}
