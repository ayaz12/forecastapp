package markematics.active17213.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.markematics.Active17213.R;

import java.util.List;

import markematics.active17213.Model.QuestionS12;

/**
 * Created by Muhammad Haris on 10/4/2017.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<QuestionS12> {
    Typeface tf;
    LayoutInflater flater;


    public CustomSpinnerAdapter(Activity context, int resouceId, int textviewId, List<QuestionS12> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    //    tf = Typeface.createFromAsset(context.getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        QuestionS12 rowItem = getItem(position);

        View rowview = flater.inflate(R.layout.spinner_item,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.txt);
   //     txtTitle.setTypeface(tf);
        txtTitle.setText(rowItem.getStatement() + (rowItem.getOpenEnded().isEmpty()?"":"("+rowItem.getOpenEnded()+")"));



        return rowview;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

            convertView = flater.inflate(R.layout.spinner_item,parent, false);

        QuestionS12 rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txt);
  //      txtTitle.setTypeface(tf);
        txtTitle.setText(rowItem.getStatement() + (rowItem.getOpenEnded().isEmpty()?"":"("+rowItem.getOpenEnded()+")"));

        return convertView;
    }
}
