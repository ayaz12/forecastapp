package markematics.active17213.app.SectionS;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityIntro;

public class ActivityS8 extends AppCompatActivity{

    Spinner spinnerS8;
    TextView textViewS8;
    Typeface tf;
    Button btnNext;
    Toolbar toolbar;
    Active active;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s8);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S8");
        toolbar.setTitleTextColor(Color.WHITE);
        textViewS8= (TextView) findViewById(R.id.tViewS8);
        spinnerS8 = (Spinner) findViewById(R.id.spinner_s8);



        btnNext =(Button) findViewById(R.id.btnS8);

        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

        spinnerS8.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position <= 3) {
                    btnNext.setText("Finish");
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                }
                else
                {
                    btnNext.setText("Next");
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

           new AsyncHandler(REQUEST_ADD).execute();
        }
    };


    private void add()
    {
        active.setQuestionS8(""+spinnerS8.getSelectedItemPosition());
    }
    private boolean validationSuccess() {

        if (spinnerS8.getSelectedItemPosition() == 0)
        {

            showDialog("Please Select At least one option ");
            return  false;
        }
        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS8.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                            if (validationSuccess()) {
                                add();
                                if (btnNext.getText().toString().equalsIgnoreCase("Finish")) {
                                    Utility.showExitDialog(ActivityS8.this, "کیا آپ انٹرویو ختم کرنا چاہتی ہیں", active);

                                } else {
                                    Intent nextPage = new Intent(ActivityS8.this, ActivityS9.class);
                                    nextPage.putExtra("active", active);
                                    startActivity(nextPage);

                                }
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            btnNext.setTextColor(Color.parseColor("#FFFFFF"));

                            active = (Active) getIntent().getSerializableExtra("active");
                            ArrayAdapter<String> spinnerS8ArrayAdapter = new ArrayAdapter<String>(ActivityS8.this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayS8));
                            spinnerS8.setAdapter(spinnerS8ArrayAdapter);
                            tf = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            textViewS8.setTypeface(tf);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
