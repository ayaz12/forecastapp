package markematics.active17213.app.SectionA;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.CustomSpinnerAdapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionS12;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA6A7A8A9 extends AppCompatActivity {
    TextView tViewA6, tViewA7, tViewA8, tViewA9;
    EditText editTextA7, editTextA8, editTextA9;
    Button btnA6;
    Typeface tf;
    Spinner spinnerA6;
    Active active;
    Toolbar toolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a6_a7_a8_a9);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A6 A7 A8 A9");
        toolbar.setTitleTextColor(Color.WHITE);
        active = (Active) getIntent().getSerializableExtra("active");
        tViewA6 = (TextView) findViewById(R.id.tViewA6);
        tViewA7 = (TextView) findViewById(R.id.tviewA7);
        tViewA8 = (TextView) findViewById(R.id.tviewA8);
        tViewA9 = (TextView) findViewById(R.id.tviewA9);
        editTextA7 = (EditText) findViewById(R.id.editTextA7);
        editTextA8 = (EditText) findViewById(R.id.editTextA8);
        editTextA9 = (EditText) findViewById(R.id.editTextA9);
        spinnerA6=(Spinner)findViewById(R.id.spinnerA6);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewA6.setTypeface(tf);
        tViewA7.setTypeface(tf);
        tViewA8.setTypeface(tf);
        tViewA9.setTypeface(tf);
        editTextA7.setTypeface(tf);
        editTextA8.setTypeface(tf);
        editTextA9.setTypeface(tf);
        btnA6=(Button)findViewById(R.id.btnA6);
        btnA6.setOnClickListener(btnNext_OnClickListener);
        Type listType = new TypeToken<ArrayList<QuestionS12>>() {
        }.getType();
        List<QuestionS12> questionS12s = new Gson().fromJson(active.getQuestionS12(), listType);
       CustomSpinnerAdapter spinnerA6ArrayAdapter = new CustomSpinnerAdapter(ActivityA6A7A8A9.this,R.layout.spinner_item,R.id.txt, questionS12s);
        spinnerA6.setAdapter(spinnerA6ArrayAdapter);

        spinnerA6.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                String val = (String)adapterView.getSelectedItem();
                if (val.equalsIgnoreCase("کپڑا")||val.equalsIgnoreCase("تولیہ")||val.equalsIgnoreCase("پلاسٹک پینٹیز")||val.equalsIgnoreCase("واٹر پروف میٹ"))
                {
                    editTextA9.setVisibility(View.GONE);
                    tViewA9.setVisibility(View.GONE);
                    editTextA8.setVisibility(View.VISIBLE);
                    tViewA8.setVisibility(View.VISIBLE);
                    tViewA7.setText((String) adapterView.getSelectedItem() + " کو زیادہ تر استعمال کروانے کی کیا وجوہات ہیں ؟ ");
                    editTextA7.setVisibility(View.VISIBLE);
                    tViewA7.setVisibility(View.VISIBLE);
                }
                else if (val.equalsIgnoreCase("ڈائپرز"))
                {
                    editTextA9.setVisibility(View.VISIBLE);
                    tViewA9.setVisibility(View.VISIBLE);
                    editTextA8.setVisibility(View.GONE);
                    tViewA8.setVisibility(View.GONE);
                    editTextA7.setVisibility(View.GONE);
                    tViewA7.setVisibility(View.GONE);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validationSuccess())
            {
                add();
                Intent intent = new Intent(ActivityA6A7A8A9.this, ActivityA10.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    };


    private void add()
    {
        active.setQuestionA6(""+spinnerA6.getSelectedItemPosition());
        active.setQuestionA7(editTextA7.getText().toString());
        active.setQuestionA8(editTextA8.getText().toString());
        active.setQuestionA9(editTextA9.getText().toString());
    }
    private boolean validationSuccess() {


        if (spinnerA6.getSelectedItemPosition() == 0)
        {
            showDialog("Please Select at least one option");
            return false;
        }
        if (editTextA7.getVisibility() == View.VISIBLE) {
            if (editTextA7.getText().toString().isEmpty()) {
                editTextA7.setError("درج کریں ");
                return false;

            }
        }
        if (editTextA8.getVisibility() == View.VISIBLE) {
            if (editTextA8.getText().toString().isEmpty()) {
                editTextA8.setError("درج کریں ");
                return false;

            }
        }
        if (editTextA9.getVisibility() == View.VISIBLE) {
            if (editTextA9.getText().toString().isEmpty()) {
                editTextA9.setError("درج کریں ");
                return false;

            }
        }
        return true;
    }



    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
