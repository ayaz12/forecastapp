package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.lassana.recorder.AudioRecorder;
import com.markematics.Active17213.R;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionB.ActivityB1;
import markematics.active17213.app.SectionB.ActivityB1B2;
import markematics.active17213.app.SectionB.ActivityStart;
import markematics.active17213.app.SectionS.ActivityS14S15;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA12A13 extends AppCompatActivity {
    TextView tViewA12, tViewA13;
    Typeface tf;
    Button btnA11;
    Spinner spinnerA12;
    EditText editTextA13;
    String A13;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a12_a13);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A12 A13");
        toolbar.setTitleTextColor(Color.WHITE);


        tViewA12 = (TextView) findViewById(R.id.tViewA12);
        tViewA13 = (TextView) findViewById(R.id.tViewA13);
        btnA11 = (Button) findViewById(R.id.btnNext);
         btnA11.setOnClickListener(btnNext_OnClickListener);
        editTextA13 = (EditText) findViewById(R.id.editTextA13);
        spinnerA12 = (Spinner) findViewById(R.id.spinnerA12);

        new AsyncHandler(REQUEST_RENDER).execute();

        spinnerA12.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position == 1) {
                    editTextA13.setVisibility(View.VISIBLE);
                    tViewA13.setVisibility(View.VISIBLE);
                } else {
                    editTextA13.setVisibility(View.GONE);
                    tViewA13.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

           new AsyncHandler(REQUEST_ADD).execute();
        }
    };
    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityA12A13.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityA12A13.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    Intent intent = new Intent(ActivityA12A13.this, ActivityStart.class);
                    intent.putExtra("active", active);

                    startActivity(intent);
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private void add() {
        active.setQuestionA13(editTextA13.getText().toString());
        active.setQuestionA12("" + spinnerA12.getSelectedItemPosition());
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private boolean validationSuccess() {


        if (spinnerA12.getSelectedItemPosition() == 0) {
            showDialog("Please select at least one option");
            return false;
        }
        if (editTextA13.getVisibility() == View.VISIBLE) {
            if (editTextA13.getText().toString().isEmpty()) {
                editTextA13.setError("درج کریں برانڈ");
                return false;

            }
        }

        return true;
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA12A13.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validationSuccess()) {

                                add();
                                StopRecording();

                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

                            active = (Active) getIntent().getSerializableExtra("active");
                            tViewA12.setTypeface(tf);
                            tViewA13.setTypeface(tf);

                            ArrayAdapter<String> spinnerA12ArrayAdapter = new ArrayAdapter<String>(ActivityA12A13.this, R.layout.spinner_item, getResources().getStringArray(R.array.spinnerarrayA12));
                            spinnerA12.setAdapter(spinnerA12ArrayAdapter);

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}