package markematics.active17213.Model;

import android.database.Cursor;

import java.io.Serializable;

public class Active implements Serializable {



    private int Id;
    private String city;
    private String Area;
    private String userName;
    private String startTime;
    private String reposndentName;
    private String EarnerName;
    private String phoneNo;
    private String mobileNo;
    private String houseNo;
    private String StreetNo;
    private String sector;
    private String block;
    private String AreaName;
    private String landmark;
    private String questionS1;
    private String questionS2;
    private String questionS3;
    private String companyName;
    private String designation;
    private String natureOfWork;
    private String sec;
    private String questionS4;
    private String questionS5;
    private String questionS6;
    private String questionS7;
    private String questionS8;
    private String questionS9;
    private String childName;
    private String questionS9b;
    private String questionS10;
    private String questionS11;
    private String questionS12;
    private String questionS13;
    private String questionS14;
    private String questionS15;
    private String questionA1;
    private String questionA2a;
    private String questionA2b;
    private String questionA3a;
    private String questionA3b;
    private String questionA4;
    private String questionA5a;
    private String questionA5b;
    private String questionA6;
    private String questionA7;
    private String questionA8;
    private String questionA9;
    private String questionA10;
    private int tape;
    private int pant;
    private String questionA11a;
    private String questionA11b;
    private String questionA12;
    private String questionA13;
    private String questionB1;
    private String questionB2;
    private String questionB3;
    private String questionB4;
    private String questionB5;
    private String questionB6;
    private String questionB7;
    private String questionB8a;
    private String questionB8b;
    private String questionB9;
    private String questionB10;
    private String questionB11;
    private String questionB12;
    private String questionB13;
    private String questionB14;
    private String questionB15;
    private String questionB16;
    private String questionB16Other;
    private String questionB17;
    private String questionB18a;
    private String questionB18;
    private String questionB19;
    private String questionB20;
    private String questionC1;
    private String questionD1;
    private String questionD2;
    private String questionD3;
    private String questionD4;
    private String questionD5;
    private String questionD6;
    private String questionE1;
    private String questionE2;
    private String questionE3;
    private String questionE4a;
    private String questionE4b;
    private String questionE4bOther;
    private String questionE5a;
    private String questionE5b;
    private String questionE5c;
    private String questionE5d;
    private String questionE6;
    private String questionE7;
    private String questionE8;
    private String questionE8Brand;
    private String questionE8Other;
    private String questionF1;
    private String questionF2;
    private String questionF3;
    private String questionF4a;
    private String questionF4b;
    private String questionF5;
    private String questionF6;
    private String questionF7;
    private String questionR1;
    private String questionR2;
    private String questionG1;
    private String questionH1;
    private String questionH2;
    private String questionH3a;
    private String questionH3b;
    private String questionH4;
    private String questionH5;
    private String questionH6a;
    private String questionH6b;
    private String questionH7;
    private String questionH8;
    private String questionH9;
    private String questionH10;
    private String questionH11;
    private String questionH12a;
    private String questionH12b;
    private String questionH13;
    private String questionH14;
    private String questionH15;
    private String questionH16;
    private String questionH17a;
    private String questionH17b;
    private String questionN1;
    private String questionN2;
    private String questionN3;
    private String questionN4;
    private String questionN5;
    private String hR_ID;
    private String hR_Password;
    private String activeJSON;
    private double lat;
    private double lng;
    private String dateTime;
    private String audioPathSecS;
    private String audioPathSecA;
    private String audioPathSecB;
    private String audioPathSecC;
    private String audioPathSecD;
    private String audioPathSecE;
    private String audioPathSecF;
    private String audioPathSecG;
    private String audioPathSecH;
    private String audioPathSecR;
    public String getAudioPathSecS() {
        return audioPathSecS;
    }

    public void setAudioPathSecS(String audioPathSecS) {
        this.audioPathSecS = audioPathSecS;
    }

    public String getActiveJSON() {
        return activeJSON;
    }

    public void setActiveJSON(String activeJSON) {
        this.activeJSON = activeJSON;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getQuestionS9() {
        return questionS9;
    }

    public void setQuestionS9(String questionS9) {
        this.questionS9 = questionS9;
    }

    public String gethR_ID() {
        return hR_ID;
    }

    public void sethR_ID(String hR_ID) {
        this.hR_ID = hR_ID;
    }

    public String gethR_Password() {
        return hR_Password;
    }

    public void sethR_Password(String hR_Password) {
        this.hR_Password = hR_Password;
    }

    public String getSector() {
        return sector;
    }

    public String getQuestionB18a() {
        return questionB18a;
    }

    public void setQuestionB18a(String questionB18a) {
        this.questionB18a = questionB18a;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getReposndentName() {
        return reposndentName;
    }

    public void setReposndentName(String reposndentName) {
        this.reposndentName = reposndentName;
    }

    public String getEarnerName() {
        return EarnerName;
    }

    public void setEarnerName(String earnerName) {
        EarnerName = earnerName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreetNo() {
        return StreetNo;
    }

    public void setStreetNo(String streetNo) {
        StreetNo = streetNo;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String areaName) {
        AreaName = areaName;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getQuestionS1() {
        return questionS1;
    }

    public void setQuestionS1(String questionS1) {
        this.questionS1 = questionS1;
    }

    public String getQuestionS2() {
        return questionS2;
    }

    public void setQuestionS2(String questionS2) {
        this.questionS2 = questionS2;
    }

    public String getQuestionS3() {
        return questionS3;
    }

    public void setQuestionS3(String questionS3) {
        this.questionS3 = questionS3;
    }

    public String getQuestionS4() {
        return questionS4;
    }

    public void setQuestionS4(String questionS4) {
        this.questionS4 = questionS4;
    }

    public String getQuestionS5() {
        return questionS5;
    }

    public void setQuestionS5(String questionS5) {
        this.questionS5 = questionS5;
    }

    public String getQuestionS6() {
        return questionS6;
    }

    public String getQuestionB16Other() {
        return questionB16Other;
    }

    public void setQuestionB16Other(String questionB16Other) {
        this.questionB16Other = questionB16Other;
    }

    public void setQuestionS6(String questionS6) {
        this.questionS6 = questionS6;
    }

    public String getQuestionS7() {
        return questionS7;
    }

    public void setQuestionS7(String questionS7) {
        this.questionS7 = questionS7;
    }

    public String getQuestionS8() {
        return questionS8;
    }

    public void setQuestionS8(String questionS8) {
        this.questionS8 = questionS8;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getQuestionS9b() {
        return questionS9b;
    }

    public void setQuestionS9b(String questionS9b) {
        this.questionS9b = questionS9b;
    }

    public String getQuestionS10() {
        return questionS10;
    }

    public void setQuestionS10(String questionS10) {
        this.questionS10 = questionS10;
    }

    public String getQuestionS11() {
        return questionS11;
    }

    public void setQuestionS11(String questionS11) {
        this.questionS11 = questionS11;
    }

    public String getQuestionS12() {
        return questionS12;
    }

    public void setQuestionS12(String questionS12) {
        this.questionS12 = questionS12;
    }

    public String getQuestionS13() {
        return questionS13;
    }

    public void setQuestionS13(String questionS13) {
        this.questionS13 = questionS13;
    }

    public String getQuestionS14() {
        return questionS14;
    }

    public void setQuestionS14(String questionS14) {
        this.questionS14 = questionS14;
    }

    public String getQuestionS15() {
        return questionS15;
    }

    public void setQuestionS15(String questionS15) {
        this.questionS15 = questionS15;
    }

    public String getQuestionA1() {
        return questionA1;
    }

    public void setQuestionA1(String questionA1) {
        this.questionA1 = questionA1;
    }

    public String getQuestionA2a() {
        return questionA2a;
    }

    public void setQuestionA2a(String questionA2a) {
        this.questionA2a = questionA2a;
    }

    public String getQuestionA2b() {
        return questionA2b;
    }

    public void setQuestionA2b(String questionA2b) {
        this.questionA2b = questionA2b;
    }

    public String getQuestionA3a() {
        return questionA3a;
    }

    public void setQuestionA3a(String questionA3a) {
        this.questionA3a = questionA3a;
    }

    public String getQuestionA3b() {
        return questionA3b;
    }

    public void setQuestionA3b(String questionA3b) {
        this.questionA3b = questionA3b;
    }

    public String getQuestionA4() {
        return questionA4;
    }

    public void setQuestionA4(String questionA4) {
        this.questionA4 = questionA4;
    }

    public String getQuestionA5a() {
        return questionA5a;
    }

    public void setQuestionA5a(String questionA5a) {
        this.questionA5a = questionA5a;
    }

    public String getQuestionA5b() {
        return questionA5b;
    }

    public void setQuestionA5b(String questionA5b) {
        this.questionA5b = questionA5b;
    }

    public String getQuestionA6() {
        return questionA6;
    }

    public void setQuestionA6(String questionA6) {
        this.questionA6 = questionA6;
    }

    public String getQuestionA7() {
        return questionA7;
    }

    public void setQuestionA7(String questionA7) {
        this.questionA7 = questionA7;
    }

    public String getQuestionA8() {
        return questionA8;
    }

    public void setQuestionA8(String questionA8) {
        this.questionA8 = questionA8;
    }

    public String getQuestionA9() {
        return questionA9;
    }

    public void setQuestionA9(String questionA9) {
        this.questionA9 = questionA9;
    }

    public String getQuestionA10() {
        return questionA10;
    }

    public void setQuestionA10(String questionA10) {
        this.questionA10 = questionA10;
    }

    public String getQuestionA11a() {
        return questionA11a;
    }

    public void setQuestionA11a(String questionA11a) {
        this.questionA11a = questionA11a;
    }

    public String getQuestionA11b() {
        return questionA11b;
    }

    public void setQuestionA11b(String questionA11b) {
        this.questionA11b = questionA11b;
    }

    public String getQuestionA12() {
        return questionA12;
    }

    public void setQuestionA12(String questionA12) {
        this.questionA12 = questionA12;
    }

    public String getQuestionA13() {
        return questionA13;
    }

    public void setQuestionA13(String questionA13) {
        this.questionA13 = questionA13;
    }

    public String getQuestionB1() {
        return questionB1;
    }

    public void setQuestionB1(String questionB1) {
        this.questionB1 = questionB1;
    }

    public String getQuestionB2() {
        return questionB2;
    }

    public void setQuestionB2(String questionB2) {
        this.questionB2 = questionB2;
    }

    public String getQuestionB3() {
        return questionB3;
    }

    public void setQuestionB3(String questionB3) {
        this.questionB3 = questionB3;
    }

    public String getQuestionB4() {
        return questionB4;
    }

    public void setQuestionB4(String questionB4) {
        this.questionB4 = questionB4;
    }

    public String getQuestionB5() {
        return questionB5;
    }

    public void setQuestionB5(String questionB5) {
        this.questionB5 = questionB5;
    }

    public String getQuestionB6() {
        return questionB6;
    }

    public void setQuestionB6(String questionB6) {
        this.questionB6 = questionB6;
    }

    public String getQuestionB7() {
        return questionB7;
    }

    public void setQuestionB7(String questionB7) {
        this.questionB7 = questionB7;
    }

    public String getQuestionB8a() {
        return questionB8a;
    }

    public void setQuestionB8a(String questionB8a) {
        this.questionB8a = questionB8a;
    }

    public String getQuestionB8b() {
        return questionB8b;
    }

    public void setQuestionB8b(String questionB8b) {
        this.questionB8b = questionB8b;
    }

    public String getQuestionB9() {
        return questionB9;
    }

    public void setQuestionB9(String questionB9) {
        this.questionB9 = questionB9;
    }

    public String getQuestionB10() {
        return questionB10;
    }

    public void setQuestionB10(String questionB10) {
        this.questionB10 = questionB10;
    }

    public String getQuestionB11() {
        return questionB11;
    }

    public void setQuestionB11(String questionB11) {
        this.questionB11 = questionB11;
    }

    public String getQuestionB12() {
        return questionB12;
    }

    public void setQuestionB12(String questionB12) {
        this.questionB12 = questionB12;
    }

    public String getQuestionB13() {
        return questionB13;
    }

    public void setQuestionB13(String questionB13) {
        this.questionB13 = questionB13;
    }

    public String getQuestionB14() {
        return questionB14;
    }

    public void setQuestionB14(String questionB14) {
        this.questionB14 = questionB14;
    }

    public String getQuestionB15() {
        return questionB15;
    }

    public void setQuestionB15(String questionB15) {
        this.questionB15 = questionB15;
    }

    public String getQuestionB16() {
        return questionB16;
    }

    public void setQuestionB16(String questionB16) {
        this.questionB16 = questionB16;
    }

    public String getQuestionB17() {
        return questionB17;
    }

    public void setQuestionB17(String questionB17) {
        this.questionB17 = questionB17;
    }

    public String getQuestionB18() {
        return questionB18;
    }

    public void setQuestionB18(String questionB18) {
        this.questionB18 = questionB18;
    }

    public String getQuestionB19() {
        return questionB19;
    }

    public void setQuestionB19(String questionB19) {
        this.questionB19 = questionB19;
    }

    public String getQuestionB20() {
        return questionB20;
    }

    public void setQuestionB20(String questionB20) {
        this.questionB20 = questionB20;
    }

    public String getQuestionC1() {
        return questionC1;
    }

    public void setQuestionC1(String questionC1) {
        this.questionC1 = questionC1;
    }

    public String getQuestionD1() {
        return questionD1;
    }

    public void setQuestionD1(String questionD1) {
        this.questionD1 = questionD1;
    }

    public String getQuestionD2() {
        return questionD2;
    }

    public void setQuestionD2(String questionD2) {
        this.questionD2 = questionD2;
    }

    public String getQuestionD3() {
        return questionD3;
    }

    public void setQuestionD3(String questionD3) {
        this.questionD3 = questionD3;
    }

    public String getQuestionD4() {
        return questionD4;
    }

    public void setQuestionD4(String questionD4) {
        this.questionD4 = questionD4;
    }

    public String getQuestionD5() {
        return questionD5;
    }

    public void setQuestionD5(String questionD5) {
        this.questionD5 = questionD5;
    }

    public String getQuestionD6() {
        return questionD6;
    }

    public void setQuestionD6(String questionD6) {
        this.questionD6 = questionD6;
    }

    public String getQuestionE1() {
        return questionE1;
    }

    public void setQuestionE1(String questionE1) {
        this.questionE1 = questionE1;
    }

    public String getQuestionE2() {
        return questionE2;
    }

    public void setQuestionE2(String questionE2) {
        this.questionE2 = questionE2;
    }

    public String getQuestionE3() {
        return questionE3;
    }

    public void setQuestionE3(String questionE3) {
        this.questionE3 = questionE3;
    }

    public String getQuestionE4a() {
        return questionE4a;
    }

    public void setQuestionE4a(String questionE4a) {
        this.questionE4a = questionE4a;
    }

    public String getQuestionE4b() {
        return questionE4b;
    }

    public String getQuestionE4bOther() {
        return questionE4bOther;
    }

    public void setQuestionE4bOther(String questionE4bOther) {
        this.questionE4bOther = questionE4bOther;
    }

    public void setQuestionE4b(String questionE4b) {
        this.questionE4b = questionE4b;
    }

    public String getQuestionE5a() {
        return questionE5a;
    }

    public void setQuestionE5a(String questionE5a) {
        this.questionE5a = questionE5a;
    }

    public String getQuestionE5b() {
        return questionE5b;
    }

    public void setQuestionE5b(String questionE5b) {
        this.questionE5b = questionE5b;
    }

    public String getQuestionE5c() {
        return questionE5c;
    }

    public void setQuestionE5c(String questionE5c) {
        this.questionE5c = questionE5c;
    }

    public String getQuestionE5d() {
        return questionE5d;
    }

    public void setQuestionE5d(String questionE5d) {
        this.questionE5d = questionE5d;
    }

    public String getQuestionE6() {
        return questionE6;
    }

    public void setQuestionE6(String questionE6) {
        this.questionE6 = questionE6;
    }

    public String getQuestionE7() {
        return questionE7;
    }

    public void setQuestionE7(String questionE7) {
        this.questionE7 = questionE7;
    }

    public String getQuestionE8() {
        return questionE8;
    }

    public void setQuestionE8(String questionE8) {
        this.questionE8 = questionE8;
    }

    public String getQuestionE8Brand() {
        return questionE8Brand;
    }

    public void setQuestionE8Brand(String questionE8Brand) {
        this.questionE8Brand = questionE8Brand;
    }

    public String getQuestionE8Other() {
        return questionE8Other;
    }

    public void setQuestionE8Other(String questionE8Other) {
        this.questionE8Other = questionE8Other;
    }

    public String getQuestionF1() {
        return questionF1;
    }

    public void setQuestionF1(String questionF1) {
        this.questionF1 = questionF1;
    }

    public String getQuestionF2() {
        return questionF2;
    }

    public void setQuestionF2(String questionF2) {
        this.questionF2 = questionF2;
    }

    public String getQuestionF3() {
        return questionF3;
    }

    public void setQuestionF3(String questionF3) {
        this.questionF3 = questionF3;
    }

    public String getQuestionF4a() {
        return questionF4a;
    }

    public void setQuestionF4a(String questionF4a) {
        this.questionF4a = questionF4a;
    }

    public String getQuestionF4b() { return questionF4b; }

    public void setQuestionF4b(String questionF4b) { this.questionF4b = questionF4b; }

    public String getQuestionF5() {
        return questionF5;
    }

    public void setQuestionF5(String questionF5) {
        this.questionF5 = questionF5;
    }

    public String getQuestionF6() {
        return questionF6;
    }

    public void setQuestionF6(String questionF6) {
        this.questionF6 = questionF6;
    }

    public String getQuestionF7() {
        return questionF7;
    }

    public void setQuestionF7(String questionF7) {
        this.questionF7 = questionF7;
    }

    public String getQuestionR1() {
        return questionR1;
    }

    public void setQuestionR1(String questionR1) {
        this.questionR1 = questionR1;
    }

    public String getQuestionR2() {
        return questionR2;
    }

    public void setQuestionR2(String questionR2) {
        this.questionR2 = questionR2;
    }

    public String getQuestionG1() {
        return questionG1;
    }

    public void setQuestionG1(String questionG1) {
        this.questionG1 = questionG1;
    }

    public String getQuestionH1() {
        return questionH1;
    }

    public void setQuestionH1(String questionH1) {
        this.questionH1 = questionH1;
    }

    public String getQuestionH2() {
        return questionH2;
    }

    public void setQuestionH2(String questionH2) {
        this.questionH2 = questionH2;
    }

    public String getQuestionH3a() {
        return questionH3a;
    }

    public void setQuestionH3a(String questionH3a) {
        this.questionH3a = questionH3a;
    }

    public String getQuestionH3b() {
        return questionH3b;
    }

    public void setQuestionH3b(String questionH3b) {
        this.questionH3b = questionH3b;
    }

    public String getQuestionH4() {
        return questionH4;
    }

    public void setQuestionH4(String questionH4) {
        this.questionH4 = questionH4;
    }

    public String getQuestionH5() {
        return questionH5;
    }

    public void setQuestionH5(String questionH5) {
        this.questionH5 = questionH5;
    }

    public String getQuestionH6a() {
        return questionH6a;
    }

    public void setQuestionH6a(String questionH6a) {
        this.questionH6a = questionH6a;
    }

    public String getQuestionH6b() {
        return questionH6b;
    }

    public void setQuestionH6b(String questionH6b) {
        this.questionH6b = questionH6b;
    }

    public String getQuestionH7() {
        return questionH7;
    }

    public void setQuestionH7(String questionH7) {
        this.questionH7 = questionH7;
    }

    public String getQuestionH8() {
        return questionH8;
    }

    public void setQuestionH8(String questionH8) {
        this.questionH8 = questionH8;
    }

    public String getQuestionH9() {
        return questionH9;
    }

    public void setQuestionH9(String questionH9) {
        this.questionH9 = questionH9;
    }

    public String getQuestionH10() {
        return questionH10;
    }

    public void setQuestionH10(String questionH10) {
        this.questionH10 = questionH10;
    }

    public String getQuestionH11() {
        return questionH11;
    }

    public void setQuestionH11(String questionH11) {
        this.questionH11 = questionH11;
    }

    public String getQuestionH12a() {
        return questionH12a;
    }

    public void setQuestionH12a(String questionH12a) {
        this.questionH12a = questionH12a;
    }

    public String getQuestionH12b() {
        return questionH12b;
    }

    public void setQuestionH12b(String questionH12b) {
        this.questionH12b = questionH12b;
    }

    public String getQuestionH13() {
        return questionH13;
    }

    public void setQuestionH13(String questionH13) {
        this.questionH13 = questionH13;
    }

    public String getQuestionH14() {
        return questionH14;
    }

    public void setQuestionH14(String questionH14) {
        this.questionH14 = questionH14;
    }

    public String getQuestionH15() {
        return questionH15;
    }

    public void setQuestionH15(String questionH15) {
        this.questionH15 = questionH15;
    }

    public String getQuestionH16() {
        return questionH16;
    }

    public void setQuestionH16(String questionH16) {
        this.questionH16 = questionH16;
    }

    public String getQuestionH17a() {
        return questionH17a;
    }

    public void setQuestionH17a(String questionH17a) {
        this.questionH17a = questionH17a;
    }

    public String getQuestionH17b() {
        return questionH17b;
    }

    public void setQuestionH17b(String questionH17b) {
        this.questionH17b = questionH17b;
    }

    public String getQuestionN1() {
        return questionN1;
    }

    public void setQuestionN1(String questionN1) {
        this.questionN1 = questionN1;
    }

    public String getQuestionN2() {
        return questionN2;
    }

    public void setQuestionN2(String questionN2) {
        this.questionN2 = questionN2;
    }

    public String getQuestionN3() {
        return questionN3;
    }

    public void setQuestionN3(String questionN3) {
        this.questionN3 = questionN3;
    }

    public String getQuestionN4() {
        return questionN4;
    }

    public void setQuestionN4(String questionN4) {
        this.questionN4 = questionN4;
    }

    public String getQuestionN5() {
        return questionN5;
    }

    public void setQuestionN5(String questionN5) {
        this.questionN5 = questionN5;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getNatureOfWork() {
        return natureOfWork;
    }

    public void setNatureOfWork(String natureOfWork) {
        this.natureOfWork = natureOfWork;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public int getTape() {
        return tape;
    }

    public void setTape(int tape) {
        this.tape = tape;
    }

    public int getPant() {
        return pant;
    }

    public void setPant(int pant) {
        this.pant = pant;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAudioPathSecA() {
        return audioPathSecA;
    }

    public void setAudioPathSecA(String audioPathSecA) {
        this.audioPathSecA = audioPathSecA;
    }

    public String getAudioPathSecB() {
        return audioPathSecB;
    }

    public void setAudioPathSecB(String audioPathSecB) {
        this.audioPathSecB = audioPathSecB;
    }

    public String getAudioPathSecC() {
        return audioPathSecC;
    }

    public void setAudioPathSecC(String audioPathSecC) {
        this.audioPathSecC = audioPathSecC;
    }

    public String getAudioPathSecD() {
        return audioPathSecD;
    }

    public void setAudioPathSecD(String audioPathSecD) {
        this.audioPathSecD = audioPathSecD;
    }

    public String getAudioPathSecE() {
        return audioPathSecE;
    }

    public void setAudioPathSecE(String audioPathSecE) {
        this.audioPathSecE = audioPathSecE;
    }

    public String getAudioPathSecF() {
        return audioPathSecF;
    }

    public void setAudioPathSecF(String audioPathSecF) {
        this.audioPathSecF = audioPathSecF;
    }

    public String getAudioPathSecG() {
        return audioPathSecG;
    }

    public void setAudioPathSecG(String audioPathSecG) {
        this.audioPathSecG = audioPathSecG;
    }

    public String getAudioPathSecH() {
        return audioPathSecH;
    }

    public void setAudioPathSecH(String audioPathSecH) {
        this.audioPathSecH = audioPathSecH;
    }

    public String getAudioPathSecR() {
        return audioPathSecR;
    }

    public void setAudioPathSecR(String audioPathSecR) {
        this.audioPathSecR = audioPathSecR;
    }

    public static String TABLE_NAME = "tActive17213";
    public static String FIELD_ID = "Id";
    public static String FIELD_HR_ID = "hrId";
    public static String FIELD_USER_NAME = "userName";
    public static String FIELD_CITY = "City";
    public static String FIELD_AREA = "Area";
    public static String FIELD_ACTIVE = "activeJSON";
    public static String FIELD_START_TIME = "startTime";
    public static String FIELD_DATETIME = "dateTime";


    public static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + FIELD_HR_ID + " TEXT,"
            + FIELD_USER_NAME + " TEXT,"
            + FIELD_CITY + " TEXT,"
            + FIELD_AREA + " TEXT,"
            + FIELD_ACTIVE + " TEXT,"
            + FIELD_START_TIME + " TEXT,"
            + FIELD_DATETIME + " TEXT"
            + ")";

    public static String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;


    public static String SELECT_QUERY = "SELECT * FROM " + TABLE_NAME;








    public static Active ConvertToEntity(Cursor cursor)
    {
        Active active = new Active();
        active.setId(cursor.getInt(0));
        active.sethR_ID(cursor.getString(1));
        active.setUserName(cursor.getString(2));
        active.setCity(cursor.getString(3));
        active.setArea(cursor.getString(4));
        active.setActiveJSON(cursor.getString(5));
        active.setStartTime(cursor.getString(6));
        active.setDateTime(cursor.getString(7));
        return  active;

    }




}