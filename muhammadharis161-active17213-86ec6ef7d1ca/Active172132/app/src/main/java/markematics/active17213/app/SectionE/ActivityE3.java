package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionE3;

/**
 * Created by ZEB Rana on 16-Oct-17.
 */

public class ActivityE3 extends AppCompatActivity {

    TextView tViewE3;
    LinearLayout e3layout;
    Typeface tf;
    Button btnNext;
    Toolbar toolbar;
    Active active;
    List<QuestionE3> questionE3s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e3);

        tViewE3 = (TextView) findViewById(R.id.tViewE3);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question E3");
        toolbar.setTitleTextColor(Color.WHITE);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");


        e3layout = (LinearLayout) findViewById(R.id.e3_layout);

        btnNext = (Button) findViewById(R.id.btnNext);

        tViewE3.setTypeface(tf);
        active = (Active) getIntent().getSerializableExtra("active");

        randerE3Layout();

        btnNext.setOnClickListener(btnNext_OnClickListener);
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate()) {
                Intent intent = new Intent(ActivityE3.this, ActivityE4a.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    };

    private void randerE3Layout() {
        String[] arrayE3 = getResources().getStringArray(R.array.arrayE3);
        for (int i = 0; i < arrayE3.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            label.setTypeface(tf);
            label.setText(arrayE3[i]);
            label.setTag(i + 1);
            if (label.getText().toString().equalsIgnoreCase("کوئی رشتہ دار (وضاحت )")||label.getText().toString().contains("دیگر")) {
                editText.setVisibility(View.VISIBLE);
            }
            e3layout.addView(v);
        }
    }

    private void add() {

        questionE3s = new ArrayList<>();
        for (int i = 0; i < e3layout.getChildCount(); i++) {
            View v = e3layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            QuestionE3 questionE3 = new QuestionE3();
            if (chkBox.isChecked()) {
                questionE3.setLabel(label.getText().toString());
                questionE3.setCode((int) label.getTag());
                questionE3.setOpenEnded(editText.getText().toString());
                questionE3s.add(questionE3);
            }

        }

        active.setQuestionE3(new Gson().toJson(questionE3s));
    }

    private boolean validate() {

        for (int i=0;i<questionE3s.size();i++)
        {
            if (questionE3s.get(i).getLabel().equalsIgnoreCase("دیگر")||questionE3s.get(i).getLabel().equalsIgnoreCase("کوئی رشتہ دار (وضاحت )"))
            {
                if (questionE3s.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter");
                    return false;
                }
            }
        }
        if (questionE3s.size() == 0) {
            showDialog("Please Select At least one option");
            return false;
        }


        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

}
