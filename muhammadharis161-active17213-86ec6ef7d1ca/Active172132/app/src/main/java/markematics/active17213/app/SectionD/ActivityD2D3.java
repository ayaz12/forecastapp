package markematics.active17213.app.SectionD;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.D2SpinnerAdapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionD1;
import markematics.active17213.Model.QuestionD3;
import markematics.active17213.Model.QuestionS12;


public class ActivityD2D3 extends AppCompatActivity {

    TextView  tViewD2, tViewD3, lblChildName;
    LinearLayout D3layout;
    Spinner spinnerD2;
    Typeface tf;
    Active active;
    Button btnNext;
    Toolbar toolbar;
    List<QuestionD3> questionD3s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d2_d3);

        tViewD2 = (TextView) findViewById(R.id.tViewD2);
        tViewD3 = (TextView) findViewById(R.id.tViewD3);
        lblChildName = (TextView)findViewById(R.id.lblChildName);

        spinnerD2 = (Spinner) findViewById(R.id.spinnerd2);
        D3layout = (LinearLayout) findViewById(R.id.d3_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question D2 D3");
        toolbar.setTitleTextColor(Color.WHITE);


        tViewD2 = (TextView)findViewById(R.id.tViewD2);
        tViewD3 = (TextView)findViewById(R.id.tViewD3);

        spinnerD2 = (Spinner)findViewById(R.id.spinnerd2);
        D3layout = (LinearLayout) findViewById(R.id.d3_layout);
        btnNext = (Button) findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        active = (Active) getIntent().getSerializableExtra("active");
        tViewD2.setTypeface(tf);
        tViewD3.setTypeface(tf);
        lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
        lblChildName.setTypeface(tf);

        randerD3Layout();
        Type listType = new TypeToken<ArrayList<QuestionD1>>() {
        }.getType();
        List<QuestionD1> questionD1s = new Gson().fromJson(active.getQuestionD1(), listType);
        QuestionD1 questionD1 = new QuestionD1();
        questionD1.setStatement("Select");
        questionD1.setCode(0);
        questionD1.setOpenEnded("");
        questionD1s.add(0,questionD1);
        Spinner spinnersD2 = (Spinner) findViewById(R.id.spinnerd2);
        D2SpinnerAdapter d2SpinnerAdapter = new D2SpinnerAdapter(ActivityD2D3.this,R.layout.spinner_item,R.id.txt,questionD1s);
        spinnersD2.setAdapter(d2SpinnerAdapter);
         btnNext.setOnClickListener(btnNext_OnClickListener);
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate())
            {
                Intent intent = new Intent(ActivityD2D3.this, ActivityD4.class);
                intent.putExtra("active",active);
                startActivity(intent);
            }
        }
    };


    private void randerD3Layout() {
        String[] arrayD3 = getResources().getStringArray(R.array.arrayd3);
        for (int i = 0; i < arrayD3.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label2 = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);

            label2.setText(arrayD3[i]);
            label2.setTypeface(tf);
            label2.setTag(i+1);
            if (arrayD3[i].equalsIgnoreCase("دیگر"))
            {
                editText.setVisibility(View.VISIBLE);
            }
            D3layout.addView(v);
        }
    }

    private void add(){

        QuestionD1 questionD1 =(QuestionD1) spinnerD2.getSelectedItem();
        active.setQuestionD2(""+questionD1.getCode());

        questionD3s = new ArrayList<>();
        for (int i = 0; i < D3layout.getChildCount(); i++) {
            View v = D3layout.getChildAt(i);
            TextView label2 = (TextView) v.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);

            if (checkBox.isChecked()) {
                QuestionD3 questionD3 = new QuestionD3();
                questionD3.setCode((int) label2.getTag());
                questionD3.setStatement(label2.getText().toString());
                questionD3.setOpenEnded(editText.getText().toString());
                questionD3s.add(questionD3);
            }

        }

        active.setQuestionD3(new Gson().toJson(questionD3s));

    }
    private boolean validate()
    {


        if (spinnerD2.getSelectedItemPosition() == 0)
        {
            showDialog("Please fill all D2 values");
            return false;
        }
        for (int i=0;i<questionD3s.size();i++)
        {
            if (questionD3s.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionD3s.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other ");
                    return false;
                }
            }
        }
        if (questionD3s.size() == 0)
        {
            showDialog("Please fill D3 values");
            return false;
        }

        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
