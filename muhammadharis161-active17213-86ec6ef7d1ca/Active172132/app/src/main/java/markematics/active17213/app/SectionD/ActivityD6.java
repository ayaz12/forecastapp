package markematics.active17213.app.SectionD;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.lassana.recorder.AudioRecorder;
import com.google.gson.Gson;
import com.markematics.Active17213.R;


import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionD4;
import markematics.active17213.Model.QuestionD5;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionA.ActivityA11;
import markematics.active17213.app.SectionC.ActivityC1;
import markematics.active17213.app.SectionE.ActivityE1E2;
import markematics.active17213.app.SectionE.ActivityE1E2E3;

/**
 * Created by mas on 9/21/2017.
 */

public class ActivityD6 extends AppCompatActivity {

    TextView tViewD6,lblChildName;
    Spinner spinnerd6;
    Button btnNext;
    Active active;
    Toolbar toolbar;
    Typeface tf;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d6);

        tViewD6 = (TextView) findViewById(R.id.tViewD6);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question D6");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewD6 = (TextView) findViewById(R.id.tViewD6);
        spinnerd6 = (Spinner) findViewById(R.id.spinnerd6);
        lblChildName = (TextView)findViewById(R.id.lblChildName);

        btnNext = (Button) findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewD6.setTypeface(tf);


        active = (Active) getIntent().getSerializableExtra("active");
        lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
        lblChildName.setTypeface(tf);

        ArrayAdapter<String> spinnersD6ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayd6));
        spinnerd6.setAdapter(spinnersD6ArrayAdapter);
        btnNext.setOnClickListener(btnNext_OnClickListener);

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate()) {

            StopRecording();
            }
        }
    };
    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityD6.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityD6.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    Intent intent = new Intent(ActivityD6.this, ActivityE1E2.class);
                    intent.putExtra("active", active);
                    startActivity(intent);
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private void add() {
        active.setQuestionD6("" + spinnerd6.getSelectedItemPosition());
    }

    private boolean validate() {


        if (spinnerd6.getSelectedItemPosition() == 0) {
            showDialog("Please fill all D6 values");
            return false;
        }
        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}