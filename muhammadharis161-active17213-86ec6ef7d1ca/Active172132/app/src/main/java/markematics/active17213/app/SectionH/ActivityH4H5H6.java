package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH6a;
import markematics.active17213.Model.QuestionH6b;


public class ActivityH4H5H6 extends AppCompatActivity {

    TextView tViewH4, tViewH5,tViewH6a,tViewH6b;
    EditText editTextH5;
    Button btnNext;
    String H6;
    Spinner spinnerH4;
    LinearLayout H6alayout, H6blayout;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionH6a> questionH6aList;
    List<QuestionH6b> questionH6bList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h4_h5_h6);

        tViewH5 = (TextView) findViewById(R.id.tViewH5);
        tViewH4 = (TextView) findViewById(R.id.tViewH4);
        tViewH6a = (TextView) findViewById(R.id.tViewH6a);
        tViewH6b = (TextView) findViewById(R.id.tViewH6b);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H4 H5 H6");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        editTextH5 = (EditText) findViewById(R.id.editTextH5);

        spinnerH4 = (Spinner) findViewById(R.id.spinnerH4);

        H6alayout = (LinearLayout) findViewById(R.id.H6a_layout);
        H6blayout = (LinearLayout) findViewById(R.id.H6b_layout);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH4.setTypeface(tf);
        tViewH5.setTypeface(tf);

        editTextH5.setTypeface(tf);
        editTextH5.setTypeface(tf);

        ArrayAdapter<String> spinnerH5ArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH4.setAdapter(spinnerH5ArrayAdapter);


        btnNext = (Button) findViewById(R.id.btnNext);
        randerH6aLayout();
        randerH6bLayout();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

        spinnerH4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 2)
                {
                    tViewH5.setVisibility(View.GONE);
                    tViewH6a.setVisibility(View.GONE);
                    tViewH6b.setVisibility(View.GONE);
                    editTextH5.setVisibility(View.GONE);
                    H6alayout.setVisibility(View.GONE);
                    H6blayout.setVisibility(View.GONE);
                }
                else
                {

                    tViewH5.setVisibility(View.VISIBLE);
                    tViewH6a.setVisibility(View.VISIBLE);
                    tViewH6b.setVisibility(View.VISIBLE);
                    editTextH5.setVisibility(View.VISIBLE);
                    H6alayout.setVisibility(View.VISIBLE);
                    H6blayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void next() {
        initialize();
        add();
        if (validationSuccess()) {

            proceed();
        } else {


        }


    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (editTextH5.getVisibility() == View.VISIBLE) {
            if (H6.isEmpty()) {
                editTextH5.setError("درج کریں");
                return false;
            }

            if (questionH6aList.size() == 0)
            {
                showDialog("Please select at least opne option H6a");
                return false;
            }
            if (questionH6bList.size() == 0)
            {
                showDialog("Please select at least opne option H6b");
                return false;
            }
        }
        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private void proceed() {
        Intent nextPage = new Intent(ActivityH4H5H6.this, ActivityH7H8H9H10H11.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void initialize() {


        H6 = editTextH5.getText().toString().trim();

    }

    private void randerH6aLayout() {
        String[] arrayH6a = getResources().getStringArray(R.array.arrayH3b);
        for (int i = 0; i < arrayH6a.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH6a[i]);
            H6alayout.addView(v);
        }
    }

    private void randerH6bLayout() {
        String[] arrayH6b = getResources().getStringArray(R.array.arrayH6B);
        for (int i = 0; i < arrayH6b.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH6b[i]);
            H6blayout.addView(v);
        }
    }

    private void add() {

        active.setQuestionH4("" + spinnerH4.getSelectedItem());
        active.setQuestionH5(editTextH5.toString());

        questionH6aList = new ArrayList<>();
        for (int j = 0; j < H6alayout.getChildCount(); j++) {
            View view = H6alayout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            QuestionH6a questionH6a = new QuestionH6a();

            if (checkBox.isChecked()) {
                questionH6a.setCode((int) labeled.getTag());
                questionH6a.setStatement(labeled.getText().toString());
                questionH6aList.add(questionH6a);
            }
        }


        questionH6bList = new ArrayList<>();
        for (int j = 0; j < H6blayout.getChildCount(); j++) {
            View view = H6alayout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            QuestionH6b questionH6b = new QuestionH6b();

            if (checkBox.isChecked()) {
                questionH6b.setCode((int) labeled.getTag());
                questionH6b.setStatement(labeled.getText().toString());
                questionH6bList.add(questionH6b);
            }
        }
    }
}






