package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.CustomSpinnerAdapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionA4;
import markematics.active17213.Model.QuestionS12;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA5 extends AppCompatActivity {

    TextView tViewA5, lblChildName;
    LinearLayout a5_layout;
    Typeface tf;
    Button btnA5;
    Active active;
    Toolbar toolbar;
    List<QuestionA4> questionA5s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a5);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        lblChildName = (TextView) findViewById(R.id.lblChildName);
        toolbar.setTitle("Question A5a");
        lblChildName = (TextView) findViewById(R.id.lblChildName);
        toolbar.setTitleTextColor(Color.WHITE);

        tViewA5 = (TextView) findViewById(R.id.tViewA5);
        a5_layout = (LinearLayout) findViewById(R.id.a5_layout);

        btnA5 = (Button) findViewById(R.id.btnA5);

        btnA5.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void randerA5Layout() {
        a5_layout.removeAllViews();
        Type listType = new TypeToken<ArrayList<QuestionS12>>() {
        }.getType();
        List<QuestionS12> questionS12s = new Gson().fromJson(active.getQuestionS12(), listType);
        QuestionS12 questionS12 = new QuestionS12();
        questionS12.setStatement("کچھ بھی استعمال نہیں کرتی");
        questionS12.setCode(6);
        questionS12.setOpenEnded("");
        questionS12s.add(questionS12);
        questionS12 = new QuestionS12();
        questionS12.setStatement("نہیں جانتی/کہہ نہیں سکتی");
        questionS12.setCode(0);
        questionS12.setOpenEnded("");
        questionS12s.add(questionS12);
        questionS12 = new QuestionS12();
        questionS12.setStatement("Select");
        questionS12.setCode(0);
        questionS12.setOpenEnded("");
        questionS12s.add(0, questionS12);
        String[] spinnerarrayA5 = getResources().getStringArray(R.array.arrayA5);
        for (int i = 0; i < spinnerarrayA5.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.a1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lb2);
            label.setTypeface(tf);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);

            CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(ActivityA5.this,
                    R.layout.spinner_item, R.id.txt, questionS12s);
            spinnerLayoutA1.setAdapter(adapter);
            label.setText(spinnerarrayA5[i]);
            label.setTag(i + 1);
            a5_layout.addView(v);
        }
    }

    private void add() {
        questionA5s = new ArrayList<>();
        for (int i = 0; i < a5_layout.getChildCount(); i++) {
            View v = a5_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lb2);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);
            QuestionA4 questionA4 = new QuestionA4();
            questionA4.setStatement(label.getText().toString());
            questionA4.setCode((int) label.getTag());
            QuestionS12 questionS12 = (QuestionS12) spinnerLayoutA1.getSelectedItem();
            questionA4.setS12Code(questionS12.getCode());
            questionA5s.add(questionA4);

        }

        active.setQuestionA5a(new Gson().toJson(questionA5s));

    }

    private boolean validate() {
        for (int i = 0; i < a5_layout.getChildCount(); i++) {
            View v = a5_layout.getChildAt(i);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);

            if (spinnerLayoutA1.getSelectedItemPosition() == 0) {
                showDialog("Please fill all values");
                return false;
            }

        }


        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA5.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate()) {
                                add();
                                Intent intent = new Intent(ActivityA5.this, ActivityA5b.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            active = (Active) getIntent().getSerializableExtra("active");
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewA5.setTypeface(tf);
                            lblChildName.setText(active.getChildName() + " منتخب بچے کا نام ");
                            lblChildName.setTypeface(tf);
                            randerA5Layout();

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}

