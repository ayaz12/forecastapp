package markematics.active17213.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.markematics.Active17213.R;

import markematics.active17213.Helper.KeyValueDB;
import markematics.active17213.Model.Active;
import markematics.active17213.app.SectionB.ActivityB1;
import markematics.active17213.app.SectionH.ActivityH1;

/**
 * Created by mas on 10/4/2017.
 */

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {



                final KeyValueDB keyValueDB = new KeyValueDB(getSharedPreferences(getResources().getString(R.string.keyValueDb), Context.MODE_PRIVATE));
                String hrId = keyValueDB.getValue("hrId", "");
                if (hrId.isEmpty()) {
                  Intent  i = new Intent(SplashScreen.this, ActivityLogin.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    finish();
                } else

                {


                  Intent  i = new Intent(SplashScreen.this, ActivityHome.class);
//                    Active active = new Active();
//                    i.putExtra("active",active);

                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    finish();
             }


            }}, SPLASH_TIME_OUT);
    }
}

