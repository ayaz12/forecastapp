package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionA10;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA10 extends AppCompatActivity {

    TextView tViewA10;
    LinearLayout A10_layout;
    Typeface tf;
    Button btnA10;
    Active active;
    Toolbar toolbar;
    List<QuestionA10> questionA10s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a10);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A10");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewA10 = (TextView) findViewById(R.id.tViewA10);
        A10_layout = (LinearLayout) findViewById(R.id.A10_layout);

        btnA10=(Button)findViewById(R.id.btnA10);

        btnA10.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };


    private void randerA10Layout() {

        A10_layout.removeAllViews();
        String[] spinnerarrayA10 = getResources().getStringArray(R.array.arrayA10);
        for (int i=0;i<spinnerarrayA10.length;i++) {
            View v = getLayoutInflater().inflate(R.layout.a1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lb2);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);
            label.setTypeface(tf);
            ArrayAdapter<String> spinnerA1ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.spinnerA10));
            spinnerLayoutA1.setAdapter(spinnerA1ArrayAdapter);
            label.setText( spinnerarrayA10[i]);
            A10_layout.addView(v);
        }
    }


    private void add() {
        questionA10s = new ArrayList<>();
        for (int i = 0; i < A10_layout.getChildCount(); i++) {
            View v = A10_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lb2);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);
            QuestionA10 questionA10 = new QuestionA10();
            questionA10.setLabel(label.getText().toString());
            questionA10.setStatement((String) spinnerLayoutA1.getSelectedItem());
            questionA10.setCode(spinnerLayoutA1.getSelectedItemPosition());
            questionA10s.add(questionA10);

        }
        active.setQuestionA10(new Gson().toJson(questionA10s));

    }

    private boolean validate()
    {
        for (int i = 0;i<A10_layout.getChildCount();i++) {
            View v = A10_layout.getChildAt(i);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);



            if (spinnerLayoutA1.getSelectedItemPosition() == 0)
            {
                showDialog("Please fill all values");
                return false;
            }

        }




        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA10.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate())
                            {
                                add();
                                Intent intent = new Intent(ActivityA10.this, ActivityA11.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            active = (Active) getIntent().getSerializableExtra("active");
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewA10.setTypeface(tf);
                            randerA10Layout();

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}

