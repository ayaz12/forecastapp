package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH12;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionN.ActivityN1N2N3N4N5;

public class ActivityH14 extends AppCompatActivity {

    TextView  tViewH14;

    Spinner  spinnerH14;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h14);


        tViewH14 = (TextView) findViewById(R.id.tViewH14);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H14");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");


        spinnerH14 = (Spinner) findViewById(R.id.spinnerH14);

        btnNext = (Button) findViewById(R.id.btnNext);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH14.setTypeface(tf);


        ArrayAdapter<String> spinnerH12bArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH14.setAdapter(spinnerH12bArrayAdapter);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });


    }

    private void next() {

        if (spinnerH14.getSelectedItemPosition() != 0) {
            add();
            proceed();
        } else {
            showDialog("please select at least one option H14");
        }


    }


    private void proceed() {if (spinnerH14.getSelectedItemPosition()==1)
    {
        Intent intent = new Intent(ActivityH14.this, ActivityH15.class);
        intent.putExtra("active", active);
        startActivity(intent);


    }
    else
    {
        Intent intent = new Intent(ActivityH14.this, ActivityN1N2N3N4N5.class);
        intent.putExtra("active", active);
        startActivity(intent);

    }
    }

    private void add(){


        active.setQuestionH14("" + spinnerH14.getSelectedItemPosition());

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}