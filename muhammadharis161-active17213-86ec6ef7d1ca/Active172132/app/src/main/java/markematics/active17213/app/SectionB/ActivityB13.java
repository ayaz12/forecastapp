package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionB12;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB13 extends AppCompatActivity {

    TextView tViewb13, lblChildName, lblBrandName;
    LinearLayout b12Layout;
    Button btnNext;
    EditText editTextB13;
    Toolbar toolbar;
    Typeface tf;
    Active active;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b13);
        lblBrandName = (TextView) findViewById(R.id.lblBrandName);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B13");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewb13 = (TextView) findViewById(R.id.tviewB13);
        lblChildName = (TextView) findViewById(R.id.lblChildName);
        btnNext = (Button) findViewById(R.id.btnNext);
        editTextB13 = (EditText) findViewById(R.id.editTextB13);



        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private boolean validationSuccess() {

        boolean valid = true;

        if (editTextB13.getText().toString().isEmpty()) {
            editTextB13.setError("درج کریں");
            valid = false;

        }
        return valid;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB13.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validationSuccess()) {
                                active.setQuestionB13(editTextB13.getText().toString());
                                Intent intent = new Intent(ActivityB13.this, ActivityB14.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewb13.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");
                            lblChildName.setText(active.getChildName() + " منتخب بچے کا نام ");
                            lblChildName.setTypeface(tf);
                            Type listType = new TypeToken<ArrayList<QuestionB12>>() {
                            }.getType();
                            List<QuestionB12> questionB12s = new Gson().fromJson(active.getQuestionB12(), listType);
                            lblBrandName.setText(questionB12s.get(0).getOpenEnded()+" برانڈ کا نام "  );
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
