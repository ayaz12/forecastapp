package markematics.active17213.DataAccess;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Location;


public class DataBaseUtil extends SQLiteOpenHelper {


    private static String DB_NAME = "ActiveQuant";
    private static int DB_VERSION = 2;

    public DataBaseUtil(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // execute the query string to the database.


          db.execSQL(Active.CREATE_TABLE);
          db.execSQL(Location.CREATE_LOCATION_TABLE);
//        db.execSQL(RecQuestionnadb.execSQL(Active.CREATE_TABLE);ire.CREATE_FOOTFALL_TABLE);
//        db.execSQL(Rank.CREATE_RANK_TABLE);



    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion >= newVersion)
            return;

        // drop tables.
        db.execSQL(Active.DROP_TABLE);
        db.execSQL(Location.DROP_LOCATION_TABLE);
//        db.execSQL(RecQuestionnaire.DROP_FOOTFALL_TABLE);
//        db.execSQL(Rank.DROP_RANK_TABLE);


        onCreate(db);
    }

    public SQLiteDatabase openConnection() {
        SQLiteDatabase dbConnection = this.getWritableDatabase();
        return dbConnection;
    }

    public void closeConnection() {
        this.close();
    }

}
