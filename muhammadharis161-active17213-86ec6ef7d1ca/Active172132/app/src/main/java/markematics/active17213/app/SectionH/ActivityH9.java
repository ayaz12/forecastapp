package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH9;


public class ActivityH9 extends AppCompatActivity {

    TextView tViewH9 ;
    Button btnNext;
    LinearLayout H9layout;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionH9> questionH9List;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h9);

        active = (Active) getIntent().getSerializableExtra("active");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H9");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewH9=(TextView)findViewById(R.id.tViewH9);


        btnNext = (Button)findViewById(R.id.btnNext);

        H9layout = (LinearLayout)findViewById(R.id.H9_layout);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH9.setTypeface(tf);


        randerH9aLayout();


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {
       add();
        if (validationSuccess()) {
            proceed();
        }
        }


    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validationSuccess() {


        boolean valid = true;
            if (questionH9List.size() ==0)
            {
                showDialog("Please select at least one option in H9");
                valid = false;
            }
        return valid;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH9.this, ActivityH10.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }


    private void randerH9aLayout() {
        String[] arrayH9 = getResources().getStringArray(R.array.arrayH9);
        for (int i = 0; i < arrayH9.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH9[i]);
            label.setTag(i+1);
            H9layout.addView(v);
        }
    }

    private void add(){
        questionH9List = new ArrayList<>();
        for (int j = 0; j < H9layout.getChildCount(); j++) {
            View view = H9layout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            QuestionH9 questionH9 = new QuestionH9();

            if (checkBox.isChecked()) {
                questionH9.setCode((int) labeled.getTag());
                questionH9.setStatement(labeled.getText().toString());
                questionH9List.add(questionH9);
            }
        }

        active.setQuestionH9(new Gson().toJson(questionH9List));

    }
}