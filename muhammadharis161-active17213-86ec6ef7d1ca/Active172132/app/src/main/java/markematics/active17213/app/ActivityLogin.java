package markematics.active17213.app;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.markematics.Active17213.R;

import org.json.JSONException;
import org.json.JSONObject;

import markematics.active17213.DataAccess.HttpHandler;
import markematics.active17213.Helper.KeyValueDB;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.User;

/**
 * Created by ZEB Rana on 09-Oct-17.
 */

public class ActivityLogin extends AppCompatActivity {


    EditText editTextHRId, editTextPassword;
    Button btnLogin;
    Typeface tf;
    Spinner spinnerCityDropDown;
    String hrID, hRPassword;
    Active active;
    Toolbar toolbar;
    private static final int REQUEST_CODE_PERMISSIONS = 0x1;
    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextHRId = (EditText) findViewById(R.id.textViewLogin);
        editTextPassword = (EditText) findViewById(R.id.textViewPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        spinnerCityDropDown = (Spinner) findViewById(R.id.spinnerCity);
        spinnerCityDropDown.setOnItemSelectedListener(null);

        ArrayAdapter<String> spinnerCityArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.cityList));
        spinnerCityDropDown.setAdapter(spinnerCityArrayAdapter);

        editTextHRId.setTypeface(tf);
        editTextPassword.setTypeface(tf);
        active = (Active) getIntent().getSerializableExtra("active");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Login");
        toolbar.setTitleTextColor(Color.WHITE);
        tryStart();

        btnLogin.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if (isNetworkAvailable()) {
                        login();
                    } else {
                        showDialog("Internet Connection not found");
                    }
                }
            }
        });


        spinnerCityDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0) {
                   // active.setCity((String) adapterView.getItemAtPosition(position));
                    btnLogin.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void tryStart() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final int checkAudio = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            final int checkStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            final int checkLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            final int checkCorse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (checkAudio != PackageManager.PERMISSION_GRANTED || checkStorage != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                    showNeedPermissionsMessage();
                } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showNeedPermissionsMessage();
                } else {
                    requestPermissions(new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.RECORD_AUDIO,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.SEND_SMS},
                            REQUEST_CODE_PERMISSIONS);
                }
            } else {

            }
        } else {

        }
    }
    private void showNeedPermissionsMessage() {

        message(getString(R.string.error_no_permissions));
    }
    private void message(String message) {
        Toast.makeText(ActivityLogin.this,message,Toast.LENGTH_SHORT).show();
    }
    private boolean validate() {

        initialize();
        if (hrID.isEmpty()) {

            editTextHRId.setError("Enter HR ID");
            return false;
        } else if (hRPassword.isEmpty()) {
           editTextPassword.setError("Enter Password");
            return false;
        } else if (spinnerCityDropDown.getSelectedItemPosition() == 0) {
            showDialog("Please Select At least one option");
            return false;
        }

        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private void proceed() {
        Intent intent = new Intent(ActivityLogin.this, ActivityHome.class);
        intent.putExtra("active", active);
        startActivity(intent);
    }

    private void initialize() {

        hrID = editTextHRId.getText().toString().trim();
        hRPassword = editTextPassword.getText().toString().trim();
    }


    public void add() {

        active.sethR_ID(editTextHRId.getText().toString());
        active.sethR_Password(editTextPassword.getText().toString());
    }
    private void login() {

        String url = "http://94.75.217.75/Ontex/ontex.svc/loginActive";
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("userName", editTextHRId.getText().toString());
            parameters.put("password",editTextPassword.getText().toString());
            parameters.put("City",(String)spinnerCityDropDown.getSelectedItem());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("Url", url);

        new AsyncHandler().execute(url, parameters);




    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void showError(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);

        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityLogin.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;
            String url = (String) params[0];
            JSONObject jsonObject = (JSONObject)params[1];
            responseObject = new HttpHandler(url, jsonObject,ActivityLogin.this).PostServer();
            try {
                JsonParser parser = new JsonParser();
                String retVal = parser.parse(responseObject).getAsString();
                JSONObject json = new JSONObject(retVal);
                //   JSONArray jsonArray = new JSONArray(responseObject);

                Gson gson = new Gson();
                //  json = jsonArray.getJSONObject(0);
                User user = gson.fromJson(json.getJSONObject("user").toString(), User.class);

             //   String userName = json.getString("userName");

                final KeyValueDB keyValueDB = new KeyValueDB(getSharedPreferences(getResources().getString(R.string.keyValueDb), Context.MODE_PRIVATE));


                if (user.getHrId() !=null) {
                    keyValueDB.save("hrId", user.getHrId());
                    keyValueDB.save("name", user.getName());
                    keyValueDB.save("city",user.getCity());
                    keyValueDB.save("areaList",json.getJSONArray("areaList").toString());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Intent i = new Intent(ActivityLogin.this, ActivityHome.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    });


                }
                else
                {
//                    keyValueDB.save("shops","");
//                    keyValueDB.save("userName", userName);
//                    keyValueDB.save("UId", UId);
//                    Intent i = new Intent(ActivityLogin.this, ActivityHome.class);
//                    startActivity(i);
//                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showError("Please Enter Correct UserName And Password.");
                        }
                    });

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }

}