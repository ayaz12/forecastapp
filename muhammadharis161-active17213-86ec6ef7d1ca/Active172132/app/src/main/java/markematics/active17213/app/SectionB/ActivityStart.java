package markematics.active17213.app.SectionB;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Model.Active;
import markematics.active17213.app.SectionS.ActivityS1;
import markematics.active17213.app.SectionS.ActivityS10;

/**
 * Created by mas on 10/18/2017.
 */

public class ActivityStart extends AppCompatActivity {

    Toolbar toolbar;
    TextView tViewB;
    Button btnStart;
    Typeface tf;
    Active active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_section_b);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Section B");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewB =(TextView)findViewById(R.id.tViewstartB) ;
        btnStart = (Button) findViewById(R.id.btnstart);
        btnStart.setTextColor(Color.parseColor("#FFFFFF"));
        tf = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewB.setTypeface(tf);
        btnStart.setOnClickListener(btnNext_OnClickListener);
        active = (Active)getIntent().getSerializableExtra("active");

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ActivityStart.this, ActivityB1.class);
            intent.putExtra("active", active);
            startActivity(intent);
        }
    };
}
