package markematics.active17213.app.SectionF;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.app.SectionG.ActivityG1;
import markematics.active17213.app.SectionR.ActivityR1;
import markematics.active17213.app.SectionR.ActivityR2;


public class ActivityF5 extends AppCompatActivity {
    TextView tViewf5,tViewF5ins;
    LinearLayout f5_layout;
    EditText editTextF7;
    Button btnNext;
    String f7;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    List<QuestionB1> questionF5s;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f5);


        tViewf5 = (TextView) findViewById(R.id.tViewF5);
        tViewF5ins=(TextView)findViewById(R.id.tViewF5Ins);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question F5");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        f5_layout = (LinearLayout) findViewById(R.id.f5_layout);
//        F4blayout = (LinearLayout) findViewById(R.id.F4b_layout);
//        F5layout = (LinearLayout) findViewById(R.id.F5_layout);
//        F6layout = (LinearLayout) findViewById(R.id.F6_layout);

        btnNext = (Button) findViewById(R.id.btnNext);


        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewf5.setTypeface(tf);
        tViewF5ins.setTypeface(tf);

        randerF5Layout();


        btnNext.setOnClickListener(btnNext_OnClickListener);

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate()) {

                if (questionF5s.size() == 1)
                {
                    active.setQuestionF6(new Gson().toJson(questionF5s));
                    Intent nextPage = new Intent(ActivityF5.this, ActivityF7.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);
                    return;
                }else {

                    Intent nextPage = new Intent(ActivityF5.this, ActivityF6.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);
                    return;
                }

            }
        }
    };

    private boolean validate() {

        if (questionF5s.size() == 0) {
            showDialog("Please select at least one option 5");
            return false;
        }
        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityF5.this, ActivityR1.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);

    }



    private void randerF5Layout() {
        String[] arrayF5 = getResources().getStringArray(R.array.arrayf4);

        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionF4as = new Gson().fromJson(active.getQuestionF4a(), listType);
        List<QuestionB1> questionF4bs = new Gson().fromJson(active.getQuestionF4b(), listType);

        List<Brand> brands = new ArrayList<>();
        for (int i =0;i<questionF4as.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionF4as.get(i).getCode());
            brand.setName(questionF4as.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionF4as.get(i).getOpenEnded():questionF4as.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionF4bs.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionF4bs.get(i).getCode());
            brand.setName(questionF4bs.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionF4bs.get(i).getOpenEnded():questionF4bs.get(i).getBrand());
            brands.add(brand);
        }

        for (int i = 0; i <brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(brands.get(i).getName());
            label.setTypeface(tf);
            label.setTag(brands.get(i).getCode());
            chkBox1.setTag(v);

            if (brands.get(i).getName().equalsIgnoreCase("Others")) {
                editText.setVisibility(View.VISIBLE);
            }
            f5_layout.addView(v);
            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
    }

    private CompoundButton.OnCheckedChangeListener checkBox1_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            if (chkBox1.isChecked()) {

                for (int i = 0; i < f5_layout.getChildCount(); i++) {
                    View childView = f5_layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < f5_layout.getChildCount(); i++) {
                    View childView = f5_layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }
            }

        }
    };
    private void add() {
        questionF5s = new ArrayList<>();
        for (int i = 0; i < f5_layout.getChildCount(); i++) {
            View v = f5_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            if (chkBox1.isChecked()) {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int) label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionF5s.add(questionB1);
            }
            active.setQuestionF5(new Gson().toJson(questionF5s));

        }
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
