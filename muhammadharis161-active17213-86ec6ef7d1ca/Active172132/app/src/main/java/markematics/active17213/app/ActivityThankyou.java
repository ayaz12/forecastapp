package markematics.active17213.app;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.markematics.Active17213.R;

/**
 * Created by ZEB Rana on 12-Oct-17.
 */

public class ActivityThankyou extends AppCompatActivity {

    TextView textViewThankYou;
    Button buttonSubmit;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Thankyou Screen");
        toolbar.setTitleTextColor(Color.WHITE);

        textViewThankYou = (TextView)findViewById(R.id.textViewThankyou);
        buttonSubmit =(Button)findViewById(R.id.btnSubmit);

}}
