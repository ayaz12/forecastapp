package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.SpinnerB18Adapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB8 extends AppCompatActivity {

    TextView tViewB7, lblChildName;
    Spinner spinnersB8;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b7_b8);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B8a");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewB7 = (TextView) findViewById(R.id.tViewb8);
        lblChildName = (TextView) findViewById(R.id.lblChildName);
        btnNext = (Button) findViewById(R.id.btnNext);
        spinnersB8 = (Spinner) findViewById(R.id.spinnerb7);

        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }

    private void setSpinner() {
        List<Brand> brands = new ArrayList<>();
        Brand brand = new Brand();
        brand.setCode(0);
        brand.setName("Select");
        brands.add(brand);
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listType);
        for (int i = 0; i < questionB4s.size(); i++) {
            brand = new Brand();
            brand.setCode(questionB4s.get(i).getCode());
            brand.setName(questionB4s.get(i).getBrand());
            brands.add(brand);
        }
        SpinnerB18Adapter adapter = new SpinnerB18Adapter(ActivityB8.this, R.layout.spinner_item, R.id.txt, brands);
        spinnersB8.setAdapter(adapter);
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

           new AsyncHandler(REQUEST_ADD).execute();

        }
    };


    private boolean validate() {


        if (spinnersB8.getSelectedItemPosition() == 0) {
            showDialog("Please fill all 8a values");
            return false;
        }


        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {



        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB8.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            final String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            boolean isDifferent = false;
                            if (validate()) {
                                Brand brand = (Brand) spinnersB8.getSelectedItem();

                                active.setQuestionB8a("" + brand.getCode());
                                Type listType = new TypeToken<ArrayList<QuestionB4>>() {
                                }.getType();
                                List<QuestionB4> questionB6s = new Gson().fromJson(active.getQuestionB6(), listType);
                                for (int i = 0; i < questionB6s.size(); i++) {
                                    //   String B8Brand =  (String) spinnersB8.getSelectedItem();
                                    int b6Brand = questionB6s.get(i).getCode();
                                    if (brand.getCode() == b6Brand) {
                                        isDifferent = true;
                                    }
                                }

                                if (isDifferent) {
                                    Intent intent = new Intent(ActivityB8.this, ActivityB9.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(ActivityB8.this, ActivityB8b.class);
                                    intent.putExtra("active", active);
                                    intent.putExtra("brandName", brand.getName());
                                    startActivity(intent);
                                }

                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB7.setTypeface(tf);

                            lblChildName.setTypeface(tf);

                            active = (Active) getIntent().getSerializableExtra("active");
                            lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
                            setSpinner();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }

}