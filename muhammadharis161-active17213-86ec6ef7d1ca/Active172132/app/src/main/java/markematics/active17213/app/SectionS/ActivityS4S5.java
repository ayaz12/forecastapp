package markematics.active17213.app.SectionS;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

import java.util.HashMap;
import java.util.Map;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityIntro;
import markematics.active17213.app.SectionB.ActivityB4;
import markematics.active17213.app.SectionB.ActivityB5;

/**
 * Created by mas on 10/3/2017.
 */

public class ActivityS4S5 extends AppCompatActivity {
    Spinner spinners4, spinners5;
    TextView tViewS4, tViewS5;
    EditText editTextcompanyname, editTextdesignation, editTextnatureofwork;
    Button btnS4;
    View view;
    Typeface tf;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s4_s5);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S4 AND S5");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewS4 = (TextView) findViewById(R.id.tViewS4);
        tViewS5 = (TextView) findViewById(R.id.tViewS5);
        editTextcompanyname = (EditText) findViewById(R.id.comapnyname);
        editTextdesignation = (EditText) findViewById(R.id.designation);
        editTextnatureofwork = (EditText) findViewById(R.id.natureofwork);
        spinners4 = (Spinner)findViewById(R.id.spinS4);
        spinners5 = (Spinner)findViewById(R.id.spinS5);
        btnS4 = (Button) findViewById(R.id.btnS4);
        btnS4.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

        spinners4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,  int position, long l) {
                if (position > 11)
                {
                    btnS4.setTextColor(Color.parseColor("#FFFFFF"));
                    active.setQuestionS4(""+position);
                    btnS4.setText("Finish");

                }
                else
                {
                    active.setQuestionS4(""+position);
                    btnS4.setText("Next");
                    btnS4.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                btnS4.setTextColor(Color.parseColor("#FFFFFF"));

            }

            });
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        new AsyncHandler(REQUEST_ADD).execute();
        }
    };
    private void checkSec(int occupation,int education)
    {
        String sec = getSec(occupation, education);
        if (sec!=null) {
            if (sec.equalsIgnoreCase("A1") || sec.equalsIgnoreCase("A2") || sec.equalsIgnoreCase("B") || sec.equalsIgnoreCase("C")) {
                btnS4.setText("Next");
            } else {
                btnS4.setText("Finish");
            }
        }
    }
    private void next() {


        if (validationSuccess()) {
            checkSec(spinners4.getSelectedItemPosition(),spinners5.getSelectedItemPosition());
            add();
            if(btnS4.getText().toString().equalsIgnoreCase("Finish"))
            {
                Utility.showExitDialog(ActivityS4S5.this, "کیا آپ انٹرویو ختم کرنا چاہتی ہیں", active );
//                Intent intent = new Intent(ActivityS4S5.this, ActivityIntro.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                return;
            }
            else
            {
                proceed();
            }


        }
    }


    private void proceed() {
        {Intent nextPage = new Intent(ActivityS4S5.this, ActivityS7.class);
            nextPage.putExtra("active",active);
            startActivity(nextPage);

        }
    }


    private void add()
    {
        active.setCompanyName(editTextcompanyname.getText().toString());
        active.setDesignation(editTextdesignation.getText().toString());
        active.setNatureOfWork(editTextnatureofwork.getText().toString());


        String sec = getSec( spinners4.getSelectedItemPosition(), spinners5.getSelectedItemPosition());
        active.setSec(sec);
        active.setQuestionS5(""+spinners5.getSelectedItemPosition());
        Toast.makeText(ActivityS4S5.this,""+sec,Toast.LENGTH_SHORT).show();
    }
    private boolean validationSuccess() {
        boolean valid = true;
        if (editTextcompanyname.getText().toString().isEmpty()) {
            editTextcompanyname.setError("درج کریں ادارے کا نام");
            return false;

        }
        if (editTextdesignation.getText().toString().isEmpty()) {
            editTextdesignation.setError("درج کریں عہدہ/گریڈ");
            return false;

        }
        if (editTextnatureofwork.getText().toString().isEmpty()) {
            editTextnatureofwork.setError("درج کریں کام کی نوعیت");
            return false;

        }
        if (spinners4.getSelectedItemPosition() == 0)
        {

            showDialog("برائے مہربانی یہ بتائیے کہ آپ کے گھرانے کے خرچےمیں جو سب سے زیادہ حصہ ملاتے ہیں ان کا پیشہ کیا ہے یعنی ‘ملازمت کا عہدہ یا بزنس کی نوعیت کیا ہے ؟برائے مہربانی یہ بتائیے کہ آپ کے گھرانے کے خرچےمیں جو سب سے زیادہ حصہ ملاتے ہیں ان کا پیشہ کیا ہے یعنی ‘ملازمت کا عہدہ یا بزنس کی نوعیت کیا ہے ؟");
            return  false;
        }
        if (spinners5.getSelectedItemPosition() == 0)
        {

            showDialog("جو گھرانے کے خرچے میں سب سے زیادہ حصہ دیتے ہیں ان کی تعلیم کتنی ہے؟ ");
            return  false;
        }
        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private String getSec(int occupation,int education)
    {

        Map<String,String> map = new HashMap();
        map.put("11","E2");
        map.put("12","E2");
        map.put("13","E1");
        map.put("14","E1");
        map.put("15","D");
        map.put("16","D");
        map.put("17","C");
        map.put("21","E2");
        map.put("22","E2");
        map.put("23","E2");
        map.put("24","E1");
        map.put("25","D");
        map.put("26","C");
        map.put("27","C");
        map.put("31","E2");
        map.put("32","E2");
        map.put("33","E2");
        map.put("34","D");
        map.put("35","D");
        map.put("36","C");
        map.put("37","C");
        map.put("41","E2");
        map.put("42","E2");
        map.put("43","D");
        map.put("44","D");
        map.put("45","D");
        map.put("46","C");
        map.put("47","C");
        map.put("51","D");
        map.put("52","D");
        map.put("53","C");
        map.put("54","C");
        map.put("55","B");
        map.put("56","B");
        map.put("57","B");
        map.put("61","D");
        map.put("62","D");
        map.put("63","C");
        map.put("64","C");
        map.put("65","B");
        map.put("66","B");
        map.put("67","A2");
        map.put("71","D");
        map.put("72","C");
        map.put("73","C");
        map.put("74","C");
        map.put("75","B");
        map.put("76","B");
        map.put("77","A2");
        map.put("81","B");
        map.put("82","B");
        map.put("83","A2");
        map.put("84","A2");
        map.put("85","A2");
        map.put("86","A1");
        map.put("87","A1");
        map.put("91","B");
        map.put("92","A2");
        map.put("93","A2");
        map.put("94","A2");
        map.put("95","A2");
        map.put("96","A1");
        map.put("97","A1");
        map.put("101","B");
        map.put("102","A2");
        map.put("103","A2");
        map.put("104","A2");
        map.put("105","A1");
        map.put("106","A1");
        map.put("107","A1");
        map.put("111","A2");
        map.put("112","A2");
        map.put("113","A2");
        map.put("114","A2");
        map.put("115","A1");
        map.put("116","A1");
        map.put("117","A1");


        String sec = map.get(""+occupation+""+education);
        return sec;
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS4S5.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                          next();
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            btnS4.setTextColor(Color.parseColor("#FFFFFF"));
                            active = (Active)getIntent().getSerializableExtra("active");
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewS4.setTypeface(tf);
                            tViewS5.setTypeface(tf);
                            editTextcompanyname.setTypeface(tf);
                            editTextdesignation.setTypeface(tf);
                            editTextnatureofwork.setTypeface(tf);



                            ArrayAdapter<String> spinners4ArrayAdapter = new ArrayAdapter<String>(ActivityS4S5.this, R.layout.spinner_item, getResources().getStringArray(R.array.spinS4));
                            spinners4.setAdapter(spinners4ArrayAdapter);


                            ArrayAdapter<String> spinners5ArrayAdapter = new ArrayAdapter<String>(ActivityS4S5.this, R.layout.spinner_item, getResources().getStringArray(R.array.spinS5));
                            spinners5.setAdapter(spinners5ArrayAdapter);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}