package markematics.active17213.app.SectionB;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.app.SectionA.ActivityA2b;
import markematics.active17213.app.SectionA.ActivityA3aA3b;


/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB1B2 extends AppCompatActivity {

    TextView tViewB1, tViewB2, tViewB3;
    Spinner spinB1;
    LinearLayout b1_b2_b3_Layout;
    Button btnB1;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB1> questionB1s;
    List<QuestionB1> questionB2s;
    List<QuestionB1> questionB3s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b1_b2);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B1 B2 B3");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB1 = (TextView) findViewById(R.id.tViewB1);
        tViewB2 = (TextView) findViewById(R.id.tViewB2);
        tViewB3 = (TextView) findViewById(R.id.tViewB3);
        //   spinB1 = (Spinner)findViewById(R.id.spinB1);
        b1_b2_b3_Layout = (LinearLayout) findViewById(R.id.b1_b2_b3_layout);

        btnB1 = (Button) findViewById(R.id.btnB1);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewB1.setTypeface(tf);
        tViewB2.setTypeface(tf);
        tViewB3.setTypeface(tf);
        active = (Active) getIntent().getSerializableExtra("active");
        btnB1.setOnClickListener(btnNext_OnClickListener);
        renderB1B2B3Layout();

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate())
            {
                Intent intent = new Intent(ActivityB1B2.this, ActivityB4B5B6.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox1_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            if (chkBox1.isChecked()) {
                chkBox2.setEnabled(false);
                chkBox3.setEnabled(false);
                for (int i = 0; i < b1_b2_b3_Layout.getChildCount(); i++) {
                    View childView = b1_b2_b3_Layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < b1_b2_b3_Layout.getChildCount(); i++) {
                    View childView = b1_b2_b3_Layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }
                chkBox2.setEnabled(true);
                chkBox3.setEnabled(true);
            }

        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox2_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            if (chkBox2.isChecked()) {
                chkBox1.setEnabled(false);
                chkBox3.setEnabled(false);

            } else {
                chkBox1.setEnabled(true);
                chkBox3.setEnabled(true);
            }

        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox3_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            if (chkBox3.isChecked()) {
                chkBox1.setEnabled(false);
                chkBox2.setEnabled(false);

            } else {
                chkBox1.setEnabled(true);
                chkBox2.setEnabled(true);
            }

        }
    };

    private void renderB1B2B3Layout() {
        String[] arrayB1 = getResources().getStringArray(R.array.arrayB2);
        for (int i = 0; i < arrayB1.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_b2_b3_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            label.setText(arrayB1[i]);
            label.setTypeface(tf);
            label.setTag(i + 1);
            chkBox1.setTag(v);
            chkBox2.setTag(v);
            chkBox3.setTag(v);
            chkBox1.setOnCheckedChangeListener(checkBox1_OnCheckedChangeListener);
            chkBox2.setOnCheckedChangeListener(checkBox2_OnCheckedChangeListener);
            chkBox3.setOnCheckedChangeListener(checkBox3_OnCheckedChangeListener);
            if (arrayB1[i].equalsIgnoreCase("Others")) {
                editText.setVisibility(View.VISIBLE);
            }
            b1_b2_b3_Layout.addView(v);
            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
    }


    private void add()
    {
        questionB2s = new ArrayList<>();
        questionB1s = new ArrayList<>();
        questionB3s = new ArrayList<>();
        for (int i = 0; i < b1_b2_b3_Layout.getChildCount(); i++) {
            View v = b1_b2_b3_Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            if (chkBox1.isChecked())
            {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionB1s.add(questionB1);
            }
            if (chkBox2.isChecked())
            {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionB2s.add(questionB1);
            }
            if (chkBox3.isChecked())
            {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionB3s.add(questionB1);
            }
        }

        active.setQuestionB1(new Gson().toJson(questionB1s));
        active.setQuestionB2(new Gson().toJson(questionB2s));
        active.setQuestionB3(new Gson().toJson(questionB3s));
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validate()
    {
        if (questionB1s.size() == 0)
        {
            showDialog("Please select at least one option B1");
            return false;
        }
        if (questionB2s.size() == 0)
        {
            showDialog("Please select at least one option B2");
            return false;
        }
        if (questionB3s.size() == 0)
        {
            showDialog("Please select at least one option B3");
            return false;
        }
        return true;
    }
}
