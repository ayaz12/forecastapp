package markematics.active17213.app.SectionF;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionF2;
import markematics.active17213.Model.QuestionF3;
import markematics.active17213.app.SectionG.ActivityG1;
import markematics.active17213.app.SectionR.ActivityR1;
import markematics.active17213.app.SectionR.ActivityR2;


public class ActivityF3 extends AppCompatActivity {
    TextView tViewF1,tViewF3Ins;
    LinearLayout  F3layout;
    Button btnNext;
    Typeface tf;
    Spinner spinnerF1;
    Toolbar toolbar;
    Active active;
    List<QuestionF2> questionF2s;
    List<QuestionF3> questionF3s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f3);

        tViewF1 = (TextView) findViewById(R.id.tViewF3);
        tViewF3Ins =(TextView)findViewById(R.id.tViewF3Ins);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question  F3");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");


        F3layout = (LinearLayout) findViewById(R.id.F3_layout);

        btnNext = (Button) findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewF1.setTypeface(tf);
        tViewF3Ins.setTypeface(tf);


        renderF3Layout();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {

        add();
        if (validate()) {


                boolean isWipes= false;
                for (int i =0;i<questionF3s.size();i++)
                {
                    if (questionF3s.get(i).getStatement().equalsIgnoreCase("(Wipes) وا ئپس"))
                    {
                        isWipes = true;
                    }
                }
                if (!isWipes)
                {
                    Type listType = new TypeToken<ArrayList<QuestionB1>>() {
                    }.getType();
                    List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
                    List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
                    List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
                    List<Brand> brands = new ArrayList<>();
                    for (int i =0;i<questionB1s.size();i++)
                    {
                        Brand brand = new Brand();
                        brand.setCode(questionB1s.get(i).getCode());
                        brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
                                questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
                        brands.add(brand);
                    }
                    for (int i =0;i<questionB2s.size();i++)
                    {
                        Brand brand = new Brand();
                        brand.setCode(questionB2s.get(i).getCode());
                        brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
                                questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
                        brands.add(brand);
                    }
                    for (int i =0;i<questionB3s.size();i++)
                    {
                        Brand brand = new Brand();
                        brand.setCode(questionB3s.get(i).getCode());
                        brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
                                questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
                        brands.add(brand);
                    }
                    boolean isCanbebe = false;
                    for (int i = 0;i<brands.size();i++)
                    {
                        if (brands.get(i).getName().equalsIgnoreCase("Canbebe"))
                        {
                            isCanbebe = true;
                        }
                    }
                    if (isCanbebe)
                    {
                        Type listTypeB4 = new TypeToken<ArrayList<QuestionB4>>() {
                        }.getType();
                        List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listTypeB4);
                        boolean isCanbebeB4 = false;
                        for (int i = 0; i< questionB4s.size(); i++)
                        {
                            if (questionB4s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                            {
                                isCanbebeB4 = true;
                            }
                        }
                        if (isCanbebeB4)
                        {
                            boolean isCanbebeB5 = false;
                            Type listTypeB5 = new TypeToken<ArrayList<QuestionB4>>() {
                            }.getType();
                            List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listTypeB5);

                            for (int i = 0; i< questionB5s.size(); i++)
                            {
                                if (questionB5s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                                {
                                    isCanbebeB5 = true;

                                }
                            }
                            if (isCanbebeB5)
                            {
                                Intent nextPage = new Intent(ActivityF3.this, ActivityG1.class);
                                nextPage.putExtra("active", active);
                                startActivity(nextPage);
                                return;
                            }
                            else
                            {
                                Intent nextPage = new Intent(ActivityF3.this, ActivityR2.class);
                                nextPage.putExtra("active", active);
                                startActivity(nextPage);
                                return;
                            }
                        }
                        else
                        {
                            Intent nextPage = new Intent(ActivityF3.this, ActivityR1.class);
                            nextPage.putExtra("active", active);
                            startActivity(nextPage);
                            return;
                        }
                    }
                    else
                    {
                        Intent nextPage = new Intent(ActivityF3.this, ActivityG1.class);
                        nextPage.putExtra("active", active);
                        startActivity(nextPage);
                        return;
                    }
                }

                    Intent nextPage = new Intent(ActivityF3.this, ActivityF4a.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);



        }

    }




    private void renderF3Layout() {
        Type listType = new TypeToken<ArrayList<QuestionF2>>() {
        }.getType();
        List<QuestionF2> questionF2s = new Gson().fromJson(active.getQuestionF2(), listType);

        String[] arrayF3 = getResources().getStringArray(R.array.arrayF2);
        for (int i = 0; i < questionF2s.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.f3_layout, null);

            TextView label = (TextView) v.findViewById(R.id.lb3);
           EditText editText = (EditText)v.findViewById(R.id.editText);

            Spinner spinnerLayoutG1 = (Spinner) v.findViewById(R.id.spinnerlayoutG1);
            ArrayAdapter<String> spinnerA1ArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, getResources().getStringArray(R.array.arrayF3));
            spinnerLayoutG1.setAdapter(spinnerA1ArrayAdapter);
            label.setTypeface(tf);
            label.setText(questionF2s.get(i).getStatement());
            label.setTag(questionF2s.get(i).getCode());
            if (questionF2s.get(i).getStatement().equalsIgnoreCase("تیل(oil)")||questionF2s.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                editText.setVisibility(View.VISIBLE);
            }

            F3layout.addView(v);
        }
    }

    private void add() {


            questionF3s = new ArrayList<>();
            for (int j = 0; j < F3layout.getChildCount(); j++) {
                View view = F3layout.getChildAt(j);
                    TextView labeled = (TextView) view.findViewById(R.id.lb3);
                    EditText editText = (EditText)view.findViewById(R.id.editText);
                    Spinner spinnerLayoutG1 = (Spinner) view.findViewById(R.id.spinnerlayoutG1);
                    QuestionF3 questionF3 = new QuestionF3();
                    questionF3.setCode((int) labeled.getTag());
                    questionF3.setStatement(labeled.getText().toString());
                    questionF3.setOpenEnded(editText.getText().toString());
                    questionF3.setResponseCode(spinnerLayoutG1.getSelectedItemPosition());
                    questionF3s.add(questionF3);

            }


            active.setQuestionF3(new Gson().toJson(questionF3s));

    }

    private boolean validate()
    {

        if (F3layout.getVisibility() == View.VISIBLE) {
            if (questionF3s.size() == 0) {
                showDialog("please select at least one option F3");
                return false;
            }
        }
        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}