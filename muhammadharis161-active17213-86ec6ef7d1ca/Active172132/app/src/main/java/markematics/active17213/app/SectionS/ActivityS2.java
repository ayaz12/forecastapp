package markematics.active17213.app.SectionS;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionS2;
import markematics.active17213.app.ActivityIntro;


public class ActivityS2 extends Activity implements AdapterView.OnItemSelectedListener {

    LinearLayout s2Layout;
    TextView tViewS2,lblRespondentName;
    Button btnNext;
    View view;
    Typeface tf;
    EditText editTextRespondentName;
    Active active;
    Toolbar toolbar;
    List<QuestionS2> questionS2s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s2);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S2");
        toolbar.setTitleTextColor(Color.WHITE);
        editTextRespondentName = (EditText)findViewById(R.id.ediTextRespondentName);
        lblRespondentName = (TextView)findViewById(R.id.lblRespondentName);
        s2Layout = (LinearLayout)  findViewById(R.id.s2_layout);
        tViewS2 = (TextView)findViewById(R.id.tViewS2);
        btnNext = (Button)findViewById(R.id.btnS2);
        btnNext.setTextColor(Color.parseColor("#FFFFFF"));
       btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };
    private void proceed() {
        {
            Intent nextPage = new Intent(ActivityS2.this, ActivityS3.class);
            nextPage.putExtra("active", active);
            startActivity(nextPage);
        }
    }




    private void randerS2Layout(){
        s2Layout.removeAllViews();
        String[] arrayS2 = getResources().getStringArray(R.array.s2Array);
        for (int i=0;i<arrayS2.length;i++)
        {
            View v = getLayoutInflater().inflate(R.layout.s2_layout,null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setText( arrayS2[i]);
            label.setTypeface(tf);
            label.setTag(i+1);
            s2Layout.addView(v);
        }
    }




    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        btnNext.setTextColor(Color.parseColor("#FFFFFF"));
        String item = adapterView.getItemAtPosition(position).toString();

        Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        btnNext.setTextColor(Color.parseColor("#FFFFFF"));

    }

    private void add()
    {
        questionS2s = new ArrayList<>();
        active.setReposndentName(editTextRespondentName.getText().toString());
        for (int i=0;i<s2Layout.getChildCount();i++)
        {
            View v = s2Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            QuestionS2 questionS2 = new QuestionS2();

                if (chkBox.isChecked())
                {
                    questionS2.setCode((int)label.getTag());
                    questionS2.setStatement(label.getText().toString());
                    questionS2s.add(questionS2);

                }


        }
        active.setQuestionS2(new Gson().toJson(questionS2s));
    }

    private boolean validate()
    {

            if (editTextRespondentName.getText().toString().isEmpty())
        {
            editTextRespondentName.setError("Please enter Name");
            return false;
        }
        if (questionS2s.size() == 0)
        {
            showDialog("Please Select At least one option");
            return false;
        }


        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS2.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate())
                            {
                                for (int i = 0;i<questionS2s.size();i++)
                                {
                                    if (questionS2s.get(i).getCode() != 5)
                                    {
                                        Utility.showExitDialog(ActivityS2.this, "کیا آپ انٹرویو ختم کرنا چاہتی ہیں", active );
                                        return;

                                    }
                                }
                                proceed();
                            }

                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewS2.setTypeface(tf);
                            lblRespondentName.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");
                            randerS2Layout();

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}

