package markematics.active17213.app.SectionF;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionF4a;
import markematics.active17213.app.SectionG.ActivityG1;
import markematics.active17213.app.SectionR.ActivityR1;
import markematics.active17213.app.SectionR.ActivityR2;


public class ActivityF4b extends AppCompatActivity {
    TextView  tViewf4b;
    LinearLayout F4b_layout;

    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    List<QuestionB1> questionF4bs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f4b);


        tViewf4b = (TextView) findViewById(R.id.tViewF4b);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question F4B");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        F4b_layout = (LinearLayout) findViewById(R.id.f4b_layout);
//        F4blayout = (LinearLayout) findViewById(R.id.F4b_layout);
//        F5layout = (LinearLayout) findViewById(R.id.F5_layout);
//        F6layout = (LinearLayout) findViewById(R.id.F6_layout);



        btnNext = (Button) findViewById(R.id.btnNext);


        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewf4b.setTypeface(tf);

        randerF4bLayout();


        btnNext.setOnClickListener(btnNext_OnClickListener);

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate()) {

                Intent nextPage = new Intent(ActivityF4b.this, ActivityF5.class);
                nextPage.putExtra("active", active);
                startActivity(nextPage);
                return;


            }
        }
    };

    private boolean validate() {

        if (questionF4bs.size() == 0) {
            showDialog("Please select at least one option 4bs");
            return false;
        }

        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityF4b.this, ActivityR1.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);

    }




    private void randerF4bLayout() {
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionF4as = new Gson().fromJson(active.getQuestionF4a(), listType);

        String[] arrayF4b = getResources().getStringArray(R.array.arrayf4);

        for (int i = 0; i < arrayF4b.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);

            if (!questionF4as.get(0).getBrand().equalsIgnoreCase(arrayF4b[i])) {
                label.setTypeface(tf);
                label.setText(arrayF4b[i]);
                label.setTypeface(tf);
                label.setTag(i + 1);
                chkBox1.setTag(v);

                if (arrayF4b[i].equalsIgnoreCase("Others")) {
                    editText.setVisibility(View.VISIBLE);
                }
                F4b_layout.addView(v);
            }
            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
    }

    private CompoundButton.OnCheckedChangeListener checkBox1_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            if (chkBox1.isChecked()) {


                for (int i = 0; i < F4b_layout.getChildCount(); i++) {
                    View childView = F4b_layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < F4b_layout.getChildCount(); i++) {
                    View childView = F4b_layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }


            }

        }
    };
    private void add() {


        questionF4bs = new ArrayList<>();
        for (int i = 0; i < F4b_layout.getChildCount(); i++) {
            View v = F4b_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            if (chkBox1.isChecked()) {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int) label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionF4bs.add(questionB1);
            }

            active.setQuestionF4b(new Gson().toJson(questionF4bs));
        }}
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
