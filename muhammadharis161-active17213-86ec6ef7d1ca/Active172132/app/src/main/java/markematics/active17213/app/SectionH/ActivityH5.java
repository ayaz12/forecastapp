package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH6a;
import markematics.active17213.Model.QuestionH6b;


public class ActivityH5 extends AppCompatActivity {

    TextView tViewH5;
    EditText editTextH5,editTextH5a,editTextH5b,editTextH5c,editTextH5d;
    Button btnNext;
    String H5;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h5);

        tViewH5 = (TextView) findViewById(R.id.tViewH5);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H5");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        editTextH5 = (EditText) findViewById(R.id.editTextH5);
        editTextH5a = (EditText) findViewById(R.id.editTextH5a);
        editTextH5b = (EditText) findViewById(R.id.editTextH5b);
        editTextH5c = (EditText) findViewById(R.id.editTextH5c);
        editTextH5d = (EditText) findViewById(R.id.editTextH5d);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH5.setTypeface(tf);

        editTextH5.setTypeface(tf);
        editTextH5a.setTypeface(tf);
        editTextH5b.setTypeface(tf);
        editTextH5c.setTypeface(tf);
        editTextH5d.setTypeface(tf);



        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });}


    private void next() {
        initialize();
        add();
        if (validationSuccess()) {

            proceed();
        } else {


        }


    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (editTextH5.getVisibility() == View.VISIBLE) {
            if (editTextH5.getText().toString().isEmpty()) {
                editTextH5.setError("درج کریں");
                return false;
            }
        }
        return true;
    }



    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private void proceed() {
        Intent nextPage = new Intent(ActivityH5.this, ActivityH6a.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void initialize() {


        H5 = editTextH5.getText().toString().trim();

    }


    private void add() {
        List<String> channelList = new ArrayList<>();
        channelList.add(editTextH5.getText().toString());
        if (!editTextH5a.getText().toString().isEmpty())
        {
            channelList.add(editTextH5a.getText().toString());
        }
        if (!editTextH5b.getText().toString().isEmpty())
        {
            channelList.add(editTextH5b.getText().toString());
        }
        if (!editTextH5c.getText().toString().isEmpty())
        {
            channelList.add(editTextH5c.getText().toString());
        }
        if (!editTextH5d.getText().toString().isEmpty())
        {
            channelList.add(editTextH5d.getText().toString());
        }
        active.setQuestionH5(new Gson().toJson(channelList));

    }
}






