package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/27/2017.
 */

public class ActivityB8b extends AppCompatActivity {

    TextView tViewb8b,lblChildName,lblBrandName;
    Button btnNext;
    Spinner spinner8b ;
    Typeface tf;

    Toolbar toolbar;
    Active active;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b8a_b8b);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B8b");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewb8b = (TextView) findViewById(R.id.tViewb8b);
        lblChildName = (TextView)findViewById(R.id.lblChildName);
        spinner8b = (Spinner) findViewById(R.id.spinnerb8b);
        lblBrandName = (TextView)findViewById(R.id.lblBrandName);
        btnNext = (Button) findViewById(R.id.btnNext);

          btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();

        }
    };
    private boolean validate()
    {


        if (spinner8b.getSelectedItemPosition() == 0)
        {
            showDialog("Please fill all 8b values");
            return false;
        }


        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB8b.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate()) {
                                active.setQuestionB8b("" + spinner8b.getSelectedItemPosition());
                                Intent intent = new Intent(ActivityB8b.this, ActivityB9.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            active = (Active)getIntent().getSerializableExtra("active");
                            lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
                            String brandName = getIntent().getExtras().getString("brandName");
                            lblBrandName.setText(brandName+" برانڈ کا نام ");

                            ArrayAdapter<String> spinnerb8bArrayAdapter = new ArrayAdapter<String>(ActivityB8b.this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayB8b));
                            spinner8b.setAdapter(spinnerb8bArrayAdapter);
                            lblBrandName.setTypeface(tf);
                            tViewb8b.setTypeface(tf);
                            lblChildName.setTypeface(tf);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}