package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/5/2017.
 */

public class QuestionB4 {

    private int code;
    private String brand;
    private String openEnded;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getOpenEnded() {
        return openEnded;
    }

    public void setOpenEnded(String openEnded) {
        this.openEnded = openEnded;
    }
}
