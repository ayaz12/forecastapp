package markematics.active17213.app.SectionS;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityIntro;

public class ActivityS7 extends AppCompatActivity {

    Spinner spinnerS7,spinnerAge;
    TextView textViewS7;
    Typeface tf;
    Button btnNext;
    Active active;
    Toolbar toolbar;
    RelativeLayout relativeLayout;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s7);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S7");
        toolbar.setTitleTextColor(Color.WHITE);

        textViewS7= (TextView) findViewById(R.id.tViewS7);
        spinnerS7 = (Spinner) findViewById(R.id.spinner_s7);
        spinnerAge=(Spinner)findViewById(R.id.spinnerAge);
        relativeLayout = (RelativeLayout)findViewById(R.id.unitLayout1);
        ArrayAdapter<String> spinnerS7ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayS7));
        spinnerS7.setAdapter(spinnerS7ArrayAdapter);

        ArrayAdapter<String> spinnerSAgeArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.ageArray));
        spinnerAge.setAdapter(spinnerSAgeArrayAdapter);
        btnNext =(Button) findViewById(R.id.btnS7);
        btnNext.setTextColor(Color.parseColor("#FFFFFF"));


        btnNext.setOnClickListener(btnNext_OnClickListener);
            new AsyncHandler(REQUEST_RENDER).execute();
        spinnerAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position !=0)
                {
                    String val = (String)adapterView.getSelectedItem();
                    if (Integer.parseInt(val) >40 || Integer.parseInt(val) <22)
                    {
                        btnNext.setText("Finish");
//                    relativeLayout.setVisibility(View.GONE);
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                    }
                    else
                    {
                        btnNext.setText("Next");
//                    relativeLayout.setVisibility(View.VISIBLE);
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };
   private void add()
   {
       String age =(String)spinnerAge.getSelectedItem();
       active.setQuestionS7(age);
   }

    private boolean validationSuccess() {

        if (spinnerAge.getSelectedItemPosition() == 0)
        {

            showDialog("Please Select At least one option ");
            return  false;
        }
        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS7.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                            if (validationSuccess()) {
                                add();
                                if (btnNext.getText().toString().equalsIgnoreCase("Finish")) {
                                    Utility.showExitDialog(ActivityS7.this, "کیا آپ انٹرویو ختم کرنا چاہتی ہیں", active );

                                } else {
                                    Intent nextPage = new Intent(ActivityS7.this, ActivityS8.class);
                                    nextPage.putExtra("active",active);
                                    startActivity(nextPage);

                                }
                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            active = (Active) getIntent().getSerializableExtra("active");
                            tf= Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            textViewS7.setTypeface(tf);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
