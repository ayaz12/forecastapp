package markematics.active17213.app.SectionC;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.github.lassana.recorder.AudioRecorderBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionC1;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionB.ActivityB1;
import markematics.active17213.app.SectionB.ActivityB19B20;
import markematics.active17213.app.SectionD.ActivityD1;

/**
 * Created by mas on 10/9/2017.
 */

public class ActivityC1 extends AppCompatActivity {


    LinearLayout c1_layout;
    Toolbar toolbar;
    Active active;
    Typeface tf;
    Button btnNext,btnBack;
    List<QuestionC1> questionC1s;
    String cStatementVal = "";
    TextView tViewC1,textViewStatement,tViewIns;
    String[] cArray;

    int count = 0;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c1);
        textViewStatement = (TextView)findViewById(R.id.tViewStatement);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question C1");
        toolbar.setTitleTextColor(Color.WHITE);
        btnBack = (Button)findViewById(R.id.btnBack);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        btnNext = (Button) findViewById(R.id.btnNext);
        c1_layout = (LinearLayout) findViewById(R.id.c1layout);
        tViewC1 = (TextView)findViewById(R.id.tViewbC1);
        tViewIns =(TextView)findViewById(R.id.tViewlblC1);
        active = (Active) getIntent().getSerializableExtra("active");
        tViewC1.setTypeface(tf);
        tViewIns.setTypeface(tf);
        cArray = getResources().getStringArray(R.array.ArrayC1);

        questionC1s = createList();

        renderLayout(0);
        btnNext.setOnClickListener(btnNext_OnClickListener);
        btnBack.setOnClickListener(btnBack_OnClickListener);

        StartRecording();
    }

    private void StartRecording()
    {
        if (ViewController.mAudioRecorder == null) {
            String filePath = active.gethR_ID() + "_" + active.getCity() + "_SecC_" + System.currentTimeMillis();
            active.setAudioPathSecC(filePath);
            recordAudio(filePath);

        }
    }
    private List<QuestionC1> createList()
    {
        List<QuestionC1> questionC1ArrayList = new ArrayList<>();
        for (int i =0;i<cArray.length;i++)
        {
            QuestionC1 questionC1 = new QuestionC1();
            questionC1.setCode(i+1);
            questionC1.setStatement(cArray[i]);
            questionC1ArrayList.add(questionC1);
        }
        return questionC1ArrayList;
    }
    private void recordAudio(String fileName) {


        final RecorderApplication application = RecorderApplication.getApplication(ActivityC1.this);
        ViewController.mAudioRecorder = application.getRecorder();
        if (ViewController.mAudioRecorder == null
                || ViewController.mAudioRecorder.getStatus() == AudioRecorder.Status.STATUS_UNKNOWN) {
            ViewController.mAudioRecorder = AudioRecorderBuilder.with(application)
                    .fileName(createDirectoryAndSaveFile(fileName))
                    .config(AudioRecorder.MediaRecorderConfig.DEFAULT)
                    .loggable()
                    .build();
            application.setRecorder(ViewController.mAudioRecorder);
        }
        start();
    }
    private void message(String message) {
        Toast.makeText(ActivityC1.this,message,Toast.LENGTH_SHORT).show();
    }
    private void start()
    {
        ViewController.mAudioRecorder .start(new AudioRecorder.OnStartListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onException(Exception e) {

                //   invalidateViews();
                message(getString(R.string.error_audio_recorder, e));
            }
        });
    }
    private String createDirectoryAndSaveFile( String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.ActiveQuant/Audio/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.ActiveQuant/Audio/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.ActiveQuant/Audio/"), fileName+ ".mp3" );
        if (file.exists()) {
            file.delete();
        }

        return file.getPath();
    }
    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityC1.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityC1.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    Intent i = new Intent(ActivityC1.this,ActivityD1.class);
                    i.putExtra("active",active);
                    startActivity(i);
                    finish();
                    Toast.makeText(ActivityC1.this,"Section C Completed",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (count == questionC1s.size()-1)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        StopRecording();
                        active.setQuestionC1(new Gson().toJson(questionC1s));

                    }
                });


            }
            else {
                add();
                if (validate()) {

                    count++;
                    renderLayout(count);

                }
                else
                {
                    Toast.makeText(ActivityC1.this,"Please select at least one option",Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private View.OnClickListener btnBack_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            count--;
            renderLayout(count);
        }
    };

    private void renderLayout(int position) {

        c1_layout.removeAllViews();
        textViewStatement.setTypeface(tf);
        textViewStatement.setText(cArray[position]);

        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
        List<Brand> brands = new ArrayList<>();
        for (int i = 0; i < questionB1s.size(); i++) {
            Brand brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB1s.get(i).getOpenEnded() : questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < questionB2s.size(); i++) {
            Brand brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB2s.get(i).getOpenEnded() : questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < questionB3s.size(); i++) {
            Brand brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB3s.get(i).getOpenEnded() : questionB3s.get(i).getBrand());
            brands.add(brand);
        }
        Brand brand = new Brand();
        brand.setCode(99);
        brand.setName("None");
        brands.add(brand);

        for (int i =0;i<brands.size();i++) {
            View v = getLayoutInflater().inflate(R.layout.c1_layout, null);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            checkBox.setTypeface(tf);
            checkBox.setText(brands.get(i).getName());
            checkBox.setTag(brands.get(i).getCode());
            c1_layout.addView(v);
        }

        if (count == 0)
        {
            btnBack.setVisibility(View.GONE);

        }
        else
        {
            btnBack.setVisibility(View.VISIBLE);

        }

    }

    private CompoundButton.OnCheckedChangeListener CheckBox_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            if (isChecked)
            {
                Brand brand = new Brand();
                brand.setCode((int)buttonView.getTag());
                brand.setName(buttonView.getText().toString());
            }
        }
    };

    private void add() {

        Brand[] brands = new Brand[c1_layout.getChildCount()];

        for (int i = 0; i < c1_layout.getChildCount(); i++) {
            View v = c1_layout.getChildAt(i);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            if (checkBox.isChecked()) {
                Brand brand = new Brand();
                brand.setCode((int) checkBox.getTag());
                brand.setName(checkBox.getText().toString());
                brands[i] = brand;
            }
        }

        questionC1s.get(count).setBrands(brands);

    }

    private boolean validate() {

        for (int i = 0;i<c1_layout.getChildCount() ;i++) {
            View v = c1_layout.getChildAt(i);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            if (checkBox.isChecked()) {
                return true;
            }
        }
        return false;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

}
