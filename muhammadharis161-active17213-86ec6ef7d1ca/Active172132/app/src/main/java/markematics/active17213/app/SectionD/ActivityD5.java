package markematics.active17213.app.SectionD;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.markematics.Active17213.R;


import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionD4;
import markematics.active17213.Model.QuestionD5;
import markematics.active17213.app.SectionE.ActivityE1E2E3;

/**
 * Created by mas on 9/21/2017.
 */

public class ActivityD5 extends AppCompatActivity {

    TextView tViewD5;
    LinearLayout d5layout;
    Button btnNext;
    Active active;
    List<QuestionD5> questionD5s;
    Toolbar toolbar;
    Typeface tf;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d5);

        tViewD5 = (TextView) findViewById(R.id.tViewD5);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question D5");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewD5 = (TextView) findViewById(R.id.tViewD5);
        d5layout = (LinearLayout) findViewById(R.id.d5_layout);

        btnNext = (Button) findViewById(R.id.btnNext);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewD5.setTypeface(tf);

        active = (Active) getIntent().getSerializableExtra("active");
        randerD5Layout();

        btnNext.setOnClickListener(btnNext_OnClickListener);

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    add();
                    if (validate()) {
                        Intent intent = new Intent(ActivityD5.this, ActivityD6.class);
                        intent.putExtra("active", active);
                        startActivity(intent);
                    }
                }
            });

        }
    };

    private void randerD5Layout() {
        String[] arrayD5 = getResources().getStringArray(R.array.arrayd5);
        for (int i = 0; i < arrayD5.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            label.setText(arrayD5[i]);
            label.setTypeface(tf);
            label.setTag(i + 1);
            if (arrayD5[i].equalsIgnoreCase("دیگر"))
            {
                editText.setVisibility(View.VISIBLE);
            }
            d5layout.addView(v);
        }
    }

    private void add() {

        questionD5s = new ArrayList<>();
        for (int i = 0; i < d5layout.getChildCount(); i++) {
            View v = d5layout.getChildAt(i);
            TextView label2 = (TextView) v.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            QuestionD5 questionD5 = new QuestionD5();

            if (checkBox.isChecked()) {
                questionD5.setCode((int) label2.getTag());
                questionD5.setStatement(label2.getText().toString());
                questionD5.setOpenEnded(editText.getText().toString());
                questionD5s.add(questionD5);
            }
        }

        active.setQuestionD5(new Gson().toJson(questionD5s));
    }

    private boolean validate() {
        for (int i=0;i<questionD5s.size();i++)
        {
            if (questionD5s.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionD5s.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other ");
                    return false;
                }
            }
        }
        if (questionD5s.size() == 0) {
            showDialog("Please fill D5 values");
            return false;
        }

        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}