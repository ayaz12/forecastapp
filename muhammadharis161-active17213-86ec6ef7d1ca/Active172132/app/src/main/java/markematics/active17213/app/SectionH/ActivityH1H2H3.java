package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH3a;
import markematics.active17213.Model.QuestionH3b;

public class ActivityH1H2H3 extends AppCompatActivity {

    TextView tViewH1, tViewH2, tViewH3b, tViewH3a;
    EditText editTextH2;
    Button btnNext;
    String H2;
    Spinner spinnerH1;
    Typeface tf;
    LinearLayout H3blayout, H3alayout;
    Toolbar toolbar;
    Active active;
    List<QuestionH3a> questionH3aH;
    List<QuestionH3b> questionH3bH;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h1_h2_h3);

        tViewH1 = (TextView) findViewById(R.id.tViewH1);
        tViewH2 = (TextView) findViewById(R.id.tViewH2);
        tViewH3a = (TextView) findViewById(R.id.tViewH3a);
        tViewH3b = (TextView) findViewById(R.id.tViewH3b);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H1 H2 H3");
        toolbar.setTitleTextColor(Color.WHITE);

        editTextH2 = (EditText) findViewById(R.id.editTextH2);


        active = (Active) getIntent().getSerializableExtra("active");

        spinnerH1 = (Spinner) findViewById(R.id.spinnerH1);

        H3alayout = (LinearLayout) findViewById(R.id.H3a_layout);
        H3blayout = (LinearLayout) findViewById(R.id.H3b_layout);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH1.setTypeface(tf);
        tViewH2.setTypeface(tf);
        tViewH3a.setTypeface(tf);
        tViewH3b.setTypeface(tf);
        editTextH2.setTypeface(tf);

        ArrayAdapter<String> spinnerE8ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH1.setAdapter(spinnerE8ArrayAdapter);
        randerH3aLayout();
        randerH3bLayout();
        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

        spinnerH1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 2)
                {
                    tViewH2.setVisibility(View.GONE);
                    tViewH3a.setVisibility(View.GONE);
                    tViewH3b.setVisibility(View.GONE);
                    H3alayout.setVisibility(View.GONE);
                    H3blayout.setVisibility(View.GONE);
                    editTextH2.setVisibility(View.GONE);

                }
                else
                {
                    tViewH2.setVisibility(View.VISIBLE);
                    tViewH3a.setVisibility(View.VISIBLE);
                    tViewH3b.setVisibility(View.VISIBLE);
                    H3alayout.setVisibility(View.VISIBLE);
                    H3blayout.setVisibility(View.VISIBLE);
                    editTextH2.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void next() {
        initialize();
        add();
        if (validationSuccess()) {

            proceed();
        } else {



        }


    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (editTextH2.getVisibility() == View.VISIBLE) {
            if (H2.isEmpty()) {
                editTextH2.setError("درج کریں");
               return false;
            }

            if (questionH3aH.size() == 0)
            {
                showDialog("please select at least one option H3a");
                return false;
            }
            if (questionH3bH.size() == 0)
            {
                showDialog("please select at least one option H3b");
                return false;
            }
        }
        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH1H2H3.this, ActivityH4H5H6.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private void initialize() {


        H2 = editTextH2.getText().toString().trim();

    }

    private void randerH3aLayout() {
        String[] arrayH3a = getResources().getStringArray(R.array.arrayH3a);
        for (int i = 0; i < arrayH3a.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH3a[i]);
            label.setTag(i+1);
            H3alayout.addView(v);
        }
    }

    private void randerH3bLayout() {
        String[] arrayH3b = getResources().getStringArray(R.array.arrayH3b);
        for (int i = 0; i < arrayH3b.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH3b[i]);
            label.setTag(i+1);
            H3blayout.addView(v);
        }
    }

    private void add() {

        active.setQuestionH1("" + spinnerH1.getSelectedItem());
        active.setQuestionH2(editTextH2.getText().toString());

        questionH3aH = new ArrayList<>();
        for (int i = 0; i < H3alayout.getChildCount(); i++) {
            View v = H3alayout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            QuestionH3a questionH3a = new QuestionH3a();

            if (chkBox.isChecked()) {
                questionH3a.setCode((int) label.getTag());
                questionH3a.setStatement(label.getText().toString());
                questionH3aH.add(questionH3a);

            }


            questionH3bH = new ArrayList<>();
            for (int j = 0; j < H3blayout.getChildCount(); j++) {
                View view = H3blayout.getChildAt(j);
                TextView labeled = (TextView) view.findViewById(R.id.lbl);
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                QuestionH3b questionH3b = new QuestionH3b();

                if (checkBox.isChecked()) {
                    questionH3b.setCode((int) labeled.getTag());
                    questionH3b.setStatement(labeled.getText().toString());
                    questionH3bH.add(questionH3b);
                }
            }
        }

        active.setQuestionH3a(new Gson().toJson(questionH3aH));
        active.setQuestionH3b(new Gson().toJson(questionH3bH));


    }
}