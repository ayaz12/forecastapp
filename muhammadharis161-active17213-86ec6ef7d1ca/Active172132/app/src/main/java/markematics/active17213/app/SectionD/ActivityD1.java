package markematics.active17213.app.SectionD;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.github.lassana.recorder.AudioRecorderBuilder;
import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionD1;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionC.ActivityC1;


public class ActivityD1 extends AppCompatActivity {
    TextView tViewD1;
    LinearLayout D1layout;
    Typeface tf;
    Active active;
    Button btnNext;
    Toolbar toolbar;
    List<QuestionD1> questionD1s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d1);

        tViewD1 = (TextView) findViewById(R.id.tViewD1);
        D1layout = (LinearLayout) findViewById(R.id.d1_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question D1");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewD1 = (TextView)findViewById(R.id.tViewD1);
        D1layout = (LinearLayout)findViewById(R.id.d1_layout);
        btnNext = (Button) findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewD1.setTypeface(tf);

        active = (Active) getIntent().getSerializableExtra("active");



        btnNext.setOnClickListener(btnNext_OnClickListener);
        StartRecording();
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate())
            {
                Intent intent = new Intent(ActivityD1.this, ActivityD2D3.class);
                intent.putExtra("active",active);
                startActivity(intent);
            }
        }
    };
    private void StartRecording()
    {
        if (ViewController.mAudioRecorder == null) {
            String filePath = active.gethR_ID() + "_" + active.getCity() + "_SecD_" + System.currentTimeMillis();
            active.setAudioPathSecD(filePath);
            recordAudio(filePath);

        }
    }
    private void recordAudio(String fileName) {


        final RecorderApplication application = RecorderApplication.getApplication(ActivityD1.this);
        ViewController.mAudioRecorder = application.getRecorder();
        if (ViewController.mAudioRecorder == null
                || ViewController.mAudioRecorder.getStatus() == AudioRecorder.Status.STATUS_UNKNOWN) {
            ViewController.mAudioRecorder = AudioRecorderBuilder.with(application)
                    .fileName(createDirectoryAndSaveFile(fileName))
                    .config(AudioRecorder.MediaRecorderConfig.DEFAULT)
                    .loggable()
                    .build();
            application.setRecorder(ViewController.mAudioRecorder);
        }
        start();
    }
    private void message(String message) {
        Toast.makeText(ActivityD1.this,message,Toast.LENGTH_SHORT).show();
    }
    private void start()
    {
        ViewController.mAudioRecorder .start(new AudioRecorder.OnStartListener() {
            @Override
            public void onStarted() {
                randerD1Layout();
            }

            @Override
            public void onException(Exception e) {

                //   invalidateViews();
                message(getString(R.string.error_audio_recorder, e));
            }
        });
    }
    private String createDirectoryAndSaveFile( String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.ActiveQuant/Audio/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.ActiveQuant/Audio/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.ActiveQuant/Audio/"), fileName+ ".mp3" );
        if (file.exists()) {
            file.delete();
        }

        return file.getPath();
    }
    private void randerD1Layout() {
        String[] arrayD1 = getResources().getStringArray(R.array.arrayd1);
        for (int i = 0; i < arrayD1.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label1 = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);
           label1.setText(arrayD1[i]);
            label1.setTypeface(tf);
            label1.setTag(i+1);
            if (arrayD1[i].equalsIgnoreCase("دیگر"))
            {
                editText.setVisibility(View.VISIBLE);
            }
            D1layout.addView(v);
        }
    }


    private void add(){

        questionD1s = new ArrayList<>();
        for (int i = 0; i < D1layout.getChildCount(); i++) {
            View v = D1layout.getChildAt(i);
            TextView label1 = (TextView) v.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);

            if (checkBox.isChecked()) {
                QuestionD1 questionD1 = new QuestionD1();
                questionD1.setCode((int) label1.getTag());
                questionD1.setStatement(label1.getText().toString());
                questionD1.setOpenEnded(editText.getText().toString());
                questionD1s.add(questionD1);
            }
        }



        active.setQuestionD1(new Gson().toJson(questionD1s));

    }

    private boolean validate()
    {
        for (int i=0;i<questionD1s.size();i++)
        {
            if (questionD1s.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionD1s.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other ");
                    return false;
                }
            }
        }
        if (questionD1s.size() == 0) {
            showDialog("Please fill  D1 values");
            return false;
        }
        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
