package markematics.active17213.Helper;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.github.lassana.recorder.AudioRecorder;
import com.markematics.Active17213.R;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityHome;
import markematics.active17213.app.ActivityIntro;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionS.ActivityS1;

public class Utility {
	
	
	
	
	public static String TrimText(String value, int numberOfCharacters, String attachText)
	{
		String remainingText = value;
		int valueCharacters = value.length();
		if (valueCharacters > numberOfCharacters)
		{
			remainingText = value.substring(0, value.length() - (valueCharacters - numberOfCharacters)) + " " + attachText;
		}

		return remainingText;
	}

	private static final int SECOND_MILLIS = 1000;
	private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
	private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
	private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
	

	public static String getTimeAgo(long time) {
	    if (time < 1000000000000L) {
	        // if timestamp given in seconds, convert to millis
	        time *= 1000;
	    }

	    long now = System.currentTimeMillis();;
	    if (time > now || time <= 0) {
	        return null;
	    }

	    // TODO: localize
	    final long diff = now - time;
	    if (diff < MINUTE_MILLIS) {
	        return "just now";
	    } else if (diff < 2 * MINUTE_MILLIS) {
	        return "1 minute ago";
	    } else if (diff < 50 * MINUTE_MILLIS) {
	        return " "+ diff / MINUTE_MILLIS + " minutes ago";
	    } else if (diff < 90 * MINUTE_MILLIS) {
	        return "an hour ago";
	    } else if (diff < 24 * HOUR_MILLIS) {
	        return ""+ diff / HOUR_MILLIS + " hours ago";
	    } else if (diff < 48 * HOUR_MILLIS) {
	        return "yesterday";
	    } else {
	        return ""+diff / DAY_MILLIS + " days ago";
	    }
	}

	public static boolean getFileExt(String FileName)
	{    
		String ext = FileName.substring((FileName.lastIndexOf(".") + 1), FileName.length());
		ArrayList<String> listOfExtension = new ArrayList<>();

		listOfExtension.add("pdf");
		listOfExtension.add("doc");
		listOfExtension.add("docx");
		listOfExtension.add("ppt");
		listOfExtension.add("pptx");
		listOfExtension.add("xls");
		listOfExtension.add("xlsx");  
		if(listOfExtension.contains(ext))
		{
			 return true;
		}
		return false;
	    
	}
	

	public static boolean getVideoExtension(String FileName)
	{
		boolean isVideo = false;
		String ext = FileName.substring((FileName.lastIndexOf(".") + 1), FileName.length());
		if (ext.equalsIgnoreCase("mp4")||ext.equalsIgnoreCase("3gp")||ext.equalsIgnoreCase("mpeg")||ext.equalsIgnoreCase("flv")) {
			
			isVideo = true;
			}
		
		
		return isVideo;
	}
	public static String getFileExtName(String FileName)
	{    
		String ext = FileName.substring((FileName.lastIndexOf(".") + 1), FileName.length());
		
		return ext;
	    
	}
	public static long getLongTime(String timeDate)
	{
		String toParse = timeDate; // Results in 2014-10-28 06:39:56
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD hh:mm:ss"); // I assume d-M, you may refer to M-d for month-day instead.
		Date date = null;
		try {
			date = formatter.parse(toParse);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // You will need try/catch around this
		long millis = date.getTime();
		
		return millis;
	}
	

	public static String getNewsType(String type)
	{
		String value = "";
		if (type.equals("1")) 
			value = "London";
		else if(type.equals("2"))
				value="Birmingham";
		else if(type.equals("3"))
			value="Manchester";
		else if(type.equals("4"))
			value="Business";
		else if(type.equals("5"))
			value="Sports";
		else if(type.equals("6"))
			value="Entertainment";
		
		return value;
		
	}
	
	public static String getRealPathFromURI(Context context, Uri contentURI)
	{
	    Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
	    if (cursor == null) { // Source is Dropbox or other similar local file path
	        return contentURI.getPath();
	    } else { 
	        cursor.moveToFirst(); 
	        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
	        return cursor.getString(idx); 
	    }
	}
	
	
	
	public static void SharLink(Context context, String Subject, String url)
	{
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_SUBJECT, Subject);
		i.putExtra(Intent.EXTRA_TEXT, url);
		context.startActivity(Intent.createChooser(i, "Share Link"));
	}
	
	public final static boolean isValidEmail(CharSequence target) {
		  return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	
	
	public static String getPath(final Context context, final Uri uri) {

	    final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

	    // DocumentProvider
	    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
	        // ExternalStorageProvider
	        if (isExternalStorageDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            if ("primary".equalsIgnoreCase(type)) {
	                return Environment.getExternalStorageDirectory() + "/" + split[1];
	            }

	            // TODO handle non-primary volumes
	        }
	        // DownloadsProvider
	        else if (isDownloadsDocument(uri)) {

	            final String id = DocumentsContract.getDocumentId(uri);
	            final Uri contentUri = ContentUris.withAppendedId(
	                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

	            return getDataColumn(context, contentUri, null, null);
	        }
	        // MediaProvider
	        else if (isMediaDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            Uri contentUri = null;
	            if ("image".equals(type)) {
	                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	            } else if ("video".equals(type)) {
	                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
	            } else if ("audio".equals(type)) {
	                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
	            }

	            final String selection = "_id=?";
	            final String[] selectionArgs = new String[] {
	                    split[1]
	            };

	            return getDataColumn(context, contentUri, selection, selectionArgs);
	        }
	    }
	    // MediaStore (and general)
	    else if ("content".equalsIgnoreCase(uri.getScheme())) {
	        return getDataColumn(context, uri, null, null);
	    }
	    // File
	    else if ("file".equalsIgnoreCase(uri.getScheme())) {
	        return uri.getPath();
	    }

	    return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @param selection (Optional) Filter used in the query.
	 * @param selectionArgs (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

	    Cursor cursor = null;
	    final String column = "_data";
	    final String[] projection = {
	            column
	    };

	    try {
	        cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
	                null);
	        if (cursor != null && cursor.moveToFirst()) {
	            final int column_index = cursor.getColumnIndexOrThrow(column);
	            return cursor.getString(column_index);
	        }
	    } finally {
	        if (cursor != null)
	            cursor.close();
	    }
	    return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
	    return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
	    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
	    return "com.android.providers.media.documents".equals(uri.getAuthority());
	}
	
	
	
	public static boolean getFilesize(String fileName)
	{
		File file = new File(fileName);

		// Get length of file in bytes
		long fileSizeInBytes = file.length();
		// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
		long fileSizeInKB = fileSizeInBytes / 1024;
		// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
		long fileSizeInMB = fileSizeInKB / 1024;
		if (fileSizeInMB > 11) {
			  
			return false;
			}
		
		return true;
		
		
	}

	public static void showExitDialog(final Activity context, String message, final Active active) {
		final AlertDialog.Builder builder =
				new AlertDialog.Builder(context);
		builder.setTitle("Alert!");
		builder.setMessage(message);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
                CRUD crud = new CRUD(context);
                crud.add(active);
				if (ViewController.mAudioRecorder != null) {
					ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
						@Override
						public void onPaused(String activeRecordFileName) {
							ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, context);
							final RecorderApplication application = RecorderApplication.getApplication(context);
							ViewController.mAudioRecorder = application.getRecorder();
							application.setRecorder(null);
							ViewController.mAudioRecorder = null;
						}

						@Override
						public void onException(Exception e) {

						}
					});
				}
                Intent intent = new Intent(context, ActivityHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				dialogInterface.dismiss();

			}
		});

		builder.show();
	}
}
