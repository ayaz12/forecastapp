package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/5/2017.
 */

public class Brand {
    private int code;
    private String name;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
