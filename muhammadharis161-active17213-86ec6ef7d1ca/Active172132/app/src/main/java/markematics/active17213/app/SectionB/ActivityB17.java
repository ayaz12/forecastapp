package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.SpinnerB18Adapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 10/9/2017.
 */

public class ActivityB17 extends AppCompatActivity {

    TextView tViewB17;
    Button btnB17;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    EditText editTextB17;
    String etB17;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b17);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B17");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB17 = (TextView) findViewById(R.id.tViewB17);
        btnB17 = (Button) findViewById(R.id.btnB17);
        editTextB17=(EditText)findViewById(R.id.editTextB17);
       btnB17.setOnClickListener(btnNext_OnClickListener);

        new AsyncHandler(REQUEST_RENDER).execute();


    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void next() {

        initialize();
        if (validationSuccess()) {
            add();
            proceed();
        } else {


        }

    }

    private boolean validationSuccess() {

        if (editTextB17.getText().toString().isEmpty())
        {
            editTextB17.setError("Enter");
            return false;
        }

         return true;
    }

    private void proceed() {
        Intent intent = new Intent(ActivityB17.this, ActivityB18.class);
        intent.putExtra("active", active);
        startActivity(intent);
    }

    private void initialize() {
        etB17 = editTextB17.getText().toString().trim();


    }
    private void add(){
        active.setQuestionB17(editTextB17.getText().toString().trim());

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB17.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            final String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                           next();
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

                            tViewB17.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");

                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
