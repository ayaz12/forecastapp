package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;


/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB5 extends AppCompatActivity {

    TextView tViewB5,tViewIns;
    Spinner spinB1;
    LinearLayout b5_Layout;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    List<QuestionB4> questionB5s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b5);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B5");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewB5 = (TextView) findViewById(R.id.tViewB5);
        //   spinB1 = (Spinner)findViewById(R.id.spinB1);
        b5_Layout = (LinearLayout) findViewById(R.id.b5_layout);
        tViewIns = (TextView)findViewById(R.id.lblInsB5);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };


    private void renderB1B2B3Layout() {
        b5_Layout.removeAllViews();
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listType);

        List<Brand> brands = new ArrayList<>();
        for (int i =0;i<questionB4s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB4s.get(i).getCode());
            brand.setName(questionB4s.get(i).getBrand());
            brands.add(brand);
        }

        for (int i = 0; i <brands.size(); i++) {

                    View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
                    TextView label = (TextView) v.findViewById(R.id.lbl);
                    EditText editText = (EditText) v.findViewById(R.id.editText);
                    CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);


                    label.setTypeface(tf);
                    label.setText(brands.get(i).getName());
                    label.setTypeface(tf);
                    label.setTag(brands.get(i).getCode());
                    chkBox1.setTag(v);


                    if (brands.get(i).getName().equalsIgnoreCase("Others")) {
                        editText.setVisibility(View.VISIBLE);
                    }
                    b5_Layout.addView(v);
                    if (i % 2 == 0) {
                        v.setBackgroundColor(Color.parseColor("#E0E0E0"));
                    }
                }

    }


    private void add()
    {

        questionB5s = new ArrayList<>();
        for (int i = 0; i < b5_Layout.getChildCount(); i++) {
            View v = b5_Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);


            if (chkBox1.isChecked())
            {
                QuestionB4 questionB1 = new QuestionB4();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB5s.add(questionB1);
            }
        }


        active.setQuestionB5(new Gson().toJson(questionB5s));
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validate()
    {

        if (questionB5s.size() == 0)
        {
            showDialog("Please select at least one option B5");
            return false;
        }
        return true;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB5.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate()) {
                                if (questionB5s.size() > 1) {
                                    Intent intent = new Intent(ActivityB5.this, ActivityB6.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                } else {
                                    active.setQuestionB6(new Gson().toJson(questionB5s));
                                    Intent intent = new Intent(ActivityB5.this, ActivityB7.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }
                            }
                        }
                        else
                        {
                            tf = Typeface.createFromAsset(getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB5.setTypeface(tf);
                            tViewIns.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");
                            renderB1B2B3Layout();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
