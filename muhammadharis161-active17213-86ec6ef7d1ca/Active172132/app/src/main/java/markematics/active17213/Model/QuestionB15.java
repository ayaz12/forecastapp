package markematics.active17213.Model;

/**
 * Created by mas on 10/9/2017.
 */

public class QuestionB15 {

    private int brandCode;
    private String brand;
    private String Statement;
    private String openEnded;

    public String getOpenEnded() {
        return openEnded;
    }

    public void setOpenEnded(String openEnded) {
        this.openEnded = openEnded;
    }

    public int getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(int brandCode) {
        this.brandCode = brandCode;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getStatement() {
        return Statement;
    }

    public void setStatement(String statement) {
        Statement = statement;
    }
}
