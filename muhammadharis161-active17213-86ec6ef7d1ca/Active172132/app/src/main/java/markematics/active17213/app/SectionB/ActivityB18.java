package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB18;
import markematics.active17213.Model.QuestionB4;


/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB18 extends AppCompatActivity {

    TextView tViewB18,lbl1,lbl2,lbl3,lbl4,lbl5;
    LinearLayout b18_Layout;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB18> questionB18s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b18);

        lbl1 = (TextView)findViewById(R.id.lbl1);
        lbl2 = (TextView)findViewById(R.id.lbl2);
        lbl3 = (TextView)findViewById(R.id.lbl3);
        lbl4 = (TextView)findViewById(R.id.lbl4);
        lbl5 = (TextView)findViewById(R.id.lbl5);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B18");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB18 = (TextView) findViewById(R.id.tViewB18);

        //   spinB1 = (Spinner)findViewById(R.id.spinB1);
        b18_Layout = (LinearLayout) findViewById(R.id.b18layout);

        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(btnNext_OnClickListener);
       new AsyncHandler(REQUEST_RENDER).execute();
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

           new AsyncHandler(REQUEST_ADD).execute();
        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox1_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            CheckBox chkBox5 = (CheckBox) v.findViewById(R.id.checkBox5);
            if (chkBox1.isChecked()) {
                chkBox2.setEnabled(false);
                chkBox3.setEnabled(false);
                chkBox4.setEnabled(false);
                chkBox5.setEnabled(false);
                for (int i = 0; i < b18_Layout.getChildCount(); i++) {
                    View childView = b18_Layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < b18_Layout.getChildCount(); i++) {
                    View childView = b18_Layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }
                chkBox2.setEnabled(true);
                chkBox3.setEnabled(true);
                chkBox4.setEnabled(true);
                chkBox5.setEnabled(true);
            }

        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox2_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            CheckBox chkBox5 = (CheckBox) v.findViewById(R.id.checkBox5);
            if (chkBox2.isChecked()) {
                chkBox1.setEnabled(false);
                chkBox3.setEnabled(false);
                chkBox4.setEnabled(false);
                chkBox5.setEnabled(false);

            } else {
                chkBox1.setEnabled(true);
                chkBox3.setEnabled(true);
                chkBox4.setEnabled(true);
                chkBox5.setEnabled(true);
            }

        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox3_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            CheckBox chkBox5 = (CheckBox) v.findViewById(R.id.checkBox5);
            if (chkBox3.isChecked()) {
                chkBox1.setEnabled(false);
                chkBox2.setEnabled(false);
                chkBox4.setEnabled(false);
                chkBox5.setEnabled(false);

            } else {
                chkBox1.setEnabled(true);
                chkBox2.setEnabled(true);
                chkBox4.setEnabled(true);
                chkBox5.setEnabled(true);;
            }

        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox4_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            CheckBox chkBox5 = (CheckBox) v.findViewById(R.id.checkBox5);
            if (chkBox4.isChecked()) {
                chkBox1.setEnabled(false);
                chkBox2.setEnabled(false);
                chkBox3.setEnabled(false);
                chkBox5.setEnabled(false);

            } else {
                chkBox1.setEnabled(true);
                chkBox2.setEnabled(true);
                chkBox3.setEnabled(true);
                chkBox5.setEnabled(true);;
            }

        }
    };
    private CompoundButton.OnCheckedChangeListener checkBox5_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            CheckBox chkBox5 = (CheckBox) v.findViewById(R.id.checkBox5);
            if (chkBox5.isChecked()) {
                chkBox1.setEnabled(false);
                chkBox2.setEnabled(false);
                chkBox3.setEnabled(false);
                chkBox4.setEnabled(false);

            } else {
                chkBox1.setEnabled(true);
                chkBox2.setEnabled(true);
                chkBox3.setEnabled(true);
                chkBox4.setEnabled(true);;
            }

        }
    };
    private void renderB18Layout() {

        b18_Layout.removeAllViews();
        String[] arrayB1 = getResources().getStringArray(R.array.arrayB2);
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
        List<Brand> brands = new ArrayList<>();
        Brand brand ;

        for (int i = 0; i < questionB1s.size(); i++) {
            brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB1s.get(i).getOpenEnded() : questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < questionB2s.size(); i++) {
            brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB2s.get(i).getOpenEnded() : questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < questionB3s.size(); i++) {
            brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB3s.get(i).getOpenEnded() : questionB3s.get(i).getBrand());
            brands.add(brand);
        }
        brand = new Brand();
        brand.setName("None");
        brand.setCode(100);
        brands.add(brand);
        for (int i = 0; i < brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.b18_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            CheckBox chkBox5 = (CheckBox) v.findViewById(R.id.checkBox5);
            label.setTypeface(tf);
            label.setText(brands.get(i).getName());
            label.setTypeface(tf);
            label.setTag(brands.get(i).getCode());
            chkBox1.setTag(v);
            chkBox2.setTag(v);
            chkBox3.setTag(v);
            chkBox4.setTag(v);
            chkBox5.setTag(v);
            chkBox1.setOnCheckedChangeListener(checkBox1_OnCheckedChangeListener);
            chkBox2.setOnCheckedChangeListener(checkBox2_OnCheckedChangeListener);
            chkBox3.setOnCheckedChangeListener(checkBox3_OnCheckedChangeListener);
            chkBox4.setOnCheckedChangeListener(checkBox4_OnCheckedChangeListener);
            chkBox5.setOnCheckedChangeListener(checkBox5_OnCheckedChangeListener);

            b18_Layout.addView(v);
            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
    }


    private void add()
    {

        questionB18s = new ArrayList<>();

        for (int i = 0; i < b18_Layout.getChildCount(); i++) {
            View v = b18_Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);

            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            CheckBox chkBox5 = (CheckBox) v.findViewById(R.id.checkBox5);
            if (chkBox1.isChecked())
            {
                QuestionB18 questionB1 = new QuestionB18();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setStatement(""+1);
                questionB18s.add(questionB1);
            }
            if (chkBox2.isChecked())
            {
                QuestionB18 questionB1 = new QuestionB18();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setStatement(""+2);
                questionB18s.add(questionB1);
            }
            if (chkBox3.isChecked())
            {
                QuestionB18 questionB1 = new QuestionB18();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setStatement(""+3);
                questionB18s.add(questionB1);
            }
            if (chkBox4.isChecked())
            {
                QuestionB18 questionB1 = new QuestionB18();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setStatement(""+4);
                questionB18s.add(questionB1);
            }
            if (chkBox5.isChecked())
            {
                QuestionB18 questionB1 = new QuestionB18();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setStatement(""+5);
                questionB18s.add(questionB1);
            }
        }

        active.setQuestionB18(new Gson().toJson(questionB18s));

    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validate()
    {
//        if (questionB18s.size() == 0)
//        {
//            showDialog("Please select at least one option B18");
//            return false;
//        }
        boolean isValid = false;
        for (int i = 0;i<b18_Layout.getChildCount();i++)
        {
            View v = b18_Layout.getChildAt(i);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            CheckBox chkBox2 = (CheckBox) v.findViewById(R.id.checkBox2);
            CheckBox chkBox3 = (CheckBox) v.findViewById(R.id.checkBox3);
            CheckBox chkBox4 = (CheckBox) v.findViewById(R.id.checkBox4);
            CheckBox chkBox5 = (CheckBox) v.findViewById(R.id.checkBox5);
            if (chkBox1.isChecked() || chkBox2.isChecked() || chkBox3.isChecked() || chkBox4.isChecked() || chkBox5.isChecked()) {
                isValid = true;
            }
            else
            {
                return false;
            }


        }

        return isValid;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB18.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            final String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate())
                            {
                                add();
                                Intent intent = new Intent(ActivityB18.this, ActivityB19B20.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                            else
                            {
                                Toast.makeText(ActivityB18.this,"Please select all options B18",Toast.LENGTH_LONG).show();
                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB18.setTypeface(tf);
                            lbl1.setTypeface(tf);
                            lbl2.setTypeface(tf);
                            lbl3.setTypeface(tf);
                            lbl4.setTypeface(tf);
                            lbl5.setTypeface(tf);

                            active = (Active) getIntent().getSerializableExtra("active");
                           renderB18Layout();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
