package markematics.active17213.Model;

/**
 * Created by mas on 10/5/2017.
 */

public class QuestionE3 {
    String statement;
    String label;
    int code;
    String openEnded;

    public String getOpenEnded() {
        return openEnded;
    }

    public void setOpenEnded(String openEnded) {
        this.openEnded = openEnded;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
