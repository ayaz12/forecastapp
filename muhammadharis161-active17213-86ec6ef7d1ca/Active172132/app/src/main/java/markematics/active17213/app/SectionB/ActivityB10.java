package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionB10;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB10 extends AppCompatActivity {

    TextView tViewB10;

    Spinner spinnerb11;
    Button btnNext;
    Typeface tf;
    View view;
    Toolbar toolbar;
    Active active;
    LinearLayout b10Layout;
    List<QuestionB10> questionB10s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b10);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B10");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB10 = (TextView) findViewById(R.id.tViewB10);
        b10Layout = (LinearLayout) findViewById(R.id.layout10);
        btnNext = (Button) findViewById(R.id.btnNext);


        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void renderB10Layout() {
        b10Layout.removeAllViews();
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);

        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText) findViewById(R.id.editTextB10);
            lbl.setText(questionB5s.get(i).getBrand());
            lbl.setTag(questionB5s.get(i).getCode());
            b10Layout.addView(v);
        }
    }

    private void add() {
        questionB10s = new ArrayList<>();
        for (int i = 0; i < b10Layout.getChildCount(); i++)

        {
            View v = b10Layout.getChildAt(i);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText) findViewById(R.id.editTextB10);
            QuestionB10 questionB10 = new QuestionB10();
            questionB10.setCode((int) lbl.getTag());
            questionB10.setBrand(lbl.getText().toString());
            questionB10.setReason(editText.getText().toString());
            questionB10s.add(questionB10);

        }
        active.setQuestionB10(new Gson().toJson(questionB10s));
    }

    private boolean validate() {
        for (int i = 0; i < b10Layout.getChildCount(); i++) {
            View v = b10Layout.getChildAt(i);

            EditText editText = (EditText)v. findViewById(R.id.editTextB10);


            if (editText.getText().toString().isEmpty()) {
                showDialog("Please Enter Reasons");
                return false;
            }

        }


        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB10.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate()) {
                                add();
                                Intent intent = new Intent(ActivityB10.this, ActivityB11.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB10.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");
                            renderB10Layout();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
