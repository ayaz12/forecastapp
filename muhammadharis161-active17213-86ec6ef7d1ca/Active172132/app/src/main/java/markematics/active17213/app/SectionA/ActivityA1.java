package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.github.lassana.recorder.AudioRecorderBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionA1;
import markematics.active17213.Model.QuestionS12;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionB.ActivityB1;
import markematics.active17213.app.SectionB.ActivityB2;


public class ActivityA1 extends AppCompatActivity {

    TextView tViewA1;
    LinearLayout a1_layout;
    Typeface tf;
    Button btnA1;
    List<QuestionA1>questionA1s;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A1");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewA1 = (TextView) findViewById(R.id.tViewA1);
        a1_layout = (LinearLayout) findViewById(R.id.a1_layout);

        btnA1=(Button)findViewById(R.id.btnA1);
        btnA1.setOnClickListener(btnA1_OnClickListener);
       new AsyncHandler(REQUEST_RENDER).execute();

    }
    private void StartRecording(Active active) {

        if (ViewController.mAudioRecorder == null)
        {
            String filePath = active.gethR_ID()+"_"+active.getCity()+"_SecA"+"_"+System.currentTimeMillis();
            active.setAudioPathSecA(filePath);
            recordAudio(filePath);

        }
    }
    private View.OnClickListener btnA1_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void randerA1Layout() {
        a1_layout.removeAllViews();
        Type listType = new TypeToken<ArrayList<QuestionS12>>() {
        }.getType();
        List<QuestionS12> questionS12s = new Gson().fromJson(active.getQuestionS12(), listType);

        for (int i=0;i<questionS12s.size();i++) {
            View v = getLayoutInflater().inflate(R.layout.a1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lb2);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);
            label.setTypeface(tf);
            ArrayAdapter<String> spinnerA1ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayA1));
            spinnerLayoutA1.setAdapter(spinnerA1ArrayAdapter);
            label.setText(questionS12s.get(i).getStatement() + (questionS12s.get(i).getOpenEnded().isEmpty()?"":"("+questionS12s.get(i).getOpenEnded()+")"));
            a1_layout.addView(v);
        }
    }

    private void  add()
    {
        questionA1s = new ArrayList<>();
        for (int i=0;i<a1_layout.getChildCount();i++)
        {
            View v= a1_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lb2);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);
            if (spinnerLayoutA1.getSelectedItemPosition() !=0) {
                QuestionA1 questionA1 = new QuestionA1();
                questionA1.setLabel(label.getText().toString());
                questionA1.setStatement((String) spinnerLayoutA1.getSelectedItem());
                questionA1.setCode(spinnerLayoutA1.getSelectedItemPosition());
                questionA1s.add(questionA1);
            }

        }

        active.setQuestionA1(new Gson().toJson(questionA1s));
    }
    private boolean validate()
    {
        for (int i = 0;i<a1_layout.getChildCount();i++) {
            View v = a1_layout.getChildAt(i);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);



            if (spinnerLayoutA1.getSelectedItemPosition() == 0)
            {
                showDialog("Please fill all values");
                return false;
            }

        }

        if (questionA1s.size() == 0)
        {
            showDialog("Please Select At least one option");
            return false;
        }


        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private void recordAudio(String fileName) {


        final RecorderApplication application = RecorderApplication.getApplication(ActivityA1.this);
        ViewController.mAudioRecorder = application.getRecorder();
        if (ViewController.mAudioRecorder == null
                || ViewController.mAudioRecorder.getStatus() == AudioRecorder.Status.STATUS_UNKNOWN) {
            ViewController.mAudioRecorder = AudioRecorderBuilder.with(application)
                    .fileName(createDirectoryAndSaveFile(fileName))
                    .config(AudioRecorder.MediaRecorderConfig.DEFAULT)
                    .loggable()
                    .build();
            application.setRecorder(ViewController.mAudioRecorder);
        }
        start();
    }
    private void message(String message) {
        Toast.makeText(ActivityA1.this,message,Toast.LENGTH_SHORT).show();
    }
    private void start()
    {
        ViewController.mAudioRecorder .start(new AudioRecorder.OnStartListener() {
            @Override
            public void onStarted() {
                randerA1Layout();
            }

            @Override
            public void onException(Exception e) {

                //   invalidateViews();
                message(getString(R.string.error_audio_recorder, e));
            }
        });
    }
    private String createDirectoryAndSaveFile( String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.ActiveQuant/Audio/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.ActiveQuant/Audio/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.ActiveQuant/Audio/"), fileName+ ".mp3" );
        if (file.exists()) {
            file.delete();
        }

        return file.getPath();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA1.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate())
                            {
                                Intent intent = new Intent(ActivityA1.this, ActivityA2a.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewA1.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");

                            StartRecording(active);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
