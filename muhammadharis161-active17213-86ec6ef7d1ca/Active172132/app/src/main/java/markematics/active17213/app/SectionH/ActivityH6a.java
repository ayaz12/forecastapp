package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH6a;
import markematics.active17213.Model.QuestionH6b;


public class ActivityH6a extends AppCompatActivity {

    TextView tViewH6a;

    Button btnNext;
    LinearLayout H6alayout ;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionH6a> questionH6aList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h6a);

        tViewH6a = (TextView) findViewById(R.id.tViewH6a);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H6a");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        H6alayout = (LinearLayout) findViewById(R.id.H6a_layout);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH6a.setTypeface(tf);



        btnNext = (Button) findViewById(R.id.btnNext);
        randerH6aLayout();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });}


    private void next() {
        add();
        if (validationSuccess()) {

            proceed();
        } else {


        }


    }

    private boolean validationSuccess() {

        boolean valid = true;


        if (questionH6aList.size() == 0) {
            showDialog("Please select at least opne option H6a");
            return false;
        }


        return true;

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private void proceed() {
        Intent nextPage = new Intent(ActivityH6a.this, ActivityH6b.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }


    private void randerH6aLayout() {
        String[] arrayH6a = getResources().getStringArray(R.array.arrayH3b);
        for (int i = 0; i < arrayH6a.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH6a[i]);
            label.setTag(i+1);
            H6alayout.addView(v);
        }
    }

    private void add() {

        questionH6aList = new ArrayList<>();
        for (int j = 0; j < H6alayout.getChildCount(); j++) {
            View view = H6alayout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            QuestionH6a questionH6a = new QuestionH6a();

            if (checkBox.isChecked()) {
                questionH6a.setCode((int) labeled.getTag());
                questionH6a.setStatement(labeled.getText().toString());
                questionH6aList.add(questionH6a);
            }
        }
        active.setQuestionH6a(new Gson().toJson(questionH6aList));

    }
}






