package markematics.active17213.app.SectionG;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionC1;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionA.ActivityA11;
import markematics.active17213.app.SectionC.ActivityC1;
import markematics.active17213.app.SectionD.ActivityD1;
import markematics.active17213.app.SectionF.ActivityF7;
import markematics.active17213.app.SectionH.ActivityH1;
import markematics.active17213.app.SectionH.ActivityH1H2H3;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionG1;


public class ActivityG1 extends AppCompatActivity {
    TextView tViewG1,textViewStatement;
    LinearLayout g1_layout;
    Typeface tf;
    List<QuestionG1> questionG1s;
    Button btnNext,btnBack;
    Toolbar toolbar;
    Active active;
    String[] gArray;
    int count = 0;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g1);
        tViewG1 = (TextView) findViewById(R.id.tViewG1);
        textViewStatement = (TextView)findViewById(R.id.tViewStatement);
        g1_layout = (LinearLayout) findViewById(R.id.g1_layout);
        btnBack = (Button)findViewById(R.id.btnBack);
        btnNext = (Button) findViewById(R.id.btnNext);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question G1");
        toolbar.setTitleTextColor(Color.WHITE);
        gArray = getResources().getStringArray(R.array.arrayG1);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        active = (Active) getIntent().getSerializableExtra("active");
        tViewG1.setTypeface(tf);
        questionG1s = createList();
        renderLayout(0);

        btnBack.setOnClickListener(btnBack_OnClickListener);
        btnNext.setOnClickListener(btnNext_OnClickListener);
    }



    private List<QuestionG1> createList()
    {
        List<QuestionG1> questionG1ArrayList = new ArrayList<>();
        for (int i = 0; i< gArray.length; i++)
        {
            QuestionG1 questionG1 = new QuestionG1();
            questionG1.setCode(i+1);
            questionG1.setStatement(gArray[i]);
            questionG1ArrayList.add(questionG1);
        }
        return questionG1ArrayList;
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (count == questionG1s.size()-1)
            {
//
                active.setQuestionG1(new Gson().toJson(questionG1s));
                StopRecording();
            }
            else {
                add();
                if (validate()) {

                    count++;
                    renderLayout(count);

                }
                else
                {
                    Toast.makeText(ActivityG1.this,"Please select at least one option",Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityG1.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityG1.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    Intent i = new Intent(ActivityG1.this,ActivityH1.class);
                    i.putExtra("active",active);
                    startActivity(i);
                    finish();
                    Toast.makeText(ActivityG1.this,"Section G Completed",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private View.OnClickListener btnBack_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            count--;
            renderLayout(count);
        }
    };

    private void renderLayout(int position) {

        g1_layout.removeAllViews();
        textViewStatement.setTypeface(tf);
        textViewStatement.setText(gArray[position]);
        View v = getLayoutInflater().inflate(R.layout.g1_layout, null);
        Spinner spinner = (Spinner)v.findViewById(R.id.spinnerG1) ;
        ArrayAdapter<String> spinnerCityArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.SpinnerG1));
        spinner.setAdapter(spinnerCityArrayAdapter);
        g1_layout.addView(v);

        if (count == 0)
        {
            btnBack.setVisibility(View.GONE);

        }
        else
        {
            btnBack.setVisibility(View.VISIBLE);

        }

    }


    private void add() {

        for (int i = 0; i < g1_layout.getChildCount(); i++) {
            View v = g1_layout.getChildAt(i);
            Spinner spinner = (Spinner)v.findViewById(R.id.spinnerG1) ;
            questionG1s.get(count).setSelectedCode(spinner.getSelectedItemPosition());
        }

    }

    private boolean validate() {
        for (int i = 0; i < g1_layout.getChildCount(); i++) {
            View v = g1_layout.getChildAt(i);
            Spinner spinner = (Spinner) v.findViewById(R.id.spinnerG1);
            if (spinner.getSelectedItemPosition() == 0) {
                return false;
            }
        }


        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

}
