package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.SpinnerB18Adapter;
import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB12;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB12 extends AppCompatActivity {
    TextView tViewb12, lblBrandName, tViewb14;
    LinearLayout b12Layout;
    Button btnB12;
    Spinner spinnerB14;
    Toolbar toolbar;
    Typeface tf;
    Active active;
    List<QuestionB12> questionB12s;

    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b12);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B12");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewb12 = (TextView) findViewById(R.id.tViewB12);
        b12Layout = (LinearLayout) findViewById(R.id.b12Layout);
        btnB12 = (Button) findViewById(R.id.btnNext);
        lblBrandName = (TextView)findViewById(R.id.lblBrandName);

        btnB12.setOnClickListener(btnNext_OnClickListener);


        new AsyncHandler(REQUEST_RENDER).execute();

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            new AsyncHandler(REQUEST_ADD).execute();

        }
    };

    private void renderB12Layout() {
        b12Layout.removeAllViews();
        List<Brand> brands = new ArrayList<>();
        Brand brand = new Brand();
        brand.setCode(0);
        brand.setName("Select");
        brands.add(brand);

        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);

        for (int i =0;i<questionB1s.size();i++)
        {
             brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB2s.size();i++)
        {
             brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB3s.size();i++)
        {
             brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
            brands.add(brand);
        }

        String[] arrayB12 = getResources().getStringArray(R.array.b12array);
        for (int i = 0; i < arrayB12.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b12_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            chkBox.setOnCheckedChangeListener(checkBox_OnCheckedChangeListener);
            chkBox.setTag(v);
            Spinner spinnerBrand = (Spinner)v.findViewById(R.id.spinnerBrand);


            SpinnerB18Adapter adapter = new SpinnerB18Adapter(ActivityB12.this, R.layout.spinner_item, R.id.txt, brands);
            spinnerBrand.setAdapter(adapter);

            RelativeLayout relativeLayout = (RelativeLayout)v.findViewById(R.id.brandLayout);
            if (i == 1)
            {
                relativeLayout.setVisibility(View.VISIBLE);
            }

                label.setTag(i+1);
                label.setText(arrayB12[i]);


            label.setTypeface(tf);
            b12Layout.addView(v);
        }
    }

    private CompoundButton.OnCheckedChangeListener checkBox_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            View v = (View) compoundButton.getTag();
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            if (chkBox1.isChecked()) {

                for (int i = 0; i < b12Layout.getChildCount(); i++) {
                    View childView = b12Layout.getChildAt(i);
                    TextView labelChild = (TextView) childView.findViewById(R.id.lbl);
                    CheckBox chkBoxChild = (CheckBox) childView.findViewById(R.id.checkBox1);
                    Spinner spinnerBrand = (Spinner) childView.findViewById(R.id.spinnerBrand);
                    if (!label.getText().toString().equalsIgnoreCase(labelChild.getText().toString())) {
                        chkBoxChild.setEnabled(false);
                        spinnerBrand.setEnabled(false);
                    }
                }
            }
            else
            {
                for (int i = 0; i < b12Layout.getChildCount(); i++) {
                    View childView = b12Layout.getChildAt(i);
                    TextView labelChild = (TextView) childView.findViewById(R.id.lbl);
                    CheckBox chkBoxChild = (CheckBox) childView.findViewById(R.id.checkBox1);
                    Spinner spinnerBrand = (Spinner) childView.findViewById(R.id.spinnerBrand);
                    if (!label.getText().toString().equalsIgnoreCase(labelChild.getText().toString())) {
                        chkBoxChild.setEnabled(true);
                        spinnerBrand.setEnabled(true);
                    }
                }
            }
        }
    };

    private void add() {
        questionB12s = new ArrayList<>();
        for (int i = 0; i < b12Layout.getChildCount(); i++) {
            View v = b12Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            Spinner spinnerBrand = (Spinner)v.findViewById(R.id.spinnerBrand);
            if (chkBox.isChecked())
            {

                QuestionB12 questionB12 = new QuestionB12();
                questionB12.setCode((int)label.getTag());
                questionB12.setStatement(label.getText().toString());
                if ((int)label.getTag() == 2) {
                    Brand brand = (Brand) spinnerBrand.getSelectedItem();
                    questionB12.setOpenEnded(brand.getName());
                }
                else
                {
                    questionB12.setOpenEnded("");
                }
                questionB12s.add(questionB12);
            }


        }

        active.setQuestionB12(new Gson().toJson(questionB12s));

    }
    private boolean validate()
    {
        for (int i =0;i<questionB12s.size();i++)
        {
            if (questionB12s.get(i).getOpenEnded().equalsIgnoreCase("Select"))
            {
                showDialog("Please fill all values");
                return false;
            }
        }
        if (questionB12s.size() == 0)
        {
            showDialog("Please select at least one option");
            return false;
        }




        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }


        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB12.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate()) {
                                if (questionB12s.get(0).getOpenEnded().isEmpty()) {
                                    Intent intent = new Intent(ActivityB12.this, ActivityB14.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(ActivityB12.this, ActivityB13.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }

                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewb12.setTypeface(tf);
                            lblBrandName.setTypeface(tf);
                            active = (Active)getIntent().getSerializableExtra("active");
                            Type listType = new TypeToken<ArrayList<QuestionB4>>() {
                            }.getType();
                            List<QuestionB4> questionB6s = new Gson().fromJson(active.getQuestionB6(), listType);
                            lblBrandName.setText(questionB6s.get(0).getBrand()+" برانڈ کا نام ");
                            renderB12Layout();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
