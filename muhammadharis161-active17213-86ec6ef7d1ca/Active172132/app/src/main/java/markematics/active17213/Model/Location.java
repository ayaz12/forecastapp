package markematics.active17213.Model;

import android.database.Cursor;

/**
 * Created by Haris on 11/14/2016.
 */

public class Location {

    private int id;
    private String hrId;
    private double lat;
    private double lng;
    private String date;
    private String userName;
    private String city;


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHrId() {
        return hrId;
    }

    public void setHrId(String hrId) {
        this.hrId = hrId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



    public static String TABLE_NAME = "tLocation";
    public static String FIELD_ID = "Id";
    public static String FIELD_UID = "hrId";
    public static String FIELD_LAT = "lat";
    public static String FIELD_LNG = "lng";
    public static String FIELD_DATE = "date";
    public static String FIELD_USERNAME = "userName";
    public static String FIELD_CITY = "city";



    public static String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + FIELD_UID + " TEXT,"
            + FIELD_LAT + " TEXT,"
            + FIELD_LNG + " TEXT,"
            + FIELD_DATE + " TEXT,"
             + FIELD_USERNAME + " TEXT,"
            + FIELD_CITY + " TEXT"
            + ")";

    public static String DROP_LOCATION_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;


    public static String SELECT_LOCATION_QUERY = "SELECT * FROM " + TABLE_NAME;



    public static String getLocation(String rId,String Lat,String Lng,String userId)
    {
        String SELECT_LAST_RECORD = "SELECT * FROM " + TABLE_NAME
                + " WHERE "
                + FIELD_LAT + " = '"+Lat+"' AND "
                + FIELD_LNG + " = '"+Lng+"' AND "
                + FIELD_UID + " = '"+userId+"'";
        return SELECT_LAST_RECORD;
    }
    public static String getLastVolumePrice()
    {
        String SELECT_LAST_RECORD = "SELECT * FROM " + TABLE_NAME
                + " ORDER BY "
                + FIELD_LAT + " DESC LIMIT 1";
        return SELECT_LAST_RECORD;
    }

    public static String getLastTwoUser()
    {
        String SELECT_LAST_RECORD = "SELECT * FROM " + TABLE_NAME
                + " ORDER BY "
                + FIELD_LAT + " DESC LIMIT 2";
        return SELECT_LAST_RECORD;
    }
    public static Location ConvertToLocation(Cursor cursor) {
        Location location = new Location();

        location.setId(cursor.getInt(0));
        location.setHrId(cursor.getString(1));
        location.setLat(Double.parseDouble(cursor.getString(2)));
        location.setLng(Double.parseDouble(cursor.getString(3)));
        location.setDate(cursor.getString(4));
        location.setUserName(cursor.getString(5));
        location.setCity(cursor.getString(6));

        return location;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
