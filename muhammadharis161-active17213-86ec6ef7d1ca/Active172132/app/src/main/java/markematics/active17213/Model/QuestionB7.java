package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/6/2017.
 */

public class QuestionB7 {

    private int code;
    private int brandCode;
    private String brand;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(int brandCode) {
        this.brandCode = brandCode;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
