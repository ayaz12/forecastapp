package markematics.active17213.app.SectionF;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.github.lassana.recorder.AudioRecorderBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionF2;
import markematics.active17213.Model.QuestionF3;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionE.ActivityE1E2;
import markematics.active17213.app.SectionG.ActivityG1;
import markematics.active17213.app.SectionR.ActivityR1;
import markematics.active17213.app.SectionR.ActivityR2;


public class ActivityF1F2 extends AppCompatActivity {
    TextView tViewF1, tViewF2;
    LinearLayout F2layout;
    Button btnNext;
    Typeface tf;
    Spinner spinnerF1;
    Toolbar toolbar;
    Active active;
    List<QuestionF2> questionF2s;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f1_f2_f3);

        tViewF1 = (TextView) findViewById(R.id.tViewF1);
        tViewF2 = (TextView) findViewById(R.id.tViewF2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question F1 F2");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        F2layout = (LinearLayout) findViewById(R.id.F2_layout);

        btnNext = (Button) findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewF1.setTypeface(tf);
        tViewF2.setTypeface(tf);

        spinnerF1 = (Spinner) findViewById(R.id.spinnerf1);
        ArrayAdapter<String> spinnerF1ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayf1));
        spinnerF1.setAdapter(spinnerF1ArrayAdapter);


        StartRecording();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
        spinnerF1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    if (position == 1)
                    {
                        tViewF2.setVisibility(View.VISIBLE);

                        F2layout.setVisibility(View.VISIBLE);


                    }
                    else
                    {
                        tViewF2.setVisibility(View.GONE);

                        F2layout.setVisibility(View.GONE);

                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }
    private void StartRecording()
    {
        if (ViewController.mAudioRecorder == null)
        {
            String filePath = active.gethR_ID()+"_"+active.getCity()+"_SecFGR_"+System.currentTimeMillis();
            active.setAudioPathSecF(filePath);
            recordAudio(filePath);

        }
    }
    private void recordAudio(String fileName) {


        final RecorderApplication application = RecorderApplication.getApplication(ActivityF1F2.this);
        ViewController.mAudioRecorder = application.getRecorder();
        if (ViewController.mAudioRecorder == null
                || ViewController.mAudioRecorder.getStatus() == AudioRecorder.Status.STATUS_UNKNOWN) {
            ViewController.mAudioRecorder = AudioRecorderBuilder.with(application)
                    .fileName(createDirectoryAndSaveFile(fileName))
                    .config(AudioRecorder.MediaRecorderConfig.DEFAULT)
                    .loggable()
                    .build();
            application.setRecorder(ViewController.mAudioRecorder);
        }
        start();
    }
    private void message(String message) {
        Toast.makeText(ActivityF1F2.this,message,Toast.LENGTH_SHORT).show();
    }
    private void start()
    {
        ViewController.mAudioRecorder .start(new AudioRecorder.OnStartListener() {
            @Override
            public void onStarted() {
                renderF2Layout();
            }

            @Override
            public void onException(Exception e) {

                //   invalidateViews();
                message(getString(R.string.error_audio_recorder, e));
            }
        });
    }
    private String createDirectoryAndSaveFile( String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.ActiveQuant/Audio/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.ActiveQuant/Audio/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.ActiveQuant/Audio/"), fileName+ ".mp3" );
        if (file.exists()) {
            file.delete();
        }

        return file.getPath();
    }
    private void next() {

        add();
        if (validate()) {
            if (spinnerF1.getSelectedItemPosition() == 2) {
                Type listType = new TypeToken<ArrayList<QuestionB1>>() {
                }.getType();
                List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
                List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
                List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
                List<Brand> brands = new ArrayList<>();
                for (int i =0;i<questionB1s.size();i++)
                {
                    Brand brand = new Brand();
                    brand.setCode(questionB1s.get(i).getCode());
                    brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
                            questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
                    brands.add(brand);
                }
                for (int i =0;i<questionB2s.size();i++)
                {
                    Brand brand = new Brand();
                    brand.setCode(questionB2s.get(i).getCode());
                    brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
                            questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
                    brands.add(brand);
                }
                for (int i =0;i<questionB3s.size();i++)
                {
                    Brand brand = new Brand();
                    brand.setCode(questionB3s.get(i).getCode());
                    brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
                            questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
                    brands.add(brand);
                }
                boolean isCanbebe = false;
                for (int i = 0;i<brands.size();i++)
                {
                    if (brands.get(i).getName().equalsIgnoreCase("Canbebe"))
                    {
                        isCanbebe = true;
                    }
                }
                if (isCanbebe)
                {
                    Type listTypeB4 = new TypeToken<ArrayList<QuestionB4>>() {
                    }.getType();
                    List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listTypeB4);
                    boolean isCanbebeB4 = false;
                    for (int i = 0; i< questionB4s.size(); i++)
                    {
                        if (questionB4s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                        {
                            isCanbebeB4 = true;
                        }
                    }
                    if (isCanbebeB4)
                    {
                        boolean isCanbebeB5 = false;
                        Type listTypeB5 = new TypeToken<ArrayList<QuestionB4>>() {
                        }.getType();
                        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listTypeB5);

                        for (int i = 0; i< questionB5s.size(); i++)
                        {
                            if (questionB5s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
                            {
                                isCanbebeB5 = true;

                            }
                        }
                        if (isCanbebeB5)
                        {
                            Intent nextPage = new Intent(ActivityF1F2.this, ActivityG1.class);
                            nextPage.putExtra("active", active);
                            startActivity(nextPage);
                            return;
                        }
                        else
                        {
                            Intent nextPage = new Intent(ActivityF1F2.this, ActivityR2.class);
                            nextPage.putExtra("active", active);
                            startActivity(nextPage);
                            return;
                        }
                    }
                    else
                    {
                        Intent nextPage = new Intent(ActivityF1F2.this, ActivityR1.class);
                        nextPage.putExtra("active", active);
                        startActivity(nextPage);
                        return;
                    }
                }
                else
                {
                    Intent nextPage = new Intent(ActivityF1F2.this, ActivityG1.class);
                    nextPage.putExtra("active", active);
                    startActivity(nextPage);
                    return;
                }
            } else {

                boolean isWipes= false;
                for (int i =0;i<questionF2s.size();i++)
                {
                    if (questionF2s.get(i).getStatement().equalsIgnoreCase("(Wipes) وا ئپس"))
                    {
                        isWipes = true;
                    }
                }
                if (!isWipes) {
//                    Type listType = new TypeToken<ArrayList<QuestionB1>>() {
//                    }.getType();
//                    List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
//                    List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
//                    List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
//                    List<Brand> brands = new ArrayList<>();
//                    for (int i =0;i<questionB1s.size();i++)
//                    {
//                        Brand brand = new Brand();
//                        brand.setCode(questionB1s.get(i).getCode());
//                        brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
//                                questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
//                        brands.add(brand);
//                    }
//                    for (int i =0;i<questionB2s.size();i++)
//                    {
//                        Brand brand = new Brand();
//                        brand.setCode(questionB2s.get(i).getCode());
//                        brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
//                                questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
//                        brands.add(brand);
//                    }
//                    for (int i =0;i<questionB3s.size();i++)
//                    {
//                        Brand brand = new Brand();
//                        brand.setCode(questionB3s.get(i).getCode());
//                        brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
//                                questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
//                        brands.add(brand);
//                    }
//                    boolean isCanbebe = false;
//                    for (int i = 0;i<brands.size();i++)
//                    {
//                        if (brands.get(i).getName().equalsIgnoreCase("Canbebe"))
//                        {
//                            isCanbebe = true;
//                        }
//                    }
//                    if (isCanbebe)
//                    {
//                        Type listTypeB4 = new TypeToken<ArrayList<QuestionB4>>() {
//                        }.getType();
//                        List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listTypeB4);
//                        boolean isCanbebeB4 = false;
//                        for (int i = 0; i< questionB4s.size(); i++)
//                        {
//                            if (questionB4s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
//                            {
//                                isCanbebeB4 = true;
//                            }
//                        }
//                        if (isCanbebeB4)
//                        {
//                            boolean isCanbebeB5 = false;
//                            Type listTypeB5 = new TypeToken<ArrayList<QuestionB4>>() {
//                            }.getType();
//                            List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listTypeB5);
//
//                            for (int i = 0; i< questionB5s.size(); i++)
//                            {
//                                if (questionB5s.get(i).getBrand().equalsIgnoreCase("Canbebe"))
//                                {
//                                    isCanbebeB5 = true;
//
//                                }
//                            }
//                            if (isCanbebeB5)
//                            {
//                                Intent nextPage = new Intent(ActivityF1F2.this, ActivityG1.class);
//                                nextPage.putExtra("active", active);
//                                startActivity(nextPage);
//                                return;
//                            }
//                            else
//                            {
//                                Intent nextPage = new Intent(ActivityF1F2.this, ActivityR2.class);
//                                nextPage.putExtra("active", active);
//                                startActivity(nextPage);
//                                return;
//                            }
//                        }
//                        else
//                        {
//                            Intent nextPage = new Intent(ActivityF1F2.this, ActivityR1.class);
//                            nextPage.putExtra("active", active);
//                            startActivity(nextPage);
//                            return;
//                        }
//                    }
//                    else
//                    {
//                        Intent nextPage = new Intent(ActivityF1F2.this, ActivityG1.class);
//                        nextPage.putExtra("active", active);
//                        startActivity(nextPage);
//                        return;
//                    }
//                }

                }

                Intent nextPage = new Intent(ActivityF1F2.this, ActivityF3.class);
                nextPage.putExtra("active", active);
                startActivity(nextPage);

            }
        }

    }


    private void renderF2Layout() {
        String[] arrayF2 = getResources().getStringArray(R.array.arrayF2);
        for (int i = 0; i < arrayF2.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);

            label.setTypeface(tf);
            label.setText(arrayF2[i]);
            label.setTag(i+1);
            if ( i == 1 || i == 2 || i == 5 ) {
                editText.setVisibility(View.VISIBLE);
            }
            F2layout.addView(v);
        }
    }



    private void add() {

        active.setQuestionF1("" + spinnerF1.getSelectedItemPosition());

        questionF2s = new ArrayList<>();
        for (int i = 0; i < F2layout.getChildCount(); i++) {
            View v = F2layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            QuestionF2 questionF2 = new QuestionF2();

            if (chkBox.isChecked()) {
                questionF2.setCode((int) label.getTag());
                questionF2.setStatement(label.getText().toString());
                questionF2.setOpenEnded(editText.getText().toString());
                questionF2s.add(questionF2);

            }
        }


            active.setQuestionF2(new Gson().toJson(questionF2s));


    }

    private boolean validate()
    {
        if ( spinnerF1.getSelectedItemPosition() == 0)
        {
            showDialog("please select F1");
            return false;
        }
        if (F2layout.getVisibility() == View.VISIBLE) {

            for (int i=0;i<questionF2s.size();i++)
            {
                if (questionF2s.get(i).getStatement().equalsIgnoreCase("تیل")||questionF2s.get(i).getStatement().equalsIgnoreCase("بے بی پاؤڈر ۔پریکلی ہیٹ پاؤڈر")
                        ||questionF2s.get(i).getStatement().equalsIgnoreCase("دیگر"))
                {
                    if (questionF2s.get(i).getOpenEnded().isEmpty())
                    {
                        showDialog("Please enter  ");
                        return false;
                    }
                }
            }

            if (questionF2s.size() == 0) {
                showDialog("please select at least one option F2");
                return false;
            }
        }

        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}