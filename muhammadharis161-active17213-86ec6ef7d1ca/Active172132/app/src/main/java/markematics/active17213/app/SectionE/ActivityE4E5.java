package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB14;
import markematics.active17213.Model.QuestionB4;


public class ActivityE4E5 extends AppCompatActivity {
    TextView tViewE4a, tViewE4b, tViewE5a, tViewE5b, tViewE5c, tViewE5d,t5a,t5b,t5c,t5d,tViewE5Instruction,lblChildName;
    Spinner  spinnere4b;
    String e5a, e5b, e5c, e5d;
    LinearLayout e4aLayout,e5aLayout,e5bLayout,e5cLayout,e5dLayout;
    EditText editTextE4b;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB14> questionE4as;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e4a_e4b_e5a_e5b_e5c_e5d);
        tViewE5Instruction = (TextView)findViewById(R.id.tViewE5Instruction);
        tViewE4a = (TextView) findViewById(R.id.tViewE4a);
        tViewE4b = (TextView) findViewById(R.id.tViewE4b);
        tViewE5a = (TextView) findViewById(R.id.tViewE5a);
        tViewE5b = (TextView) findViewById(R.id.tViewE5b);
        tViewE5c = (TextView) findViewById(R.id.tViewE5c);
        tViewE5d = (TextView) findViewById(R.id.tViewE5d);
        lblChildName = (TextView)findViewById(R.id.lblChildName);
        t5a = (TextView)findViewById(R.id.lbl5a);
        t5b = (TextView)findViewById(R.id.lbl5b);
        t5c = (TextView)findViewById(R.id.lbl5c);
        t5d = (TextView)findViewById(R.id.lbl5d);
        e4aLayout = (LinearLayout)findViewById(R.id.e4aLayout);
        e5aLayout = (LinearLayout)findViewById(R.id.e5a);
        e5bLayout = (LinearLayout)findViewById(R.id.e5b);
        e5cLayout = (LinearLayout)findViewById(R.id.e5c);
        e5dLayout = (LinearLayout)findViewById(R.id.e5d);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        editTextE4b = (EditText)findViewById(R.id.editText1);
        toolbar.setTitle("Question E4 E5");
        toolbar.setTitleTextColor(Color.WHITE);
        active = (Active) getIntent().getSerializableExtra("active");
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        spinnere4b = (Spinner) findViewById(R.id.spinnere4b);


        btnNext = (Button) findViewById(R.id.btnNext);

        tViewE4b.setTypeface(tf);
        tViewE5a.setTypeface(tf);
        tViewE5b.setTypeface(tf);
        tViewE5c.setTypeface(tf);
        tViewE5d.setTypeface(tf);
        tViewE4a.setTypeface(tf);
        t5a.setTypeface(tf);
        t5b.setTypeface(tf);
        t5c.setTypeface(tf);
        t5d.setTypeface(tf);
        tViewE5Instruction.setTypeface(tf);
        t5a.setText("کونسا پیک سائز زیادہ تر خریدتی ہیں؟ ");
        t5b.setText("ایک  پیک میں    Piecesکی تعداد");
        t5c.setText("ایک وقت میں پیک /Pieceکی خریداری");
        t5d.setText("ایک پیک  /Pieceکی قیمت");
        lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
        lblChildName.setTypeface(tf);

        renderE4a();
        renderE5Layout();
        ArrayAdapter<String> spinnersE4bArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arraye4b));
        spinnere4b.setAdapter(spinnersE4bArrayAdapter);

        spinnere4b.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 4)
                {
                    editTextE4b.setVisibility(View.VISIBLE);
                }
                else
                {
                    editTextE4b.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

      btnNext.setOnClickListener(btnNext_OnClickListener);
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validationSuccess())
            {
                add();
                proceed();
            }
        }
    };


    private void renderE4a()
    {
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        //   List<QuestionB4> questionB6s = new Gson().fromJson(active.getQuestionB6(), listType);
        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);
        List<Brand> brands = new ArrayList<>();
//        for (int i =0;i<questionB6s.size();i++)
//        {
//            Brand brand = new Brand();
//            brand.setCode(questionB6s.get(i).getCode());
//            brand.setName(questionB6s.get(i).getStatement());
//            brands.add(brand);
//        }
        for (int i =0;i<questionB5s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB5s.get(i).getCode());
            brand.setName(questionB5s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.b14_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);
            ArrayAdapter<String> spinnerb8bArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arraye4a));
            spinnerBrand.setAdapter(spinnerb8bArrayAdapter);
            RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.brandLayout);
            label.setText(brands.get(i).getName());
            label.setTag(brands.get(i).getCode());
            e4aLayout.addView(v);
        }
    }
    private void renderE5Layout() {
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);

        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            lbl.setText(questionB5s.get(i).getBrand());
            lbl.setTag(questionB5s.get(i).getCode());
            editText.setHint("");
            e5aLayout.addView(v);
        }
        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            lbl.setText(questionB5s.get(i).getBrand());
            editText.setHint("");
            lbl.setTag(questionB5s.get(i).getCode());
            e5bLayout.addView(v);
        }
        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            lbl.setText(questionB5s.get(i).getBrand());
            editText.setHint("");
            lbl.setTag(questionB5s.get(i).getCode());
            e5cLayout.addView(v);
        }
        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText) v.findViewById(R.id.editTextB10);
            editText.setHint("");
            lbl.setText(questionB5s.get(i).getBrand());
            lbl.setTag(questionB5s.get(i).getCode());
            e5dLayout.addView(v);
        }
    }
    private void proceed() {
        {
            Intent intent = new Intent(ActivityE4E5.this, ActivityE6E7E8.class);
            intent.putExtra("active", active);
            startActivity(intent);

        }
    }



    private boolean validationSuccess() {

        for (int i = 0;i<e4aLayout.getChildCount();i++) {
            View v = e4aLayout.getChildAt(i);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);



            if (spinnerBrand.getSelectedItemPosition() == 0)
            {
                showDialog("Please fill all values");
                return false;
            }

        }
        if (spinnere4b.getSelectedItemPosition() == 0 )
        {
            showDialog("Please select 4b");
            return false;
        }
        for (int i = 0;i<e5aLayout.getChildCount();i++) {
            View v = e5aLayout.getChildAt(i);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);



            if (editText.getText().toString().isEmpty())
            {
                editText.setError("Please fill");
                return false;
            }

        }
        for (int i = 0;i<e5bLayout.getChildCount();i++) {
            View v = e5bLayout.getChildAt(i);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);



            if (editText.getText().toString().isEmpty())
            {
                editText.setError("Please fill");
                return false;
            }

        }
        for (int i = 0;i<e5cLayout.getChildCount();i++) {
            View v = e5cLayout.getChildAt(i);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);



            if (editText.getText().toString().isEmpty())
            {
                editText.setError("Please fill");
                return false;
            }

        }
        for (int i = 0;i<e5dLayout.getChildCount();i++) {
            View v = e5dLayout.getChildAt(i);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);



            if (editText.getText().toString().isEmpty())
            {
                editText.setError("Please fill");
                return false;
            }

        }

        if (editTextE4b.getVisibility() == View.VISIBLE)
        {
            if (editTextE4b.getText().toString().isEmpty())
            {
                editTextE4b.setError("Please enter");
            }
        }
        return true;
    }

    private void add() {

        questionE4as = new ArrayList<>();
        for (int i = 0; i< e4aLayout.getChildCount();i++)
        {
            View v = e4aLayout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);
            if (spinnerBrand.getSelectedItemPosition() !=0)
            {
                QuestionB14 questionB14 = new QuestionB14();
                questionB14.setBrandName(label.getText().toString());
                questionB14.setBrandCode((int)label.getTag());
                questionB14.setResponseCode(spinnerBrand.getSelectedItemPosition());
                questionE4as.add(questionB14);
            }
        }
        active.setQuestionE4b(""+spinnere4b.getSelectedItemPosition());
        active.setQuestionE4bOther(editTextE4b.getText().toString());
        active.setQuestionE4a(new Gson().toJson(questionE4as));


    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
