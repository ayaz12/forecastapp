package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.markematics.Active17213.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.DataAccess.HttpHandler;
import markematics.active17213.Helper.KeyValueDB;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionS12;
import markematics.active17213.Model.User;
import markematics.active17213.app.ActivityHome;
import markematics.active17213.app.ActivityLogin;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA3b extends AppCompatActivity {

    TextView tViewA3b;
    LinearLayout A3b_layout;
    Button btnNext;
    Typeface tf;
    Active active;
    Toolbar toolbar;
    List<QuestionS12> questionA3bs;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3a_a3b);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A3");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewA3b=(TextView)findViewById(R.id.tViewA3b);
        A3b_layout=(LinearLayout)findViewById(R.id.A3b_layout);
        btnNext =(Button)findViewById(R.id.btnA3a);


        btnNext.setOnClickListener(btnNext_OnClickListener);


        new AsyncHandler(REQUEST_RENDER).execute();



    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            new AsyncHandler(REQUEST_ADD).execute();

        }
    };

    private void randerA3bLayout() {
        A3b_layout.removeAllViews();
        String[] arrayA3b = getResources().getStringArray(R.array.arrayA3b);
        for (int i = 0; i < arrayA3b.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            label.setTypeface(tf);
             label.setText(arrayA3b[i]);
            label.setTag(i+1);
            if (i == 3)
            {
                editText.setVisibility(View.VISIBLE);
            }
            A3b_layout.addView(v);
        }

    }

    private void add()
    {
        questionA3bs = new ArrayList<>();


        for (int i = 0; i < A3b_layout.getChildCount(); i++) {
            View v = A3b_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);

            if (chkBox.isChecked())
            {
                QuestionS12 questionS12 = new QuestionS12();
                questionS12.setCode((int)label.getTag());
                questionS12.setOpenEnded(editText.getText().toString());
                questionS12.setStatement(label.getText().toString());
                questionA3bs.add(questionS12);
            }

        }

        active.setQuestionA3b(new Gson().toJson(questionA3bs));
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validate()
    {
        for (int i=0;i<questionA3bs.size();i++)
        {
            if (questionA3bs.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionA3bs.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("دیگر");
                    return false;
                }
            }
        }
        if (questionA3bs.size() == 0)
        {
            showDialog("Please select at least one option");
            return false;
        }
        return true;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA3b.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate()) {

                                Intent intent = new Intent(ActivityA3b.this, ActivityA4.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewA3b.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");
                            randerA3bLayout();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
