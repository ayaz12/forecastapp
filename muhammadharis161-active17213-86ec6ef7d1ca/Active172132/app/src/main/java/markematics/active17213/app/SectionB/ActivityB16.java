package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.SpinnerB18Adapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB16 extends AppCompatActivity {

    TextView tViewB16,tViewB17,lblBrandName;
    Spinner spinnersB16;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    EditText editTextOther,editText17;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b16);


        lblBrandName = (TextView)findViewById(R.id.lblBrandName);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Question B16");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewB16 = (TextView)findViewById(R.id.tViewb16);
        tViewB17 = (TextView)findViewById(R.id.tViewB17);
        editTextOther = (EditText)findViewById(R.id.editTextOther);
        editText17 = (EditText)findViewById(R.id.editText17);
        btnNext = (Button)findViewById(R.id.btnNext);
         spinnersB16 = (Spinner) findViewById(R.id.spinnerb16);


        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();



        spinnersB16.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Brand val = (Brand)adapterView.getSelectedItem();
                if (val.getName().equalsIgnoreCase("Others"))
                {
                    editTextOther.setVisibility(View.VISIBLE);
                }
                else
                {
                    editTextOther.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            new AsyncHandler(REQUEST_ADD).execute();
        }
    };


    private boolean validate()
    {


            if (spinnersB16.getSelectedItemPosition() == 0)
            {
                showDialog("Please fill all 16 values");
                return false;
            }

            if (editTextOther.getVisibility() == View.VISIBLE)
            {
                if (editTextOther.getText().toString().isEmpty())
                {
                    editTextOther.setError("Enter Brand Name");
                    return false;
                }
            }
            if (editText17.getText().toString().isEmpty())
            {
                editText17.setError("Enter");
                return false;
            }
        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB16.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            final String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate()) {
                                Brand brand = (Brand)spinnersB16.getSelectedItem();

                                active.setQuestionB16("" +brand.getCode());
                                Brand val = (Brand)spinnersB16.getSelectedItem();

                                if (val.getName().equalsIgnoreCase("Others"))
                                {
                                    active.setQuestionB16Other(editTextOther.getText().toString());
                                }
                                active.setQuestionB17(editText17.getText().toString());

                                Intent intent = new Intent(ActivityB16.this, ActivityB18.class);
                                intent.putExtra("active", active);
                                startActivity(intent);

                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB16.setTypeface(tf);
                            tViewB17.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");
                            List<Brand> brands = new ArrayList<>();
                            Brand brand = new Brand();
                            brand.setCode(0);
                            brand.setName("Select");
                            brands.add(brand);
                            Type listType = new TypeToken<ArrayList<QuestionB4>>() {
                            }.getType();
                            List<QuestionB4> questionB4s = new Gson().fromJson(active.getQuestionB4(), listType);
                            for (int i = 0; i < questionB4s.size(); i++) {
                                brand = new Brand();
                                brand.setCode(questionB4s.get(i).getCode());
                                brand.setName(questionB4s.get(i).getBrand());
                                brands.add(brand);
                            }
                            List<QuestionB4> questionB6s = new Gson().fromJson(active.getQuestionB6(), listType);
                            for (int j = 0;j<brands.size();j++)
                            {
                                for (int i = 0;i< questionB6s.size();i++)
                                {
                                    if(brands.get(j).getName().equalsIgnoreCase(questionB6s.get(i).getBrand()))
                                    {
                                        brands.remove(brands.get(j));
                                    }
                                }

                            }
                            lblBrandName.setText(questionB6s.get(0).getBrand()+" برانڈ کا نام ");
                            lblBrandName.setTypeface(tf);
                            SpinnerB18Adapter adapter = new SpinnerB18Adapter(ActivityB16.this,R.layout.spinner_item, R.id.txt, brands);
                            spinnersB16.setAdapter(adapter);
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}