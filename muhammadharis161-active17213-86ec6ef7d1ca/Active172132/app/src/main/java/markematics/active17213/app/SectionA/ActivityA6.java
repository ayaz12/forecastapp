package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.CustomSpinnerAdapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionS12;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA6 extends AppCompatActivity {
    TextView tViewA6,lblChildName;
    Button btnNext;
    Typeface tf;
    Spinner spinnerA6;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a6);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A6");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewA6 = (TextView) findViewById(R.id.tViewA6);
        lblChildName = (TextView)findViewById(R.id.lblChildName);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        spinnerA6=(Spinner)findViewById(R.id.spinnerA6);
        btnNext =(Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(btnNext_OnClickListener);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewA6.setTypeface(tf);
        new AsyncHandler(REQUEST_RENDER).execute();


    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };



    private boolean validationSuccess() {


        if (spinnerA6.getSelectedItemPosition() == 0)
        {
            showDialog("Please Select at least one option");
            return false;
        }

        return true;
    }



    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA6.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validationSuccess())
                            {

                                QuestionS12 questionS12 =  (QuestionS12)spinnerA6.getSelectedItem();
                                active.setQuestionA6(""+questionS12.getCode());
                                if (questionS12.getStatement().equalsIgnoreCase("کپڑا")||questionS12.getStatement().equalsIgnoreCase("تولیہ")||questionS12.getStatement().equalsIgnoreCase("پلاسٹک پینٹیز")||questionS12.getStatement().equalsIgnoreCase("واٹر پروف میٹ"))
                                {
                                    Intent intent = new Intent(ActivityA6.this, ActivityA7.class);
                                    intent.putExtra("active", active);
                                    intent.putExtra("s12",questionS12.getStatement());
                                    startActivity(intent);
                                }
                                else if (questionS12.getStatement().equalsIgnoreCase("ڈائپرز"))
                                {

                                    Intent intent = new Intent(ActivityA6.this, ActivityA9.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }

                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            active = (Active) getIntent().getSerializableExtra("active");
                            lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
                            lblChildName.setTypeface(tf);

                            Type listType = new TypeToken<ArrayList<QuestionS12>>() {
                            }.getType();
                            List<QuestionS12> questionS12s = new Gson().fromJson(active.getQuestionS12(), listType);
                            QuestionS12 questionS12 = new QuestionS12();
                            questionS12.setStatement("Select");
                            questionS12.setCode(0);
                            questionS12.setOpenEnded("");
                            questionS12s.add(0,questionS12);
                            CustomSpinnerAdapter spinnerA6ArrayAdapter = new CustomSpinnerAdapter(ActivityA6.this,R.layout.spinner_item,R.id.txt, questionS12s);
                            spinnerA6.setAdapter(spinnerA6ArrayAdapter);

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
