package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.github.lassana.recorder.AudioRecorderBuilder;
import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.app.RecorderApplication;


/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB1 extends AppCompatActivity {

    TextView tViewB1, tViewlblInsB1;
    LinearLayout b1_Layout;
    Button btnB1;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB1> questionB1s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b1);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B1");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB1 = (TextView) findViewById(R.id.tViewB1);
        tViewlblInsB1 = (TextView) findViewById(R.id.lblInsB1);
        //   spinB1 = (Spinner)findViewById(R.id.spinB1);
        b1_Layout = (LinearLayout) findViewById(R.id.b1_layout);

        btnB1 = (Button) findViewById(R.id.btnB1);
        btnB1.setOnClickListener(btnNext_OnClickListener);

        new AsyncHandler(REQUEST_RENDER).execute();
    }

    private void StartRecording() {
        if (ViewController.mAudioRecorder == null) {
            String filePath = active.gethR_ID() + "_" + active.getCity() + "_SecB_" + System.currentTimeMillis();
            active.setAudioPathSecB(filePath);
            recordAudio(filePath);

        }
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void recordAudio(String fileName) {


        final RecorderApplication application = RecorderApplication.getApplication(ActivityB1.this);
        ViewController.mAudioRecorder = application.getRecorder();
        if (ViewController.mAudioRecorder == null
                || ViewController.mAudioRecorder.getStatus() == AudioRecorder.Status.STATUS_UNKNOWN) {
            ViewController.mAudioRecorder = AudioRecorderBuilder.with(application)
                    .fileName(createDirectoryAndSaveFile(fileName))
                    .config(AudioRecorder.MediaRecorderConfig.DEFAULT)
                    .loggable()
                    .build();
            application.setRecorder(ViewController.mAudioRecorder);
        }
        start();
    }

    private void message(String message) {
        Toast.makeText(ActivityB1.this, message, Toast.LENGTH_SHORT).show();
    }

    private void start() {
        ViewController.mAudioRecorder.start(new AudioRecorder.OnStartListener() {
            @Override
            public void onStarted() {
                renderB1Layout();
            }

            @Override
            public void onException(Exception e) {

                //   invalidateViews();
                message(getString(R.string.error_audio_recorder, e));
            }
        });
    }

    private String createDirectoryAndSaveFile(String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.ActiveQuant/Audio/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.ActiveQuant/Audio/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.ActiveQuant/Audio/"), fileName + ".mp3");
        if (file.exists()) {
            file.delete();
        }

        return file.getPath();
    }

    private CompoundButton.OnCheckedChangeListener checkBox1_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);

            if (chkBox1.isChecked()) {

                for (int i = 0; i < b1_Layout.getChildCount(); i++) {
                    View childView = b1_Layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < b1_Layout.getChildCount(); i++) {
                    View childView = b1_Layout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }

            }

        }
    };


    private void renderB1Layout() {
        b1_Layout.removeAllViews();
        String[] arrayB1 = getResources().getStringArray(R.array.arrayB2);
        for (int i = 0; i < arrayB1.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);

            label.setTypeface(tf);
            label.setText(arrayB1[i]);
            label.setTypeface(tf);
            label.setTag(i + 1);
            chkBox1.setTag(v);

            chkBox1.setOnCheckedChangeListener(checkBox1_OnCheckedChangeListener);

            if (arrayB1[i].equalsIgnoreCase("Others")) {
                editText.setVisibility(View.VISIBLE);
            }
            b1_Layout.addView(v);
            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
    }


    private void add() {

        questionB1s = new ArrayList<>();

        for (int i = 0; i < b1_Layout.getChildCount(); i++) {
            View v = b1_Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);

            if (chkBox1.isChecked()) {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int) label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionB1s.add(questionB1);
            }

        }

        active.setQuestionB1(new Gson().toJson(questionB1s));

    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private boolean validate() {
        for (int i = 0; i < questionB1s.size(); i++) {
            if (questionB1s.get(i).getBrand().equalsIgnoreCase("Others")) {
                if (questionB1s.get(i).getOpenEnded().isEmpty()) {
                    showDialog("Please enter Other Brand");
                    return false;
                }
            }
        }
        if (questionB1s.size() == 0) {
            showDialog("Please select at least one option B1");
            return false;
        }

        return true;
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB1.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate()) {
                                Intent intent = new Intent(ActivityB1.this, ActivityB2.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB1.setTypeface(tf);
                            tViewlblInsB1.setTypeface(tf);

                            active = (Active) getIntent().getSerializableExtra("active");
                            ViewController.active = active;

                            StartRecording();
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
