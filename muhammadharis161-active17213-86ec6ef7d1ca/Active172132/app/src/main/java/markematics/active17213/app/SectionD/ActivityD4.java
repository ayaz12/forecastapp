package markematics.active17213.app.SectionD;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.markematics.Active17213.R;


import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionD4;

/**
 * Created by mas on 9/21/2017.
 */

public class ActivityD4 extends AppCompatActivity {

    TextView tViewD4;
    LinearLayout d4layout;
    Button btnNext;
    Active active;
    List<QuestionD4> questionD4s;
    Toolbar toolbar;
    Typeface tf;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d4);

        tViewD4 = (TextView) findViewById(R.id.tViewD4);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question D4");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewD4 = (TextView) findViewById(R.id.tViewD4);

        d4layout = (LinearLayout) findViewById(R.id.d4_layout);

        btnNext = (Button) findViewById(R.id.btnNext);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewD4.setTypeface(tf);

        active = (Active) getIntent().getSerializableExtra("active");
        randerD4Layout();

        btnNext.setOnClickListener(btnNext_OnClickListener);

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate()) {
                Intent intent = new Intent(ActivityD4.this, ActivityD5.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    };

    private void randerD4Layout() {
        String[] arrayD4 = getResources().getStringArray(R.array.arrayd4);
        for (int i = 0; i < arrayD4.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            label.setText(arrayD4[i]);
            label.setTypeface(tf);
            label.setTag(i + 1);
            if (arrayD4[i].equalsIgnoreCase("دیگر")) {
                editText.setVisibility(View.VISIBLE);
            }
            d4layout.addView(v);
        }
    }


    private void add() {

        questionD4s = new ArrayList<>();
        for (int i = 0; i < d4layout.getChildCount(); i++) {
            View v = d4layout.getChildAt(i);
            TextView label1 = (TextView) v.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            QuestionD4 questionD4 = new QuestionD4();

            if (checkBox.isChecked()) {
                questionD4.setCode((int) label1.getTag());
                questionD4.setStatement(label1.getText().toString());
                questionD4.setOpenEnded(editText.getText().toString());
                questionD4s.add(questionD4);
            }
        }


        active.setQuestionD4(new Gson().toJson(questionD4s));

    }

    private boolean validate() {

        for (int i=0;i<questionD4s.size();i++)
        {
            if (questionD4s.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionD4s.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other ");
                    return false;
                }
            }
        }
        if (questionD4s.size() == 0) {
            showDialog("Please fill  D4 values");
            return false;
        }


        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}