package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB9 extends AppCompatActivity {

    TextView tViewB9,lblBrandName;
    EditText editTextB9;
    Button btnNext;
    Typeface tf;
    View view;
    Toolbar toolbar;
    Active  active;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b9);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B9");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB9 = (TextView) findViewById(R.id.tViewB9);
        lblBrandName = (TextView)findViewById(R.id.lblBrandName);
        editTextB9 = (EditText) findViewById(R.id.editTextB9);
        btnNext = (Button) findViewById(R.id.btnB9);

        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();
    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        new AsyncHandler(REQUEST_ADD).execute();
        }
    };
    private boolean validationSuccess() {

        boolean valid = true;


        if (editTextB9.getText().toString().isEmpty()) {
            editTextB9.setError("درج کریں");
            valid = false;

        }
        return valid;
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB9.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validationSuccess()) {
                                active.setQuestionB9(editTextB9.getText().toString());
                                Type listType = new TypeToken<ArrayList<QuestionB4>>() {
                                }.getType();
                                List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);
                                if (questionB5s.size() > 1) {
                                    Intent intent = new Intent(ActivityB9.this, ActivityB10.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);

                                } else {
                                    Intent intent = new Intent(ActivityB9.this, ActivityB11.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }

                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB9.setTypeface(tf);
                            editTextB9.setTypeface(tf);

                            lblBrandName.setTypeface(tf);
                            active = (Active)getIntent().getSerializableExtra("active");
                            Type listType = new TypeToken<ArrayList<QuestionB4>>() {
                            }.getType();
                            List<QuestionB4> questionB6s = new Gson().fromJson(active.getQuestionB6(), listType);
                            lblBrandName.setText(questionB6s.get(0).getBrand()+" برانڈ کا نام ");
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}






