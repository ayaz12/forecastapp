package markematics.active17213.app.SectionS;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.markematics.Active17213.R;

import markematics.active17213.Model.Active;


public class ActivityInfo extends AppCompatActivity {

    EditText editTextnameofcheifwageearner,editTextphonenumber,editTextmobilenumber,editTexthousenumber,editTextstreetnumber,editTextsector,editTextblock,editTextareaname,editTextnearestlandmark;
    Button btninfo;
    String nameofcheifwageearner,phnumber,mnumber,hnumber,street,sector,blk,aname,nlandmark;
    View view;
    Typeface tf;
    Active active;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Respondent Information");
        toolbar.setTitleTextColor(Color.WHITE);

        editTextnameofcheifwageearner = (EditText)findViewById(R.id.nameofchiefwagegenrel);
        editTextphonenumber = (EditText)findViewById(R.id.phonenumber);
        editTextmobilenumber = (EditText)findViewById(R.id.mobilenumber);
        editTexthousenumber = (EditText)findViewById(R.id.housenumber);
        editTextstreetnumber = (EditText)findViewById(R.id.streetnumber);
        editTextsector = (EditText)findViewById(R.id.sector);
        editTextblock = (EditText)findViewById(R.id.block);
        editTextareaname = (EditText)findViewById(R.id.areaname);
        editTextnearestlandmark = (EditText)findViewById(R.id.nearestlandmark);
        btninfo = (Button)findViewById(R.id.btninfo);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        editTextnameofcheifwageearner.setTypeface(tf);
        editTextphonenumber.setTypeface(tf);
        editTextmobilenumber.setTypeface(tf);
        editTexthousenumber.setTypeface(tf);
        editTextstreetnumber.setTypeface(tf);
        editTextsector.setTypeface(tf);
        editTextblock.setTypeface(tf);
        editTextareaname.setTypeface(tf);
        editTextnearestlandmark.setTypeface(tf);
        active = (Active)getIntent().getSerializableExtra("active");


        btninfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }



    private void next() {
        initialize();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n",Toast.LENGTH_SHORT).show();
        } else {
            add();
            proceed();

        }
    }

    private void proceed() {
        Intent intent = new Intent(ActivityInfo.this, ActivityS2.class);
        intent.putExtra("active", active);
        startActivity(intent);
    }



    private boolean validationSuccess() {
        boolean valid = true;

        if (hnumber.isEmpty()) {
            editTexthousenumber.setError("درج کریں مکان نمبر");
            valid = false;
        }

        if (aname.isEmpty()) {
            editTextareaname.setError("درج کریں علاقے کا نام");
            valid = false;
        }
        return valid;
    }


    private void initialize() {

        nameofcheifwageearner = editTextnameofcheifwageearner.getText().toString().trim();
        phnumber = editTextphonenumber.getText().toString().trim();
        mnumber = editTextmobilenumber.getText().toString().trim();
        hnumber = editTexthousenumber.getText().toString().trim();
        street = editTextstreetnumber.getText().toString().trim();
        sector = editTextsector.getText().toString().trim();
        blk = editTextblock.getText().toString().trim();
        aname = editTextareaname.getText().toString().trim();
        nlandmark = editTextnearestlandmark.getText().toString().trim();
    }


    public void add(){
        active.setEarnerName(nameofcheifwageearner);
        active.setPhoneNo(phnumber);
        active.setMobileNo(mnumber);
        active.setHouseNo(hnumber);
        active.setStreetNo(street);
        active.setSector(sector);
        active.setBlock(blk);
        active.setLandmark(nlandmark);
        active.setAreaName(aname);
    }
}

