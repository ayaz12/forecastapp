package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH6a;
import markematics.active17213.Model.QuestionH6b;


public class ActivityH4 extends AppCompatActivity {

    TextView tViewH4;

    Button btnNext;
    Spinner spinnerH4;

    Typeface tf;
    Toolbar toolbar;
    Active active;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h4);

        tViewH4 = (TextView) findViewById(R.id.tViewH4);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H4 ");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        spinnerH4 = (Spinner) findViewById(R.id.spinnerH4);
    tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH4.setTypeface(tf);

        ArrayAdapter<String> spinnerH5ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH4.setAdapter(spinnerH5ArrayAdapter);


        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spinnerH4.getSelectedItemPosition()!=0)

                {
                next();
                }
                else {
                    showDialog("Please Select Atleast One Option");

                }

            }
        });

     }

    private void next() {

        add();
            proceed();


        }
  private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private void proceed() {if (spinnerH4.getSelectedItemPosition()==1)
        {
            Intent intent = new Intent(ActivityH4.this, ActivityH5.class);
            intent.putExtra("active", active);
            startActivity(intent);


        }
    else
        {
            Intent intent = new Intent(ActivityH4.this, ActivityH7.class);
            intent.putExtra("active", active);
            startActivity(intent);

        }

    }


    private void add() {

        active.setQuestionH4("" + spinnerH4.getSelectedItemPosition());
}}






