package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB14;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionE5;

/**
 * Created by ZEB Rana on 16-Oct-17.
 */

public class ActivityE5 extends AppCompatActivity {

    TextView tViewE5a, tViewE5b, tViewE5c, tViewE5d,t5a,t5b,t5c,t5d,tViewE5Instruction;
    String e5a, e5b, e5c, e5d;
    LinearLayout e5aLayout,e5bLayout,e5cLayout,e5dLayout;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionE5> questionE5as;
    List<QuestionE5> questionE5bs;
    List<QuestionE5> questionE5cs;
    List<QuestionE5> questionE5ds;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e5);

        tViewE5Instruction = (TextView)findViewById(R.id.tViewE5Instruction);

        tViewE5a = (TextView) findViewById(R.id.tViewE5a);
        tViewE5b = (TextView) findViewById(R.id.tViewE5b);
        tViewE5c = (TextView) findViewById(R.id.tViewE5c);
        tViewE5d = (TextView) findViewById(R.id.tViewE5d);

        t5a = (TextView)findViewById(R.id.lbl5a);
        t5b = (TextView)findViewById(R.id.lbl5b);
        t5c = (TextView)findViewById(R.id.lbl5c);
        t5d = (TextView)findViewById(R.id.lbl5d);

        e5aLayout = (LinearLayout)findViewById(R.id.e5a);
        e5bLayout = (LinearLayout)findViewById(R.id.e5b);
        e5cLayout = (LinearLayout)findViewById(R.id.e5c);
        e5dLayout = (LinearLayout)findViewById(R.id.e5d);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("Question E5");
        toolbar.setTitleTextColor(Color.WHITE);
        active = (Active) getIntent().getSerializableExtra("active");
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");



        btnNext = (Button) findViewById(R.id.btnNext);

        tViewE5a.setTypeface(tf);
        tViewE5b.setTypeface(tf);
        tViewE5c.setTypeface(tf);
        tViewE5d.setTypeface(tf);
        t5a.setTypeface(tf);
        t5b.setTypeface(tf);
        t5c.setTypeface(tf);
        t5d.setTypeface(tf);
        tViewE5Instruction.setTypeface(tf);
        t5a.setText("کونسا پیک سائز زیادہ تر خریدتی ہیں؟ ");
        t5b.setText("ایک  پیک میں    Piecesکی تعداد");
        t5c.setText("ایک وقت میں پیک /Pieceکی خریداری");
        t5d.setText("ایک پیک  /Pieceکی قیمت");

        renderE5Layout();

        btnNext.setOnClickListener(btnNext_OnClickListener);
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validationSuccess())
            {
                add();
                proceed();
            }
        }
    };


    private void renderE5Layout() {
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);

        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            lbl.setText(questionB5s.get(i).getBrand());
            lbl.setTag(questionB5s.get(i).getCode());
            editText.setHint("");
            e5aLayout.addView(v);
        }
        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            lbl.setText(questionB5s.get(i).getBrand());
            editText.setHint("");
            lbl.setTag(questionB5s.get(i).getCode());
            e5bLayout.addView(v);
        }
        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            lbl.setText(questionB5s.get(i).getBrand());
            editText.setHint("");
            editText.setInputType( InputType.TYPE_CLASS_NUMBER);
            lbl.setTag(questionB5s.get(i).getCode());
            e5cLayout.addView(v);
        }
        for (int i = 0; i < questionB5s.size(); i++)

        {
            View v = getLayoutInflater().inflate(R.layout.b10_layout, null);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText) v.findViewById(R.id.editTextB10);
            editText.setHint("");
            lbl.setText(questionB5s.get(i).getBrand());
            lbl.setTag(questionB5s.get(i).getCode());
            editText.setInputType( InputType.TYPE_CLASS_NUMBER);
            e5dLayout.addView(v);
        }
    }
    private void proceed() {
        {
            Intent intent = new Intent(ActivityE5.this, ActivityE6.class);
            intent.putExtra("active", active);
            startActivity(intent);

        }
    }



    private boolean validationSuccess() {


        for (int i = 0;i<e5aLayout.getChildCount();i++) {
            View v = e5aLayout.getChildAt(i);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);



            if (editText.getText().toString().isEmpty())
            {
                editText.setError("Please fill");
                return false;
            }

        }
        for (int i = 0;i<e5bLayout.getChildCount();i++) {
            View v = e5bLayout.getChildAt(i);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);



            if (editText.getText().toString().isEmpty())
            {
                editText.setError("Please fill");
                return false;
            }

        }
        for (int i = 0;i<e5cLayout.getChildCount();i++) {
            View v = e5cLayout.getChildAt(i);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);



            if (editText.getText().toString().isEmpty())
            {
                editText.setError("Please fill");
                return false;
            }

        }
        for (int i = 0;i<e5dLayout.getChildCount();i++) {
            View v = e5dLayout.getChildAt(i);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);



            if (editText.getText().toString().isEmpty())
            {
                editText.setError("Please fill");
                return false;
            }

        }

        return true;
    }

    private void add() {

        questionE5as = new ArrayList<>();
        for (int i = 0;i<e5aLayout.getChildCount();i++) {
            View v = e5aLayout.getChildAt(i);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            QuestionE5 questionE5 = new QuestionE5();
            questionE5.setBrandName(lbl.getText().toString());
            questionE5.setBrandCode((int)lbl.getTag());
            questionE5.setOpenEnded(editText.getText().toString());
            questionE5as.add(questionE5);
        }
        questionE5bs = new ArrayList<>();
        for (int i = 0;i<e5bLayout.getChildCount();i++) {
            View v = e5bLayout.getChildAt(i);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            QuestionE5 questionE5 = new QuestionE5();
            questionE5.setBrandName(lbl.getText().toString());
            questionE5.setBrandCode((int)lbl.getTag());
            questionE5.setOpenEnded(editText.getText().toString());
            questionE5bs.add(questionE5);
        }
        questionE5cs = new ArrayList<>();
        for (int i = 0;i<e5cLayout.getChildCount();i++) {
            View v = e5cLayout.getChildAt(i);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            QuestionE5 questionE5 = new QuestionE5();
            questionE5.setBrandName(lbl.getText().toString());
            questionE5.setBrandCode((int)lbl.getTag());
            questionE5.setOpenEnded(editText.getText().toString());
            questionE5cs.add(questionE5);
        }
        questionE5ds = new ArrayList<>();
        for (int i = 0;i<e5dLayout.getChildCount();i++) {
            View v = e5dLayout.getChildAt(i);
            TextView lbl = (TextView) v.findViewById(R.id.lblBrand);
            EditText editText = (EditText)v. findViewById(R.id.editTextB10);
            QuestionE5 questionE5 = new QuestionE5();
            questionE5.setBrandName(lbl.getText().toString());
            questionE5.setBrandCode((int)lbl.getTag());
            questionE5.setOpenEnded(editText.getText().toString());
            questionE5ds.add(questionE5);
        }

        active.setQuestionE5a(new Gson().toJson(questionE5as));
        active.setQuestionE5b(new Gson().toJson(questionE5bs));
        active.setQuestionE5c(new Gson().toJson(questionE5cs));
        active.setQuestionE5d(new Gson().toJson(questionE5ds));
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
