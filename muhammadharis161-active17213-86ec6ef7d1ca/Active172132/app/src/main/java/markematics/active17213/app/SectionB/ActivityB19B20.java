package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.lassana.recorder.AudioRecorder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB20;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionA.ActivityA11;
import markematics.active17213.app.SectionA.ActivityA12A13;
import markematics.active17213.app.SectionC.ActivityC1;

/**
 * Created by mas on 9/21/2017.
 */

public class ActivityB19B20 extends AppCompatActivity {
    TextView tViewB19,tViewB20,tViewB18;
    Spinner spinnerB19;
    LinearLayout b20layout;
    View view;
    Button btnB19;
    Typeface tf;
    Toolbar toolbar;
    Spinner spinnersB19;
    List<QuestionB20> questionB20s;
    Active active;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b18_b19_b20);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B19 B20");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB19 = (TextView)findViewById(R.id.tViewB19);
        tViewB20 = (TextView)findViewById(R.id.tViewB20);
        spinnerB19 = (Spinner)findViewById(R.id.spinnerb19);
        b20layout =(LinearLayout)findViewById(R.id.b20_layout);
        btnB19 =(Button) findViewById(R.id.btnB19);

        spinnersB19 = (Spinner) findViewById(R.id.spinnerb19);

        btnB19.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();
        spinnersB19.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 1)
                {
                    tViewB20.setVisibility(View.VISIBLE);
                    b20layout.setVisibility(View.VISIBLE);
                }
                else
                {
                    tViewB20.setVisibility(View.GONE);
                    b20layout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        new AsyncHandler(REQUEST_ADD).execute();

        }
    };
    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityB19B20.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityB19B20.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    Intent intent = new Intent(ActivityB19B20.this, ActivityC1.class);
                    intent.putExtra("active", active);
                    startActivity(intent);
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private void randerB20Layout() {
        b20layout.removeAllViews();
        String[] arrayB20 = getResources().getStringArray(R.array.arrayB20);
        List<Brand> brands = new ArrayList<>();
        Brand brand ;


        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);

        for (int i =0;i<questionB1s.size();i++)
        {
            brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB2s.size();i++)
        {
            brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB3s.size();i++)
        {
            brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
            brands.add(brand);
        }

        for (int i = 0; i < brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            label.setText(brands.get(i).getName());
            label.setTag(brands.get(i).getCode());
            label.setTypeface(tf);
            if (arrayB20[i].equalsIgnoreCase("دیگر"))
            {
                editText.setVisibility(View.VISIBLE);
            }
            b20layout.addView(v);
        }
    }

    private void add()
    {
        active.setQuestionB19(""+spinnerB19.getSelectedItemPosition());
        questionB20s = new ArrayList<>();
        for (int i=0;i< b20layout.getChildCount();i++)
        {
            View v = b20layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            if (chkBox.isChecked())
            {
                QuestionB20 questionB20 = new QuestionB20();
                questionB20.setCode((int)label.getTag());
                questionB20.setStatement(label.getText().toString());
                questionB20.setOpenEnded(editText.getText().toString());
                questionB20s.add(questionB20);

            }
        }
        active.setQuestionB20(new Gson().toJson(questionB20s));
    }
    private boolean validate()
    {

        if (spinnerB19.getSelectedItemPosition() == 0)
        {
            showDialog("Please select B19");
            return false;
        }
        if (b20layout.getVisibility() == View.VISIBLE) {
            if (questionB20s.size() == 0) {
                showDialog("Please Select At least one option");
                return false;
            }
        }


        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB19B20.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate())
                            {
                                StopRecording();



                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB19.setTypeface(tf);
                            tViewB20.setTypeface(tf);
                            active = (Active)getIntent().getSerializableExtra("active");
                            ArrayAdapter<String> spinnersB19ArrayAdapter = new ArrayAdapter<String>(ActivityB19B20.this,R.layout.spinner_item, getResources().getStringArray(R.array.arrayB19));
                            spinnersB19.setAdapter(spinnersB19ArrayAdapter);
                            randerB20Layout();
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }

}
