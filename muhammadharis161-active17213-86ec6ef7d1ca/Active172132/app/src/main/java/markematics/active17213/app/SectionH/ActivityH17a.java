package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH12;
import markematics.active17213.app.SectionN.ActivityN1N2N3N4N5;

public class ActivityH17a extends AppCompatActivity {

    TextView tViewH17a;
    Spinner  spinnerH17a;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h17a);


        tViewH17a = (TextView) findViewById(R.id.tViewH17a);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H17a");
        toolbar.setTitleTextColor(Color.WHITE);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        active = (Active) getIntent().getSerializableExtra("active");
       spinnerH17a = (Spinner) findViewById(R.id.spinnerH17a);
        btnNext = (Button) findViewById(R.id.btnNext);
        tViewH17a.setTypeface(tf);




        ArrayAdapter<String> spinnerH15ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH17a.setAdapter(spinnerH15ArrayAdapter);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {
        if (spinnerH17a.getSelectedItemPosition() !=0) {
            add();
            proceed();
        }
        else
        {
            showDialog("Please select at least one option H17a");
        }
        }



    private void proceed() {
        Intent nextPage = new Intent(ActivityH17a.this, ActivityH17b.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }


    private void add(){


        active.setQuestionH17a("" + spinnerH17a.getSelectedItemPosition());

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}