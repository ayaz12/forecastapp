package markematics.active17213.Model;

/**
 * Created by mas on 10/5/2017.
 */

public class QuestionG1 {
    private int code;
    private String statement;
    private int selectedCode;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public int getSelectedCode() {
        return selectedCode;
    }

    public void setSelectedCode(int selectedCode) {
        this.selectedCode = selectedCode;
    }
}
