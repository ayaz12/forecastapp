package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB15;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 10/9/2017.
 */

public class ActivityB15 extends AppCompatActivity {

    LinearLayout b15layout;
    TextView tViewB15;
    Button btnB15;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB15> questionB15s;
    String n1aVal = "";
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b15);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B15");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB15 = (TextView) findViewById(R.id.tViewB15);
        btnB15 = (Button) findViewById(R.id.btnB15);
        b15layout = (LinearLayout) findViewById(R.id.B15Layout);



        btnB15.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();


    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        new AsyncHandler(REQUEST_ADD).execute();
        }
    };
    private void renderB15Layout() {
        b15layout.removeAllViews();
        String[] strArray = getResources().getStringArray(R.array.arrayB8);
        List<Brand> brands = new ArrayList<>();
        for (int i = 0;i<strArray.length;i++)
        {
            if (i == Integer.parseInt(active.getQuestionB8a()))
            {
                Brand brand = new Brand();
                brand.setCode(Integer.parseInt(active.getQuestionB8a()));
                brand.setName(strArray[i]);
                brands.add(brand);
            }
        }
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();

        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);


        for (int i =0;i<questionB5s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB5s.get(i).getCode());
            brand.setName(questionB5s.get(i).getBrand());
            brands.add(brand);
        }

        for (int i = 0; i < brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.b15_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            TextView statementlbl = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editText = (EditText)v.findViewById(R.id.editText);

            label.setText(brands.get(i).getName());
            label.setTag(brands.get(i).getCode());
            label.setTypeface(tf);
            statementlbl.setTag(v);

            statementlbl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(view);
                }
            });
            b15layout.addView(v);
        }
    }

    private void add() {
        questionB15s = new ArrayList<>();
        for (int i = 0; i < b15layout.getChildCount(); i++) {
            View v = b15layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            TextView statementlbl = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editText = (EditText)v.findViewById(R.id.editText);

            QuestionB15 questionB15 = new QuestionB15();
            questionB15.setBrandCode((int)label.getTag());
            questionB15.setBrand(label.getText().toString());

            questionB15.setStatement(statementlbl.getText().toString());
            questionB15.setOpenEnded(editText.getText().toString());
            questionB15s.add(questionB15);


        }

        active.setQuestionB15(new Gson().toJson(questionB15s));
    }

    private  void showDialog(final View lblView)
    {
        n1aVal = "";
        View rootView = getLayoutInflater().inflate(R.layout.custom_layout_n1, null);
        final LinearLayout statementGrid = (LinearLayout)rootView.findViewById(R.id.statementGrid);
        TextView lblN1a = (TextView)rootView.findViewById(R.id.lbl);
        final Button btnOk = (Button)rootView.findViewById(R.id.btnOK);
        lblN1a.setTypeface(tf);
        final String[] array = getResources().getStringArray(R.array.arrayB15);
        for (int i =0;i<array   .length;i++) {
            View v = getLayoutInflater().inflate(R.layout.multi_choice, null);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkbox1);
            checkBox.setId(i);
            checkBox.setChecked(false);
            checkBox.setOnCheckedChangeListener(CheckBox_OnCheckedChangeListener);
            checkBox.setText(array[i]);
            checkBox.setTypeface(tf);
            checkBox.setTag(i+1);
            statementGrid.addView(v);

            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
        btnOk.setTag(rootView);

        final android.app.AlertDialog deleteDialog = new android.app.AlertDialog.Builder(ActivityB15.this).create();
        deleteDialog.setView(rootView);
        deleteDialog.show();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


             //   n1aVal += (int) lblView.getTag() + ",";
                if (!n1aVal.isEmpty()) {
                    View lblTextView = (View) lblView.getTag();
                    TextView textView = (TextView) lblTextView.findViewById(R.id.lblCheckList);
                    textView.setText(n1aVal.substring(0, n1aVal.length() - 1));
                    deleteDialog.dismiss();
                }

            }
        });


    }
    private CompoundButton.OnCheckedChangeListener CheckBox_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


            int val = (int) buttonView.getTag();
            if (isChecked) {
                n1aVal += val + ",";
                Toast.makeText(ActivityB15.this, "" + n1aVal, Toast.LENGTH_SHORT).show();
            }
            else
            {
                n1aVal = n1aVal.replace(val + ",","");
            }
        }
    };
    private boolean validate()
    {

         for (int i = 0; i < b15layout.getChildCount(); i++) {
            View v = b15layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            TextView lblStatements = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editText = (EditText)v.findViewById(R.id.editText);

            if (lblStatements.getText().toString().equalsIgnoreCase("Select"))
            {
                showDialog("Please Select At least one option");
                return false;
            }
        }
//        if (questionB15s.size() == 0)
//        {
//            showDialog("Please Select At least one option");
//            return false;
//        }


        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }


        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB15.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate())
                            {
                                add();

                                if (Integer.parseInt(active.getQuestionB11()) == 1)
                                {
                                    Intent intent = new Intent(ActivityB15.this, ActivityB18.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }
                                else
                                {
                                    Intent intent = new Intent(ActivityB15.this, ActivityB16.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }
                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB15.setTypeface(tf);
                            active = (Active)getIntent().getSerializableExtra("active");
                            renderB15Layout();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}