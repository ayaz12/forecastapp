package markematics.active17213.DataAccess.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;



import java.util.ArrayList;
import java.util.List;

import markematics.active17213.DataAccess.DataBaseUtil;
import markematics.active17213.Model.Location;

/**
 * Created by hafiz on 8/9/2016.
 */
public class LocationDAO {
    Context context;
    private SQLiteDatabase db;
    DataBaseUtil dbUtil;

    public LocationDAO(Context context)
    {
        this.context = context;

    }
    public Boolean add(Location location) {
        try
        {

            //	delete(); // because at a time only one user record exist in
            // database

            dbUtil = new DataBaseUtil(context);
            db = dbUtil.openConnection();

            ContentValues values = new ContentValues();

            values.put(Location.FIELD_DATE, location.getDate());
            values.put(Location.FIELD_LAT, location.getLat());
            values.put(Location.FIELD_LNG, location.getLng());
            values.put(Location.FIELD_UID,location.getHrId());
            values.put(Location.FIELD_USERNAME,location.getUserName());
            values.put(Location.FIELD_CITY,location.getCity());



            db.insert(Location.TABLE_NAME, null, values);
            return true;

        } catch (SQLException e)
        {
            Log.e("DB Exception", e.toString());

            return false;
        } finally
        {
            dbUtil.closeConnection();
        }
    }


    public Boolean delete() {
        try
        {

            dbUtil = new DataBaseUtil(context);
            this.db = dbUtil.openConnection();
            db.delete(Location.TABLE_NAME, null, null);

            return true;

        } catch (Exception e)
        {
            Log.e("DB Exception", e.toString());
            return false;

        } finally
        {
            dbUtil.closeConnection();
        }

    }

    public Boolean delete(Location location) {
        try {

            dbUtil = new DataBaseUtil(context);
            this.db = dbUtil.openConnection();
            db.delete(Location.TABLE_NAME, "Id=?", new String[] { String.valueOf(location.getId()) });
            //  db.delete(Report.TABLE_NAME, null, null);

            return true;

        } catch (Exception e) {
            Log.e("DB Exception", e.toString());
            return false;

        } finally {
            dbUtil.closeConnection();
        }

    }



    public Location getLocation(String rId,String Lat,String Lng,String userId) {
        Location location = null;

        try
        {

            dbUtil = new DataBaseUtil(context);
            db = dbUtil.openConnection();

            Cursor cursor = db.rawQuery(Location.getLocation(rId,Lat,Lng,userId), null);

            if (cursor.moveToFirst())
            {
                do
                {
                    location = Location.ConvertToLocation(cursor);
                } while (cursor.moveToNext());
            }
            cursor.close();
            return location;
        } catch (SQLException e)
        {
            Log.e("DB Exception", e.toString());
            return location;
        } finally
        {

            dbUtil.closeConnection();
        }
    }
    public List<Location> getLocations() {
        List<Location> locations = new ArrayList<Location>();

        try
        {

            dbUtil = new DataBaseUtil(context);
            db = dbUtil.openConnection();

            Cursor cursor = db.rawQuery(Location.SELECT_LOCATION_QUERY, null);

            if (cursor.moveToFirst()) {
                do {
                    Location location = Location.ConvertToLocation(cursor);

                    locations.add(location);

                } while (cursor.moveToNext());
            }
            cursor.close();
            return locations;
        } catch (SQLException e)
        {
            Log.e("DB Exception", e.toString());
            return locations;
        } finally
        {

            dbUtil.closeConnection();
        }
    }



}
