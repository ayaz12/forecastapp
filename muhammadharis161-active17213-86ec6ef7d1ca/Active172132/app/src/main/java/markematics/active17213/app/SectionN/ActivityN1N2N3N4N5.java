package markematics.active17213.app.SectionN;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.markematics.Active17213.R;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityHome;
import markematics.active17213.app.ActivityIntro;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionA.ActivityA11;
import markematics.active17213.app.SectionH.ActivityH17b;


public class ActivityN1N2N3N4N5 extends AppCompatActivity {

    TextView tViewN1,tViewN2,tViewN3,tViewN4,tViewN5;
    EditText editTextN3, editTextN4;
    Spinner spinnerN1,spinnerN2,spinnerN5;
    Button btnNext;
    String N3,N4;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_n1_n2_n3_n4_n5);

        tViewN1 = (TextView)findViewById(R.id.tViewN1);
        tViewN2 = (TextView)findViewById(R.id.tViewN2);
        tViewN3 = (TextView)findViewById(R.id.tViewN3);
        tViewN4 = (TextView)findViewById(R.id.tViewN4);
        tViewN5 = (TextView)findViewById(R.id.tViewN5);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question N1 N2 N3 N4 N5 ");
        toolbar.setTitleTextColor(Color.WHITE);

        editTextN3 = (EditText)findViewById(R.id.editTextN3);
        editTextN4 = (EditText)findViewById(R.id.editTextN4);

        spinnerN1 = (Spinner)findViewById(R.id.spinnerN1);
        spinnerN2 = (Spinner)findViewById(R.id.spinnerN2);
        spinnerN5 = (Spinner)findViewById(R.id.spinnerN5);

        btnNext = (Button)findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewN1.setTypeface(tf);
        tViewN2.setTypeface(tf);
        tViewN3.setTypeface(tf);
        tViewN4.setTypeface(tf);
        tViewN5.setTypeface(tf);

        editTextN3.setTypeface(tf);
        editTextN4.setTypeface(tf);
        active = (Active)getIntent().getSerializableExtra("active");


    ArrayAdapter<String> spinnerN1ArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, getResources().getStringArray(R.array.arrayN1));
        spinnerN1.setAdapter(spinnerN1ArrayAdapter);

    ArrayAdapter<String> spinnerN2ArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, getResources().getStringArray(R.array.arrayN2));
        spinnerN2.setAdapter(spinnerN2ArrayAdapter);

        ArrayAdapter<String> spinnerN5ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayN5));
        spinnerN5.setAdapter(spinnerN5ArrayAdapter);


        btnNext.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            next();
        }
    });
}

    private void next() {

        initialize();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n", Toast.LENGTH_SHORT).show();
        } else {
            add();
            StopRecording();

        }

    }
    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityN1N2N3N4N5.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityN1N2N3N4N5.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    proceed();
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private boolean validationSuccess() {

        boolean valid = true;
//        if (N3.isEmpty()) {
//            editTextN3.setError("درج کریں");
//            valid = false;
//        }
        if (N4.isEmpty()) {
            editTextN4.setError("درج کریں");
            valid = false;
        }
        return valid;
    }

    private void proceed() {

        Utility.showExitDialog(ActivityN1N2N3N4N5.this,"Thankyou for Interview",active);

    }

    private void initialize() {
     //   N3 = editTextN3.getText().toString().trim();
        N4 = editTextN4.getText().toString().trim();

    }
    private void add(){
        active.setQuestionN1(""+spinnerN1.getSelectedItemPosition());
        active.setQuestionN2(""+spinnerN2.getSelectedItemPosition());
     //   active.setQuestionN3(editTextN3.getText().toString().trim());
        active.setQuestionN4(editTextN4.getText().toString().trim());
        active.setQuestionN5(""+spinnerN5.getSelectedItemPosition());

    }
}