package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/10/2017.
 */

public class QuestionE5 {

    private String openEnded;
    private String BrandName;
    private int brandCode;
    private int code;

    public String getOpenEnded() {
        return openEnded;
    }

    public void setOpenEnded(String openEnded) {
        this.openEnded = openEnded;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public int getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(int brandCode) {
        this.brandCode = brandCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
