package markematics.active17213.app.Sectionc;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Model.Active;
import markematics.active17213.app.SectionS.ActivityS10;

/**
 * Created by mas on 10/18/2017.
 */

public class ActivityStartC extends AppCompatActivity {

    Toolbar toolbar;
    TextView tViewC;
    Button btnStart;
    Typeface tf;
    Active active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_section_c);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Section C");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewC =(TextView)findViewById(R.id.tViewstartC) ;
        btnStart = (Button) findViewById(R.id.btnstart);
        btnStart.setTextColor(Color.parseColor("#FFFFFF"));
        tf = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewC.setTypeface(tf);
        btnStart.setOnClickListener(btnNext_OnClickListener);
        active = (Active)getIntent().getSerializableExtra("active");

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ActivityStartC.this, ActivityS10.class);
            intent.putExtra("active", active);
            startActivity(intent);
        }
    };
}
