package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB14;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB14 extends AppCompatActivity {

    TextView tViewb14,lblChildName;
    Button btnNext;
    Toolbar toolbar;
    Typeface tf;
    Active active;
    LinearLayout b14Layout;
    List<QuestionB14> questionB14s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b14);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B14");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewb14 = (TextView) findViewById(R.id.tViewB14);
        lblChildName = (TextView)findViewById(R.id.lblChildName);

        btnNext = (Button) findViewById(R.id.btnNext);

        b14Layout = (LinearLayout)findViewById(R.id.layoutB14);


        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();
    }

    private void renderB14()
    {
        b14Layout.removeAllViews();
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
    }.getType();

        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);
        List<Brand> brands = new ArrayList<>();
       for (int i =0;i<questionB5s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB5s.get(i).getCode());
            brand.setName(questionB5s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.b14_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);
            ArrayAdapter<String> spinnerb8bArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.spinnerB14));
            spinnerBrand.setAdapter(spinnerb8bArrayAdapter);
            RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.brandLayout);
            label.setText(brands.get(i).getName());
            label.setTag(brands.get(i).getCode());
            b14Layout.addView(v);
        }
    }

    private void add()
    {
        questionB14s = new ArrayList<>();
        for (int i = 0; i < b14Layout.getChildCount(); i++) {
            View v = b14Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);


            if (spinnerBrand.getSelectedItemPosition() !=0)
            {
                QuestionB14 questionB14 = new QuestionB14();
                questionB14.setBrandCode((int)label.getTag());
                questionB14.setBrandName(label.getText().toString());
                questionB14.setResponseCode(spinnerBrand.getSelectedItemPosition());
                questionB14s.add(questionB14);
            }



        }

        active.setQuestionB14(new Gson().toJson(questionB14s));
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

           new AsyncHandler(REQUEST_ADD).execute();
        }
    };
    private boolean validate()
    {
        for (int i = 0;i<b14Layout.getChildCount();i++) {
            View v = b14Layout.getChildAt(i);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);



            if (spinnerBrand.getSelectedItemPosition() == 0)
            {
                showDialog("Please fill all B14 values");
                return false;
            }

        }




        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }


        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB14.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate())
                            {
                                //   active.setQuestionB8b(""+spinnerB14.getSelectedItemPosition());
                                add();
                                Intent intent = new Intent(ActivityB14.this, ActivityB15.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewb14.setTypeface(tf);

                            lblChildName.setTypeface(tf);
                            active = (Active)getIntent().getSerializableExtra("active");
                            lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
                            renderB14();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
