package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/9/2017.
 */

public class QuestionB10 {

    private String brand;
    private int code;
    private String reason;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
