package markematics.active17213.app.SectionB;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.SpinnerB18Adapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB18;

/**
 * Created by mas on 9/27/2017.
 */

public class ActivityB18Old extends AppCompatActivity {

    TextView tViewB18, tViewb181;
    Spinner spinnerb18;
    Button btnb18;
    Typeface tf;
    LinearLayout b18_layout;
    Toolbar toolbar;
    Active active;
    String n1aVal = "";
    List<QuestionB18> questionB18s;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b18_old);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B18");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB18 = (TextView) findViewById(R.id.tViewB18);
        tViewb181 = (TextView) findViewById(R.id.tViewb181);
        spinnerb18 = (Spinner) findViewById(R.id.spinnerb18);
        btnb18 = (Button) findViewById(R.id.btnb18);
        tViewB18.setTypeface(tf);
        tViewb181.setTypeface(tf);
        b18_layout = (LinearLayout) findViewById(R.id.b18layout);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewB18.setTypeface(tf);
        tViewb181.setTypeface(tf);
        active = (Active) getIntent().getSerializableExtra("active");
        renderB18Layout();
        btnb18.setOnClickListener(b18a_OnClickListener);

    }

    private View.OnClickListener b18a_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            add();
            if (validate())
            {
                Intent intent = new Intent(ActivityB18Old.this, ActivityB19B20.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    };

    private void renderB18Layout() {
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
        List<Brand> brands = new ArrayList<>();
        Brand brand = new Brand();
        brand.setCode(0);
        brand.setName("Select");
        brands.add(brand);
        for (int i = 0; i < questionB1s.size(); i++) {
             brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB1s.get(i).getOpenEnded() : questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < questionB2s.size(); i++) {
             brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB2s.get(i).getOpenEnded() : questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < questionB3s.size(); i++) {
             brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB3s.get(i).getOpenEnded() : questionB3s.get(i).getBrand());
            brands.add(brand);
        }
        SpinnerB18Adapter adapter = new SpinnerB18Adapter(ActivityB18Old.this,R.layout.spinner_item, R.id.txt, brands);
        spinnerb18.setAdapter(adapter);
        String[] arr = getResources().getStringArray(R.array.arrayb18);
        for (int i = 0; i < arr.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b15_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            TextView statementlbl = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editText = (EditText)v.findViewById(R.id.editText);

            label.setText(arr[i]);
            label.setTag(i+1);
            label.setTypeface(tf);
            statementlbl.setTag(v);


            statementlbl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(view);
                }
            });
            b18_layout.addView(v);
        }


    }
    private  void showDialog(final View lblView)
    {
        n1aVal = "";
        View rootView = getLayoutInflater().inflate(R.layout.custom_layout_n1, null);
        final LinearLayout statementGrid = (LinearLayout)rootView.findViewById(R.id.statementGrid);
        TextView lblN1a = (TextView)rootView.findViewById(R.id.lbl);
        final Button btnOk = (Button)rootView.findViewById(R.id.btnOK);
        lblN1a.setTypeface(tf);
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
        List<Brand> brands = new ArrayList<>();
        for (int i = 0; i < questionB1s.size(); i++) {
            Brand brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB1s.get(i).getOpenEnded() : questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < questionB2s.size(); i++) {
            Brand brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB2s.get(i).getOpenEnded() : questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < questionB3s.size(); i++) {
            Brand brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others") ?
                    questionB3s.get(i).getOpenEnded() : questionB3s.get(i).getBrand());
            brands.add(brand);
        }

        for (int i =0;i<brands.size();i++) {
            View v = getLayoutInflater().inflate(R.layout.multi_choice, null);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkbox1);
            checkBox.setId(i);
            checkBox.setChecked(false);
            checkBox.setOnCheckedChangeListener(CheckBox_OnCheckedChangeListener);
            checkBox.setText(brands.get(i).getName());
            checkBox.setTypeface(tf);
            checkBox.setTag(brands.get(i).getCode());
            statementGrid.addView(v);

            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
        btnOk.setTag(rootView);

        final android.app.AlertDialog deleteDialog = new android.app.AlertDialog.Builder(ActivityB18Old.this).create();
        deleteDialog.setView(rootView);
        deleteDialog.show();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //   n1aVal += (int) lblView.getTag() + ",";
                if (!n1aVal.isEmpty()) {
                    View lblTextView = (View) lblView.getTag();
                    TextView textView = (TextView) lblTextView.findViewById(R.id.lblCheckList);
                    textView.setText(n1aVal.substring(0, n1aVal.length() - 1));
                    deleteDialog.dismiss();
                }

            }
        });


    }
    private CompoundButton.OnCheckedChangeListener CheckBox_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            int val = (int) buttonView.getTag();

            n1aVal +=val+",";
            Toast.makeText(ActivityB18Old.this,""+n1aVal,Toast.LENGTH_SHORT).show();
        }
    };


    private void add()
    {
        Brand brand = (Brand)spinnerb18.getSelectedItem();
        active.setQuestionB18a(""+brand.getCode());
        questionB18s = new ArrayList<>();
        for (int i =0;i<b18_layout.getChildCount();i++)
        {
            View v = b18_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            TextView statementlbl = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editText = (EditText)v.findViewById(R.id.editText);

            QuestionB18 questionB18 = new QuestionB18();
            questionB18.setBrand(statementlbl.getText().toString());
            questionB18.setCode((int)label.getTag());
            questionB18s.add(questionB18);

        }
        active.setQuestionB18(new Gson().toJson(questionB18s));
    }


    private boolean validate()
    {

        if (spinnerb18.getSelectedItemPosition() == 0)
        {
            showDialog("Please select B18a");
            return false;
        }
        if (questionB18s.size() == 0)
        {
            showDialog("Please Select At least one option");
            return false;
        }


        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

}