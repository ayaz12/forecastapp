package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH12;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionC.ActivityC1;
import markematics.active17213.app.SectionN.ActivityN1N2N3N4N5;

public class ActivityH17b extends AppCompatActivity {

    TextView tViewH17b;
    Spinner  spinnerH17b;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h17b);


        tViewH17b = (TextView) findViewById(R.id.tViewH17b);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H17b");
        toolbar.setTitleTextColor(Color.WHITE);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

         active = (Active) getIntent().getSerializableExtra("active");
         spinnerH17b = (Spinner) findViewById(R.id.spinnerH17b);

        btnNext = (Button) findViewById(R.id.btnNext);
        tViewH17b.setTypeface(tf);

        ArrayAdapter<String> spinnerH16ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH17b.setAdapter(spinnerH16ArrayAdapter);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {
        if (spinnerH17b.getSelectedItemPosition() != 0) {
            add();
           proceed();
        }else
        {
            showDialog("Please select at least one option H17b");
        }


    }


    private void proceed() {
        Intent nextPage = new Intent(ActivityH17b.this, ActivityN1N2N3N4N5.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void add(){


        active.setQuestionH17b("" + spinnerH17b.getSelectedItemPosition());

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}