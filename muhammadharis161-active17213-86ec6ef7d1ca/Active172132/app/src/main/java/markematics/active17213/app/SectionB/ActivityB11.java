package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB11 extends AppCompatActivity {

    TextView tViewB11,lblBrandName;
    Spinner spinnerB11;
    Button btnNext;
    Typeface tf;
    View view;
    Toolbar toolbar;
    Active active;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b11);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B11");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB11 = (TextView) findViewById(R.id.tViewB11);
        lblBrandName = (TextView)findViewById(R.id.lblBrandName);
        spinnerB11 = (Spinner) findViewById(R.id.spinnerb11);
        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(btnNext_OnClickListener);

        new AsyncHandler(REQUEST_RENDER).execute();
       }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };



    private boolean validationSuccess() {

      if (spinnerB11.getSelectedItemPosition() == 0)
      {
          showDialog("Please Select B11");
          return false;
      }

        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB11.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            final String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validationSuccess()) {

                                active.setQuestionB11("" + spinnerB11.getSelectedItemPosition());

                                int childCount = Integer.parseInt(active.getQuestionS9());
                                if (childCount > 1) {
                                    Intent intent = new Intent(ActivityB11.this, ActivityB12.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(ActivityB11.this, ActivityB14.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }
                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB11.setTypeface(tf);
                           active = (Active)getIntent().getSerializableExtra("active");
                            ArrayAdapter<String> spinnersB11ArrayAdapter = new ArrayAdapter<String>(ActivityB11.this,R.layout.spinner_item, getResources().getStringArray(R.array.arrayb11));
                            spinnerB11.setAdapter(spinnersB11ArrayAdapter);

                            Type listType = new TypeToken<ArrayList<QuestionB4>>() {
                            }.getType();
                            List<QuestionB4> questionB6s = new Gson().fromJson(active.getQuestionB6(), listType);
                            lblBrandName.setText(questionB6s.get(0).getBrand()+" برانڈ کا نام ");
                            lblBrandName.setTypeface(tf);
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
