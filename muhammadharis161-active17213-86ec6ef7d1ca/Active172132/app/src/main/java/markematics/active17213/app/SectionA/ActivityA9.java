package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Adapter.CustomSpinnerAdapter;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionS12;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA9 extends AppCompatActivity {
    TextView tViewA9;
    EditText editTextA9;
    Button btnA6;
    Typeface tf;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a9);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A9");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewA9 = (TextView) findViewById(R.id.tviewA9);
        editTextA9 = (EditText) findViewById(R.id.editTextA9);


        btnA6=(Button)findViewById(R.id.btnNext);
        btnA6.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };



    private boolean validationSuccess() {



        if (editTextA9.getVisibility() == View.VISIBLE) {
            if (editTextA9.getText().toString().isEmpty()) {
                editTextA9.setError("درج کریں ");
                return false;

            }
        }
        return true;
    }



    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA9.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validationSuccess())
                            {
                                active.setQuestionA9(editTextA9.getText().toString());
                                Intent intent = new Intent(ActivityA9.this, ActivityA10.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            active = (Active) getIntent().getSerializableExtra("active");
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewA9.setTypeface(tf);
                            editTextA9.setTypeface(tf);

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
