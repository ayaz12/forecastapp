package markematics.active17213.app.SectionS;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityIntro;

public class ActivityS10 extends AppCompatActivity {

    Spinner spinnerS10;
    TextView textViewS10, lblChildName;
    Typeface tf;
    Button btnNext;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s10);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S10");
        toolbar.setTitleTextColor(Color.WHITE);
        lblChildName = (TextView) findViewById(R.id.lblChildName);
        textViewS10 = (TextView) findViewById(R.id.tViewS10);
        spinnerS10 = (Spinner) findViewById(R.id.spinner_s10);


        btnNext = (Button) findViewById(R.id.btnS10);
        btnNext.setTextColor(Color.parseColor("#FFFFFF"));
        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

        spinnerS10.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 1) {
                    btnNext.setText("Finish");
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                } else {
                    btnNext.setText("NEXT");
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void add() {
        active.setQuestionS10("" + spinnerS10.getSelectedItemPosition());
    }

    private boolean validationSuccess() {

        if (spinnerS10.getSelectedItemPosition() == 0) {
            btnNext.setTextColor(Color.parseColor("#FFFFFF"));
            showDialog("Please Select At least one option ");
            return false;
        }
        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS10.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                            if (validationSuccess()) {
                                add();

                                if (spinnerS10.getSelectedItemPosition() == 1) {

                                    Intent intent = new Intent(ActivityS10.this, ActivityS11.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                } else if (spinnerS10.getSelectedItemPosition() == 2 || spinnerS10.getSelectedItemPosition() == 3) {
                                    Intent intent = new Intent(ActivityS10.this, ActivityS12.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            tf = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            textViewS10.setTypeface(tf);
                            ArrayAdapter<String> spinnerS10ArrayAdapter = new ArrayAdapter<String>(ActivityS10.this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayS10));
                            spinnerS10.setAdapter(spinnerS10ArrayAdapter);
                            active = (Active) getIntent().getSerializableExtra("active");
                            lblChildName.setText(active.getChildName() + " منتخب بچے کا نام ");
                            lblChildName.setTypeface(tf);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}