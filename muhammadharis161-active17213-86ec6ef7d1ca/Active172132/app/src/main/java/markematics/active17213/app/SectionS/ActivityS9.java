package markematics.active17213.app.SectionS;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionS9;


public class ActivityS9 extends AppCompatActivity {

    TextView tViewS9a,tViewS9b;
    LinearLayout s9Layout;
    Typeface tf;
    Button btnNext;
    Toolbar toolbar;
    Active active;
    Spinner spinners9a;
    List<QuestionS9> questionS9s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s9);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S9");
        toolbar.setTitleTextColor(Color.WHITE);
        s9Layout = (LinearLayout) findViewById(R.id.s9_layout);

        tViewS9a = (TextView) findViewById(R.id.tViewS9a);
        tViewS9b = (TextView)findViewById(R.id.tViewS9b);

        btnNext = (Button) findViewById(R.id.btnS9);

        btnNext.setOnClickListener(btnNext_OnClickListener);

        spinners9a = (Spinner) findViewById(R.id.spinnerS9a);
        btnNext.setTextColor(Color.parseColor("#FFFFFF"));

        new AsyncHandler(REQUEST_RENDER).execute();

    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void randerS9Layout() {
            s9Layout.removeAllViews();
        for (int i = 0; i <= 5; i++) {
            View v = getLayoutInflater().inflate(R.layout.s9_layout, null);
            Spinner spinnerGender = (Spinner) v.findViewById(R.id.spinnerGender);
            EditText editTextName = (EditText) v.findViewById(R.id.editTextName);
            TextView lblDateOfBirth = (TextView) v.findViewById(R.id.txtBirth);
            lblDateOfBirth.setText("Select Date");
            ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.genderArray));
            spinnerGender.setAdapter(genderAdapter);
            ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.yearArray));
            lblDateOfBirth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDateDialog(view);
                }
            });

            s9Layout.addView(v);
        }
    }


    private void showDateDialog(final View textView) {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(ActivityS9.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, selectedyear);
                calendar.set(Calendar.MONTH, selectedmonth);
                calendar.set(Calendar.DAY_OF_MONTH, selectedday);
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String formattedDate = df.format(calendar.getTime());
                if (textView instanceof TextView) {
                    ((TextView) textView).setText(formattedDate);
                }
             //
                //   Toast.makeText(ActivityS9.this, "Date " + formattedDate, Toast.LENGTH_SHORT).show();

            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private boolean validate() {
        if (spinners9a.getSelectedItemPosition() == 0) {
            showDialog("Please select Children ");
            return false;
        }
        if (questionS9s.size() == 0) {
            showDialog("Please select at least one option");
            return false;
        }
//        for (int i =0;i<questionS9s.size();i++)
//        {
//            if (questionS9s.get(i).getBirth() == null)
//            {
//                showDialog("Please select Birth date");
//                return false;
//
//            }
//        }
        return true;
    }


    private void add() {
        questionS9s = new ArrayList<>();
        for (int i = 0; i < s9Layout.getChildCount(); i++) {
            View v = s9Layout.getChildAt(i);
            Spinner spinnerGender = (Spinner) v.findViewById(R.id.spinnerGender);
            EditText editTextName = (EditText) v.findViewById(R.id.editTextName);
            TextView lblDateOfBirth = (TextView) v.findViewById(R.id.txtBirth);

            if (!(editTextName.getText().toString().isEmpty()) && spinnerGender.getSelectedItemPosition() != 0
                    && !(lblDateOfBirth.getText().toString().isEmpty())) {
                QuestionS9 questionS9 = new QuestionS9();
                questionS9.setName(editTextName.getText().toString());
                questionS9.setGender(spinnerGender.getSelectedItemPosition());
                questionS9.setBirth(lblDateOfBirth.getText().toString());
                questionS9.setTotalDays(calculateNextBirthday(lblDateOfBirth.getText().toString()));
                questionS9s.add(questionS9);
            }
        }

        active.setQuestionS9((String) spinners9a.getSelectedItem());
        active.setQuestionS9b(new Gson().toJson(questionS9s));
    }
    private Date getDateNearest(List<Date> dates, Date targetDate){
        return new TreeSet<Date>(dates).lower(targetDate);
    }
    private void getChild()
    {
        final long now = System.currentTimeMillis();
        List<Long> days = new ArrayList<>();
        Calendar c = Calendar.getInstance();

        for (int i = 0;i<questionS9s.size();i++)
        {
          days.add(questionS9s.get(i).getTotalDays());
        }

          long val =   findMinMax(days);
        for (int i = 0;i<questionS9s.size();i++)
        {
           if (val == questionS9s.get(i).getTotalDays()) {
               active.setChildName(questionS9s.get(i).getName());
               Toast.makeText(ActivityS9.this, "Closest Date:" + questionS9s.get(i).getBirth() +" Name:"+questionS9s.get(i).getName(), Toast.LENGTH_SHORT).show();
           }
        }



    }

    public long  findMinMax(List<Long> array) {
        if (array == null || array.size() < 1)
            return 0;
        long min = array.get(0);
        long max = array.get(0);

        for (int i = 1; i <= array.size() - 1; i++) {

            if (min >  array.get(i)) {
                min =  array.get(i);
            }

        }
        return min;
    }
    public static Date closerDate(Date originalDate, Collection<Date> unsortedDates) {
        List<Date> dateList = new LinkedList<Date>(unsortedDates);
        Collections.sort(dateList);
        Iterator<Date> iterator = dateList.iterator();
        Date previousDate = null;
        while (iterator.hasNext()) {
            Date nextDate = iterator.next();
            if (nextDate.before(originalDate)) {
                previousDate = nextDate;
                continue;
            } else if (nextDate.after(originalDate)) {
                if (previousDate == null || isCloserToNextDate(originalDate, previousDate, nextDate)) {
                    return nextDate;
                }
            } else {
                return nextDate;
            }
        }
        return previousDate;
    }

    private static boolean isCloserToNextDate(Date originalDate, Date previousDate, Date nextDate) {
        if(previousDate.after(nextDate))
            throw new IllegalArgumentException("previousDate > nextDate");
        return ((nextDate.getTime() - previousDate.getTime()) / 2 + previousDate.getTime() <= originalDate.getTime());
    }
    private Date stringToDate(String aDate,String aFormat) {

        if(aDate==null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;

    }
    private long calculateNextBirthday(String date) {
        // TODO Auto-generated method stub
     Date birthDate =    stringToDate(date,"dd-MMM-yyyy");
    /*Date dt = new Date();  //this is just current date, which was causing issue. It should be user selected date
    SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy/MM/dd");
    final String strBDay = sdf.format(dt);
    try {

        dt = sdf.parse(strBDay);
    } catch (final java.text.ParseException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }*/

        final Calendar c = Calendar.getInstance();

        c.setTime(birthDate);  //this is birthdate. set it to date you get from date picker
        // c.add(Calendar.DATE, value);
        final Calendar today = Calendar.getInstance();
        // Take your DOB Month and compare it to current
        // month
        final int BMonth = c.get(Calendar.MONTH);
        final int CMonth = today.get(Calendar.MONTH);

        final int BDate = c.get(Calendar.DAY_OF_MONTH);
        final int CDate = today.get(Calendar.DAY_OF_MONTH);

        c.set(Calendar.YEAR, today.get(Calendar.YEAR));
        c.set(Calendar.DAY_OF_WEEK,
                today.get(Calendar.DAY_OF_WEEK));
        if (BMonth < CMonth) {
            c.set(Calendar.YEAR,
                    today.get(Calendar.YEAR) + 1);
        }
        //added this condition so that it doesn't add in case birthdate is greater than current date . i.e. yet to come in this month.
        else if (BMonth == CMonth){
            if(BDate < CDate){
                c.set(Calendar.YEAR,
                        today.get(Calendar.YEAR) + 1);
            }
        }
        // Result in millis

        final long millis = c.getTimeInMillis()
                - today.getTimeInMillis();
        // Convert to days
        final long days = millis / 86400000; // Precalculated
        // (24 *
        // 60 *
        // 60 *
        // 1000)
        // final String dayOfTheWeek =
        // sdf.format(BDay.getTime());
    /*sdf = new SimpleDateFormat("EEEE");
    final String dayOfTheWeek = sdf.format(dt);*/
//        Toast.makeText(getApplicationContext(), String.valueOf(days), Toast.LENGTH_SHORT).show();
        return days;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS9.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                            add();
                            if (validate()) {
                                getChild();
                                Intent nextPage = new Intent(ActivityS9.this, ActivityMessageS9.class);
                                nextPage.putExtra("active", active);
                                startActivity(nextPage);
                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            active = (Active) getIntent().getSerializableExtra("active");
                            tf = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

                            tViewS9b.setTypeface(tf);
                            tViewS9a.setTypeface(tf);

                            ArrayAdapter<String> spinnerS1ArrayAdapter = new ArrayAdapter<String>(ActivityS9.this, R.layout.spinner_item, getResources().getStringArray(R.array.spinnerarrayS9a));
                            spinners9a.setAdapter(spinnerS1ArrayAdapter);

                            randerS9Layout();
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
