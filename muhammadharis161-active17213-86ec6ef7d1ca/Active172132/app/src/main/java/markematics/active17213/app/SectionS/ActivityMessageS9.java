package markematics.active17213.app.SectionS;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Model.Active;

/**
 * Created by mas on 10/18/2017.
 */

public class ActivityMessageS9 extends AppCompatActivity {

    Toolbar toolbar;
    TextView tViewMessage,lblChildName;
    Button btnNext;
    Typeface tf;
    Active active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_s9);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Child Name");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewMessage =(TextView)findViewById(R.id.tViewS9_10) ;
        btnNext = (Button) findViewById(R.id.btnS9);
        btnNext.setTextColor(Color.parseColor("#FFFFFF"));
        tf = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        lblChildName = (TextView)findViewById(R.id.lblChildName);
        tViewMessage.setTypeface(tf);
        btnNext.setOnClickListener(btnNext_OnClickListener);
        active = (Active)getIntent().getSerializableExtra("active");
        lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
        lblChildName.setTypeface(tf);

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ActivityMessageS9.this, ActivityS10.class);
            intent.putExtra("active", active);
            startActivity(intent);
        }
    };
}
