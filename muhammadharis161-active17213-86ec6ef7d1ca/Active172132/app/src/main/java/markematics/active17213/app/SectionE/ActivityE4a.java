package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB14;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by ZEB Rana on 16-Oct-17.
 */

public class ActivityE4a extends AppCompatActivity {
    TextView tViewE4a, lblChildName;
    LinearLayout e4aLayout;
     Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB14> questionE4as;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e4a);

        tViewE4a = (TextView) findViewById(R.id.tViewE4a);
        lblChildName = (TextView)findViewById(R.id.lblChildName);
        e4aLayout = (LinearLayout)findViewById(R.id.e4aLayout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question E4a");
        toolbar.setTitleTextColor(Color.WHITE);
        active = (Active) getIntent().getSerializableExtra("active");
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        btnNext = (Button) findViewById(R.id.btnNext);


        tViewE4a.setTypeface(tf);
        lblChildName.setText(active.getChildName()+" منتخب بچے کا نام ");
        lblChildName.setTypeface(tf);

        btnNext.setOnClickListener(btnNext_OnClickListener);
        renderE4a();
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validationSuccess())
            {
                add();
                proceed();
            }
        }
    };


    private void renderE4a()
    {
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        //   List<QuestionB4> questionB6s = new Gson().fromJson(active.getQuestionB6(), listType);
        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);
        List<Brand> brands = new ArrayList<>();
//        for (int i =0;i<questionB6s.size();i++)
//        {
//            Brand brand = new Brand();
//            brand.setCode(questionB6s.get(i).getCode());
//            brand.setName(questionB6s.get(i).getStatement());
//            brands.add(brand);
//        }
        for (int i =0;i<questionB5s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB5s.get(i).getCode());
            brand.setName(questionB5s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i = 0; i < brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.b14_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);
            ArrayAdapter<String> spinnerb8bArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arraye4a));
            spinnerBrand.setAdapter(spinnerb8bArrayAdapter);
            RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.brandLayout);
            label.setText(brands.get(i).getName());
            label.setTag(brands.get(i).getCode());
            e4aLayout.addView(v);
        }
    }

    private void proceed() {
        {
            Intent intent = new Intent(ActivityE4a.this, ActivityE4b.class);
            intent.putExtra("active", active);
            startActivity(intent);

        }
    }



    private boolean validationSuccess() {

        for (int i = 0;i<e4aLayout.getChildCount();i++) {
            View v = e4aLayout.getChildAt(i);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);

            if (spinnerBrand.getSelectedItemPosition() == 0)
            {
                showDialog("Please fill all values");
                return false;
            }

        }
        return true;
    }

    private void add() {

        questionE4as = new ArrayList<>();
        for (int i = 0; i< e4aLayout.getChildCount();i++)
        {
            View v = e4aLayout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            Spinner spinnerBrand = (Spinner) v.findViewById(R.id.spinnerBrand);
            if (spinnerBrand.getSelectedItemPosition() !=0)
            {
                QuestionB14 questionB14 = new QuestionB14();
                questionB14.setBrandName(label.getText().toString());
                questionB14.setBrandCode((int)label.getTag());
                questionB14.setResponseCode(spinnerBrand.getSelectedItemPosition());
                questionE4as.add(questionB14);
            }
        }
        active.setQuestionE4a(new Gson().toJson(questionE4as));


    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
