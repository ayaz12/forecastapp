package markematics.active17213.Model;

/**
 * Created by ZEB Rana on 06-Oct-17.
 */

public class QuestionH9 {

    private String statement;
    private int code;
    private String lable;

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }
}
