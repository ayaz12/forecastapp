package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/4/2017.
 */

public class Question11a {
    private int tape;
    private int pant;

    public int getTape() {
        return tape;
    }

    public void setTape(int tape) {
        this.tape = tape;
    }

    public int getPant() {
        return pant;
    }

    public void setPant(int pant) {
        this.pant = pant;
    }
}
