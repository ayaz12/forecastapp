package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH9;


public class ActivityH7H8H9H10H11 extends AppCompatActivity {

    TextView tViewH9,tViewH7,tViewH8, tViewH10,tViewH11 ;
    EditText editTextH11,editTextH8;
    Button btnNext;
    String H11,H8;
    Spinner spinnerH10,spinnerH7;
    LinearLayout H9layout;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionH9> questionH9List;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h7_h8_h9_h10_h11);

        tViewH7 = (TextView) findViewById(R.id.tViewH7);
        tViewH8 = (TextView) findViewById(R.id.tViewH8);

        active = new Active();
        active = (Active) getIntent().getSerializableExtra("active");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H7 H8 H9 H10 H11");
        toolbar.setTitleTextColor(Color.WHITE);

        editTextH8 = (EditText) findViewById(R.id.editTextH8);
        spinnerH7 = (Spinner)findViewById(R.id.spinnerH7);
        tViewH9=(TextView)findViewById(R.id.tViewH9);
        tViewH10=(TextView)findViewById(R.id.tViewH10);
        tViewH11=(TextView)findViewById(R.id.tViewH11);


        editTextH11 = (EditText)findViewById(R.id.editTextH11);
        btnNext = (Button)findViewById(R.id.btnNext);
        spinnerH10 =(Spinner)findViewById(R.id.spinnerH10);

        H9layout = (LinearLayout)findViewById(R.id.H9_layout);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH7.setTypeface(tf);
        tViewH8.setTypeface(tf);
        tViewH9.setTypeface(tf);
        tViewH10.setTypeface(tf);
        tViewH11.setTypeface(tf);


        editTextH11.setTypeface(tf);
        ArrayAdapter<String> spinnerH7ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH7.setAdapter(spinnerH7ArrayAdapter);

        ArrayAdapter<String> spinnerH10ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH10.setAdapter(spinnerH10ArrayAdapter);

        randerH9aLayout();


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
        spinnerH7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 2)
                {
                    tViewH8.setVisibility(View.GONE);
                    tViewH9.setVisibility(View.GONE);
                    editTextH8.setVisibility(View.GONE);
                    H9layout.setVisibility(View.GONE);
                }
                else
                {
                    tViewH8.setVisibility(View.VISIBLE);
                    tViewH9.setVisibility(View.VISIBLE);
                    editTextH8.setVisibility(View.VISIBLE);
                    H9layout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerH10.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position == 2)
                {
                    tViewH11.setVisibility(View.GONE);
                    editTextH11.setVisibility(View.GONE);
                }
                else
                {
                    tViewH11.setVisibility(View.VISIBLE);
                    editTextH11.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void next() {

        initialize();
        add();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n",Toast.LENGTH_SHORT).show();
        } else {

            proceed();

        }

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validationSuccess() {


        boolean valid = true;
        if (editTextH8.getVisibility() == View.VISIBLE) {
            if (H8.isEmpty()) {
                editTextH8.setError("درج کریں");
                valid = false;
            }
            if (questionH9List.size() ==0)
            {
                showDialog("Please select at least one option in H9");
                valid = false;
            }
        }
        if (editTextH11.getVisibility() == View.VISIBLE) {
            if (H11.isEmpty()) {
                editTextH11.setError("درج کریں");
                valid = false;
            }
        }
        return valid;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH7H8H9H10H11.this, ActivityH12H13H14H15H16H17.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void initialize() {
        H11=editTextH11.getText().toString().trim();
        H8=editTextH8.getText().toString().trim();

    }

    private void randerH9aLayout() {
        String[] arrayH9 = getResources().getStringArray(R.array.arrayH9);
        for (int i = 0; i < arrayH9.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH9[i]);
            H9layout.addView(v);
        }
    }

    private void add(){

        active.setQuestionH7(""+ spinnerH7.getSelectedItem());
        active.setQuestionH8(editTextH8.toString());

        questionH9List = new ArrayList<>();
        for (int j = 0; j < H9layout.getChildCount(); j++) {
            View view = H9layout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            QuestionH9 questionH9 = new QuestionH9();

            if (checkBox.isChecked()) {
                questionH9.setCode((int) labeled.getTag());
                questionH9.setStatement(labeled.getText().toString());
                questionH9List.add(questionH9);
            }
        }

        active.setQuestionH10(""+ spinnerH10.getSelectedItem());
        active.setQuestionH11(editTextH11.toString());


    }
}