package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/4/2017.
 */

public class QuestionA4 {
    private int code;
    private String statement;
    private int s12Code;
    private String openEnded;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public int getS12Code() {
        return s12Code;
    }

    public void setS12Code(int s12Code) {
        this.s12Code = s12Code;
    }

    public String getOpenEnded() {
        return openEnded;
    }

    public void setOpenEnded(String openEnded) {
        this.openEnded = openEnded;
    }
}
