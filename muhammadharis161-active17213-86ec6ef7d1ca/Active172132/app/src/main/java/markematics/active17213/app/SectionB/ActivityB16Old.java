package markematics.active17213.app.SectionB;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionB15;
import markematics.active17213.app.ActivityIntro;

/**
 * Created by mas on 10/9/2017.
 */

public class ActivityB16Old extends AppCompatActivity {
    TextView tViewB16;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    EditText editTextB16;
    List<QuestionB15> questionB15s;
    String etB16;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b16);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B16");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB16 = (TextView) findViewById(R.id.tViewB16);
        btnNext = (Button) findViewById(R.id.btnNext);
        active = (Active) getIntent().getSerializableExtra("active");
        editTextB16=(EditText)findViewById(R.id.editTextB16);
        active = (Active)getIntent().getSerializableExtra("active");
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewB16.setTypeface(tf);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });


    }

    private void next() {

        initialize();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n", Toast.LENGTH_SHORT).show();
        } else {
            add();
            proceed();

        }

    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (etB16.isEmpty()) {
            editTextB16.setError("درج کریں");
            valid = false;
        }

        return valid;
    }

    private void proceed() {
        Intent intent = new Intent(ActivityB16Old.this, ActivityIntro.class);
        intent.putExtra("active", active);
        startActivity(intent);
    }

    private void initialize() {
        etB16 = editTextB16.getText().toString().trim();


    }
    private void add(){
        active.setQuestionB16(editTextB16.getText().toString().trim());

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
