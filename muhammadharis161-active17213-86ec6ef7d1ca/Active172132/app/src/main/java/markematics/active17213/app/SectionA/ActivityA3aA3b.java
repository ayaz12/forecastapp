package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.markematics.Active17213.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.DataAccess.HttpHandler;
import markematics.active17213.Helper.KeyValueDB;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionS12;
import markematics.active17213.Model.User;
import markematics.active17213.app.ActivityHome;
import markematics.active17213.app.ActivityLogin;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA3aA3b extends AppCompatActivity {

    TextView tViewA3a,tViewA3b;
    LinearLayout A3a_layout,A3b_layout;
    Button btnNext;
    Typeface tf;
    Active active;
    Toolbar toolbar;
    List<QuestionS12> questionA3as;
    List<QuestionS12> questionA3bs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3a_a3b);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A3");
        toolbar.setTitleTextColor(Color.WHITE);
        active = (Active) getIntent().getSerializableExtra("active");
        tViewA3a=(TextView)findViewById(R.id.tViewA3a);
        tViewA3b=(TextView)findViewById(R.id.tViewA3b);
        A3a_layout=(LinearLayout)findViewById(R.id.A3a_layout);
        A3b_layout=(LinearLayout)findViewById(R.id.A3b_layout);
        btnNext =(Button)findViewById(R.id.btnA3a);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewA3a.setTypeface(tf);
        tViewA3b.setTypeface(tf);
        btnNext.setOnClickListener(btnNext_OnClickListener);
        randerA3aLayout();
        randerA3bLayout();




    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            new AsyncHandler().execute();

        }
    };
    private void randerA3aLayout() {
        String[] arrayA3a = getResources().getStringArray(R.array.arrayA3a);
        for (int i = 0; i < arrayA3a.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            label.setTypeface(tf);

            if (i ==2||i == 7)
            {
                editText.setVisibility(View.VISIBLE);
            }
            label.setTag(i+1);
            label.setText(arrayA3a[i]);
            A3a_layout.addView(v);
        }
    }

    private void randerA3bLayout() {
        String[] arrayA3b = getResources().getStringArray(R.array.arrayA3b);
        for (int i = 0; i < arrayA3b.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            label.setTypeface(tf);

            label.setText(arrayA3b[i]);
            label.setTag(i+1);
            if (i == 3)
            {
                editText.setVisibility(View.VISIBLE);
            }
                A3b_layout.addView(v);
            }

        }

        private void add()
        {
            questionA3as = new ArrayList<>();
            questionA3bs = new ArrayList<>();
            for (int i = 0; i < A3a_layout.getChildCount(); i++) {
                View v = A3a_layout.getChildAt(i);
                TextView label = (TextView) v.findViewById(R.id.lbl);
                CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
                EditText editText = (EditText)v.findViewById(R.id.editText);

                if (chkBox.isChecked())
                {
                    QuestionS12 questionS12 = new QuestionS12();
                    questionS12.setCode((int)label.getTag());
                    questionS12.setOpenEnded(editText.getText().toString());
                    questionS12.setStatement(label.getText().toString());
                    questionA3as.add(questionS12);
                }

            }

            for (int i = 0; i < A3b_layout.getChildCount(); i++) {
                View v = A3b_layout.getChildAt(i);
                TextView label = (TextView) v.findViewById(R.id.lbl);
                CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
                EditText editText = (EditText)v.findViewById(R.id.editText);

                if (chkBox.isChecked())
                {
                    QuestionS12 questionS12 = new QuestionS12();
                    questionS12.setCode((int)label.getTag());
                    questionS12.setOpenEnded(editText.getText().toString());
                    questionS12.setStatement(label.getText().toString());
                    questionA3bs.add(questionS12);
                }

            }

            active.setQuestionA3a(new Gson().toJson(questionA3as));
            active.setQuestionA3b(new Gson().toJson(questionA3bs));
        }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validate()
    {
        if (questionA3as.size() == 0)
        {
            showDialog("Please select at least one option");
            return false;
        }
        if (questionA3bs.size() == 0)
        {
            showDialog("Please select at least one option");
            return false;
        }
        return true;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA3aA3b.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        add();
                        if (validate())
                        {

                            Intent intent = new Intent(ActivityA3aA3b.this, ActivityA4.class);
                            intent.putExtra("active", active);
                            startActivity(intent);
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
    }
