package markematics.active17213.Helper;

import android.content.SharedPreferences;

public class KeyValueDB {
	
	SharedPreferences sharedPref;
	
	public KeyValueDB(SharedPreferences sharedPreferences) {
		sharedPref = sharedPreferences;
	}
	
	public void save(String key, String value) {
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	public String getValue(String key, String defaultValue) {
		 
		return sharedPref.getString(key, defaultValue);
	}

//	public void saveList(String key,List<Questions> arrayList)
//	{
//		SharedPreferences.Editor editor = sharedPref.edit();
//		Gson gson = new Gson();
//
//		String json = gson.toJson(arrayList);
//
//		editor.putString(key, json);
//		editor.commit();
//	}
//
//	public List<Questions> getList(String key)
//	{
//		Gson gson = new Gson();
//		String json = sharedPref.getString(key, null);
//		Type type = new TypeToken<ArrayList<Questions>>() {}.getType();
//		ArrayList<Questions> arrayList = gson.fromJson(json, type);
//		return arrayList;
//	}
}
