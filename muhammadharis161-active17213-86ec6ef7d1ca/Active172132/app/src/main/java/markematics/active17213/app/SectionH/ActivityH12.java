package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH12;
import markematics.active17213.app.SectionN.ActivityN1N2N3N4N5;

public class ActivityH12 extends AppCompatActivity {

    TextView tViewH12;
    LinearLayout H12layout;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionH12> questionH12List;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h12);


        tViewH12 =(TextView)findViewById(R.id.tViewH12);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question  H12");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        H12layout = (LinearLayout)findViewById(R.id.H12_layout);

        btnNext = (Button) findViewById(R.id.btnNext);
        tViewH12.setTypeface(tf);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH12.setTypeface(tf);
        randerH11aLayout();
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });


    }

    private void next() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                add();
                if (validationSuccess()) {
                    proceed();

                }
            }
        });


    }

    private boolean validationSuccess() {

        boolean valid = true;

        for (int i=0;i<questionH12List.size();i++)
        {
            if (questionH12List.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionH12List.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other ");
                    return false;
                }
            }
        }
        if (questionH12List.size() == 0) {
            showDialog("Please select at least opne option H12");
            return false;
        }

        return true;
    }
    private void proceed() {
        Intent nextPage = new Intent(ActivityH12.this, ActivityH13.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }
    private void randerH11aLayout() {
        String[] arrayH11a = getResources().getStringArray(R.array.arrayH11);
        for (int i = 0; i < arrayH11a.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH11a[i]);
            label.setTag(i+1);
            if (arrayH11a[i].equalsIgnoreCase("دیگر")) {
                editText.setVisibility(View.VISIBLE);
            }
            H12layout.addView(v);
        }
    }


    private void add(){
        questionH12List = new ArrayList<>();
        for (int j = 0; j < H12layout.getChildCount(); j++) {
            View view = H12layout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            EditText editText = (EditText)view.findViewById(R.id.editText);
            QuestionH12 questionH12 = new QuestionH12();

            if (checkBox.isChecked()) {
                questionH12.setCode((int) labeled.getTag());
                questionH12.setStatement(labeled.getText().toString());
                questionH12.setOpenEnded(editText.getText().toString());
                questionH12List.add(questionH12);
            }
        }

        active.setQuestionH12a(new Gson().toJson(questionH12List));

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}