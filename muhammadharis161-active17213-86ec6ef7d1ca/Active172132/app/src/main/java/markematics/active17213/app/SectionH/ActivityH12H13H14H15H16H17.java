package markematics.active17213.app.SectionH;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH12;
import markematics.active17213.app.SectionN.ActivityN1N2N3N4N5;

public class ActivityH12H13H14H15H16H17 extends AppCompatActivity {

    TextView tViewH12, tViewH13, tViewH14, tViewH17a, tViewH17b, tViewH15, tViewH16;
    EditText editTextH16;
    LinearLayout H13layout, H12layout;
    Spinner spinnerH13, spinnerH14, spinnerH17a, spinnerH17b;
    Button btnNext;
    String H16;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionH12> questionH12List;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h12_h13_h14_h15_h16_h17);


        tViewH12 =(TextView)findViewById(R.id.tViewH12);
        tViewH13 = (TextView) findViewById(R.id.tViewH13);
        tViewH14 = (TextView) findViewById(R.id.tViewH14);
        tViewH15 = (TextView) findViewById(R.id.tViewH15);
        tViewH16 = (TextView) findViewById(R.id.tViewH16);
        tViewH17a = (TextView) findViewById(R.id.tViewH17a);
        tViewH17b = (TextView) findViewById(R.id.tViewH17b);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question  H13 H14 H15 H16 H17");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        editTextH16 = (EditText) findViewById(R.id.editTextH16);

        H13layout = (LinearLayout) findViewById(R.id.H15_layout);
        H12layout = (LinearLayout)findViewById(R.id.H12_layout);
        spinnerH13 = (Spinner) findViewById(R.id.spinnerH13);
        spinnerH14 = (Spinner) findViewById(R.id.spinnerH14);
        spinnerH17a = (Spinner) findViewById(R.id.spinnerH17a);
        spinnerH17b = (Spinner) findViewById(R.id.spinnerH17b);

        btnNext = (Button) findViewById(R.id.btnNext);
        tViewH12.setTypeface(tf);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH15.setTypeface(tf);
        tViewH16.setTypeface(tf);
        tViewH15.setTypeface(tf);
        tViewH16.setTypeface(tf);
        tViewH15.setTypeface(tf);
        tViewH16.setTypeface(tf);
        randerH11aLayout();
        editTextH16.setTypeface(tf);

        ArrayAdapter<String> spinnerH12aArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH13.setAdapter(spinnerH12aArrayAdapter);

        ArrayAdapter<String> spinnerH12bArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH14.setAdapter(spinnerH12bArrayAdapter);


        ArrayAdapter<String> spinnerH15ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH17a.setAdapter(spinnerH15ArrayAdapter);

        ArrayAdapter<String> spinnerH16ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH17b.setAdapter(spinnerH16ArrayAdapter);

        randerH13Layout();
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

        spinnerH14.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                if (position == 2)
//                {
//                    tViewH15.setVisibility(View.GONE);
//                    tViewH16.setVisibility(View.GONE);
//                    tViewH17a.setVisibility(View.GONE);
//                    tViewH17b.setVisibility(View.GONE);
//
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void next() {

        initialize();
        add();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n", Toast.LENGTH_SHORT).show();
        } else {

            proceed();

        }

    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (H16.isEmpty()) {
            editTextH16.setError("درج کریں");
            valid = false;
        }
        return valid;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH12H13H14H15H16H17.this, ActivityN1N2N3N4N5.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void initialize() {
        H16 = editTextH16.getText().toString().trim();

    }
    private void randerH11aLayout() {
        String[] arrayH11a = getResources().getStringArray(R.array.arrayH11);
        for (int i = 0; i < arrayH11a.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH11a[i]);
            H12layout.addView(v);
        }
    }


    private void randerH13Layout() {
        String[] arrayH13 = getResources().getStringArray(R.array.arrayH15);
        for (int i = 0; i < arrayH13.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH13[i]);
            H13layout.addView(v);
        }
    }

    private void add(){
        questionH12List = new ArrayList<>();
        for (int j = 0; j < H12layout.getChildCount(); j++) {
            View view = H12layout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            QuestionH12 questionH12 = new QuestionH12();

            if (checkBox.isChecked()) {
                questionH12.setCode((int) labeled.getTag());
                questionH12.setStatement(labeled.getText().toString());
                questionH12List.add(questionH12);
            }
        }


        active.setQuestionH14(editTextH16.getText().toString().trim());
        active.setQuestionH13("" + spinnerH13.getSelectedItem() );
        active.setQuestionH14("" + spinnerH14.getSelectedItem());
        active.setQuestionH17a("" + spinnerH17a.getSelectedItem());
        active.setQuestionH17b("" + spinnerH17b.getSelectedItem());
        active.setQuestionH12a(new Gson().toJson(questionH12List));

    }
}