package markematics.active17213.app.SectionA;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionA1;
import markematics.active17213.Model.QuestionS12;

/**
 * Created by mas on 9/26/2017.
 */

public class ActivityA2b extends AppCompatActivity {
    TextView tViewA2b;
    LinearLayout A2b_layout;
    Typeface tf;
    Button btnA2b;
    Active active;
    Toolbar toolbar;
    List<QuestionA1> questionA2bs;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2b);
        active = (Active) getIntent().getSerializableExtra("active");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question A2b");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewA2b = (TextView) findViewById(R.id.tViewA2b);
        A2b_layout = (LinearLayout) findViewById(R.id.a2b_layout);

        btnA2b=(Button)findViewById(R.id.btnA2b);
        btnA2b.setOnClickListener(btnA1_OnClickListener);

        new AsyncHandler(REQUEST_RENDER).execute();


    }
    private View.OnClickListener btnA1_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };
    private void randerA2aLayout() {
        A2b_layout.removeAllViews();
       // String[] spinnerarrayA2b = getResources().getStringArray(R.array.spinnerarrayA1);
        Type listType = new TypeToken<ArrayList<QuestionS12>>() {
        }.getType();
        List<QuestionS12> questionS12s = new Gson().fromJson(active.getQuestionS12(), listType);
        for (int i=0;i<questionS12s.size();i++) {
            View v = getLayoutInflater().inflate(R.layout.a1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lb2);
            label.setTypeface(tf);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);
            ArrayAdapter<String> spinnerA1ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.spinnerarrayA2b));
            spinnerLayoutA1.setAdapter(spinnerA1ArrayAdapter);
            label.setText(questionS12s.get(i).getStatement() + (questionS12s.get(i).getOpenEnded().isEmpty()?"":"("+questionS12s.get(i).getOpenEnded()+")"));
            A2b_layout.addView(v);
        }
    }
    private void add() {
        questionA2bs = new ArrayList<>();
        for (int i = 0; i < A2b_layout.getChildCount(); i++) {
            View v = A2b_layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lb2);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);
            if (spinnerLayoutA1.getSelectedItemPosition() != 0) {
                QuestionA1 questionA1 = new QuestionA1();
                questionA1.setLabel(label.getText().toString());
                questionA1.setStatement((String) spinnerLayoutA1.getSelectedItem());
                questionA1.setCode(spinnerLayoutA1.getSelectedItemPosition());
                questionA2bs.add(questionA1);
            }

        }

        active.setQuestionA2b(new Gson().toJson(questionA2bs));
    }

    private boolean validate() {
        for (int i = 0;i<A2b_layout.getChildCount();i++) {
            View v = A2b_layout.getChildAt(i);
            Spinner spinnerLayoutA1 = (Spinner) v.findViewById(R.id.spinnerlayoutA1);

            if (spinnerLayoutA1.getSelectedItemPosition() == 0)
            {
                showDialog("Please fill all values");
                return false;
            }

        }
        if (questionA2bs.size() == 0) {
            showDialog("Please Select At least one option");
            return false;
        }


        return true;
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityA2b.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate()) {
                                Intent intent = new Intent(ActivityA2b.this, ActivityA3a.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            active = (Active) getIntent().getSerializableExtra("active");

                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewA2b.setTypeface(tf);

                            randerA2aLayout();

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}

