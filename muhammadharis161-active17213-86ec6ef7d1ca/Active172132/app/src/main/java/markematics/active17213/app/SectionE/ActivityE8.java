package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionE6;
import markematics.active17213.Model.QuestionE7;
import markematics.active17213.app.RecorderApplication;
import markematics.active17213.app.SectionC.ActivityC1;
import markematics.active17213.app.SectionF.ActivityF1F2;

/**
 * Created by ZEB Rana on 16-Oct-17.
 */

public class ActivityE8 extends AppCompatActivity {

    TextView tViewE8;
    Button btnNext;
    Spinner spinnere8;
    Active active;
    Typeface tf;
    Toolbar toolbar;

    EditText editText1,editText2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e8);

        tViewE8 = (TextView) findViewById(R.id.tViewE8);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question E8");
        toolbar.setTitleTextColor(Color.WHITE);


        editText1 = (EditText)findViewById(R.id.editText1);
        editText2 = (EditText)findViewById(R.id.editText2);

        spinnere8 = (Spinner) findViewById(R.id.spinnere8);

        btnNext = (Button) findViewById(R.id.btnNext);


        active = (Active) getIntent().getSerializableExtra("active");

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewE8.setTypeface(tf);
        ArrayAdapter<String> spinnerE8ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arraye8));
        spinnere8.setAdapter(spinnerE8ArrayAdapter);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
        spinnere8.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1)
                {
                    editText1.setVisibility(View.VISIBLE);
                    editText2.setVisibility(View.GONE);
                }
                else if(i == 4)
                {
                    editText2.setVisibility(View.VISIBLE);
                    editText1.setVisibility(View.GONE);
                }
                else
                {
                    editText1.setVisibility(View.GONE);
                    editText2.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    private void StopRecording()
    {
        if (ViewController.mAudioRecorder != null) {
            ViewController.mAudioRecorder.pause(new AudioRecorder.OnPauseListener() {
                @Override
                public void onPaused(String activeRecordFileName) {
                    ViewController.saveCurrentRecordToMediaDB(activeRecordFileName, ActivityE8.this);
                    final RecorderApplication application = RecorderApplication.getApplication(ActivityE8.this);
                    ViewController.mAudioRecorder = application.getRecorder();
                    application.setRecorder(null);
                    ViewController.mAudioRecorder = null;
                    Intent intent = new Intent(ActivityE8.this, ActivityF1F2.class);
                    intent.putExtra("active", active);
                    startActivity(intent);
                }

                @Override
                public void onException(Exception e) {

                }
            });
        }
    }
    private void next() {
        {

            if (validate()) {
                add();
               StopRecording();
            }
        }
    }


    private void add()

    {
        active.setQuestionE8(""+spinnere8.getSelectedItemPosition());
        active.setQuestionE8Brand(editText1.getText().toString());
        active.setQuestionE8Other(editText2.getText().toString());

    }

    private boolean validate()
    {
        if (spinnere8.getSelectedItemPosition() == 0) {
            showDialog("Please Select At least one option");
            return false;
        }

        if (editText1.getVisibility() == View.VISIBLE)
        {
            if (editText1.getText().toString().isEmpty())
            {
                editText1.setError("Enter Brand Name");
                return false;
            }
        }
        if (editText2.getVisibility() == View.VISIBLE)
        {
            if (editText2.getText().toString().isEmpty())
            {
                editText1.setError("Enter ");
                return false;
            }
        }

        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
