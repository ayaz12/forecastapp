package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH9;


public class ActivityH10 extends AppCompatActivity {

    TextView tViewH10 ;
    Button btnNext;
    Spinner spinnerH10;
    Typeface tf;
    Toolbar toolbar;
    Active active;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h10);

        active = (Active) getIntent().getSerializableExtra("active");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H10");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewH10=(TextView)findViewById(R.id.tViewH10);


        btnNext = (Button)findViewById(R.id.btnNext);
        spinnerH10 =(Spinner)findViewById(R.id.spinnerH10);


        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH10.setTypeface(tf);

        ArrayAdapter<String> spinnerH10ArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayH1));
        spinnerH10.setAdapter(spinnerH10ArrayAdapter);



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
    }

    private void next() {

        if (spinnerH10.getSelectedItemPosition()!=0) {
            add();

            proceed();
        }else
        {
            showDialog("Please select at least one option in H10");
        }
        }


    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private void proceed() {if (spinnerH10.getSelectedItemPosition()==1)
    {
        Intent intent = new Intent(ActivityH10.this, ActivityH11.class);
        intent.putExtra("active", active);
        startActivity(intent);


    }
    else
    {
        Intent intent = new Intent(ActivityH10.this, ActivityH12.class);
        intent.putExtra("active", active);
        startActivity(intent);

    }
    }

    private void add(){

        active.setQuestionH10(""+ spinnerH10.getSelectedItemPosition());


    }
}