package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH12;
import markematics.active17213.Model.QuestionH15;
import markematics.active17213.app.SectionN.ActivityN1N2N3N4N5;

public class ActivityH15 extends AppCompatActivity {

    TextView tViewH15;

    LinearLayout H15layout;
    Button btnNext;
    String H16;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    List<QuestionH15> questionH15List;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h15);


        tViewH15 = (TextView) findViewById(R.id.tViewH15);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H15");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        H15layout = (LinearLayout) findViewById(R.id.H15_layout);

        btnNext = (Button) findViewById(R.id.btnNext);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH15.setTypeface(tf);


        randerH15Layout();
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
   }

    private void next() {

        add();
        if (validationSuccess()) {
            proceed();
        }
    }


    private boolean validationSuccess() {

        boolean valid = true;

        for (int i=0;i<questionH15List.size();i++)
        {
            if (questionH15List.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionH15List.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other ");
                    return false;
                }
            }
        }
        if (questionH15List.size() == 0) {
            showDialog("Please select at least opne option H12");
            return false;
        }

        return true;
    }


    private void proceed() {

        Intent nextPage = new Intent(ActivityH15.this, ActivityH16.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void randerH15Layout() {
        String[] arrayH13 = getResources().getStringArray(R.array.arrayH15);
        for (int i = 0; i < arrayH13.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH13[i]);
            label.setTag(i+1);
            if (arrayH13[i].equalsIgnoreCase("دیگر")) {
                editText.setVisibility(View.VISIBLE);
            }
            H15layout.addView(v);
        }
    }

    private void add(){
        questionH15List = new ArrayList<>();
        for (int j = 0; j < H15layout.getChildCount(); j++) {
            View view = H15layout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            EditText editText = (EditText)view.findViewById(R.id.editText);
            QuestionH15 questionH15 = new QuestionH15();

            if (checkBox.isChecked()) {
                questionH15.setCode((int) labeled.getTag());
                questionH15.setStatement(labeled.getText().toString());
                questionH15.setOpenEnded(editText.getText().toString());
                questionH15List.add(questionH15);
            }
        }


        active.setQuestionH15(new Gson().toJson(questionH15List));

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}