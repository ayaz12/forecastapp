package markematics.active17213.DataAccess.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.DataAccess.DataBaseUtil;
import markematics.active17213.Model.Active;


/**
 * Created by Muhammad Haris on 9/19/2017.
 */

public class ActiveDAO {
    Context context;
    private SQLiteDatabase db;
    DataBaseUtil dbUtil;

    public ActiveDAO(Context context)
    {
        this.context = context;

    }

    public Boolean add(Active active) {
        try
        {

            dbUtil = new DataBaseUtil(context);
            db = dbUtil.openConnection();

            ContentValues values = new ContentValues();


            values.put(Active.FIELD_HR_ID, active.gethR_ID());
            values.put(Active.FIELD_USER_NAME, active.getUserName());
            values.put(Active.FIELD_CITY, active.getCity());
            values.put(Active.FIELD_AREA, active.getArea());
            values.put(Active.FIELD_ACTIVE, active.getActiveJSON());
            values.put(Active.FIELD_START_TIME,active.getStartTime());
            values.put(Active.FIELD_DATETIME, active.getDateTime());


            db.insert(Active.TABLE_NAME, null, values);
            return true;

        } catch (SQLException e)
        {
            Log.e("DB Exception", e.toString());

            return false;
        } finally
        {
            dbUtil.closeConnection();
        }
    }

    public List<Active> getAll() {
        List<Active> activeList = new ArrayList<Active>();
        try {

            dbUtil = new DataBaseUtil(context);
            db = dbUtil.openConnection();

            Cursor cursor = db.rawQuery(Active.SELECT_QUERY, null);

            if (cursor.moveToFirst()) {
                do {
                    Active gsbc = Active.ConvertToEntity(cursor);

                    activeList.add(gsbc);

                } while (cursor.moveToNext());
            }

            cursor.close();
            return activeList;

        } catch (SQLException e) {
            Log.e("DB Exception", e.toString());

            return activeList;
        } finally {

            dbUtil.closeConnection();
        }
    }

    public Boolean delete() {
        try
        {

            dbUtil = new DataBaseUtil(context);
            this.db = dbUtil.openConnection();
            db.delete(Active.TABLE_NAME, null, null);

            return true;

        } catch (Exception e)
        {
            Log.e("DB Exception", e.toString());
            return false;

        } finally
        {
            dbUtil.closeConnection();
        }

    }

    public Boolean delete(int rId) {
        try {

            dbUtil = new DataBaseUtil(context);
            this.db = dbUtil.openConnection();
            db.delete(Active.TABLE_NAME, Active.FIELD_ID +" =?", new String[] { String.valueOf(rId) });
            //  db.delete(Report.TABLE_NAME, null, null);

            return true;

        } catch (Exception e) {
            Log.e("DB Exception", e.toString());
            return false;

        } finally {
            dbUtil.closeConnection();
        }

    }


}
