package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH12;
import markematics.active17213.app.SectionN.ActivityN1N2N3N4N5;

public class ActivityH16 extends AppCompatActivity {

    TextView  tViewH16;
    EditText editTextH16,editTextH16a,editTextH16b,editTextH16c,editTextH16d;
    Button btnNext;
    String H16;
    Typeface tf;
    Toolbar toolbar;
    Active active;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h16);


        tViewH16 = (TextView) findViewById(R.id.tViewH16);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H16 ");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

        editTextH16 = (EditText) findViewById(R.id.editTextH16);
        editTextH16a = (EditText) findViewById(R.id.editTextH16a);
        editTextH16b = (EditText) findViewById(R.id.editTextH16b);
        editTextH16c = (EditText) findViewById(R.id.editTextH16c);
        editTextH16d = (EditText) findViewById(R.id.editTextH16d);


        btnNext = (Button) findViewById(R.id.btnNext);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH16.setTypeface(tf);
        editTextH16.setTypeface(tf);
        editTextH16a.setTypeface(tf);
        editTextH16b.setTypeface(tf);
        editTextH16c.setTypeface(tf);
        editTextH16d.setTypeface(tf);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {

        initialize();
        add();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n", Toast.LENGTH_SHORT).show();
        } else {

            proceed();

        }

    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (H16.isEmpty()) {
            editTextH16.setError("درج کریں");
            valid = false;
        }
        return valid;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH16.this, ActivityH17a.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void initialize() {
        H16 = editTextH16.getText().toString().trim();

    }

    private void add(){
        List<String> webList = new ArrayList<>();
        webList.add(editTextH16.getText().toString());
        if (!editTextH16a.getText().toString().isEmpty())
        {
            webList.add(editTextH16a.getText().toString());
        }
        if (!editTextH16b.getText().toString().isEmpty())
        {
            webList.add(editTextH16b.getText().toString());
        }
        if (!editTextH16c.getText().toString().isEmpty())
        {
            webList.add(editTextH16c.getText().toString());
        }
        if (!editTextH16d.getText().toString().isEmpty())
        {
            webList.add(editTextH16d.getText().toString());
        }
        active.setQuestionH16(new Gson().toJson(webList));

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}