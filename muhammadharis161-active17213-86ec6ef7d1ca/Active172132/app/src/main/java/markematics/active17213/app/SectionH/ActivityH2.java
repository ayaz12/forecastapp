package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH3a;
import markematics.active17213.Model.QuestionH3b;

public class ActivityH2 extends AppCompatActivity {

    TextView tViewH2;
    EditText editTextH2,editTextH2a,editTextH2b,editTextH2c,editTextH2d;
    Button btnNext;
    String H2;
    Typeface tf;
    Toolbar toolbar;
    Active active;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h2);

        tViewH2 = (TextView) findViewById(R.id.tViewH2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H2 ");
        toolbar.setTitleTextColor(Color.WHITE);

        editTextH2 = (EditText) findViewById(R.id.editTextH2);
        editTextH2a = (EditText) findViewById(R.id.editTextH2a);
        editTextH2b = (EditText) findViewById(R.id.editTextH2b);
        editTextH2c = (EditText) findViewById(R.id.editTextH2c);
        editTextH2d = (EditText) findViewById(R.id.editTextH2d);


        active = (Active) getIntent().getSerializableExtra("active");

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH2.setTypeface(tf);
        editTextH2.setTypeface(tf);
        editTextH2a.setTypeface(tf);
        editTextH2b.setTypeface(tf);
        editTextH2c.setTypeface(tf);
        editTextH2d.setTypeface(tf);

        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {

        initialize();
        add();
        if (validationSuccess()) {

            proceed();
        } else {



        }


    }

    private boolean validationSuccess() {

        boolean valid = true;
        if (editTextH2.getVisibility() == View.VISIBLE) {
            if (H2.isEmpty()) {
                editTextH2.setError("درج کریں");
                return false;
            }
        }
        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH2.this, ActivityH3a.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private void initialize() {


        H2 = editTextH2.getText().toString().trim();

    }

    private void add() {

        List<String> channelList = new ArrayList<>();
        channelList.add(editTextH2.getText().toString());
        if (!editTextH2a.getText().toString().isEmpty())
        {
            channelList.add(editTextH2a.getText().toString());
        }
        if (!editTextH2b.getText().toString().isEmpty())
        {
            channelList.add(editTextH2b.getText().toString());
        }
        if (!editTextH2c.getText().toString().isEmpty())
        {
            channelList.add(editTextH2c.getText().toString());
        }
        if (!editTextH2d.getText().toString().isEmpty())
        {
            channelList.add(editTextH2d.getText().toString());
        }
          active.setQuestionH2(new Gson().toJson(channelList));


    }
}