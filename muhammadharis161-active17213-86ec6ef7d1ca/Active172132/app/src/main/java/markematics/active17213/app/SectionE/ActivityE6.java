package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionE6;
import markematics.active17213.Model.QuestionE7;
import markematics.active17213.app.SectionF.ActivityF1F2;

/**
 * Created by ZEB Rana on 16-Oct-17.
 */

public class ActivityE6 extends AppCompatActivity {

    TextView tViewE6;
    LinearLayout e6layout;
    Button btnNext;
    Active active;
    Typeface tf;
    Toolbar toolbar;
    List<QuestionE6> questionE6s;
    String e6Val = "";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e6);

        tViewE6 = (TextView) findViewById(R.id.tViewE6);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question E6");
        toolbar.setTitleTextColor(Color.WHITE);

        e6layout = (LinearLayout) findViewById(R.id.E6_layout);

        btnNext = (Button) findViewById(R.id.btnNext);



        active = (Active) getIntent().getSerializableExtra("active");

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewE6.setTypeface(tf);
        randerE6Layout();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    add();
                    if (validate()) {
                        Intent intent = new Intent(ActivityE6.this, ActivityE7.class);
                        intent.putExtra("active", active);
                        startActivity(intent);
                    }
                }
            });

        }
    }

    private void randerE6Layout() {
        List<Brand> brands = new ArrayList<>();
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);


        for (int i =0;i<questionB5s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB5s.get(i).getCode());
            brand.setName(questionB5s.get(i).getBrand());
            brands.add(brand);
        }

        for (int i = 0; i < brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.e6_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            TextView statementlbl = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editTextOther = (EditText)v.findViewById(R.id.editTextOther);
            EditText editTextStore = (EditText)v.findViewById(R.id.editTextStore);
            label.setText(brands.get(i).getName());
            label.setTag(brands.get(i).getCode());
            label.setTypeface(tf);
            statementlbl.setTag(v);

            statementlbl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(view);
                }
            });
            e6layout.addView(v);
        }
        String[] arrayE6 = getResources().getStringArray(R.array.arraye6);
//        for (int i = 0; i < arrayE6.length; i++) {
//            View v = getLayoutInflater().inflate(R.layout.s2_layout, null);
//            TextView label = (TextView) v.findViewById(R.id.lbl);
//            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
//            label.setText(arrayE6[i]);
//            label.setTypeface(tf);
//            e6layout.addView(v);
//        }
    }


    private  void showDialog(final View lblView)
    {
        e6Val = "";
        View rootView = getLayoutInflater().inflate(R.layout.custom_layout_n1, null);
        final LinearLayout statementGrid = (LinearLayout)rootView.findViewById(R.id.statementGrid);
        TextView lblN1a = (TextView)rootView.findViewById(R.id.lbl);
        final Button btnOk = (Button)rootView.findViewById(R.id.btnOK);
        lblN1a.setTypeface(tf);
        final String[] array = getResources().getStringArray(R.array.arraye6);
        for (int i =0;i<array   .length;i++) {
            View v = getLayoutInflater().inflate(R.layout.multi_choice, null);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkbox1);
            checkBox.setId(i);
            checkBox.setChecked(false);
            checkBox.setOnCheckedChangeListener(CheckBox_OnCheckedChangeListener);
            checkBox.setText(array[i]);
            checkBox.setTypeface(tf);
            checkBox.setTag(i+1);
            statementGrid.addView(v);

            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }
        btnOk.setTag(rootView);

        final android.app.AlertDialog deleteDialog = new android.app.AlertDialog.Builder(ActivityE6.this).create();
        deleteDialog.setView(rootView);
        deleteDialog.show();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //   e6Val += (int) lblView.getTag() + ",";
                if (!e6Val.isEmpty()) {
                    View lblTextView = (View) lblView.getTag();
                    TextView textView = (TextView) lblTextView.findViewById(R.id.lblCheckList);
                    textView.setText(e6Val.substring(0, e6Val.length() - 1));
                    deleteDialog.dismiss();
                }

            }
        });


    }
    private CompoundButton.OnCheckedChangeListener CheckBox_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            int val = (int) buttonView.getTag();
            if (isChecked) {
                e6Val += val + ",";
                Toast.makeText(ActivityE6.this, "" + e6Val, Toast.LENGTH_SHORT).show();
            }
            else
            {
                e6Val = e6Val.replace(val + ",","");
            }
        }
    };
    private void add()

    {
        questionE6s = new ArrayList<>();
        for (int i=0;i<e6layout.getChildCount();i++)
        {
            View v = e6layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            TextView statementlbl = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editTextOther = (EditText)v.findViewById(R.id.editTextOther);
            EditText editTextStore = (EditText)v.findViewById(R.id.editTextStore);
            QuestionE6 questionE6 = new QuestionE6();
            questionE6.setCode((int)label.getTag());
            questionE6.setStatement(statementlbl.getText().toString());
            questionE6.setLabel(label.getText().toString());
            questionE6.setStoreName(editTextStore.getText().toString());
            questionE6.setOpenEnded(editTextOther.getText().toString());
            questionE6s.add(questionE6);
        }


        active.setQuestionE6(new Gson().toJson(questionE6s));
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    private boolean validate()
    {
        for (int i = 0;i<e6layout.getChildCount();i++) {
            View v = e6layout.getChildAt(i);
            TextView lbl = (TextView) v.findViewById(R.id.lblCheckList);

            if (lbl.getText().toString().equalsIgnoreCase("Select")) {
                lbl.setError("Please fill");
                return false;
            }

        }
        for (int i = 0;i<e6layout.getChildCount();i++) {
            View v = e6layout.getChildAt(i);
            TextView lbl = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editTextOther = (EditText)v.findViewById(R.id.editTextOther);
            if (lbl.getText().toString().contains("11")) {
                if (editTextOther.getText().toString().isEmpty()) {
                    editTextOther.setError("Please fill Other Reason");
                    return false;
                }
            }

        }
        for (int i = 0;i<e6layout.getChildCount();i++) {
            View v = e6layout.getChildAt(i);
            TextView lbl = (TextView) v.findViewById(R.id.lblCheckList);
            EditText editTextStore = (EditText)v.findViewById(R.id.editTextStore);
            if (lbl.getText().toString().contains("5")) {
                if (editTextStore.getText().toString().isEmpty()) {
                    lbl.setError("Please fill Store");
                    return false;
                }
            }

        }
        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
