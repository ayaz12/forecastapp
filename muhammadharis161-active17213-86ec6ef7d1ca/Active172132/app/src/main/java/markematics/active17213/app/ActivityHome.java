package markematics.active17213.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.lassana.recorder.AudioRecorder;
import com.github.lassana.recorder.AudioRecorderBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import markematics.active17213.Adapter.AreaAdapter;
import markematics.active17213.Helper.KeyValueDB;
import markematics.active17213.Helper.Receiver;
import markematics.active17213.Helper.ViewController;
import markematics.active17213.Model.Active;
import markematics.active17213.Model.Area;


public class ActivityHome extends AppCompatActivity {

    Button buttonQuestionnare, buttonSynchronize, buttonLogout;
    Toolbar toolbar;
    Spinner SpinnerArea;
    KeyValueDB keyValueDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        buttonQuestionnare = (Button) findViewById(R.id.btnQuestionnare);
        buttonSynchronize = (Button) findViewById(R.id.btnSynchronize);
        buttonLogout = (Button) findViewById(R.id.btnLogout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Home");
        toolbar.setTitleTextColor(Color.WHITE);
        SpinnerArea =(Spinner)findViewById(R.id.spinnerArea);
        keyValueDB = new KeyValueDB(getSharedPreferences(getResources().getString(R.string.keyValueDb), Context.MODE_PRIVATE));

        String areaList = keyValueDB.getValue("areaList","");
        Type listType = new TypeToken<ArrayList<Area>>() {
        }.getType();
        List<Area> areaList1 = new Gson().fromJson(areaList, listType);
        Area area = new Area();
        area.setArea("Select");
        areaList1.add(0,area);
        AreaAdapter areaAdapter = new AreaAdapter(ActivityHome.this,R.layout.spinner_item,R.id.txt,areaList1);
        SpinnerArea.setAdapter(areaAdapter);
        if (displayGpsStatus()) {

        }
        else
        {
            alertbox("Gps Status!!", "Your GPS is: OFF");
            return;
        }
      //  startBroadcast();
        buttonQuestionnare.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                if (displayGpsStatus()) {


                    if (SpinnerArea.getSelectedItemPosition() != 0) {
                        Active active = new Active();
                        active.sethR_ID(keyValueDB.getValue("hrId", ""));
                        active.setUserName(keyValueDB.getValue("name", ""));
                        active.setCity(keyValueDB.getValue("city", ""));
                        Area area = (Area) SpinnerArea.getSelectedItem();
                        active.setArea(area.getArea());
                        active.setStartTime(getDateTime());
                        StartRecording(active);

                        Intent intent = new Intent(ActivityHome.this, ActivityIntro.class);
                        intent.putExtra("active", active);
                        startActivity(intent);

                    } else {
                        Toast.makeText(ActivityHome.this, "Please select Area", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    alertbox("Gps Status!!", "Your GPS is: OFF");
                    return;
                }
            }

        });


        buttonSynchronize.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ActivityHome.this, ActivitySync.class);
                startActivity(intent);
            }
        });


        buttonLogout.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
            showDialog("کیا آپ واقعی LogOut کرنا چاہتے ہیں");

            }

        });

    }

    private void StartRecording(Active active) {

        if (ViewController.mAudioRecorder == null)
        {
            String filePath = active.gethR_ID()+"_"+active.getCity()+"_SecS"+"_"+System.currentTimeMillis();
            active.setAudioPathSecS(filePath);
            recordAudio(filePath);

        }
    }

    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext().getContentResolver();
        boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
                contentResolver, LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Device's GPS is Disable Please Enable GPS and restart the application.")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Gps On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent myIntent = new Intent(
                                        Settings.ACTION_SECURITY_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void startBroadcast()
    {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ActivityHome.this, Receiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(ActivityHome.this, 0, intent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 180000,
                pendingIntent);

    }
    private void stopAlarm()
    {
        AlarmManager alarmManager=(AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ActivityHome.this, Receiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(ActivityHome.this, 0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                keyValueDB.save("hrId","");
                keyValueDB.save("name","");
                keyValueDB.save("city","");
                keyValueDB.save("areaList","");
                stopAlarm();
                Intent intent = new Intent(ActivityHome.this, ActivityLogin.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.show();
    }

    public void onBackPressed() {

    }
    private String getDateTime()
    {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    private void recordAudio(String fileName) {


        final RecorderApplication application = RecorderApplication.getApplication(ActivityHome.this);
        ViewController.mAudioRecorder = application.getRecorder();
        if (ViewController.mAudioRecorder == null
                || ViewController.mAudioRecorder.getStatus() == AudioRecorder.Status.STATUS_UNKNOWN) {
            ViewController.mAudioRecorder = AudioRecorderBuilder.with(application)
                    .fileName(createDirectoryAndSaveFile(fileName))
                    .config(AudioRecorder.MediaRecorderConfig.DEFAULT)
                    .loggable()
                    .build();
            application.setRecorder(ViewController.mAudioRecorder);
        }
        start();
    }
    private void message(String message) {
        Toast.makeText(ActivityHome.this,message,Toast.LENGTH_SHORT).show();
    }
    private void start()
    {
        ViewController.mAudioRecorder .start(new AudioRecorder.OnStartListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onException(Exception e) {

                //   invalidateViews();
                message(getString(R.string.error_audio_recorder, e));
            }
        });
    }
    private String createDirectoryAndSaveFile( String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/.ActiveQuant/Audio/");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/.ActiveQuant/Audio/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/.ActiveQuant/Audio/"), fileName+ ".mp3" );
        if (file.exists()) {
            file.delete();
        }

        return file.getPath();
    }
}
