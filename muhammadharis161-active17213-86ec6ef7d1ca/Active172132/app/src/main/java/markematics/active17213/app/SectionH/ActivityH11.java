package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH9;


public class ActivityH11 extends AppCompatActivity {

    TextView tViewH11 ;
    EditText editTextH11,editTextH11a,editTextH11b,editTextH11c,editTextH11d;
    Button btnNext;
    String H11;
    Typeface tf;
    Toolbar toolbar;
    Active active;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h11);

        tViewH11 = (TextView) findViewById(R.id.tViewH11);



        active = (Active) getIntent().getSerializableExtra("active");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H11");
        toolbar.setTitleTextColor(Color.WHITE);


        editTextH11 = (EditText)findViewById(R.id.editTextH11);
        editTextH11a = (EditText)findViewById(R.id.editTextH11a);
        editTextH11b = (EditText)findViewById(R.id.editTextH11b);
        editTextH11c = (EditText)findViewById(R.id.editTextH11c);
        editTextH11d = (EditText)findViewById(R.id.editTextH11d);

        btnNext = (Button)findViewById(R.id.btnNext);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH11.setTypeface(tf);



        editTextH11.setTypeface(tf);
        editTextH11a.setTypeface(tf);
        editTextH11b.setTypeface(tf);
        editTextH11c.setTypeface(tf);
        editTextH11d.setTypeface(tf);



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {

        initialize();
        add();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n",Toast.LENGTH_SHORT).show();
        } else {

            proceed();

        }

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validationSuccess() {


        boolean valid = true;
        if (editTextH11.getVisibility() == View.VISIBLE) {
            if (H11.isEmpty()) {
                editTextH11.setError("درج کریں");
                valid = false;
            }
        }
        return valid;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH11.this, ActivityH12.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void initialize() {
        H11=editTextH11.getText().toString().trim();
    }

    private void add(){
        List<String> magzineList = new ArrayList<>();
        magzineList.add(editTextH11.getText().toString());
        if (!editTextH11a.getText().toString().isEmpty())
        {
            magzineList.add(editTextH11a.getText().toString());
        }
        if (!editTextH11b.getText().toString().isEmpty())
        {
            magzineList.add(editTextH11b.getText().toString());
        }
        if (!editTextH11c.getText().toString().isEmpty())
        {
            magzineList.add(editTextH11c.getText().toString());
        }
        if (!editTextH11d.getText().toString().isEmpty())
        {
            magzineList.add(editTextH11d.getText().toString());
        }
        active.setQuestionH11(new Gson().toJson(magzineList));


    }
}