package markematics.active17213.app.SectionS;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.markematics.Active17213.R;

import markematics.active17213.Helper.Utility;
import markematics.active17213.Model.Active;
import markematics.active17213.app.ActivityIntro;

public class ActivityS3 extends Activity  {

    TextView tViewS3;
    Button btnNext;
    Spinner spinnerS3;
    View view;
    Typeface tf;
    Active active;
    Toolbar toolbar;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s3);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S3");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewS3 = (TextView)findViewById(R.id.tViewS3);
        btnNext = (Button)findViewById(R.id.btnS3);
        spinnerS3 = (Spinner) findViewById(R.id.spinner_s3);

        btnNext.setOnClickListener(btnNext_OnClickListener);

        new AsyncHandler(REQUEST_RENDER).execute();
        spinnerS3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position == 1 )
                {
                    active.setQuestionS3(""+position);
                    btnNext.setText("Finish");
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                }
                else
                {
                    active.setQuestionS3(""+position);
                    btnNext.setText("Next");
                    btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private boolean validate()
    {
        if (spinnerS3.getSelectedItemPosition() == 0) {
            showDialog("Please Select At least one option");
            return false;
        }


        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request) {
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS3.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            if (validate())
                            {
                                if (btnNext.getText().toString().equalsIgnoreCase("Finish"))
                                {
                                    Utility.showExitDialog(ActivityS3.this, "کیا آپ انٹرویو ختم کرنا چاہتی ہیں", active );

                                }
                                else
                                {
                                    Intent intent = new Intent(ActivityS3.this, ActivityS4S5.class);
                                    intent.putExtra("active",active);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    return;
                                }
                            }

                        } else if (request.equalsIgnoreCase(REQUEST_RENDER)) {
                            active = (Active)getIntent().getSerializableExtra("active");
                            ArrayAdapter<String> spinnerS1ArrayAdapter = new ArrayAdapter<String>(ActivityS3.this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayS3));
                            spinnerS3.setAdapter(spinnerS1ArrayAdapter);
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewS3.setTypeface(tf);
                            btnNext.setTextColor(Color.parseColor("#FFFFFF"));

                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
