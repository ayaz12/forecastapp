package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;


/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB3 extends AppCompatActivity {

    TextView tViewB3,tViewInsB3,tViewlblInsB3;
    Spinner spinB1;
    LinearLayout b3_Layout;
    Button btnB1;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";
    List<QuestionB1> questionB3s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b3);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question  B3");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewB3 = (TextView) findViewById(R.id.tViewB3);
        //   spinB1 = (Spinner)findViewById(R.id.spinB1);
        b3_Layout = (LinearLayout) findViewById(R.id.b3_layout);
        tViewInsB3 =(TextView)findViewById(R.id.lblInsB3);
        tViewlblInsB3 =(TextView)findViewById(R.id.lblInsB1);
        btnB1 = (Button) findViewById(R.id.btnB1);
        btnB1.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void renderB3Layout() {
        b3_Layout.removeAllViews();
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<Brand> brands = new ArrayList<>();

        String[] arrayB1 = getResources().getStringArray(R.array.arrayB2);
        for (int index = 0;index<arrayB1.length;index++)
        {
            Brand brand = new Brand();
            brand.setCode(index+1);
            brand.setName(arrayB1[index]);
            brands.add(brand);
        }
        for (int j = 0;j<brands.size();j++)
        {
            for (int i = 0;i< questionB1s.size();i++)
            {
                if(brands.get(j).getName().equalsIgnoreCase(questionB1s.get(i).getBrand()))
                {
                    brands.remove(brands.get(j));
                }
            }

        }
        for (int j = 0;j<brands.size();j++)
        {
            for (int i = 0;i< questionB2s.size();i++)
            {
                if(brands.get(j).getName().equalsIgnoreCase(questionB2s.get(i).getBrand()))
                {
                    brands.remove(brands.get(j));
                }
            }

        }


        for (int i = 0; i <brands.size(); i++) {

                    View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
                    TextView label = (TextView) v.findViewById(R.id.lbl);
                    EditText editText = (EditText) v.findViewById(R.id.editText);
                    CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);


                    label.setTypeface(tf);
                    label.setText(brands.get(i).getName());
                    label.setTypeface(tf);
                    label.setTag(brands.get(i).getCode());
                    chkBox1.setTag(v);


                    if (brands.get(i).getName().equalsIgnoreCase("Others")) {
                        editText.setVisibility(View.VISIBLE);
                    }
                    b3_Layout.addView(v);
                    if (i % 2 == 0) {
                        v.setBackgroundColor(Color.parseColor("#E0E0E0"));
                    }
                }

    }


    private void add()
    {

        questionB3s = new ArrayList<>();
        for (int i = 0; i < b3_Layout.getChildCount(); i++) {
            View v = b3_Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);


            if (chkBox1.isChecked())
            {
                QuestionB1 questionB1 = new QuestionB1();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB1.setOpenEnded(editText.getText().toString());
                questionB3s.add(questionB1);
            }
        }


        active.setQuestionB3(new Gson().toJson(questionB3s));
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validate()
    {
        for (int i=0;i<questionB3s.size();i++)
        {
            if (questionB3s.get(i).getBrand().equalsIgnoreCase("Others"))
            {
                if (questionB3s.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other Brand");
                    return false;
                }
            }
        }
        if (questionB3s.size() == 0)
        {
            showDialog("Please select at least one option B3");
            return false;
        }
        return true;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB3.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate()) {
                                Intent intent = new Intent(ActivityB3.this, ActivityB4.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        }
                        else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

                            tViewB3.setTypeface(tf);
                            tViewInsB3.setTypeface(tf);
                            tViewlblInsB3.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");

                            renderB3Layout();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
