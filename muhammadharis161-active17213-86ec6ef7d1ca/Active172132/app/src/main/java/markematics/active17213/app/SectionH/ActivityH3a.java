package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH3a;
import markematics.active17213.Model.QuestionH3b;

public class ActivityH3a extends AppCompatActivity {

    TextView tViewH3a;

    Button btnNext;

    Typeface tf;
    LinearLayout  H3alayout;
    Toolbar toolbar;
    Active active;
    List<QuestionH3a> questionH3aH;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h3a);

        tViewH3a = (TextView) findViewById(R.id.tViewH3a);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H3A");
        toolbar.setTitleTextColor(Color.WHITE);



        active = (Active) getIntent().getSerializableExtra("active");

        H3alayout = (LinearLayout) findViewById(R.id.H3a_layout);

        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

        tViewH3a.setTypeface(tf);

        randerH3aLayout();
        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });


    }

    private void next() {

        add();
        if (validationSuccess()) {

            proceed();
        } else {



        }


    }

    private boolean validationSuccess() {

        boolean valid = true;
        for (int i=0;i<questionH3aH.size();i++)
        {
            if (questionH3aH.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionH3aH.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other ");
                    return false;
                }
            }
        }
            if (questionH3aH.size() == 0)
            {
                showDialog("please select at least one option H3a");
                return false;
            }
        return true;
    }

    private void proceed() {
        Intent nextPage = new Intent(ActivityH3a.this, ActivityH3b.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private CompoundButton.OnCheckedChangeListener checkBox1_OnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            View v = (View) compoundButton.getTag();
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);

            if (chkBox1.isChecked()) {

                for (int i = 0; i < H3alayout.getChildCount(); i++) {
                    View childView = H3alayout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(false);
                    }
                }

            } else {
                for (int i = 0; i < H3alayout.getChildCount(); i++) {
                    View childView = H3alayout.getChildAt(i);
                    CheckBox checkBox = (CheckBox) childView.findViewById(R.id.checkBox1);
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                        checkBox.setEnabled(true);
                    }
                }

            }

        }
    };


    private void randerH3aLayout() {
        String[] arrayH3a = getResources().getStringArray(R.array.arrayH3a);
        for (int i = 0; i < arrayH3a.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            label.setTypeface(tf);
            label.setText(arrayH3a[i]);
            label.setTag(i+1);
            if (arrayH3a[i].equalsIgnoreCase("دیگر")) {
                editText.setVisibility(View.VISIBLE);
            }

            H3alayout.addView(v);

        }
    }





    private void add() {

        questionH3aH = new ArrayList<>();
        for (int i = 0; i < H3alayout.getChildCount(); i++) {
            View v = H3alayout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText) v.findViewById(R.id.editText);
            QuestionH3a questionH3a = new QuestionH3a();

            if (chkBox.isChecked()) {
                questionH3a.setCode((int) label.getTag());
                questionH3a.setStatement(label.getText().toString());
                questionH3a.setOpenEnded(editText.getText().toString());
                questionH3aH.add(questionH3a);

            }
        }


        active.setQuestionH3a(new Gson().toJson(questionH3aH));



}}