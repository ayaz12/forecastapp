package markematics.active17213.Model;

/**
 * Created by Muhammad Haris on 10/4/2017.
 */

public class QuestionS12 {
    private int code;
    private String statement;
    private String openEnded;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getOpenEnded() {
        return openEnded;
    }

    public void setOpenEnded(String openEnded) {
        this.openEnded = openEnded;
    }
}
