package markematics.active17213.app;


import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.markematics.Active17213.R;

import markematics.active17213.Model.Active;
import markematics.active17213.app.SectionA.ActivityStartSectionA;
import markematics.active17213.app.SectionB.ActivityStart;
import markematics.active17213.app.SectionS.ActivityInfo;
import markematics.active17213.app.SectionS.ActivityS1;
import markematics.active17213.app.SectionS.ActivityStartS;

public class ActivityIntro extends Activity {

    TextView textViewIntro;
    Typeface tf;
    Button buttonIntro;
    Toolbar toolbar;
    Double latitude = 0.0;
    Double longitude = 0.0;
    private LocationManager locationMangaer = null;
    private LocationListener locationListener = null;
    private static final String TAG = "Debug";
    private Boolean flag = false;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Introduction");
        toolbar.setTitleTextColor(Color.WHITE);
        textViewIntro = (TextView) findViewById(R.id.tViewIntro);
        buttonIntro = (Button) findViewById(R.id.btnintro);
        locationMangaer = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getLocation();
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        textViewIntro.setTypeface(tf);

        buttonIntro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Active active = (Active) getIntent().getSerializableExtra("active");
           //    if (longitude !=0.0) {
                    active.setLat(latitude);
                    active.setLng(longitude);
                    Intent intent = new Intent(ActivityIntro.this, ActivityStartS.class);
                    intent.putExtra("active", active);
                    startActivity(intent);
//                }
//                else
//                {
//                    Toast.makeText(ActivityIntro.this,"Please wait getting your location",Toast.LENGTH_LONG).show();
//                }
            }
        });

    }
    private void getLocation()
    {
        flag = displayGpsStatus();
        if (flag) {

            Log.v(TAG, "onClick");

            //	editLocation.setText("Please!! move your device to see the changes in coordinates."+"\nWait..");

            // pb.setVisibility(View.VISIBLE);
            locationListener = new MyLocationListener();

            if (ActivityCompat.checkSelfPermission(ActivityIntro.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ActivityIntro.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationMangaer.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 5,
                    locationListener);

        } else {
            alertbox("Gps Status!!", "Your GPS is: OFF");
        }
    }
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext().getContentResolver();
        boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
                contentResolver, LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Gps On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent myIntent = new Intent(
                                        Settings.ACTION_SECURITY_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*----------Listener class to get coordinates ------------- */
    private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {

            //editLocation.setText("");
            //    pb.setVisibility(View.INVISIBLE);
//            Toast.makeText(getBaseContext(), "Location changed : Lat: " + loc.getLatitude()
//                    + " Lng: " + loc.getLongitude(), Toast.LENGTH_SHORT).show();
            String longitude = "Longitude: " + loc.getLongitude();

            String latitude = "Latitude: " + loc.getLatitude();

            if (!loc.hasAccuracy() || loc.getAccuracy() > 10)
            {
                Toast.makeText(getBaseContext(), "Accuracy : " + loc.getAccuracy() , Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getBaseContext(), "Accuracy : " + loc.getAccuracy() , Toast.LENGTH_SHORT).show();
                locationMangaer.removeUpdates(locationListener);
                ActivityIntro.this.longitude = loc.getLongitude();
                ActivityIntro.this.latitude = loc.getLatitude();
            }



            if (ActivityCompat.checkSelfPermission(ActivityIntro.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ActivityIntro.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(ActivityIntro.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ActivityIntro.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }

                // editLocation.setText(s);
            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }
}
