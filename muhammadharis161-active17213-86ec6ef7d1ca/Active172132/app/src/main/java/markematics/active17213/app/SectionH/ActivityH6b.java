package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH6a;
import markematics.active17213.Model.QuestionH6b;


public class ActivityH6b extends AppCompatActivity {

    TextView tViewH6b;
    Button btnNext;
    LinearLayout H6blayout;
    Typeface tf;
    Toolbar toolbar;
    Active active;

    List<QuestionH6b> questionH6bList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h6b);

        tViewH6b = (TextView) findViewById(R.id.tViewH6b);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H6b");
        toolbar.setTitleTextColor(Color.WHITE);


        active = (Active) getIntent().getSerializableExtra("active");

 H6blayout = (LinearLayout) findViewById(R.id.H6b_layout);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH6b.setTypeface(tf);


        btnNext = (Button) findViewById(R.id.btnNext);
        randerH6bLayout();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });


    }

    private void next() {
        add();
        if (validationSuccess()) {

            proceed();
        } else {


        }


    }

    private boolean validationSuccess() {

        boolean valid = true;

        for (int i=0;i<questionH6bList.size();i++)
        {
            if (questionH6bList.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionH6bList.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("Please enter Other ");
                    return false;
                }
            }
        }
        if (questionH6bList.size() == 0) {
            showDialog("Please select at least opne option H6b");
            return false;
        }

        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private void proceed() {
        Intent nextPage = new Intent(ActivityH6b.this, ActivityH7.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }


    private void randerH6bLayout() {
        String[] arrayH6b = getResources().getStringArray(R.array.arrayH6B);
        for (int i = 0; i < arrayH6b.length; i++) {
            View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);
            label.setTypeface(tf);
            label.setText(arrayH6b[i]);
            label.setTag(i+1);
            if (arrayH6b[i].equalsIgnoreCase("دیگر")) {
                editText.setVisibility(View.VISIBLE);
            }

            H6blayout.addView(v);
        }
    }

    private void add() {

        questionH6bList = new ArrayList<>();
        for (int j = 0; j < H6blayout.getChildCount(); j++) {
            View view = H6blayout.getChildAt(j);
            TextView labeled = (TextView) view.findViewById(R.id.lbl);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            EditText editText = (EditText)view.findViewById(R.id.editText);
            QuestionH6b questionH6b = new QuestionH6b();

            if (checkBox.isChecked()) {
                questionH6b.setCode((int) labeled.getTag());
                questionH6b.setStatement(labeled.getText().toString());
                questionH6b.setOpenEnded(editText.getText().toString());
                questionH6bList.add(questionH6b);
            }
        }

        active.setQuestionH6b(new Gson().toJson(questionH6bList));
    }
}






