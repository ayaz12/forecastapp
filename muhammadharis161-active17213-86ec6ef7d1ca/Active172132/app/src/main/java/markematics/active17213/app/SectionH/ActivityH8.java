package markematics.active17213.app.SectionH;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionH9;


public class ActivityH8 extends AppCompatActivity {

    TextView tViewH8 ;
    EditText editTextH8,editTextH8a,editTextH8b,editTextH8c,editTextH8d;
    Button btnNext;
    String H8;
    Typeface tf;
    Toolbar toolbar;
    Active active;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h8);

        tViewH8 = (TextView) findViewById(R.id.tViewH8);

        active = (Active) getIntent().getSerializableExtra("active");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question H8 ");
        toolbar.setTitleTextColor(Color.WHITE);

        editTextH8 = (EditText) findViewById(R.id.editTextH8);
        editTextH8a = (EditText) findViewById(R.id.editTextH8a);
        editTextH8b = (EditText) findViewById(R.id.editTextH8b);
        editTextH8c = (EditText) findViewById(R.id.editTextH8c);
        editTextH8d = (EditText) findViewById(R.id.editTextH8d);


        btnNext = (Button)findViewById(R.id.btnNext);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewH8.setTypeface(tf);


        editTextH8.setTypeface(tf);
        editTextH8a.setTypeface(tf);
        editTextH8b.setTypeface(tf);
        editTextH8c.setTypeface(tf);
        editTextH8d.setTypeface(tf);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

    }

    private void next() {

        initialize();
        add();
        if (!validationSuccess()) {
            Toast.makeText(this, "خالی جگہ\n",Toast.LENGTH_SHORT).show();
        } else {

            proceed();

        }

    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validationSuccess() {


        boolean valid = true;
        if (editTextH8.getVisibility() == View.VISIBLE) {
            if (H8.isEmpty()) {
                editTextH8.setError("درج کریں");
                valid = false;
            }

        }
        return valid;
    }


    private void proceed() {
        Intent nextPage = new Intent(ActivityH8.this, ActivityH9.class);
        nextPage.putExtra("active", active);
        startActivity(nextPage);
    }

    private void initialize() {
        H8=editTextH8.getText().toString().trim();

    }

    private void add() {
        List<String> newsPaperList = new ArrayList<>();
        newsPaperList.add(editTextH8.getText().toString());
        if (!editTextH8a.getText().toString().isEmpty())
        {
            newsPaperList.add(editTextH8a.getText().toString());
        }
        if (!editTextH8b.getText().toString().isEmpty())
        {
            newsPaperList.add(editTextH8b.getText().toString());
        }
        if (!editTextH8c.getText().toString().isEmpty())
        {
            newsPaperList.add(editTextH8c.getText().toString());
        }
        if (!editTextH8d.getText().toString().isEmpty())
        {
            newsPaperList.add(editTextH8d.getText().toString());
        }
        active.setQuestionH8(new Gson().toJson(newsPaperList));

    }
}