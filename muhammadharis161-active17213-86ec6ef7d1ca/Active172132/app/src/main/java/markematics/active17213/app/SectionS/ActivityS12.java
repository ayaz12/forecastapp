package markematics.active17213.app.SectionS;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.markematics.Active17213.R;

import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.QuestionS12;
import markematics.active17213.app.ActivityIntro;

public class ActivityS12 extends AppCompatActivity{

    TextView textViewS12;
    LinearLayout s12Layout ;
    Typeface tf;
    Button btnNext;
    Active active;
    Toolbar toolbar;
    List<QuestionS12> questionS12s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s12);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question S12");
        toolbar.setTitleTextColor(Color.WHITE);
        textViewS12 = (TextView) findViewById(R.id.tViewS12);
        s12Layout = (LinearLayout) findViewById(R.id.s2_layout);



        btnNext = (Button) findViewById(R.id.btnS12);

        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }

    private View.OnClickListener btnNext_OnClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };


    private boolean checkCode5()
    {
        boolean isChecked = false;
        for (int i =0;i<questionS12s.size();i++)
        {
            if (questionS12s.get(i).getCode() ==5)
            {
                return true;
            }
        }
        return isChecked;
    }

    private void randerS12Layout(){
        s12Layout.removeAllViews();
        String[] arrayS12 = getResources().getStringArray(R.array.arrayS12);
        for (int i=0;i<arrayS12.length;i++)
        {
            View v = getLayoutInflater().inflate(R.layout.s2_layout,null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);

            if (i == 5)
            {
                editText.setVisibility(View.VISIBLE);
            }
            label.setText(arrayS12[i]);
            label.setTag(i+1);
            label.setTypeface(tf);

            s12Layout.addView(v);
        }
    }



    private void add()
    {
        questionS12s = new ArrayList<>();
        for (int i=0;i<s12Layout.getChildCount();i++)
        {
            View v = s12Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            CheckBox chkBox = (CheckBox) v.findViewById(R.id.checkBox1);
            EditText editText = (EditText)v.findViewById(R.id.editText);

            if (chkBox.isChecked())
            {
                QuestionS12 questionS12 = new QuestionS12();
                questionS12.setCode((int)label.getTag());
                questionS12.setOpenEnded(editText.getText().toString());
                questionS12.setStatement(label.getText().toString());
                questionS12s.add(questionS12);
            }
        }

        active.setQuestionS12(new Gson().toJson(questionS12s));
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validate()
    {
        for (int i=0;i<questionS12s.size();i++)
        {
            if (questionS12s.get(i).getStatement().equalsIgnoreCase("دیگر"))
            {
                if (questionS12s.get(i).getOpenEnded().isEmpty())
                {
                    showDialog("دیگر");
                    return false;
                }
            }
        }
        if (questionS12s.size() == 0)
        {
            showDialog("Please select at least one option");
            return false;
        }
        return true;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityS12.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                            add();
                            if (validate())
                            {
                                if (checkCode5())
                                {
                                    Intent intent = new Intent(ActivityS12.this, ActivityS14S15.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }
                                else
                                {
                                    Intent intent = new Intent(ActivityS12.this, ActivityS13.class);
                                    intent.putExtra("active", active);
                                    startActivity(intent);
                                }
                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");

                            btnNext.setTextColor(Color.parseColor("#FFFFFF"));
                            active = (Active)getIntent().getSerializableExtra("active");
                            randerS12Layout();
                            textViewS12.setTypeface(tf);
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
