package markematics.active17213.Model;

/**
 * Created by ZEB Rana on 06-Oct-17.
 */

public class QuestionF4a {

    String statement;
    String label;
    int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
