package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionB7;


/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB7 extends AppCompatActivity {

    TextView tViewB7,tViewIns;
    Spinner spinB1;
    LinearLayout b7_Layout;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB7> questionB7s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b7);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B7");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewB7 = (TextView) findViewById(R.id.tViewB7);
        //   spinB1 = (Spinner)findViewById(R.id.spinB1);
        b7_Layout = (LinearLayout) findViewById(R.id.b7_layout);
        tViewIns=(TextView)findViewById(R.id.lblInsB7);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void renderB7Layout() {
        b7_Layout.removeAllViews();
        Type listType = new TypeToken<ArrayList<QuestionB4>>() {
        }.getType();
        List<QuestionB4> questionB5s = new Gson().fromJson(active.getQuestionB5(), listType);

        List<Brand> brands = new ArrayList<>();
        for (int i = 0; i < questionB5s.size(); i++) {
            Brand brand = new Brand();
            brand.setCode(questionB5s.get(i).getCode());
            brand.setName(questionB5s.get(i).getBrand());
            brands.add(brand);
        }

        for (int i = 0; i < brands.size(); i++) {

            View v = getLayoutInflater().inflate(R.layout.b7_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            final Spinner spinnerB7 = (Spinner) v.findViewById(R.id.spinnerB7);
            ArrayAdapter<String> adapterB7 = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayB7));
            spinnerB7.setAdapter(adapterB7);

            label.setTypeface(tf);
            label.setText(brands.get(i).getName());
            label.setTypeface(tf);
            label.setTag(brands.get(i).getCode());

            b7_Layout.addView(v);
            if (i % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#E0E0E0"));
            }
        }

    }


    private void add() {

        questionB7s = new ArrayList<>();
        for (int i = 0; i < b7_Layout.getChildCount(); i++) {
            View v = b7_Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            final Spinner spinnerB7 = (Spinner)v.findViewById(R.id.spinnerB7);


            if (spinnerB7.getSelectedItemPosition() !=0) {
                QuestionB7 questionB7 = new QuestionB7();
                questionB7.setCode(spinnerB7.getSelectedItemPosition());
                questionB7.setBrand(label.getText().toString());
                questionB7.setBrandCode((int)label.getTag());
                questionB7s.add(questionB7);

            }
        }


        active.setQuestionB7(new Gson().toJson(questionB7s));
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

    private boolean validate() {

        if (questionB7s.size() == 0) {
            showDialog("Please select at least one option B7");
            return false;
        }
        return true;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;
        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB7.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD)) {
                            add();
                            if (validate()) {
                                Intent intent = new Intent(ActivityB7.this, ActivityB8.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        }else if (request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB7.setTypeface(tf);
                            tViewIns.setTypeface(tf);
                            active = (Active) getIntent().getSerializableExtra("active");
                            renderB7Layout();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
