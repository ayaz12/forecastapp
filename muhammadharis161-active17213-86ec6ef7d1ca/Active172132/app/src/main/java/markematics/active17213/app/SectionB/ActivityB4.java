package markematics.active17213.app.SectionB;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;


/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB4 extends AppCompatActivity {

    TextView tViewB4,tViewIns;
    Spinner spinB1;
    LinearLayout b4_Layout;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB4> questionB4s;
    public String REQUEST_ADD = "add";
    public String REQUEST_RENDER = "render";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b4);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B4");
        toolbar.setTitleTextColor(Color.WHITE);

        tViewB4 = (TextView) findViewById(R.id.tViewB4);
        //   spinB1 = (Spinner)findViewById(R.id.spinB1);
        b4_Layout = (LinearLayout) findViewById(R.id.b4_layout);
        tViewIns = (TextView)findViewById(R.id.lblInsB4) ;
        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(btnNext_OnClickListener);
        new AsyncHandler(REQUEST_RENDER).execute();

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           new AsyncHandler(REQUEST_ADD).execute();
        }
    };

    private void renderB4Layout() {
        b4_Layout.removeAllViews();
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
        List<Brand> brands = new ArrayList<>();
        for (int i =0;i<questionB1s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB2s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB3s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
            brands.add(brand);
        }

        for (int i = 0; i <brands.size(); i++) {

                    View v = getLayoutInflater().inflate(R.layout.b1_layout, null);
                    TextView label = (TextView) v.findViewById(R.id.lbl);
                    EditText editText = (EditText) v.findViewById(R.id.editText);
                    CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);

                    label.setTypeface(tf);
                    label.setText(brands.get(i).getName());
                    label.setTypeface(tf);
                    label.setTag(brands.get(i).getCode());
                    chkBox1.setTag(v);


                    if (brands.get(i).getName().equalsIgnoreCase("Others")) {
                        editText.setVisibility(View.VISIBLE);
                    }
                    b4_Layout.addView(v);
                    if (i % 2 == 0) {
                        v.setBackgroundColor(Color.parseColor("#E0E0E0"));
                    }
                }

    }


    private void add()
    {

        questionB4s = new ArrayList<>();
        for (int i = 0; i < b4_Layout.getChildCount(); i++) {
            View v = b4_Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);

            CheckBox chkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);


            if (chkBox1.isChecked())
            {
                QuestionB4 questionB1 = new QuestionB4();
                questionB1.setBrand(label.getText().toString());
                questionB1.setCode((int)label.getTag());
                questionB4s.add(questionB1);
            }
        }


        active.setQuestionB4(new Gson().toJson(questionB4s));
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
    private boolean validate()
    {

        if (questionB4s.size() == 0)
        {
            showDialog("Please select at least one option B4");
            return false;
        }
        return true;
    }
    public class AsyncHandler extends AsyncTask<Object, Void, String> {


        ProgressDialog pDialog = null;
        String request;

        public AsyncHandler(String request)
        {
            this.request = request;
        }
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ActivityB4.this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(Object... params) {
            String responseObject = null;

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (request.equalsIgnoreCase(REQUEST_ADD))

                        {
                            add();
                            if (validate()) {
                                Intent intent = new Intent(ActivityB4.this, ActivityB5.class);
                                intent.putExtra("active", active);
                                startActivity(intent);
                            }
                        }
                        else if(request.equalsIgnoreCase(REQUEST_RENDER))
                        {
                            tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
                            tViewB4.setTypeface(tf);
                            tViewIns.setTypeface(tf);

                            active = (Active) getIntent().getSerializableExtra("active");
                            renderB4Layout();
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseObject;
        }
    }
}
