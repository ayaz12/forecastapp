package markematics.active17213.app.SectionE;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB14;
import markematics.active17213.Model.QuestionB4;

/**
 * Created by ZEB Rana on 16-Oct-17.
 */

public class ActivityE4b extends AppCompatActivity {
    TextView tViewE4b;
    Spinner spinnere4b;
    EditText editTextE4b;
    Button btnNext;
    Typeface tf;
    Toolbar toolbar;
    Active active;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e4b);

        tViewE4b = (TextView) findViewById(R.id.tViewE4b);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        editTextE4b = (EditText)findViewById(R.id.editText1);
        toolbar.setTitle("Question E4b");
        toolbar.setTitleTextColor(Color.WHITE);
        active = (Active) getIntent().getSerializableExtra("active");
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        spinnere4b = (Spinner) findViewById(R.id.spinnere4b);
        ArrayAdapter<String> spinnersE4bArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arraye4b));
        spinnere4b.setAdapter(spinnersE4bArrayAdapter);


        btnNext = (Button) findViewById(R.id.btnNext);

        tViewE4b.setTypeface(tf);

        spinnere4b.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 4)
                {
                    editTextE4b.setVisibility(View.VISIBLE);
                }
                else
                {
                    editTextE4b.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnNext.setOnClickListener(btnNext_OnClickListener);
    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validationSuccess())
            {
                add();
                proceed();
            }
        }
    };



    private void proceed() {
        {
            Intent intent = new Intent(ActivityE4b.this, ActivityE5.class);
            intent.putExtra("active", active);
            startActivity(intent);

        }
    }



    private boolean validationSuccess() {

        if (spinnere4b.getSelectedItemPosition() == 0 )
        {
            showDialog("Please select 4b");
            return false;
        }


        if (editTextE4b.getVisibility() == View.VISIBLE)
        {
            if (editTextE4b.getText().toString().isEmpty())
            {
                editTextE4b.setError("Please enter");
                return false;
            }
        }
        return true;
    }

    private void add() {

        active.setQuestionE4b(""+spinnere4b.getSelectedItemPosition());
        active.setQuestionE4bOther(editTextE4b.getText().toString());
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }
}
