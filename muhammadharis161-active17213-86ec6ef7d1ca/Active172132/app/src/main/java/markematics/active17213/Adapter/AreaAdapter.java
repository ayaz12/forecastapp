package markematics.active17213.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.markematics.Active17213.R;

import java.util.List;

import markematics.active17213.Model.Area;
import markematics.active17213.Model.Brand;

/**
 * Created by Muhammad Haris on 10/4/2017.
 */

public class AreaAdapter extends ArrayAdapter<Area> {
    LayoutInflater flater;

    public AreaAdapter(Activity context, int resouceId, int textviewId, List<Area> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Area rowItem = getItem(position);

        View rowview = flater.inflate(R.layout.spinner_item,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.txt);
        txtTitle.setText(rowItem.getArea() );



        return rowview;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(R.layout.spinner_item,parent, false);
        }
        Area rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txt);
        txtTitle.setText(rowItem.getArea());

        return convertView;
    }
}
