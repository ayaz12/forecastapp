package markematics.active17213.app.SectionB;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.markematics.Active17213.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import markematics.active17213.Model.Active;
import markematics.active17213.Model.Brand;
import markematics.active17213.Model.QuestionB1;
import markematics.active17213.Model.QuestionB4;
import markematics.active17213.Model.QuestionB7;

/**
 * Created by mas on 9/20/2017.
 */

public class ActivityB4B5B6 extends AppCompatActivity {

    TextView tViewB4, tViewB5, tViewB6,tViewB7;
   
    LinearLayout b4_b5_b6_b7_Layout;
    Button btnNext;

    View view;
    Typeface tf;
    Toolbar toolbar;
    Active active;
    List<QuestionB4> questionB4s;
    List<QuestionB4> questionB5s;
    List<QuestionB4> questionB6s;
    List<QuestionB7> questionB7s;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activityb4_b5_b6);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Question B4 B5 B6");
        toolbar.setTitleTextColor(Color.WHITE);
        tViewB4 = (TextView) findViewById(R.id.tViewB4);
        tViewB5 = (TextView) findViewById(R.id.tViewB5);
        tViewB6 = (TextView) findViewById(R.id.tViewB6);
        tViewB7 = (TextView)findViewById(R.id.tViewB7);
        b4_b5_b6_b7_Layout = (LinearLayout) findViewById(R.id.b4_b5_b6_b7_layout);
        active = (Active)getIntent().getSerializableExtra("active");

        btnNext = (Button)findViewById(R.id.btnB4);
        tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        tViewB4.setTypeface(tf);
        tViewB5.setTypeface(tf);
        tViewB6.setTypeface(tf);
        tViewB7.setTypeface(tf);
       btnNext.setOnClickListener(btnNext_OnClickListener);
        renderB4B5B6B7B8Layout();

    }

    private View.OnClickListener btnNext_OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validate()) {
                add();
                Intent intent = new Intent(ActivityB4B5B6.this, ActivityB8.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        }
    };

    private void renderB4B5B6B7B8Layout() {
        Type listType = new TypeToken<ArrayList<QuestionB1>>() {
        }.getType();
        List<QuestionB1> questionB1s = new Gson().fromJson(active.getQuestionB1(), listType);
        List<QuestionB1> questionB2s = new Gson().fromJson(active.getQuestionB2(), listType);
        List<QuestionB1> questionB3s = new Gson().fromJson(active.getQuestionB3(), listType);
        List<Brand> brands = new ArrayList<>();
        for (int i =0;i<questionB1s.size();i++)
        {
           Brand brand = new Brand();
            brand.setCode(questionB1s.get(i).getCode());
            brand.setName(questionB1s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB1s.get(i).getOpenEnded():questionB1s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB2s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB2s.get(i).getCode());
            brand.setName(questionB2s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB2s.get(i).getOpenEnded():questionB2s.get(i).getBrand());
            brands.add(brand);
        }
        for (int i =0;i<questionB3s.size();i++)
        {
            Brand brand = new Brand();
            brand.setCode(questionB3s.get(i).getCode());
            brand.setName(questionB3s.get(i).getBrand().equalsIgnoreCase("Others")?
                    questionB3s.get(i).getOpenEnded():questionB3s.get(i).getBrand());
            brands.add(brand);
        }
        String[] arrayB1 = getResources().getStringArray(R.array.arrayB2);
        for (int i = 0; i < brands.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.b4_b5_b6_b7_layout, null);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            Spinner spinnerB4 = (Spinner)v.findViewById(R.id.spinnerB4);
            final Spinner spinnerB5 = (Spinner)v.findViewById(R.id.spinnerB5);
            final Spinner spinnerB6 = (Spinner)v.findViewById(R.id.spinnerB6);
            final Spinner spinnerB7 = (Spinner)v.findViewById(R.id.spinnerB7);
            label.setText(brands.get(i).getName());
            label.setTag(brands.get(i).getCode());
            spinnerB4.setTag(v);
            spinnerB5.setTag(v);
            spinnerB6.setTag(v);

            spinnerB5.setEnabled(false);
            spinnerB6.setEnabled(false);
            spinnerB7.setEnabled(false);

            label.setTypeface(tf);
            ArrayAdapter<String> adapterB4 = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayB6));
            spinnerB4.setAdapter(adapterB4);
            ArrayAdapter<String> adapterB5 = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayB6));
            spinnerB5.setAdapter(adapterB5);
            ArrayAdapter<String> adapterB6 = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayB6));
            spinnerB6.setAdapter(adapterB6);
            ArrayAdapter<String> adapterB8 = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrayB7));
            spinnerB7.setAdapter(adapterB8);

              spinnerB4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                      if (position == 2)
                      {
                        spinnerB5.setEnabled(false);
                        spinnerB6.setEnabled(false);
                        spinnerB7.setEnabled(false);
                      }
                      if (position == 1)
                      {
                          spinnerB5.setEnabled(true);

                      }
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> adapterView) {

                  }
              });
            spinnerB5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    if (position == 2)
                    {

                        spinnerB6.setEnabled(false);

                    }
                    if (position == 1)
                    {

                        spinnerB6.setEnabled(true);
                        spinnerB7.setEnabled(true);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            spinnerB6.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    View childView = (View) spinnerB6.getTag();
                    TextView childLabel = (TextView) childView.findViewById(R.id.lbl);
                    if (position == 2) {


                        for (int i = 0; i < b4_b5_b6_b7_Layout.getChildCount(); i++) {
                            View v = b4_b5_b6_b7_Layout.getChildAt(i);
                            final Spinner spinnerB6 = (Spinner) v.findViewById(R.id.spinnerB6);
                            TextView label = (TextView) v.findViewById(R.id.lbl);
                            if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                                spinnerB6.setEnabled(true);
                            }
                        }
                        spinnerB7.setEnabled(false);
                    }
                    if (position == 1) {
                        for (int i = 0; i < b4_b5_b6_b7_Layout.getChildCount(); i++) {
                            View v = b4_b5_b6_b7_Layout.getChildAt(i);
                            final Spinner spinnerB6 = (Spinner) v.findViewById(R.id.spinnerB6);
                            TextView label = (TextView) v.findViewById(R.id.lbl);
                            if (!childLabel.getText().toString().equalsIgnoreCase(label.getText().toString())) {
                                spinnerB6.setEnabled(false);
                            }
                        }

                        spinnerB7.setEnabled(true);
                    }



                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            b4_b5_b6_b7_Layout.addView(v);
        }
    }


    private void add()
    {
        questionB4s = new ArrayList<>();
        questionB5s = new ArrayList<>();
        questionB6s = new ArrayList<>();
        questionB7s = new ArrayList<>();
        for (int i = 0;i<b4_b5_b6_b7_Layout.getChildCount();i++)
        {
            View v = b4_b5_b6_b7_Layout.getChildAt(i);
            TextView label = (TextView) v.findViewById(R.id.lbl);
            Spinner spinnerB4 = (Spinner)v.findViewById(R.id.spinnerB4);
            final Spinner spinnerB5 = (Spinner)v.findViewById(R.id.spinnerB5);
            final Spinner spinnerB6 = (Spinner)v.findViewById(R.id.spinnerB6);
            final Spinner spinnerB7 = (Spinner)v.findViewById(R.id.spinnerB7);

            if (spinnerB4.getSelectedItemPosition() == 1)
            {
                QuestionB4 questionB4 = new QuestionB4();
                questionB4.setCode((int)label.getTag());
                questionB4.setBrand(label.getText().toString());
                questionB4s.add(questionB4);
            }
            if (spinnerB5.getSelectedItemPosition() == 1)
            {
                QuestionB4 questionB4 = new QuestionB4();
                questionB4.setCode((int)label.getTag());
                questionB4.setBrand(label.getText().toString());
                questionB5s.add(questionB4);
            }
            if (spinnerB6.getSelectedItemPosition() == 1)
            {
                QuestionB4 questionB4 = new QuestionB4();
                questionB4.setCode((int)label.getTag());
                questionB4.setBrand(label.getText().toString());
                questionB6s.add(questionB4);
            }
            if (spinnerB7.isEnabled())
            {
                QuestionB7 questionB7 = new QuestionB7();
                questionB7.setCode(spinnerB7.getSelectedItemPosition());
                questionB7s.add(questionB7);
            }
        }

        active.setQuestionB4(new Gson().toJson(questionB4s));
        active.setQuestionB5(new Gson().toJson(questionB5s));
        active.setQuestionB6(new Gson().toJson(questionB6s));
        active.setQuestionB7(new Gson().toJson(questionB7s));

    }


    private boolean validate()
    {
        for (int i = 0;i<b4_b5_b6_b7_Layout.getChildCount();i++) {
            View v = b4_b5_b6_b7_Layout.getChildAt(i);
            Spinner spinnerB4 = (Spinner) v.findViewById(R.id.spinnerB4);



            if (spinnerB4.getSelectedItemPosition() == 0)
            {
                showDialog("Please fill all B4 values");
                return false;
            }

        }




        return true;
    }
    private void showDialog(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Alert!");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });

        builder.show();
    }

}
