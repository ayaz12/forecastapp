package markematics.active17213.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.markematics.Active17213.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
