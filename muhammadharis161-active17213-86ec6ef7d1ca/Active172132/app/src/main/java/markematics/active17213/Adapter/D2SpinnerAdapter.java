package markematics.active17213.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.markematics.Active17213.R;

import java.util.List;

import markematics.active17213.Model.QuestionD1;
import markematics.active17213.Model.QuestionS12;

/**
 * Created by Muhammad Haris on 10/4/2017.
 */

public class D2SpinnerAdapter extends ArrayAdapter<QuestionD1> {
    LayoutInflater flater;

    public D2SpinnerAdapter(Activity context, int resouceId, int textviewId, List<QuestionD1> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        QuestionD1 rowItem = getItem(position);

        View rowview = flater.inflate(R.layout.spinner_item,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.txt);
        txtTitle.setText(rowItem.getStatement() + (rowItem.getOpenEnded().isEmpty()?"":"("+rowItem.getOpenEnded()+")"));



        return rowview;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

            convertView = flater.inflate(R.layout.spinner_item,parent, false);

        QuestionD1 rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txt);
        txtTitle.setText(rowItem.getStatement() + (rowItem.getOpenEnded().isEmpty()?"":"("+rowItem.getOpenEnded()+")"));

        return convertView;
    }
}
