/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.github.lassana.recorder;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.github.lassana.recorder";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 131;
  public static final String VERSION_NAME = "1.3.1";
}
