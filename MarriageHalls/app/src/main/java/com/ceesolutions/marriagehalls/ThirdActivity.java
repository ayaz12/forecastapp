package com.ceesolutions.marriagehalls;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by ceeayaz on 4/14/18.
 */

public class ThirdActivity  extends AppCompatActivity {

    private Spinner spinner;
    private EditText editText;
    private String[] Events = {
            "Birthday Party",
            "Mehndi",
            "Barat",
            "Valima",
            "Nikkah Ceremony"


    };
    private Button nextBtn;
    private TextView main;
    private String eventSelected;
    private TextInputLayout spinnerTxtInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
//        getSupportActionBar().hide();
//        
        spinner = (Spinner) findViewById(R.id.accountTypeSpinner);
        editText = (EditText) findViewById(R.id.spinner);
        nextBtn = (Button) findViewById(R.id.nextBtn);
//        ImageView imageView = (ImageView) findViewById(R.id.image);



        spinnerTxtInput = (TextInputLayout) findViewById(R.id.spinnerTxtInput);
        spinnerTxtInput.setHint("Select Event");
        //Get the bitmap from the ImageView.
//        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

        //Let's apply Gaussian blur effect with radius "10.5" and set to ImageView.
//        imageView.setImageBitmap(new Constants().blur(ThirdActivity.this, bitmap, 10.5f));
        EventSpinner();
        main = (TextView) findViewById(R.id.main);

        main.setText("Please Select an Event from the list below: ");

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (eventSelected.equals("Select an Event")) {

                    Toast.makeText(ThirdActivity.this, "Please select an Event.", Toast.LENGTH_SHORT).show();

                } else {

                    Intent intent = new Intent(ThirdActivity.this, FourthActivity.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            }
        });


    }

    public void EventSpinner() {

        SpinnerAdapter purposeAdapter = new SpinnerAdapter(ThirdActivity.this, R.layout.custom_textview);
        purposeAdapter.addAll(Events);
        purposeAdapter.add("Select an Event");
        spinner.setAdapter(purposeAdapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {


                    // Get select item
                    if (spinner.getSelectedItem() == "Select an Event") {
                        eventSelected = spinner.getSelectedItem().toString();
                        Log.d("identity", "---" + eventSelected);

                    } else {

                        eventSelected = spinner.getSelectedItem().toString();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

}