package com.ceesolutions.marriagehalls;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by ceeayaz on 4/20/18.
 */

public class FifthActivity extends AppCompatActivity {

    private Spinner spinner;
    private EditText editText;
    private String[] GuestRange = {
            "Defence",
            "PECHS",
            "North Nazimabad",
            "Bahadurabad"


    };
    private Button nextBtn;
    private TextView main;
    private String guestSelect;
    private TextInputLayout spinnerTxtInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
//        getSupportActionBar().hide();
//
//        ImageView imageView = (ImageView) findViewById(R.id.image);

        spinnerTxtInput = (TextInputLayout) findViewById(R.id.spinnerTxtInput);
        spinnerTxtInput.setHint("Select Preferred Area");
//        spinnerTxtInput.setBackgroundColor(Color.parseColor("#ffffff"));


        //Get the bitmap from the ImageView.
//        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

        //Let's apply Gaussian blur effect with radius "10.5" and set to ImageView.
//        imageView.setImageBitmap(new Constants().blur(FifthActivity.this, bitmap, 5.5f));
        spinner = (Spinner) findViewById(R.id.accountTypeSpinner);
        editText = (EditText) findViewById(R.id.spinner);
        nextBtn = (Button) findViewById(R.id.nextBtn);
        GuestSpinner();
        main = (TextView) findViewById(R.id.main);
        main.setText("Please Select the Preferred Area from the below list");
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (guestSelect.equals("Select Preferred Area")) {

                    Toast.makeText(FifthActivity.this, "Please select Preferred Area.", Toast.LENGTH_SHORT).show();

                } else {

                    Intent intent = new Intent(FifthActivity.this, ListViewActivity.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            }
        });



    }

    public void GuestSpinner() {

        SpinnerAdapter purposeAdapter = new SpinnerAdapter(FifthActivity.this, R.layout.custom_textview);
        purposeAdapter.addAll(GuestRange);
        purposeAdapter.add("Select Preferred Area");
        spinner.setAdapter(purposeAdapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {


                    // Get select item
                    if (spinner.getSelectedItem() == "Select Preferred Area") {
                        guestSelect = spinner.getSelectedItem().toString();
                        Log.d("identity", "---" + guestSelect);


                    } else {

                        guestSelect = spinner.getSelectedItem().toString();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

}