package com.ceesolutions.marriagehalls;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by ceeayaz on 4/14/18.
 */

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();


        ImageView imageView = (ImageView) findViewById(R.id.image);



        //Get the bitmap from the ImageView.
//        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
//
//        //Let's apply Gaussian blur effect with radius "10.5" and set to ImageView.
//        imageView.setImageBitmap(new Constants().blur(SplashActivity.this, bitmap, 10.5f));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(SplashActivity.this, FirstActivity.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
            }
        }, 2500);

    }


}

