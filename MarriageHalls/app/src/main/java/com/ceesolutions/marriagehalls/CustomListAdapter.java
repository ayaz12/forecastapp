package com.ceesolutions.marriagehalls;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 4/15/18.
 */

public class CustomListAdapter extends BaseAdapter {
    ArrayList<Hall> hallModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView bankName;

    String type;

    public CustomListAdapter(ArrayList<Hall> hallModelsArrayList, Context mContext) {
        hallModels = hallModelsArrayList;
        context = mContext;

    }

    public int getCount() {
        // TODO Auto-generated method stub
        return hallModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.list_item, parent, false);
        }


        TextView userName = (TextView) convertView.findViewById(R.id.userName);
        TextView capacity = (TextView) convertView.findViewById(R.id.capacity);
        TextView cost = (TextView) convertView.findViewById(R.id.cost);
        TextView contact = (TextView) convertView.findViewById(R.id.contact);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.title);
        RequestOptions options = new RequestOptions();
        options.centerCrop();

        switch (position) {


            case 0: {

                Glide.with(context)
                        .load((R.drawable.royal1)).apply(options)
                        .into(imageView);
                break;
            }

            case 1: {

                Glide.with(context)
                        .load((R.drawable.al1)).apply(options)
                        .into(imageView);
                break;
            }

            case 2: {

                Glide.with(context)
                        .load((R.drawable.hill3)).apply(options)
                        .into(imageView);
                break;
            }

            default: {

                break;
            }
        }
        userName.setText(hallModels.get(position).getTitle());
        capacity.setText(hallModels.get(position).getCapacity());
        cost.setText(hallModels.get(position).getCost());
        contact.setText(hallModels.get(position).getContact());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ListDetialView.class);
                ((Activity) context).overridePendingTransition(0, 0);
                intent.putExtra("title", hallModels.get(position).getTitle());
                ((Activity) context).startActivity(intent);
            }
        });

        return convertView;
    }
}