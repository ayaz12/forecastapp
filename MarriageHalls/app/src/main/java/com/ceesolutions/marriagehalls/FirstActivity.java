package com.ceesolutions.marriagehalls;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by ceeayaz on 4/14/18.
 */

public class FirstActivity extends AppCompatActivity {

    private Spinner spinner;
    private EditText editText;
    private String[] GuestRange = {
            "300 - 500",
            "500 - 800",
            "800 - 1200",
            "1200 - 2000"


    };
    private Button nextBtn;
    private TextView main;
    private String guestSelect;
    private TextInputLayout spinnerTxtInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
//        getSupportActionBar().hide();
//
//        ImageView imageView = (ImageView) findViewById(R.id.image);

        spinnerTxtInput = (TextInputLayout) findViewById(R.id.spinnerTxtInput);
        spinnerTxtInput.setHint("Select Number of Guests");
//        spinnerTxtInput.setBackgroundColor(Color.parseColor("#ffffff"));


        //Get the bitmap from the ImageView.
//        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

        //Let's apply Gaussian blur effect with radius "10.5" and set to ImageView.
//        imageView.setImageBitmap(new Constants().blur(FirstActivity.this, bitmap, 5.5f));
        spinner = (Spinner) findViewById(R.id.accountTypeSpinner);
        editText = (EditText) findViewById(R.id.spinner);
        nextBtn = (Button) findViewById(R.id.nextBtn);
        GuestSpinner();
        main = (TextView) findViewById(R.id.main);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (guestSelect.equals("Select Number of Guests")) {

                    Toast.makeText(FirstActivity.this, "Please select Number of Guests.", Toast.LENGTH_SHORT).show();

                } else {

                    Intent intent = new Intent(FirstActivity.this,SecondActivity.class);
                    overridePendingTransition(0,0);
                    startActivity(intent);
                }
            }
        });



    }

    public void GuestSpinner() {

        SpinnerAdapter purposeAdapter = new SpinnerAdapter(FirstActivity.this, R.layout.custom_textview);
        purposeAdapter.addAll(GuestRange);
        purposeAdapter.add("Select Number of Guests");
        spinner.setAdapter(purposeAdapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {


                    // Get select item
                    if (spinner.getSelectedItem() == "Select Number of Guests") {
                        guestSelect = spinner.getSelectedItem().toString();
                        Log.d("identity", "---" + guestSelect);


                    } else {

                        guestSelect = spinner.getSelectedItem().toString();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

}