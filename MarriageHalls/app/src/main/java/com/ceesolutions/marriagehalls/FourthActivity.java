package com.ceesolutions.marriagehalls;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormatSymbols;
import java.util.Calendar;

/**
 * Created by ceeayaz on 4/14/18.
 */

public class FourthActivity extends AppCompatActivity {

    private Spinner spinner;
    private EditText editText;
    private String[] Events = {
            "Birthday Party",
            "Mehndi",
            "Barat",
            "Valima",
            "Nikkah Ceremony"


    };
    private EditText dateEditText;
    private Button nextBtn;
    private TextView main;
    private String eventSelected;
    private FrameLayout relativeLayout2, relativeLayout1;
    private int year, day, month;
    private String yearWithTwoDigits, monthName, date;
    private Dialog pd;
    final int DATE_PICKER_ID = 1;

    private TextInputLayout spinnerTxtInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
//        getSupportActionBar().hide();
//        
        spinner = (Spinner) findViewById(R.id.accountTypeSpinner);
        editText = (EditText) findViewById(R.id.spinner);
        dateEditText = (EditText) findViewById(R.id.dateTxt);
        nextBtn = (Button) findViewById(R.id.nextBtn);
        relativeLayout1 = (FrameLayout) findViewById(R.id.relativeLayout1);
        relativeLayout2 = (FrameLayout) findViewById(R.id.relativeLayout2);
        relativeLayout1.setVisibility(View.GONE);
        relativeLayout2.setVisibility(View.VISIBLE);
        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectDate();
            }
        });
        main = (TextView) findViewById(R.id.main);
//        ImageView imageView = (ImageView) findViewById(R.id.image);

        spinnerTxtInput = (TextInputLayout) findViewById(R.id.spinnerTxtInput);
        spinnerTxtInput.setHint("Select a Date");

        //Get the bitmap from the ImageView.
//        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

        //Let's apply Gaussian blur effect with radius "10.5" and set to ImageView.
//        imageView.setImageBitmap(new Constants().blur(FourthActivity.this, bitmap, 10.5f));
        main.setText("Please Select a Date for your Event: ");

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                date = dateEditText.getText().toString();

                if(!TextUtils.isEmpty(date) && !date.equals("Please select a date")){
                    Intent intent = new Intent(FourthActivity.this, FifthActivity.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }else{
                    Toast.makeText(FourthActivity.this, "Please select a Date for your event.", Toast.LENGTH_SHORT).show();

                }

            }
        });


    }

    private void SelectDate() {

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        showDialog(DATE_PICKER_ID);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            yearWithTwoDigits = String.valueOf(selectedYear);
            monthName = getMonth(selectedMonth + 1);
            day = selectedDay;

            if (day < 10) {

                dateEditText.setText(new StringBuilder().append("0").append(day)
                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

            } else {

                dateEditText.setText(new StringBuilder().append(day)
                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

            }
            // Show selected date

            date = dateEditText.getText().toString();

        }
    };

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }


}