package com.ceesolutions.marriagehalls;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.DateFormatSymbols;
import java.util.Calendar;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private Spinner spinner;
    private EditText editText;
    private Button nextBtn;
    private EditText dateEditText;
    private int year, day, month;
    private String yearWithTwoDigits, monthName, date;
    private Dialog pd;
    final int DATE_PICKER_ID = 1;
    private FrameLayout relativeLayout2;
    public static String guestRange = "";
    public static String perHead = "";
    public static String event = "";
    private String[] GuestRange = {
            "300 - 500",
            "500 - 800",
            "800 - 1200",
            "1200 - 2000"


    };

    private String[] PerHead = {
            "500 - 800",
            "800 - 1200",
            "1200 - 2000"


    };

    private String[] Events = {
            "Birthday Party",
            "Mehndi",
            "Barat",
            "Valima",
            "Nikkah Ceremony"


    };

    private TextView main;
    int count = 0;

    private String guestSelect, eventSelected, perHeadSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
        getSupportActionBar().hide();


        main = (TextView) findViewById(R.id.main);
        spinner = (Spinner) findViewById(R.id.accountTypeSpinner);
        editText = (EditText) findViewById(R.id.spinner);
        nextBtn = (Button) findViewById(R.id.nextBtn);
        dateEditText = (EditText) findViewById(R.id.dateTxt);
        relativeLayout2 = (FrameLayout) findViewById(R.id.relativeLayout2);
        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectDate();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nextBtn.setEnabled(false);
                count++;
                if (count == 1) {
                    if (guestRange.equals("Select Number of Guests")) {

                        Toast.makeText(MainActivity.this, "Please select Number of Guests.", Toast.LENGTH_SHORT).show();
                        nextBtn.setEnabled(true);
                        count = 0;
                    } else {

                        PerHeadSpinner();
                        nextBtn.setEnabled(true);
                    }
                } else if (count == 2) {

                    if (perHead.equals("Select Per Head for a Guests")) {


                        Toast.makeText(MainActivity.this, "Please select Per Head for a Guests.", Toast.LENGTH_SHORT).show();
                        nextBtn.setEnabled(true);
                        count = 1;
                    } else {

                        EventSpinner();
                    }


                } else if (count == 3) {

                    if (perHead.equals("Select an Event")) {


                        Toast.makeText(MainActivity.this, "Please select an Event.", Toast.LENGTH_SHORT).show();
                        nextBtn.setEnabled(true);
                        count = 1;
                    } else {


                    }

                }

            }
        });


    }

    public void GuestSpinner() {

        SpinnerAdapter purposeAdapter = new SpinnerAdapter(MainActivity.this, R.layout.custom_textview);
        purposeAdapter.addAll(GuestRange);
        purposeAdapter.add("Select Number of Guests");
        spinner.setAdapter(purposeAdapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {


                    // Get select item
                    if (spinner.getSelectedItem() == "Select Number of Guests") {
                        guestSelect = spinner.getSelectedItem().toString();
                        Log.d("identity", "---" + guestSelect);

                        guestRange = guestSelect;
                    } else {

                        guestSelect = spinner.getSelectedItem().toString();
                        guestRange = guestSelect;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }


    public void PerHeadSpinner() {


        SpinnerAdapter purposeAdapter = new SpinnerAdapter(MainActivity.this, R.layout.custom_textview);
        purposeAdapter.addAll(PerHead);
        purposeAdapter.add("Select Per Head for a Guests");
        spinner.setAdapter(purposeAdapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {


                    // Get select item
                    if (spinner.getSelectedItem() == "Select Per Head for a Guests") {
                        perHeadSelected = spinner.getSelectedItem().toString();
                        Log.d("identity", "---" + perHeadSelected);

                        perHead = perHeadSelected;
                    } else {

                        perHeadSelected = spinner.getSelectedItem().toString();
                        perHead = perHeadSelected;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }


    public void EventSpinner() {

        SpinnerAdapter purposeAdapter = new SpinnerAdapter(MainActivity.this, R.layout.custom_textview);
        purposeAdapter.addAll(Events);
        purposeAdapter.add("Select an Event");
        spinner.setAdapter(purposeAdapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {


                    // Get select item
                    if (spinner.getSelectedItem() == "Select an Event") {
                        eventSelected = spinner.getSelectedItem().toString();
                        Log.d("identity", "---" + eventSelected);

                        event = eventSelected;
                    } else {

                        eventSelected = spinner.getSelectedItem().toString();
                        event = eventSelected;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }


    private void SelectDate() {

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        showDialog(DATE_PICKER_ID);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            yearWithTwoDigits = String.valueOf(selectedYear);
            monthName = getMonth(selectedMonth + 1);
            day = selectedDay;

            if (day < 10) {

                dateEditText.setText(new StringBuilder().append("0").append(day)
                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

            } else {

                dateEditText.setText(new StringBuilder().append(day)
                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

            }
            // Show selected date

            date = dateEditText.getText().toString();

        }
    };

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

}
