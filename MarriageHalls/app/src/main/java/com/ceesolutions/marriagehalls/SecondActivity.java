package com.ceesolutions.marriagehalls;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by ceeayaz on 4/14/18.
 */

public class SecondActivity extends AppCompatActivity {

    private Spinner spinner;
    private EditText editText;
    private String[] PerHead = {
            "500 - 800",
            "800 - 1200",
            "1200 - 2000"


    };
    private Button nextBtn;
    private TextView main;
    private String perHeadSelect;
    private TextInputLayout spinnerTxtInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
//        getSupportActionBar().hide();
//        
        spinner = (Spinner) findViewById(R.id.accountTypeSpinner);
        editText = (EditText) findViewById(R.id.spinner);
        nextBtn = (Button) findViewById(R.id.nextBtn);
//        ImageView imageView = (ImageView) findViewById(R.id.image);

        spinnerTxtInput = (TextInputLayout) findViewById(R.id.spinnerTxtInput);
        spinnerTxtInput.setHint("Select Per Head for a Guest");
        //Get the bitmap from the ImageView.
//        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

        //Let's apply Gaussian blur effect with radius "10.5" and set to ImageView.
//        imageView.setImageBitmap(new Constants().blur(SecondActivity.this, bitmap, 5.5f));
        PerHeadSpinner();
        main = (TextView) findViewById(R.id.main);

        main.setText("Please Select Per Head for a Guests from the list below:");

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              

                if (perHeadSelect.equals("Select Per Head for a Guests")) {

                    Toast.makeText(SecondActivity.this, "Please select Per Head for a Guests.", Toast.LENGTH_SHORT).show();

                } else {

                    Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            }
        });


    }

    public void PerHeadSpinner() {


        SpinnerAdapter purposeAdapter = new SpinnerAdapter(SecondActivity.this, R.layout.custom_textview);
        purposeAdapter.addAll(PerHead);
        purposeAdapter.add("Select Per Head for a Guests");
        spinner.setAdapter(purposeAdapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {


                    // Get select item
                    if (spinner.getSelectedItem() == "Select Per Head for a Guests") {
                        perHeadSelect = spinner.getSelectedItem().toString();
                        Log.d("identity", "---" + perHeadSelect);


                    } else {

                        perHeadSelect = spinner.getSelectedItem().toString();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

}