package com.ceesolutions.marriagehalls;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 4/15/18.
 */

public class ListDetialView extends AppCompatActivity {


    private CircleIndicator indicator;
    private ViewPager mPager, pagerNonSamba;
    private ArrayList<Account> accounts;
    private static final Integer[] IMAGES = {R.drawable.hill1, R.drawable.hill2, R.drawable.hill3, R.drawable.hill4, R.drawable.hill5, R.drawable.hill6};
    private static final Integer[] IMAGES1 = {R.drawable.al1, R.drawable.al2, R.drawable.al3, R.drawable.al4, R.drawable.al5, R.drawable.al6};
    private static final Integer[] IMAGES2 = {R.drawable.royal1, R.drawable.royal2, R.drawable.royal3, R.drawable.royal4, R.drawable.royal5};
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_view);


        intent = getIntent();

        String string = intent.getStringExtra("title");
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        mPager = (ViewPager) findViewById(R.id.pagerNonSamba);

        if (string.equals("Royal Banquet")) {

            mPager.setAdapter(new CustomPagerAdapter(ListDetialView.this, IMAGES2));
            indicator.setViewPager(mPager);

        } else if (string.equals("Al Aqsa Banquet")) {
            mPager.setAdapter(new CustomPagerAdapter(ListDetialView.this, IMAGES1));
            indicator.setViewPager(mPager);

        } else {

            mPager.setAdapter(new CustomPagerAdapter(ListDetialView.this, IMAGES));
            indicator.setViewPager(mPager);

        }


    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        Integer[] image;

        public CustomPagerAdapter(Context context, Integer[] images) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            image = images;
        }

        @Override
        public int getCount() {
            return image.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.image_item, container, false);
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
            Glide.with(mContext)
                    .load((image[position])).apply(options)
                    .into(imageView);
//            imageView.setImageResource(image[position]);
            TextView textView = (TextView) itemView.findViewById(R.id.title);
            textView.setText(intent.getStringExtra("title"));

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}
