package com.ceesolutions.marriagehalls;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 4/15/18.
 */

public class ListViewActivity extends AppCompatActivity {


    private ListView listView;
    private ArrayList<Hall> hallArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_halls);

        hallArrayList = new ArrayList<>();
        listView = (ListView) findViewById(R.id.list_item);


        Hall hall = new Hall();
        hall.setTitle("Royal Banquet");
        hall.setCost("PKR 150,000");
        hall.setCapacity("500 - 1000");
        hall.setContact("Contact - (021) 36673254");
        hallArrayList.add(hall);

        Hall hall1 = new Hall();
        hall1.setTitle("Al Aqsa Banquet");
        hall1.setCapacity("1000 - 1500");
        hall1.setCost("PKR 300,000");
        hall1.setContact("Contact - (021) 34815590");
        hallArrayList.add(hall1);


        Hall hall2 = new Hall();
        hall2.setTitle("Hilltop Banquet");
        hall2.setCapacity("300 - 600");
        hall2.setCost("PKR 100,000");
        hall2.setContact("Contact - (021) 34635745");
        hallArrayList.add(hall2);



        CustomListAdapter customListAdapter = new CustomListAdapter(hallArrayList, ListViewActivity.this);
        listView.setAdapter(customListAdapter);


    }
}
