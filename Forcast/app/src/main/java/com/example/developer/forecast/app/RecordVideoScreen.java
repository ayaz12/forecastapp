package com.example.developer.forecast.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.example.developer.forecast.MainActivity;
import com.example.developer.forecast.R;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Joker on 4/11/2016.
 */
public class RecordVideoScreen extends Activity implements SurfaceHolder.Callback ,MediaRecorder.OnInfoListener{
    private GestureDetector gestureDetector;
    private SurfaceView prSurfaceView;
    private Button prStartBtn,prStopBtn;
    private Button crossBtn;
    private boolean prRecordInProcess;
    private static final String FORMAT = "%02d:%02d";
    private SurfaceHolder prSurfaceHolder;
    private Camera prCamera;
    private TextView d_countdown,textView;
    private CountDownTimer countDownTimer;
    private String outputFile;
    private Uri fileUri;
    private final String cVideoFilePath = "/sdcard/r10videocam/";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    String timeStamp;
    File mediaStorageDir;
//    private GPSTracker gps;
    private double latitude, longitude;
    private String result = null;
    private static final int MAX_CLICK_DURATION = 200;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "Videos";
    private long startClickTime;

    private Context prContext;
//    public String getLocation() {
//        gps = new GPSTracker(this);
//        // check if GPS enabled
//        if (gps.canGetLocation()) {
//            latitude = gps.getLatitude();
//            longitude = gps.getLongitude();
//        } else {
//            // can't get location
//            // GPS or Network is not enabled
//            // Ask user to enable GPS/network in settings
//            gps.showSettingsAlert();
//        }
//
//        Geocoder geo = new Geocoder(RecordVideoScreen.this.getApplicationContext(), Locale.getDefault());
//        Address adress;
//        List<Address> addresses = null;
//        try {
//            addresses = geo.getFromLocation(latitude, longitude, 1);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//
//
//
//            if (addresses.isEmpty()) {
//                Toast.makeText(RecordVideoScreen.this, "Please turn on your gps !", Toast.LENGTH_SHORT).show();
////			btn_upload.setEnabled(true);
//            } else {
//                if (addresses.size() > 0) {
//                    adress = addresses.get(0);
//                    //Converting location into String @result
//                    Log.d("", addresses.get(0).getFeatureName() + "," + addresses.get(0).getLocality() + "," + addresses.get(0).getAdminArea() + "," + addresses.get(0).getCountryName());
//                    result = adress.getAddressLine(0) + ", " + adress.getSubLocality() + ", " + adress.getLocality() + ", " + adress.getAdminArea();
//                    result = result.replace("null", "");
//                }
//            }
//        }catch (Exception e){
//
//            Log.d("---","Location"+e.getLocalizedMessage());
//        }
//        return result;
//    }
    @TargetApi(Build.VERSION_CODES.M)
    private void writeVideoPermission() {
        int hasWriteContactsPermission = checkSelfPermission(android.Manifest.permission.CAMERA);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            if (!shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow camera access.",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(new String[] {android.Manifest.permission.CAMERA},
                                        REQUEST_CODE_ASK_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(new String[] {android.Manifest.permission.CAMERA},
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(RecordVideoScreen.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Toast.makeText(RecordVideoScreen.this, "Permission Approved", Toast.LENGTH_SHORT)
                            .show();
                    startRecording();
                    prStartBtn.setVisibility(View.INVISIBLE);
                    prStopBtn.setVisibility(View.VISIBLE);
                } else {
                    // Permission Denied
                    Toast.makeText(RecordVideoScreen.this, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gestureDetector = new GestureDetector(this, new SingleTapConfirm());
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        prContext = this.getApplicationContext();
        setContentView(R.layout.record_video_screen);
//        getLocation();
        outputFile = Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/video.m4a";

        mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);
        prSurfaceView = (SurfaceView) findViewById(R.id.surface_camera);
        prStartBtn = (Button) findViewById(R.id.btnRec);
        crossBtn = (Button) findViewById(R.id.crossBtn);
        prStopBtn = (Button) findViewById(R.id.Recstop);
        prStartBtn.bringToFront();
        prStopBtn.bringToFront();
        prRecordInProcess = false;

        d_countdown = (TextView) findViewById(R.id.txttime);
        d_countdown.bringToFront();
//        d_countdown.setTypeface(effraMedium);
        d_countdown.setTextColor(Color.parseColor("#d81f27"));
        prSurfaceHolder = prSurfaceView.getHolder();
        prSurfaceHolder.addCallback(this);
        prSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        prMediaRecorder = new MediaRecorder();
        textView = (TextView)findViewById(R.id.b_tap_n_hold);
//		closeBtn.bringToFront();
        textView.bringToFront();
        textView.setVisibility(View.VISIBLE);
//        textView.setTypeface(effraLight);
        prMediaRecorder.setOnInfoListener(this);
        if (Build.VERSION.SDK_INT >= 23) {
            writeVideoPermission();
        }
        prStartBtn.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

//                onSingleTapConfirmed(event);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:


                        textView.setVisibility(View.INVISIBLE);

                        startRecording();
                        break;
                        case MotionEvent.ACTION_UP:


                        stopRecording();


                        break;

                }

                return false;
            }
        });



        prStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(RecordVideoScreen.this,"Tab and hold to record !",Toast.LENGTH_SHORT).show();
            }
        });

        crossBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prRecordInProcess) {
                    countDownTimer.cancel();
                    if (prMediaRecorder == null) {
                        finish();
                    } else {
                        prMediaRecorder.stop();
                        prMediaRecorder.release();
                        prMediaRecorder = null;
                        finish();
                    }

                } else {
                    finish();
                }
            }
        });
//        prStartBtn.setOnClickListener(new View.OnClickListener() {
//            //@Override
//            public void onClick(View v) {
//
//                startRecording();
//                prStartBtn.setVisibility(View.INVISIBLE);
//                prStopBtn.setVisibility(View.VISIBLE);
//            }
//        });
//
//        prStopBtn.setOnClickListener(new View.OnClickListener() {
//            //@Override
//            public void onClick(View v) {
//                stopRecording();
//                prStartBtn.setVisibility(View.VISIBLE);
//                prStartBtn.setEnabled(false);
//                prStopBtn.setVisibility(View.INVISIBLE);
//            }
//        });

    }
    public boolean onSingleTapConfirmed(MotionEvent e) {

        //this works for me
        Toast.makeText(RecordVideoScreen.this,"Tab and hold to record !",Toast.LENGTH_SHORT).show();
        return true;
    }
    //@Override
    public void surfaceChanged(SurfaceHolder _holder, int _format, int _width, int _height) {
        Camera.Parameters lParam = prCamera.getParameters();
        List<String> focusModes = lParam.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            // set the focus mode
            lParam.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            // set Camera parameters
            prCamera.setParameters(lParam);
        }
        try {
            prCamera.setPreviewDisplay(_holder);
            prCamera.startPreview();
            //prPreviewRunning = true;
        } catch (IOException _le) {
            _le.printStackTrace();
        }
    }

    //@Override
    public void surfaceCreated(SurfaceHolder arg0) {
        prCamera = Camera.open();
        prCamera.setDisplayOrientation(90);
//		prCamera.unlock();
        if (prCamera == null) {
            Toast.makeText(this.getApplicationContext(), "Camera is not available!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    //@Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        if (prRecordInProcess) {
//            stopRecording();
//            prMediaRecorder.stop();
//            prMediaRecorder.reset();
//            prMediaRecorder.release();
//            try {
//                prCamera.reconnect();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        } else {
            prCamera.stopPreview();
        }
//        prMediaRecorder.release();
//        prMediaRecorder = null;
        prCamera.release();
        prCamera = null;
    }

    private MediaRecorder prMediaRecorder;
    private final int cMaxRecordDurationInMs = 150000;
    private final long cMaxFileSizeInBytes = 5000000;
    private final int cFrameRate = 40;
    private File prRecordedFile;

////    private void updateEncodingOptions() {
////        if (prRecordInProcess) {
////            stopRecording();
////            startRecording();
////            Toast.makeText(prContext, "Recording restarted with new options!", Toast.LENGTH_SHORT).show();
////        } else {
////            Toast.makeText(prContext, "Recording options updated!", Toast.LENGTH_SHORT).show();
////        }
//    }

    private boolean startRecording() {
        prCamera.stopPreview();
        try {
            textView.setVisibility(View.INVISIBLE);
            prMediaRecorder = new MediaRecorder();
            prCamera.setDisplayOrientation(90);
            prCamera.unlock();
            prMediaRecorder.setCamera(prCamera);
            //set audio source as Microphone, video source as camera
            //state: Initial=>Initialized
            Method[] methods = prMediaRecorder.getClass().getMethods();
            prMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//			prMediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
            prMediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
//			prMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
            //set the file output format: 3gp or mp4
            //state: Initialized=>DataSourceConfigured
            String lVideoFileFullPath;
//			String lDisplayMsg = "Current container format: ";
//			if (Utils.puContainerFormat == SettingsDialog.cpu3GP) {
//				lDisplayMsg += "MP4\n";
//				lVideoFileFullPath = ".mp4";
            prMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//			} else if (Utils.puContainerFormat == SettingsDialog.cpuMP4) {
//				lDisplayMsg += "MP4\n";
            lVideoFileFullPath = ".mp4";
//				prMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//			} else {
//				lDisplayMsg += "MP4\n";
//				lVideoFileFullPath = ".mp4";
//				prMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//			}
            //the encoders:
            //audio: AMR-NB
//			prMediaRecorder.setAudioEncoder(AudioEncoder.AMR_NB);
//			prMediaRecorder.setAudioEncoder(AudioEncoder.AMR_WB);
            prMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//			prMediaRecorder.setAudioEncoder(AudioEncoder.AAC_ELD);
//			prMediaRecorder.setAudioEncoder(AudioEncoder.HE_AAC);
            //video: H.263, MP4-SP, or H.264
            //prMediaRecorder.setVideoEncoder(VideoEncoder.H263);
            //prMediaRecorder.setVideoEncoder(VideoEncoder.MPEG_4_SP);
//			lDisplayMsg += "Current encoding format: ";
//			if (Utils.puEncodingFormat == SettingsDialog.cpuH263) {
//				lDisplayMsg += "MPEG4-SP\n";
//				prMediaRecorder.setVideoEncoder(VideoEncoder.MPEG_4_SP);
            prMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            prRecordedFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + lVideoFileFullPath);
//			if (Utils.puResolutionChoice == SettingsDialog.cpuRes176) {
//            prMediaRecorder.setVideoSize(720, 480);
//			} else if (Utils.puResolutionChoice == SettingsDialog.cpuRes320) {
//				prMediaRecorder.setVideoSize(720, 480);
//			} else if (Utils.puResolutionChoice == SettingsDialog.cpuRes720) {
//				prMediaRecorder.setVideoSize(720, 480);
//			}
//			Toast.makeText(prContext, lDisplayMsg, Toast.LENGTH_LONG).show();
//            prMediaRecorder.setVideoFrameRate(24);
            prMediaRecorder.setVideoSize(720, 480);
            prMediaRecorder.setPreviewDisplay(prSurfaceHolder.getSurface());
            prMediaRecorder.setMaxDuration(cMaxRecordDurationInMs);
            prMediaRecorder.setMaxFileSize(cMaxFileSizeInBytes);
            prMediaRecorder.setOrientationHint(90);
//            prMediaRecorder.setVideoEncodingBitRate(512000);
//            prMediaRecorder.setAudioEncodingBitRate(128000);
//            prMediaRecorder.setAudioSamplingRate(44100);
//            prMediaRecorder.setAudioChannels(2);
            for (Method method: methods){
                try{
                    if (method.getName().equals("setAudioChannels")){
                        method.invoke(prMediaRecorder, String.format("audio-param-number-of-channels=%d", 2));
                    }
                    else
                    if(method.getName().equals("setAudioEncodingBitRate")){
                        method.invoke(prMediaRecorder,96000);
                    }
                    else if(method.getName().equals("setVideoEncodingBitRate")){
                        method.invoke(prMediaRecorder, 1000000);
                    }
                    else if(method.getName().equals("setAudioSamplingRate")){
                        method.invoke(prMediaRecorder,44100);
                    }
                    else if(method.getName().equals("setVideoFrameRate")){
                        method.invoke(prMediaRecorder,30);
                    }
                }catch (IllegalArgumentException e) {
                    prMediaRecorder.setVideoSize(640, 480);
                    Log.d("---","IllegalArgumentException3"+e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    Log.d("---","IllegalAccessException3"+e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    Log.d("---","InvocationTargetException"+e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
//			prRecordedFile = new File(lVideoFileFullPath);
            Log.d("aaa", "---" + prRecordedFile);
            prMediaRecorder.setOutputFile(prRecordedFile.getPath());
            countDownTimer = null;
            d_countdown.setText("00:15");

//                    int num = Integer.valueOf(d_countdown.getText().toString());
//                    seekBar.setProgress(num);

            countDownTimer = new CountDownTimer(15000, 1000) {

                public void onTick(long millisUntilFinished) {
//                    d_countdown.setText(millisUntilFinished / 1000 + "");
                    long ms = millisUntilFinished;
                    String text = String.format(FORMAT,
                            TimeUnit.MILLISECONDS.toMinutes(ms) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ms)),
                            TimeUnit.MILLISECONDS.toSeconds(ms) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(ms)));
                    d_countdown.setText(text);
//                            int num = Integer.valueOf(d_countdown.getText().toString());
//                            seekBar.setProgress(num);
                }
                public void onFinish() {
                    d_countdown.setText("Time is up!");
                    d_countdown.setTextSize(25.0f);
//					d_countdown1.setVisibility(View.INVISIBLE);
//                        d_startrec.setVisibility(View.GONE);
//                        d_stoprec.setVisibility(View.GONE);
//                        llsave.setVisibility(View.VISIBLE);


                    try {
                        prMediaRecorder.stop();
                        prMediaRecorder.reset();
                        try {
                            prCamera.reconnect();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//		prStartBtn.setText("Start");
                        prRecordInProcess = false;
                        prCamera.startPreview();

//						myRecorder = null;
//						isRecording = false;

                        countDownTimer.cancel();
                        prStartBtn.setVisibility(View.VISIBLE);
                        prStopBtn.setVisibility(View.INVISIBLE);

//						seekBar.setProgress(0);
//						bttnRecStop.setVisibility(View.INVISIBLE);
//						bttnRecStop.setEnabled(true);
//						btnReset.setVisibility(View.INVISIBLE);
//						btnReset.setEnabled(true);
//						btnResetDis.setVisibility(View.INVISIBLE);
//						btnPlay.setVisibility(View.INVISIBLE);
//						btnPlay.setEnabled(true);
//						btnRecDis.setVisibility(View.INVISIBLE);
//						btnRecDis.setEnabled(false);
//						imageView2.setVisibility(View.INVISIBLE);
//						imageView3.setVisibility(View.INVISIBLE);
//						imageView1.setEnabled(true);
//						Intent slideactivity = new Intent(NewRecordingScreen.this, NewShareScreen.class);
////                        slideactivity.putExtra("image",photo);
////                        slideactivity.putExtra("check","no");
//						prefs = context.getSharedPreferences("audioprof", Context.MODE_PRIVATE);
//						Log.d("tag", "QQ >> prefs " + prefs);
//						SharedPreferences.Editor editor = context.getSharedPreferences("audioprof", Context.MODE_PRIVATE).edit();
                        try {
                            Intent intent = new Intent(RecordVideoScreen.this,MainActivity.class);
                            intent.putExtra("video",prRecordedFile.getPath());
                            startActivity(intent);
                            finish();
//							Log.d("tag", "QQ >> editor " + editor);
//							editor.putString("audio_file", getAudioFile());
//							editor.apply();
//							editor.commit();
                        } catch (Exception e1) {
                            Log.d("---","Exception2"+e1.getLocalizedMessage());
                            e1.printStackTrace();
                        }
//						NewRecordingScreen.this.startActivity(slideactivity);
//						overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
//						NewRecordingScreen.this.finish();

                    } catch (IllegalStateException e) {
                        //  it is called before start()
                        Log.d("---","IllegalStateException2"+e.getLocalizedMessage());
                        e.printStackTrace();
                    } catch (RuntimeException e) {
                        // no valid audio/video data has been received
                        Log.d("---","RuntimeException2"+e.getLocalizedMessage());
                        e.printStackTrace();
                    }
                }
            }.start();

//			} else if (Utils.puEncodingFormat == SettingsDialog.cpuMP4_SP) {
//				lDisplayMsg += "MPEG4-SP\n";
//				prMediaRecorder.setVideoEncoder(VideoEncoder.MPEG_4_SP);
//			} else if (Utils.puEncodingFormat == SettingsDialog.cpuH264) {
//				lDisplayMsg += "MPEG4-SP\n";
//				prMediaRecorder.setVideoEncoder(VideoEncoder.MPEG_4_SP);
//			} else {
//				lDisplayMsg += "MPEG4-SP\n";
//				prMediaRecorder.setVideoEncoder(VideoEncoder.MPEG_4_SP);
//			}
//			lVideoFileFullPath = mediaStorageDir.getPath() + String.valueOf(System.currentTimeMillis()) + lVideoFileFullPath;
            Log.d("aaa","---"+lVideoFileFullPath);
//            outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/video.mp4";

            outputFile=Environment.getExternalStorageDirectory() + File.separator
                    + Environment.DIRECTORY_DCIM + File.separator + "video.mp4";
                    fileUri = getOutputMediaFileUri(2);


            //prepare for capturing
            //state: DataSourceConfigured => prepared
            prMediaRecorder.prepare();
            //start recording
            //state: prepared => recording
            prMediaRecorder.start();

//			prMediaRecorder.setMaxDuration(MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED);
//			if (cMaxRecordDurationInMs==100000){
//
//				stopRecording();
//			}
//			prStartBtn.setText("Stop");
            prRecordInProcess = true;
            return true;
        } catch (IOException _le) {
            _le.printStackTrace();
            Log.d("---","IOException1"+_le.getLocalizedMessage());
            return false;
        }
    }

    private void stopRecording() {
        try { prMediaRecorder.stop();
            prMediaRecorder.release();
            prMediaRecorder = null;
            prRecordInProcess = false;
            prCamera.startPreview();
            countDownTimer.cancel();
            prCamera.reconnect();
            Intent intent = new Intent(RecordVideoScreen.this,MainActivity.class);
            intent.putExtra("video",prRecordedFile.getPath());
            startActivity(intent);
            finish();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (IllegalStateException e) {
            //  it is called before start()
            e.printStackTrace();
        } catch (RuntimeException e) {
            // no valid audio/video data has been received
            e.printStackTrace();
            Toast.makeText(RecordVideoScreen.this,"Please tap and hold to record ! ",Toast.LENGTH_SHORT).show();
//            prMediaRecorder.stop();
            countDownTimer.cancel();
            textView.setVisibility(View.VISIBLE);
//            prMediaRecorder.release();
            prMediaRecorder = null;

        }
//		prStartBtn.setText("Start");

    }

    private static final int REQUEST_DECODING_OPTIONS = 0;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case REQUEST_DECODING_OPTIONS:
                if (resultCode == RESULT_OK) {
//                    updateEncodingOptions();
                    startRecording();
                }
                break;
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void onInfo(MediaRecorder mr, int what, int extra) {
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            Log.v("VIDEOCAPTURE","Maximum Duration Reached");
            mr.stop();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (prRecordInProcess) {
            countDownTimer.cancel();
           if (prMediaRecorder==null){
               finish();
           }
            else{
               prMediaRecorder.stop();
               prMediaRecorder.release();
               prMediaRecorder = null;
               finish();
           }

        } else {
            finish();
        }
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent event) {
            return true;
        }
    }
}