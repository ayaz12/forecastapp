package com.example.developer.forecast;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.media.AudioFormat;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.developer.forecast.app.AppController;
import com.example.developer.forecast.app.DataSet;
import com.example.developer.forecast.app.RecordVideoScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity {

	// json object response url
	private String urlJsonObj = "http://192.168.0.193:23832/api/Media";

	ArrayList<DataSet> dataSetArrayList;
	byte[] image,data,videodata;
	String s,s2,videoFile;
	Uri uri;

	private static String TAG = MainActivity.class.getSimpleName();

	private static final String FORMAT = "%02d:%02d";


	private Boolean isRecording = false;
	private String outputFile = null;
	private float recordTime = 0;
	private MediaRecorder myRecorder;
	private CountDownTimer countDownTimer;
	private Handler handler = new Handler();
	private Button prStartBtn;


	// Progress dialog
	private ProgressDialog pDialog;

	private ListView listView;
	private void registerUser(){
		Log.d("000","---JD");
//		final String username = editTextUsername.getText().toString().trim();
//		final String password = editTextPassword.getText().toString().trim();
//		final String email = editTextEmail.getText().toString().trim();

		StringRequest stringRequest = new StringRequest(Request.Method.GET, urlJsonObj,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("000","---JD3"+response);
						Toast.makeText(MainActivity.this,"JD KI :@"+response,Toast.LENGTH_LONG).show();
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
					}
				}){
			@Override
			protected Map<String,String> getParams(){
				Map<String,String> params = new HashMap<String, String>();
				Log.d("---","ss"+s);
				Log.d("---","ss2"+s2);
				params.put("Image",s);
				params.put("File",videoFile);
				params.put("FileType","Video");
//				params.put("UserName","CEO");
//				params.put("FirstName","Taaha");
//				params.put("LastName", "Khalid");
//				params.put("Email","0");
//				params.put("DateOfBirth","0");
//				params.put("ContactNo","0");
//				params.put("CreatedAt","0");
//				params.put("Location","karachi");
//				params.put("UserType","0");
//				params.put("AuthToken","1");
//				params.put("SocialID","0");
//				params.put("PictureUrl","http://");
//				params.put("ThumbnailUrl","http://");
//				params.put("EmailVerified","true");
//				params.put("PublicWhisperCount","0");
//				params.put("PrivateWhisperCount","0");
//				params.put("FriendsCount","0");
//				params.put("Password","ceo");


				return params;
			}

		};

		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button btn = (Button)findViewById(R.id.Record2);
		btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, RecordVideoScreen.class);
				startActivity(intent);
			}
		});
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		if (bundle != null) {

			videoFile = (String) bundle.get("video");

			FileInputStream fileInputStream = null;
			File file = new File(videoFile);
			videodata = new byte[(int) file.length()];
			Log.d("Log","---"+videodata);
			Bitmap icon = BitmapFactory.decodeResource(MainActivity.this.getResources(),
					R.drawable.sun_and_rain);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			icon.compress(Bitmap.CompressFormat.JPEG, 25, stream);
			image = stream.toByteArray();
			s = Base64.encodeToString(image, Base64.DEFAULT);

			try {
				//convert file into array of bytes
				fileInputStream = new FileInputStream(file);
				fileInputStream.read(videodata);
				fileInputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			videoFile = Base64.encodeToString(videodata, Base64.DEFAULT);
			registerUser();
			prStartBtn = (Button) findViewById(R.id.Record);

			prStartBtn.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

//                onSingleTapConfirmed(event);
					switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:


							Toast.makeText(MainActivity.this, "Recording Started", Toast.LENGTH_SHORT).show();
							prStartBtn.setText("Stop");
							RecordingStart();
							break;
						case MotionEvent.ACTION_UP:


							StopRecording();
							Toast.makeText(MainActivity.this, "Recording Stoped", Toast.LENGTH_SHORT).show();
							prStartBtn.setText("Start");

							Log.d("000", "---JD2");

							break;

					}

					return false;
				}
			});
//		}
//		try {
//			inputData = getBytes(iStream);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}



//		s = new String(image);
//		try {
//			FileInputStream fileInputStream = new FileInputStream(image);
//			fileInputStream.read(image);
//
//			String byteArrayStr= new String(Base64.encodeToString(image,0));
//
//			FileOutputStream fos = new FileOutputStream("newFilePath");
//			fos.write(Base64.decodeBase64(byteArrayStr.getBytes()));
//			fos.close();
//		}
//		catch (FileNotFoundException e) {
//			System.out.println("File Not Found.");
//			e.printStackTrace();
//		}
//		catch (IOException e1) {
//			System.out.println("Error Reading The File.");
//			e1.printStackTrace();
//		}
			dataSetArrayList = new ArrayList<>();
			pDialog = new ProgressDialog(MainActivity.this);
			listView = (ListView) findViewById(R.id.list);

//		makeJsonObjectRequest();
//		RequestQueue queue = Volley.newRequestQueue(this);

//		JSONObject params = new JSONObject();
//		try {
//			params.put("name", "Droider");
//			params.put("")
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//				urlJsonObj, params,
//				new Response.Listener<JSONObject>() {
//
//					@Override
//					public void onResponse(JSONObject response) {
//						Log.d(TAG, response.toString());
//						pDialog.hide();
//					}
//				}, new Response.ErrorListener() {
//
//			@Override
//			public void onErrorResponse(VolleyError error) {
//				VolleyLog.d(TAG, "Error: " + error.getMessage());
//				pDialog.hide();
//			}
//		}) {
//
//			@Override
//			public Map<String, String> getHeaders() throws AuthFailureError {
//				HashMap<String, String> headers = new HashMap<String, String>();
//				headers.put("Content-Type", "application/json; charset=utf-8");
//				return headers;
//			}
//
//		};

			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);

		}
	}

	/**
	 * Method to make json object request where json response starts wtih {
	 * */
	private void makeJsonObjectRequest() {

		showpDialog();


		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
				urlJsonObj, null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, response.toString());

						try {
							// Parsing json object response
							// response will be a json object

							Log.d("---","Json"+response);
							JSONObject forecast = response.getJSONObject("forecast");

							JSONObject txt_forecast = forecast.getJSONObject("txt_forecast");


							String date = txt_forecast.getString("date");


							//JSONArray forecastday = txt_forecast.getJSONArray("forecastday");


							JSONObject simpleforecast = forecast.getJSONObject("simpleforecast");


							JSONArray forecastDay = simpleforecast.getJSONArray("forecastday");




							for(int i = 0; i<forecastDay.length();i++){

								DataSet dataSet = new DataSet();

								JSONObject index =(JSONObject)forecastDay.get(i);

								JSONObject datee = index.getJSONObject("date");

								String pretty  = (String)datee.get("pretty");

								dataSet.setDate(pretty);

								String day  = (String)datee.get("weekday");

								dataSet.setDay(day);


								JSONObject high = index.getJSONObject("high");

								String highcelsius = (String)high.get("celsius");

								dataSet.setHigh(highcelsius);

								JSONObject low = index.getJSONObject("low");

								String lowcelsius = (String)low.get("celsius");

								dataSet.setLow(lowcelsius);



								dataSetArrayList.add(dataSet);

							}

							listView.setAdapter(new ListAdapter(MainActivity.this,dataSetArrayList));


						} catch (JSONException e) {
							e.printStackTrace();
							Toast.makeText(getApplicationContext(),
									"Error: " + e.getMessage(),
									Toast.LENGTH_LONG).show();
						}
						hidepDialog();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(TAG, "Error: " + error.getMessage());
						Toast.makeText(getApplicationContext(),
								error.getMessage(), Toast.LENGTH_SHORT).show();
						// hide the progress dialog
						hidepDialog();
					}
				});

		Log.d("jsonObjReq","---"+jsonObjReq);
		Log.d("AppController", "---" + AppController.getInstance());
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq);
	}

	/**
	 * Method to make json array request where response starts with [
	 * */

	private void showpDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hidepDialog() {
		if (pDialog.isShowing())
			pDialog.dismiss();
	}

	public byte[] SafeAudio(String outputFile1){

		FileInputStream fileInputStream = null;
		File fileObj = new File(outputFile1);
		data = new byte[(int) fileObj.length()];
		try {
			//convert file into array of bytes
			fileInputStream = new FileInputStream(fileObj);
			fileInputStream.read(data);
			fileInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public void RecordingStart() {
		if (!isRecording) {
			outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/sound.m4a";
			SafeAudio(outputFile);
			s2 = Base64.encodeToString(data, Base64.DEFAULT);
//			circularSeekBar.setProgress(0);
			recordTime = 0;

			myRecorder = new MediaRecorder();
			myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			myRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			myRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
			myRecorder.setAudioEncodingBitRate(128000);
			myRecorder.setAudioSamplingRate(44100);
			myRecorder.setAudioChannels(1);
//            setAudioFile(outputFile);

			myRecorder.setOutputFile(outputFile);
//			recordBtnStop.setVisibility(View.VISIBLE);
			countDownTimer = null;
//            timer_txt.setText("60");
			countDownTimer = new CountDownTimer(59000, 1000) {
				public void onTick(long millisUntilFinished) {
					long ms = millisUntilFinished;
					String text = String.format(FORMAT,
							TimeUnit.MILLISECONDS.toMinutes(ms) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ms)),
							TimeUnit.MILLISECONDS.toSeconds(ms) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(ms)));

				}

				public void onFinish() {


					try {
						Log.d("---anderaja","===");
//                        b_start.setVisibility(View.INVISIBLE);


//						recordBtnStop.setVisibility(View.INVISIBLE);
//						hideItems();
//						showHideItems();
						myRecorder.stop();
						myRecorder.release();
						myRecorder = null;
						isRecording = false;
						countDownTimer.cancel();

//						safeAudio();
					} catch (IllegalStateException e) {
						Log.d("---anderaja1","==="+e.getLocalizedMessage());
						//  it is called before start()
						e.printStackTrace();
					} catch (RuntimeException e) {
						Log.d("---anderaja2","==="+e.getLocalizedMessage());
						// no valid audio/video data has been received
						e.printStackTrace();
					}
				}
			}.start();
//			circularSeekBar.setProgress(0);
			handler.post(UpdateRecordTime);
			try {
//				circularSeekBar.setProgress(0);
				myRecorder.prepare();
				myRecorder.start();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// prepare() fails
				e.printStackTrace();
			}
			isRecording = true;
		}
	}

	Runnable UpdateRecordTime = new Runnable() {
		public void run() {
			if (isRecording) {
				recordTime += 1.7f;
//				circularSeekBar.setProgress(recordTime);
				// Delay 1s before next call
				handler.postDelayed(this, 1000);
			}
		}
	};

	public void StopRecording() {
		if (isRecording) {
//            showDialog();
			countDownTimer.cancel();
			try {
//				click_image.setEnabled(true);
//				b_start.setVisibility(View.INVISIBLE);
//				hideItems();
//				showHideItems();

				myRecorder.stop();
				myRecorder.reset();
				myRecorder.release();
				myRecorder = null;
				Log.d("---","--cancel");
			} catch (IllegalStateException e) {
				//  it is called before start()
				e.printStackTrace();
			} catch (RuntimeException e) {
				// no valid audio/video data has been received
				e.printStackTrace();
			}
			isRecording = false;
		}
	}

}
