package com.example.developer.forecast;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.developer.forecast.app.DataSet;

import java.util.ArrayList;

/**
 * Created by developer on 31/03/16.
 */
public class ListAdapter extends BaseAdapter{

    private LayoutInflater mInflater;
    private Context mContext;
    private Typeface mTypeface;
    private String mUrl;
    private String mName;
    private ArrayList<DataSet> listviewDataSet;
    private SharedPreferences prefs;

    public ListAdapter(Context context, ArrayList<DataSet> listviewData1) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        this.listviewDataSet = listviewData1;
    }

    public int getCount(){
        Log.d("ListSize", "ListSize" + listviewDataSet.size());
        return listviewDataSet.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {

        TextView high_c,low_c,date,day;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_layout, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.date = (TextView) convertView.findViewById(R.id.date);
        holder.day = (TextView) convertView.findViewById(R.id.day);
        holder.high_c = (TextView) convertView.findViewById(R.id.high_c);
        holder.low_c = (TextView) convertView.findViewById(R.id.low_c);


        holder.high_c.setText(listviewDataSet.get(position).getHigh()+"'C");

        holder.low_c.setText(listviewDataSet.get(position).getLow()+"'C");

        holder.date.setText(listviewDataSet.get(position).getDate());

        holder.day.setText(listviewDataSet.get(position).getDay());


        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });




        return convertView;
    }
}
