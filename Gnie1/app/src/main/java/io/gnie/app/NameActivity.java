package io.gnie.app;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ceeayaz on 6/27/18.
 */

public class NameActivity extends AppCompatActivity {

    private EditText editName;
    private Typeface headingTypeface, txtTypeface;
    private TextView headingTxt,phoneTxt;
    private String name;
    private RelativeLayout btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_fullname);
        getSupportActionBar().hide();
        initViews();
    }

    public void initViews(){


        headingTypeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        txtTypeface = Typeface.createFromAsset(getAssets(), "fonts/Hind-Medium.ttf");

        editName = (EditText) findViewById(R.id.editName);
        btn = (RelativeLayout) findViewById(R.id.btn);


        headingTxt = (TextView) findViewById(R.id.headingTxt);
        phoneTxt = (TextView) findViewById(R.id.phoneTxt);

        phoneTxt.setTypeface(txtTypeface);
        headingTxt.setTypeface(headingTypeface);
        editName.setTypeface(txtTypeface);



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(NameActivity.this,AgeActivity.class);
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();
            }
        });

        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                name = editable.toString().trim();

            }
        });

    }
}