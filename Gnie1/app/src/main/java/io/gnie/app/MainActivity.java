package io.gnie.app;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView loginBtn, fbLoginBtn, alreadyAccountTxt, aggrementTxt;
    private Typeface headingTypeface, txtTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        initViews();
    }

    public void initViews() {


        headingTypeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        txtTypeface = Typeface.createFromAsset(getAssets(), "fonts/Hind-Medium.ttf");

        loginBtn = (TextView) findViewById(R.id.loginBtn);
        fbLoginBtn = (TextView) findViewById(R.id.fbLoginBtn);
        alreadyAccountTxt = (TextView) findViewById(R.id.alreadyAccTxt);
        aggrementTxt = (TextView) findViewById(R.id.agreementTxt);

        loginBtn.setTypeface(headingTypeface);
        fbLoginBtn.setTypeface(headingTypeface);
        aggrementTxt.setTypeface(txtTypeface);
        alreadyAccountTxt.setTypeface(txtTypeface);

        String txt = "<font COLOR=\'##707070\'>" + "By tapping Log in, you agree to the " + "<br>" + "</font>"
                + "<font COLOR=\'##707070\'><b> Terms of Service " + "</b></font>" + "<font COLOR=\'##707070\'>" + "and " + "</font>" + "<font COLOR=\'##707070\'><b> Privacy Policy " + "</font>";
        aggrementTxt.setText(Html.fromHtml(txt));


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,SignupActivity.class);
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();
            }
        });


        fbLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        alreadyAccountTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();

            }
        });
    }
}
