package io.gnie.app;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ceeayaz on 6/27/18.
 */

public class TakePictureActivity extends AppCompatActivity {


    private Typeface headingTypeface, txtTypeface;
    private RelativeLayout btn,takePic;
    private TextView headingTxt, phoneTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.take_picture);
        getSupportActionBar().hide();
        initViews();
    }

    public void initViews() {


        headingTypeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        txtTypeface = Typeface.createFromAsset(getAssets(), "fonts/Hind-Medium.ttf");


        btn = (RelativeLayout) findViewById(R.id.btn);
        takePic = (RelativeLayout) findViewById(R.id.takePic);

        headingTxt = (TextView) findViewById(R.id.headingTxt);
        phoneTxt = (TextView) findViewById(R.id.phoneTxt);


        phoneTxt.setTypeface(txtTypeface);
        headingTxt.setTypeface(headingTypeface);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        takePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}