package io.gnie.app;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ceeayaz on 6/27/18.
 */

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText editNumber;
    private Typeface headingTypeface, txtTypeface;
    private RelativeLayout btn;
    private TextView headingTxt,phoneTxt;
    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        getSupportActionBar().hide();
        initViews();
    }

    public void initViews(){


        headingTypeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        txtTypeface = Typeface.createFromAsset(getAssets(), "fonts/Hind-Medium.ttf");

        editNumber = (EditText) findViewById(R.id.editNumber);
        btn = (RelativeLayout) findViewById(R.id.btn);


        headingTxt = (TextView) findViewById(R.id.headingTxt);
        phoneTxt = (TextView) findViewById(R.id.phoneTxt);

        phoneTxt.setTypeface(txtTypeface);
        headingTxt.setTypeface(headingTypeface);
        editNumber.setTypeface(txtTypeface);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ForgotPasswordActivity.this,CodeActivity.class);
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();
            }
        });

        editNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                phoneNumber = editable.toString().trim();

            }
        });

    }
}
