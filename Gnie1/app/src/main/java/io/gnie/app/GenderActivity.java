package io.gnie.app;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ceeayaz on 6/27/18.
 */

public class GenderActivity  extends AppCompatActivity {

    private Typeface headingTypeface, txtTypeface;
    private RelativeLayout btn;
    private TextView headingTxt,phoneTxt,manBtn,womanBtn,otherBtn;
    private String gender;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gender_activity);
        getSupportActionBar().hide();
        initViews();
    }

    public void initViews(){


        headingTypeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        txtTypeface = Typeface.createFromAsset(getAssets(), "fonts/Hind-Medium.ttf");

        manBtn = (TextView) findViewById(R.id.manBtn);
        womanBtn = (TextView) findViewById(R.id.womanBtn);
        otherBtn = (TextView) findViewById(R.id.otherBtn);
        btn = (RelativeLayout) findViewById(R.id.btn);


        headingTxt = (TextView) findViewById(R.id.headingTxt);
        phoneTxt = (TextView) findViewById(R.id.phoneTxt);

        phoneTxt.setTypeface(txtTypeface);
        headingTxt.setTypeface(headingTypeface);
        otherBtn.setTypeface(txtTypeface);
        womanBtn.setTypeface(txtTypeface);
        manBtn.setTypeface(txtTypeface);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(GenderActivity.this,TakePictureActivity.class);
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();
            }
        });

       manBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               womanBtn.setBackgroundResource(R.drawable.otherbtn);
               otherBtn.setBackgroundResource(R.drawable.otherbtn);
               manBtn.setBackgroundResource(R.drawable.loginbtn);
               gender = "man";

           }
       });

       womanBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               womanBtn.setBackgroundResource(R.drawable.loginbtn);
               otherBtn.setBackgroundResource(R.drawable.otherbtn);
               manBtn.setBackgroundResource(R.drawable.otherbtn);
               gender = "woman";
           }
       });

       otherBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               gender = "other";
               womanBtn.setBackgroundResource(R.drawable.otherbtn);
               otherBtn.setBackgroundResource(R.drawable.loginbtn);
               manBtn.setBackgroundResource(R.drawable.otherbtn);
           }
       });

    }
}