package io.gnie.app;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import io.gnie.app.views.CircleImageView;

/**
 * Created by ceeayaz on 6/27/18.
 */

public class SignInActivity extends AppCompatActivity {

    private Typeface headingTypeface, txtTypeface;
    private TextView loginTxt,aggreementTxt,signupTxt;
    private CircleImageView profileImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);
        getSupportActionBar().hide();
        initViews();
    }

    public void initViews(){


        headingTypeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        txtTypeface = Typeface.createFromAsset(getAssets(), "fonts/Hind-Medium.ttf");


        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        loginTxt = (TextView) findViewById(R.id.loginTxt);
        aggreementTxt = (TextView) findViewById(R.id.agreementTxt);
        signupTxt = (TextView) findViewById(R.id.signupTxt);

        loginTxt.setTypeface(txtTypeface);
        aggreementTxt.setTypeface(txtTypeface);
        signupTxt.setTypeface(txtTypeface);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignInActivity.this,EmailActivity.class);
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();
            }
        });

        signupTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignInActivity.this,MainActivity.class);
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();

            }
        });



    }
}