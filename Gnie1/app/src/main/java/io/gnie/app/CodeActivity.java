package io.gnie.app;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ceeayaz on 6/27/18.
 */

public class CodeActivity extends AppCompatActivity {

    private EditText editNumber;
    private Typeface headingTypeface, txtTypeface;
    private TextView headingTxt,phoneTxt;
    private TextView codeTxt,resendTxt;
    private String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.code_activity);
        getSupportActionBar().hide();
        initViews();
    }

    public void initViews(){


        headingTypeface = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        txtTypeface = Typeface.createFromAsset(getAssets(), "fonts/Hind-Medium.ttf");

        editNumber = (EditText) findViewById(R.id.editNumber);



        headingTxt = (TextView) findViewById(R.id.headingTxt);
        phoneTxt = (TextView) findViewById(R.id.phoneTxt);
        codeTxt = (TextView) findViewById(R.id.codeTxt);
        resendTxt = (TextView) findViewById(R.id.resendTxt);

        phoneTxt.setTypeface(txtTypeface);
        headingTxt.setTypeface(headingTypeface);
        editNumber.setTypeface(txtTypeface);
        codeTxt.setTypeface(txtTypeface);
        resendTxt.setTypeface(txtTypeface);


        resendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CodeActivity.this,EmailCodeActivity.class);
                overridePendingTransition(0,0);
                startActivity(intent);
                finish();
            }
        });

        editNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                code = editable.toString().trim();

            }
        });

    }
}