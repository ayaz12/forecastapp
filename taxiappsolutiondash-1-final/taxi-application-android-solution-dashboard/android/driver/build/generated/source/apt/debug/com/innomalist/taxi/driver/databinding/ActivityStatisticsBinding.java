package com.innomalist.taxi.driver.databinding;
import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityStatisticsBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.app_bar_layout, 4);
        sViewsWithIds.put(R.id.toolbar, 5);
        sViewsWithIds.put(R.id.tab_date, 6);
        sViewsWithIds.put(R.id.income_card, 7);
        sViewsWithIds.put(R.id.income_text, 8);
        sViewsWithIds.put(R.id.income_label, 9);
        sViewsWithIds.put(R.id.service_card, 10);
        sViewsWithIds.put(R.id.service_text, 11);
        sViewsWithIds.put(R.id.service_label, 12);
        sViewsWithIds.put(R.id.rating_card, 13);
        sViewsWithIds.put(R.id.rating_text, 14);
        sViewsWithIds.put(R.id.rating_label, 15);
        sViewsWithIds.put(R.id.chart_card, 16);
        sViewsWithIds.put(R.id.chart, 17);
        sViewsWithIds.put(R.id.payment_request_card, 18);
        sViewsWithIds.put(R.id.label_payment_request, 19);
    }
    // views
    @NonNull
    public final android.support.design.widget.AppBarLayout appBarLayout;
    @NonNull
    public final android.widget.Button buttonPaymentRequest;
    @NonNull
    public final com.db.chart.view.LineChartView chart;
    @NonNull
    public final android.support.v7.widget.CardView chartCard;
    @NonNull
    public final android.support.v7.widget.CardView incomeCard;
    @NonNull
    public final android.widget.TextView incomeLabel;
    @NonNull
    public final android.widget.TextView incomeText;
    @NonNull
    public final android.widget.TextView labelPaymentRequest;
    @NonNull
    private final android.support.constraint.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    public final android.support.v7.widget.CardView paymentRequestCard;
    @NonNull
    public final android.widget.ProgressBar progressPayment;
    @NonNull
    public final android.support.v7.widget.CardView ratingCard;
    @NonNull
    public final android.widget.TextView ratingLabel;
    @NonNull
    public final android.widget.TextView ratingText;
    @NonNull
    public final android.support.v7.widget.CardView serviceCard;
    @NonNull
    public final android.widget.TextView serviceLabel;
    @NonNull
    public final android.widget.TextView serviceText;
    @NonNull
    public final android.support.design.widget.TabLayout tabDate;
    @NonNull
    public final android.support.v7.widget.Toolbar toolbar;
    // variables
    @Nullable
    private com.innomalist.taxi.common.models.Driver mDriver;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityStatisticsBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 1);
        final Object[] bindings = mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds);
        this.appBarLayout = (android.support.design.widget.AppBarLayout) bindings[4];
        this.buttonPaymentRequest = (android.widget.Button) bindings[3];
        this.buttonPaymentRequest.setTag(null);
        this.chart = (com.db.chart.view.LineChartView) bindings[17];
        this.chartCard = (android.support.v7.widget.CardView) bindings[16];
        this.incomeCard = (android.support.v7.widget.CardView) bindings[7];
        this.incomeLabel = (android.widget.TextView) bindings[9];
        this.incomeText = (android.widget.TextView) bindings[8];
        this.labelPaymentRequest = (android.widget.TextView) bindings[19];
        this.mboundView0 = (android.support.constraint.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.paymentRequestCard = (android.support.v7.widget.CardView) bindings[18];
        this.progressPayment = (android.widget.ProgressBar) bindings[2];
        this.progressPayment.setTag(null);
        this.ratingCard = (android.support.v7.widget.CardView) bindings[13];
        this.ratingLabel = (android.widget.TextView) bindings[15];
        this.ratingText = (android.widget.TextView) bindings[14];
        this.serviceCard = (android.support.v7.widget.CardView) bindings[10];
        this.serviceLabel = (android.widget.TextView) bindings[12];
        this.serviceText = (android.widget.TextView) bindings[11];
        this.tabDate = (android.support.design.widget.TabLayout) bindings[6];
        this.toolbar = (android.support.v7.widget.Toolbar) bindings[5];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.driver == variableId) {
            setDriver((com.innomalist.taxi.common.models.Driver) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setDriver(@Nullable com.innomalist.taxi.common.models.Driver Driver) {
        updateRegistration(0, Driver);
        this.mDriver = Driver;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.driver);
        super.requestRebind();
    }
    @Nullable
    public com.innomalist.taxi.common.models.Driver getDriver() {
        return mDriver;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeDriver((com.innomalist.taxi.common.models.Driver) object, fieldId);
        }
        return false;
    }
    private boolean onChangeDriver(com.innomalist.taxi.common.models.Driver Driver, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Double driverBalance = null;
        com.innomalist.taxi.common.models.Driver driver = mDriver;
        boolean driverBalanceInt50BooleanTrueBooleanFalse = false;
        double androidDatabindingViewDataBindingSafeUnboxDriverBalance = 0.0;
        int intDriverBalance = 0;
        java.lang.String mboundView1AndroidStringPaymentBoundDriverBalanceFloat50d = null;
        boolean driverBalanceInt50 = false;

        if ((dirtyFlags & 0x3L) != 0) {



                if (driver != null) {
                    // read driver.balance
                    driverBalance = driver.getBalance();
                }


                // read android.databinding.ViewDataBinding.safeUnbox(driver.balance)
                androidDatabindingViewDataBindingSafeUnboxDriverBalance = android.databinding.ViewDataBinding.safeUnbox(driverBalance);
                // read @android:string/payment_bound
                mboundView1AndroidStringPaymentBoundDriverBalanceFloat50d = mboundView1.getResources().getString(R.string.payment_bound, driverBalance, 50d);


                // read (int) android.databinding.ViewDataBinding.safeUnbox(driver.balance)
                intDriverBalance = ((int) (androidDatabindingViewDataBindingSafeUnboxDriverBalance));
                // read android.databinding.ViewDataBinding.safeUnbox(driver.balance) >= 50
                driverBalanceInt50 = (androidDatabindingViewDataBindingSafeUnboxDriverBalance) >= (50);
            if((dirtyFlags & 0x3L) != 0) {
                if(driverBalanceInt50) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read android.databinding.ViewDataBinding.safeUnbox(driver.balance) >= 50 ? true : false
                driverBalanceInt50BooleanTrueBooleanFalse = ((driverBalanceInt50) ? (true) : (false));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.buttonPaymentRequest.setEnabled(driverBalanceInt50BooleanTrueBooleanFalse);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, mboundView1AndroidStringPaymentBoundDriverBalanceFloat50d);
            this.progressPayment.setProgress(intDriverBalance);
        }
        if ((dirtyFlags & 0x2L) != 0) {
            // api target 1

            this.progressPayment.setMax(50);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ActivityStatisticsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityStatisticsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ActivityStatisticsBinding>inflate(inflater, com.innomalist.taxi.driver.R.layout.activity_statistics, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ActivityStatisticsBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityStatisticsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.innomalist.taxi.driver.R.layout.activity_statistics, null, false), bindingComponent);
    }
    @NonNull
    public static ActivityStatisticsBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityStatisticsBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/activity_statistics_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ActivityStatisticsBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): driver
        flag 1 (0x2L): null
        flag 2 (0x3L): android.databinding.ViewDataBinding.safeUnbox(driver.balance) >= 50 ? true : false
        flag 3 (0x4L): android.databinding.ViewDataBinding.safeUnbox(driver.balance) >= 50 ? true : false
    flag mapping end*/
    //end
}